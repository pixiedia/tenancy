<?php

declare(strict_types=1);

namespace Common\Swagger\Provider;

use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\ServiceProvider;

/**
 * Class SwaggerModuleProvider
 */
class SwaggerModuleProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        // Force HTTPS scheme for asset URLs in local environment
        if ($this->app->environment('local')) {
            $url->forceScheme('https');
        }
    }
}
