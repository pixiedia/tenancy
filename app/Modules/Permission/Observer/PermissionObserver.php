<?php

declare(strict_types=1);

namespace Modules\Permission\Observer;

use Common\PubSub\Facade\PubSubService;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Modules\Permission\Enum\PubTopics;
use Modules\Permission\Model\Permission;
use Ramsey\Uuid\Uuid;

/**
 * Class PermissionObserver
 */
class PermissionObserver
{
    /**
     * Handle the Permission "creating" event.
     *
     * @param  Permission  $permission  The newly creating permission.
     */
    public function creating(Permission $permission): void
    {
        // Log the creation of the permission
        Log::info("Permission creating: {$permission->getId()}");

        // Generate a UUID for the permission
        $uuid = Uuid::uuid4()->toString();

        // Set the generated UUID for the permission
        $permission->setUuid($uuid);
    }

    /**
     * Handle the Permission "creating" event.
     *
     * @param  Permission  $permission  The newly creating permission.
     */
    public function created(Permission $permission): void
    {
        // Log the creation of the permission
        Log::info("Permission created: {$permission->getId()}");

        // Dispatch the created event
        Event::dispatch(PubTopics::CREATED, $permission->toDataArray());

        // Pubsub the created event
        PubSubService::publish(PubTopics::CREATED, $permission->toDataArray());
    }

    /**
     * Handle the Permission "updated" event.
     *
     * @param  Permission  $permission  The updated permission.
     */
    public function updated(Permission $permission): void
    {
        // Log the update of the permission
        Log::info("Permission updated: {$permission->getId()}");

        // Set the updated at timestamp
        $permission->setUpdatedAt(now());

        // Dispatch the updated event
        Event::dispatch(PubTopics::UPDATED, $permission->toDataArray());

        // Pubsub the updated event
        PubSubService::publish(PubTopics::UPDATED, $permission->toDataArray());
    }

    /**
     * Handle the Permission "deleted" event.
     *
     * @param  Permission  $permission  The deleted permission.
     */
    public function deleted(Permission $permission): void
    {
        // Log the deletion of the permission
        Log::info("Permission deleted: {$permission->getId()}");

        // Set the deleted at timestamp
        $permission->setDeletedAt(now());

        // Dispatch the deleted event
        Event::dispatch(PubTopics::SOFT_DELETED, $permission->toDataArray());

        // Pubsub the deleted event
        PubSubService::publish(PubTopics::SOFT_DELETED, $permission->toDataArray());
    }

    /**
     * Handle the Permission "forceDeleted" event.
     *
     * @param  Permission  $permission  The force deleted permission.
     */
    public function forceDeleted(Permission $permission): void
    {
        // Log the force deletion of the permission
        Log::info("Permission force deleted: {$permission->getId()}");

        // Set the deleted at timestamp
        $permission->setDeletedAt(now());

        // Dispatch the force deleted event
        Event::dispatch(PubTopics::DELETED, $permission->toDataArray());

        // Pubsub the deleted event
        PubSubService::publish(PubTopics::DELETED, $permission->toDataArray());
    }

    /**
     * Handle the Permission "restored" event.
     *
     * @param  Permission  $permission  The restored permission.
     */
    public function restored(Permission $permission): void
    {
        // Log the restoration of the permission
        Log::info("Permission restored: {$permission->getId()}");

        // Reset the deleted at timestamp
        $permission->setDeletedAt(null);

        // Dispatch the restored event
        Event::dispatch(PubTopics::RECOVERED, $permission->toDataArray());

        // Pubsub the restored event
        PubSubService::publish(PubTopics::RECOVERED, $permission->toDataArray());
    }
}
