<?php

declare(strict_types=1);

namespace Modules\Customer\Interface;

use App\Abstractions\Interface\AbstractServiceInterface;

/**
 * Interface CustomerServiceInterface
 */
interface CustomerServiceInterface extends AbstractServiceInterface
{
}
