<?php

declare(strict_types=1);

namespace Common\Http\Service;

use App\Util\PHPUtil;
use Common\Http\Enum\HttpMethod;
use Common\Http\Interface\HttpServiceInterface;
use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

/**
 * Class HttpService
 *
 * A service class for interacting with HTTP in Magento.
 */
class HttpService implements HttpServiceInterface
{
    /**
     * @var bool Indicates whether rate limiting is enabled.
     */
    protected $rateLimitEnabled = false;

    /**
     * @var array Stores rate limit headers.
     */
    protected $rateLimitHeaders = [];

    /**
     * @var array Rate limits for each HTTP method.
     */
    protected $rateLimits = [
        HttpMethod::GET => ['average' => 10, 'burst' => 20],
        HttpMethod::POST => ['average' => 5, 'burst' => 10],
        HttpMethod::PUT => ['average' => 5, 'burst' => 10],
        HttpMethod::DELETE => ['average' => 5, 'burst' => 10],
    ];

    /**
     * @var array Custom rate limits for specific endpoints.
     */
    protected $customRateLimits = [];

    /**
     * @var array Timestamps of the last request for each HTTP method.
     */
    protected $lastRequestTimes = [
        HttpMethod::GET => null,
        HttpMethod::POST => null,
        HttpMethod::PUT => null,
        HttpMethod::DELETE => null,
    ];

    /**
     * @var array Token buckets for rate limiting.
     */
    protected $tokenBuckets = [
        HttpMethod::GET => 0,
        HttpMethod::POST => 0,
        HttpMethod::PUT => 0,
        HttpMethod::DELETE => 0,
    ];

    /**
     * Perform a GET request.
     *
     * {@inheritDoc}
     */
    public function get($url, $headers = [], $options = [])
    {
        $this->handleRateLimit(HttpMethod::GET, $url);

        return $this->makeRequest($url, HttpMethod::GET, [], $headers, $options);
    }

    /**
     * Perform a POST request.
     *
     * {@inheritDoc}
     */
    public function post($url, $data = [], $headers = [], $options = [])
    {
        $this->handleRateLimit(HttpMethod::POST, $url);

        return $this->makeRequest($url, HttpMethod::POST, $data, $headers, $options);
    }

    /**
     * Perform a PUT request.
     *
     * {@inheritDoc}
     */
    public function put($url, $data = [], $headers = [], $options = [])
    {
        $this->handleRateLimit(HttpMethod::PUT, $url);

        return $this->makeRequest($url, HttpMethod::PUT, $data, $headers, $options);
    }

    /**
     * Perform a DELETE request.
     *
     * {@inheritDoc}
     */
    public function delete($url, $headers = [], $options = [])
    {
        $this->handleRateLimit(HttpMethod::DELETE, $url);

        return $this->makeRequest($url, HttpMethod::DELETE, [], $headers, $options);
    }

    /**
     * Set a custom rate limit for a specific endpoint and HTTP method.
     *
     * @param  string  $url  The URL of the endpoint.
     * @param  string  $method  The HTTP method.
     * @param  array  $rateLimit  The custom rate limit.
     */
    public function setCustomRateLimit($url, $method, $rateLimit)
    {
        $this->customRateLimits[$url][$method] = $rateLimit;
    }

    /**
     * Enable rate limiting for the HTTP service.
     */
    public function enableRateLimit()
    {
        $this->rateLimitEnabled = true;
    }

    /**
     * Disable rate limiting for the HTTP service.
     */
    public function disableRateLimit()
    {
        $this->rateLimitEnabled = false;
    }

    /**
     * Get the value of the X-Rate-Limit header.
     *
     * @return mixed|null The value of the X-Rate-Limit header, or null if not present.
     */
    public function getXRateLimit()
    {
        return $this->rateLimitHeaders['X-Rate-Limit'] ?? null;
    }

    /**
     * Get the value of the X-Retry-After header.
     *
     * @return mixed|null The value of the X-Retry-After header, or null if not present.
     */
    public function getXRetryAfter()
    {
        return $this->rateLimitHeaders['X-Retry-After'] ?? null;
    }

    /**
     * Get the value of the X-Reset header.
     *
     * @return mixed|null The value of the X-Reset header, or null if not present.
     */
    public function getXReset()
    {
        return $this->rateLimitHeaders['X-Reset'] ?? null;
    }

    /**
     * Make an HTTP request.
     *
     * @param  string  $url  The URL to send the request to.
     * @param  string  $method  The HTTP method (GET, POST, PUT, DELETE, etc.).
     * @param  array  $data  Optional. Data to include in the request.
     * @param  array  $headers  Optional. Additional headers for the request.
     * @param  array  $options  Optional. Additional options for the request.
     * @return mixed The response data.
     *
     * @throws Exception If the HTTP request fails.
     */
    private function makeRequest($url, $method, $data = [], $headers = [], $options = [])
    {
        try {
            // Log request information
            Log::info(
                "Making HTTP {$method} request to {$url} with payload data: " .
                    PHPUtil::jsonEncode($data)
            );

            // Perform the HTTP request
            $request = Http::withOptions($options ?: [])->withHeaders($headers ?: []);
            $response = $request->{$method}($url, $data);

            if (! $response->successful()) {
                throw new Exception("HTTP {$method} request failed");
            }

            // Capture and handle rate limit headers
            $this->captureRateLimitHeaders($response);

            return $response->json();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * Handle rate limiting for the given HTTP method and URL.
     *
     * @param  string  $method  The HTTP method.
     * @param  string  $url  The URL of the endpoint.
     *
     * @throws Exception If the rate limit is exceeded.
     */
    private function handleRateLimit($method, $url)
    {
        // NOTE: Check if rate limiting is enabled
        if ($this->isRateLimitEnabled()) {
            // NOTE: Get the effective rate limit for the given method and URL
            $rateLimit = $this->getEffectiveRateLimit($method, $url);

            // NOTE: Check if the token bucket has enough tokens
            if ($this->tokenBuckets[$method] < 1) {
                throw new Exception("Rate limit exceeded for {$method} to {$url}.");
            }

            // NOTE: Consume a token from the token bucket
            $this->tokenBuckets[$method]--;

            // NOTE: Update the last request time for the method
            $this->lastRequestTimes[$method] = microtime(true);
        }
    }

    /**
     * Get the effective rate limit for the given HTTP method and URL.
     *
     * @param  string  $method  The HTTP method.
     * @param  string  $url  The URL of the endpoint.
     * @return string[] The effective rate limit.
     */
    private function getEffectiveRateLimit($method, $url)
    {
        // NOTE: Check if there is a custom rate limit for the specific endpoint
        if (isset($this->customRateLimits[$url][$method])) {
            return $this->customRateLimits[$url][$method];
        }

        // NOTE: Use the default rate limit for the HTTP method
        return $this->rateLimits[$method];
    }

    /**
     * Check if rate limiting is enabled.
     *
     * @return bool
     */
    private function isRateLimitEnabled()
    {
        return $this->rateLimitEnabled;
    }

    /**
     * Capture and handle rate limit headers from the response.
     *
     * @param  array  $response  The response from the HTTP request.
     */
    private function captureRateLimitHeaders($response)
    {
        $rateLimitHeaders = [
            'X-Rate-Limit',
            'X-Retry-After',
            'X-Reset',
        ];

        foreach ($rateLimitHeaders as $header) {
            if (isset($response['headers'][$header])) {
                $this->rateLimitHeaders[$header] = $response['headers'][$header];
                $this->handleRateLimitHeader($header, $response['headers'][$header]);
            }
        }
    }

    /**
     * Handle a specific rate limit header.
     *
     * @param  string  $header  The name of the rate limit header.
     * @param  mixed  $value  The value of the rate limit header.
     */
    private function handleRateLimitHeader($header, $value)
    {
        // NOTE: TODO: ADD LOGIC HERE, AND ALSO FAILURE REQUEST NEED TO BE HANDLED
        switch ($header) {
            case 'X-Rate-Limit':
                // NOTE: Handle X-Rate-Limit header
                break;
            case 'X-Retry-After':
                // NOTE: Handle X-Retry-After header
                break;
            case 'X-Reset':
                // NOTE: Handle X-Reset header
                break;
                // NOTE: Add more cases for additional rate limit headers if needed
        }
    }
}
