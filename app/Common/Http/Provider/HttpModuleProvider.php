<?php

declare(strict_types=1);

namespace Common\Http\Provider;

use App\Abstractions\AbstractModuleProvider;
use Common\Http\Facade\HttpService as HttpServiceFacade;
use Common\Http\Service\HttpService;

/**
 * Class HttpModuleProvider
 */
class HttpModuleProvider extends AbstractModuleProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function registerBindings()
    {
        // Bind HttpServiceFacade to HttpService
        app()->bind(HttpServiceFacade::HTTP_SERVICE, fn ($app) => new HttpService());
    }
}
