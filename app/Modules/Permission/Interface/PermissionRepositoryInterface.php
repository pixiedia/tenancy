<?php

declare(strict_types=1);

namespace Modules\Permission\Interface;

use App\Abstractions\Interface\AbstractRepositoryInterface;

/**
 * Interface PermissionRepositoryInterface
 */
interface PermissionRepositoryInterface extends AbstractRepositoryInterface
{
}
