<?php

declare(strict_types=1);

use App\Util\PHPUtil;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\User\Interface\Data\UserInterface;
use Modules\User\Model\User;

/**
 * Class CreateUsersTableMigration
 */
return new class() extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Creating "' . UserInterface::TABLE . '" table...');

        // Create the users table
        Schema::create(UserInterface::TABLE, function (Blueprint $table) {
            // Primary key
            $table->id();

            // Universally Unique Identifier
            $table->uuid(UserInterface::UUID)->comment('Universally Unique Identifier');

            // User's name
            $table->string(UserInterface::NAME)->comment('The name of the ' . UserInterface::ENTITY_NAME);

            // Tenant status
            $table->string(UserInterface::STATUS)->comment(UserInterface::ENTITY_NAME . ' status');

            // Unique email address
            $table->string(UserInterface::EMAIL)->unique()->comment('The email address of the ' . UserInterface::ENTITY_NAME);

            // Unique phone
            $table->string(UserInterface::PHONE)->unique()->comment('The phone of the ' . UserInterface::ENTITY_NAME);

            // Timestamp for email verification
            $table->timestamp(UserInterface::EMAIL_VERIFIED_AT)->nullable()->comment('The timestamp when the email was verified');

            // User's password
            $table->string(UserInterface::PASSWORD)->comment('The password of the ' . UserInterface::ENTITY_NAME);

            // Remember token for "remember me" functionality
            $table->rememberToken();

            // Creation timestamp (nullable)
            $table->timestamp(User::CREATED_AT)->useCurrent()->comment('Creation timestamp');

            // Last update timestamp (nullable)
            $table->timestamp(User::UPDATED_AT)->nullable()->comment('Last update timestamp');

            // Deletion timestamp (nullable)
            $table->timestamp(UserInterface::DELETED_AT)->nullable()->comment('Deletion timestamp');

            // Tenant metadata (nullable JSON)
            $table->json(UserInterface::METADATA)->nullable()->comment(UserInterface::ENTITY_NAME . ' metadata');

            // Indexes
            $table->index(UserInterface::UUID);
            $table->index(UserInterface::STATUS);
            $table->index(UserInterface::DELETED_AT);
        });

        // Add a comment to the 'users' table
        Schema::table(UserInterface::TABLE, function (Blueprint $table) {
            $table->comment(UserInterface::TABLE . ' table');
        });

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ "' . UserInterface::TABLE . '" table created successfully.');
    }

    /**
     * Reverse the migrations.
     *
     * Drop the 'users' tables if they exist.
     */
    public function down(): void
    {
        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Dropping tables: ' . UserInterface::TABLE . '...');

        // Drop the 'users' table if it exists
        Schema::dropIfExists(UserInterface::TABLE);

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ Tables dropped: ' . UserInterface::TABLE . '.');
    }
};
