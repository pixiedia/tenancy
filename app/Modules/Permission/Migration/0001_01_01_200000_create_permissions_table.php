<?php

declare(strict_types=1);

use App\Util\PHPUtil;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Permission\Interface\Data\PermissionInterface;
use Modules\Permission\Model\Permission;
use Modules\User\Interface\Data\UserInterface;

/**
 * Class CreatePermissionsTableMigration
 */
return new class() extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Creating "' . PermissionInterface::TABLE . '" table...');

        // Create the permissions table
        Schema::create(PermissionInterface::TABLE, function (Blueprint $table) {
            // Primary key
            $table->id();

            // Universally Unique Identifier
            $table->uuid(PermissionInterface::UUID)->comment('Universally Unique Identifier');

            // Permission's name
            $table->string(PermissionInterface::NAME)->comment('The name of the ' . PermissionInterface::ENTITY_NAME);

            // Slug for the permission
            $table->string(PermissionInterface::SLUG)->unique()->nullable()->comment('Slug for the ' . PermissionInterface::ENTITY_NAME);

            // Tenant is active
            $table->boolean(PermissionInterface::IS_ACTIVE)->comment('Status of ' . PermissionInterface::ENTITY_NAME);

            // Tenant status
            $table->string(PermissionInterface::DESCRIPTION)->comment(PermissionInterface::ENTITY_NAME . ' description');

            // Creation timestamp (nullable)
            $table->timestamp(Permission::CREATED_AT)->useCurrent()->comment('Creation timestamp');

            // Last update timestamp (nullable)
            $table->timestamp(Permission::UPDATED_AT)->nullable()->comment('Last update timestamp');

            // Deletion timestamp (nullable)
            $table->timestamp(PermissionInterface::DELETED_AT)->nullable()->comment('Deletion timestamp');

            // Permission who last updated the tenant (nullable)
            $table->bigInteger(PermissionInterface::CREATED_BY)->unsigned()->nullable()->comment('Permission who last updated the ' . PermissionInterface::ENTITY_NAME);

            // Permission who created the tenant (nullable)
            $table->bigInteger(PermissionInterface::UPDATED_BY)->nullable()->comment('Permission who created the ' . PermissionInterface::ENTITY_NAME);

            // Permission who deleted the tenant (nullable)
            $table->bigInteger(PermissionInterface::DELETED_BY)->nullable()->comment('Permission who deleted the ' . PermissionInterface::ENTITY_NAME);

            // Tenant metadata (nullable JSON)
            $table->json(PermissionInterface::METADATA)->nullable()->comment(PermissionInterface::ENTITY_NAME . ' metadata');

            // Indexes
            $table->index(PermissionInterface::UUID);
            $table->index(PermissionInterface::IS_ACTIVE);
            $table->index(PermissionInterface::DELETED_AT);

            // Foreign keys
            $table->foreign(PermissionInterface::CREATED_BY)->references(PermissionInterface::PRIMARY_KEY)->on(UserInterface::TABLE)->onDelete('set null');
            $table->foreign(PermissionInterface::UPDATED_BY)->references(PermissionInterface::PRIMARY_KEY)->on(UserInterface::TABLE)->onDelete('set null');
            $table->foreign(PermissionInterface::DELETED_BY)->references(PermissionInterface::PRIMARY_KEY)->on(UserInterface::TABLE)->onDelete('set null');
        });

        // Add a comment to the 'permissions' table
        Schema::table(PermissionInterface::TABLE, function (Blueprint $table) {
            $table->comment(PermissionInterface::TABLE . ' table');
        });

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ "' . PermissionInterface::TABLE . '" table created successfully.');
    }

    /**
     * Reverse the migrations.
     *
     * Drop the 'permission' tables if they exist.
     */
    public function down(): void
    {
        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Dropping tables: ' . PermissionInterface::TABLE . '...');

        // Drop the 'permission' table if it exists
        Schema::dropIfExists(PermissionInterface::TABLE);

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ Tables dropped: ' . PermissionInterface::TABLE . '.');
    }
};
