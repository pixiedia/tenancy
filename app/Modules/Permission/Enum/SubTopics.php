<?php

declare(strict_types=1);

namespace Modules\Permission\Enum;

use BenSampo\Enum\Enum;

/**
 * Enumeration of (Pub/Sub) topics related to DNS subscription events.
 *
 * @method static static LIST()
 * @method static static GET()
 * @method static static CREATE()
 * @method static static UPDATE()
 * @method static static DELETE()
 * @method static static RECOVERED()
 */
final class SubTopics extends Enum
{
    /**
     * Topic for getting a list of DNSs.
     *
     * @example 'permission.list'
     */
    const LIST = 'permission.list';

    /**
     * Topic for getting information about a single DNS.
     *
     * @example 'permission.get'
     */
    const GET = 'permission.get';

    /**
     * Topic for creating a new DNS.
     *
     * @example 'permission.create'
     */
    const CREATE = 'permission.create';

    /**
     * Topic for updating an existing DNS.
     *
     * @example 'permission.update'
     */
    const UPDATE = 'permission.update';

    /**
     * Topic for deleting a DNS.
     *
     * @example 'permission.delete'
     */
    const DELETE = 'permission.delete';

    /**
     * Topic for recovery events.
     *
     * @example 'permission.recovered'
     */
    const RECOVERED = 'permission.recover';
}
