<?php

declare(strict_types=1);

namespace Common\PubSub\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * Facade for the PubSubService class.
 *
 * This facade provides a static interface for accessing the PubSubService functionality.
 */
class PubSubService extends Facade
{
    /**
     * The key used in the service container binding.
     */
    const PUB_SUB_SERVICE = 'pubSubService';

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return self::PUB_SUB_SERVICE;
    }
}
