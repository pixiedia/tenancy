@php
use Illuminate\Support\Str;
use Common\Config\Service\ConfigService;
@endphp

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>{{config('l5-swagger.documentations.'.$documentation.'.api.title')}}</title>
    <!-- Include Swagger UI CSS -->
    <link rel="stylesheet" type="text/css" href="{{ l5_swagger_asset($documentation, 'swagger-ui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/swagger/swagger.min.css') }}">

    <!-- Favicon for Swagger UI -->
    <link rel="icon" type="image/png" href="{{ url('images/swagger/favicon-32x32.png')}}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ url('images/swagger/favicon-16x16.png')}}" sizes="16x16" />
    <style>
        /* Custom styles for the Swagger UI */
        html {
            box-sizing: border-box;
            overflow: -moz-scrollbars-vertical;
            overflow-y: scroll;
        }

        *,
        *:before,
        *:after {
            box-sizing: inherit;
        }

        body {
            margin: 0;
            background: #fafafa;
        }
    </style>
</head>

<body>
    <!-- Container for Swagger UI -->
    <div id="swagger-ui"></div>

    <!-- Include Swagger UI JS Bundle -->
    <script src="{{ l5_swagger_asset($documentation, 'swagger-ui-bundle.js') }}"></script>
    <!-- Include Swagger UI Standalone Preset JS -->
    <script src="{{ l5_swagger_asset($documentation, 'swagger-ui-standalone-preset.js') }}"></script>
    <script>
        /**
         * Load Swagger UI when the window is fully loaded.
         */
        window.onload = function() {
            // Initialize Swagger UI
            // Initialize Swagger UI
            const ui = SwaggerUIBundle({
               // Target DOM element to render Swagger UI
                dom_id: '#swagger-ui',
                // URL of the Swagger JSON or YAML file
                url: "{!! $urlToDocs !!}",
                // Specify how operations should be sorted in the UI
                operationsSorter: {!! isset($operationsSorter) ? '"'.$operationsSorter.'"' : 'null' !!},
                // URL of the Swagger UI configuration file
                configUrl: {!! isset($configUrl) ? '"'.$configUrl.'"' : 'null' !!},
                // URL of the Swagger UI validator
                validatorUrl: {!! isset($validatorUrl) ? '"'.$validatorUrl.'"' : 'null' !!},
                // URL for OAuth2 redirect
                oauth2RedirectUrl: "{{ route('l5-swagger.'.$documentation.'.oauth2_callback', [], $useAbsolutePath) }}",
                // Intercept requests and add CSRF token
                requestInterceptor: function(request) {
                    request.headers['X-CSRF-TOKEN'] = '{{ csrf_token() }}';
                    return request;
                },
                // Presets and plugins for Swagger UI
                presets: [
                    SwaggerUIBundle.presets.apis,
                    SwaggerUIStandalonePreset
                ],
                plugins: [
                    SwaggerUIBundle.plugins.DownloadUrl
                ],
                // Layout and display settings for Swagger UI
                layout: "StandaloneLayout",
                docExpansion: "{!! config('l5-swagger.defaults.ui.display.doc_expansion', 'none') !!}",
                deepLinking: true,
                tryItOutEnabled: {!! config('l5-swagger.defaults.ui.display.tryItOutEnabled') ? 'true' : 'false' !!},
                filter: {!! config('l5-swagger.defaults.ui.display.filter') ? 'true' : 'false' !!},
                persistAuthorization: "{!! config('l5-swagger.defaults.ui.authorization.persist_authorization') ? 'true' : 'false' !!}"
            });


            // Expose the UI variable globally for debugging
            window.ui = ui;

            // Initialize OAuth2 if configured
            @if(in_array('oauth2', array_column(config('l5-swagger.defaults.securityDefinitions.securitySchemes'), 'type')))
            ui.initOAuth({
                usePkceWithAuthorizationCodeGrant: "{!! (bool)config('l5-swagger.defaults.ui.authorization.oauth2.use_pkce_with_authorization_code_grant') !!}"
            });
            @endif
        }
    </script>
    <script>
        console.clear();

        console.log(
            "%c 🚀 <?php echo Str::headline(ConfigService::getString("APP_NAME")); ?>",
            "font-size: 36px; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; font-weight: bold; text-align: center; display: block; margin: auto; color: #3498db;"
        );

        console.info(
            "%c <?php echo ConfigService::getString("SWAGGER_DESCRIPTION"); ?>",
            "font-size: 12px; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; font-weight: bold; margin-top: 10px; color: #2ecc71;"
        );

        console.info(
            "%c System Info: ℹ️",
            "font-size: 18px; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; font-weight: bold; color: #3498db; margin-top: 10px;"
        );
        console.table({
            "App Name": "<?php echo Str::headline(ConfigService::getString("APP_NAME")); ?>",
            "Environment": "<?php echo ConfigService::getEnv(); ?>",
            <?php if (ConfigService::getString("Host")): ?>
            "Host": "<?php echo ConfigService::getString("Host"); ?>",
            <?php endif; ?>
            <?php if (ConfigService::getString("PORT")): ?>
            "Port": "<?php echo ConfigService::getString("PORT"); ?>",
            <?php endif; ?>
            "Development Server URL": "<?php echo ConfigService::getString("APP_URL"); ?>",
            "Production Server URL": "<?php echo ConfigService::getString("PROD_SERVER_URL"); ?>",
            "Swagger Schema URL": "<?php echo ConfigService::getString("APP_URL"). ConfigService::getString("APP_URL") . "/schema"; ?>",
            "API Version": "<?php echo ConfigService::getString("SWAGGER_API_VERSION"); ?>",
            "Contact Name": "<?php echo ConfigService::getString("SWAGGER_CONTACT_NAME"); ?>",
            "Contact Email": "<?php echo ConfigService::getString("SWAGGER_CONTACT_EMAIL"); ?>",
        });


        console.info(
            "%c Developer Info: 👨🏻‍💻",
            "font-size: 18px; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; font-weight: bold; color: #e67e22; margin-top: 10px;"
        );

        console.table({
            "Developer Name": "<?php echo ConfigService::getString("AUTHOR"); ?>",
            "Email": "<?php echo ConfigService::getString("AUTHOR_EMAIL"); ?>",
            "GitHub": "<?php echo ConfigService::getString("AUTHOR_GITHUB"); ?>",
        });
    </script>
</body>

</html>