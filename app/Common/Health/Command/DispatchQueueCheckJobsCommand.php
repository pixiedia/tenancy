<?php

declare(strict_types=1);

namespace Common\Health\Command;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Spatie\Health\Commands\DispatchQueueCheckJobsCommand as SpatieDispatchQueueCheckJobsCommand;

/**
 * Class Kernel
 */
class DispatchQueueCheckJobsCommand extends ConsoleKernel
{
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(SpatieDispatchQueueCheckJobsCommand::class)->everyMinute();
    }
}
