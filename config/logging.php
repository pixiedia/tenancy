<?php

declare(strict_types=1);

use Common\Config\Service\ConfigService;
use Illuminate\Support\Str;
use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;
use Monolog\Processor\PsrLogMessageProcessor;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that is utilized to write
    | messages to your logs. The value provided here should match one of
    | the channels present in the list of "channels" configured below.
    |
    */

    'default' => ConfigService::getString('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Deprecations Log Channel
    |--------------------------------------------------------------------------
    |
    | This option controls the log channel that should be used to log warnings
    | regarding deprecated PHP and library features. This allows you to get
    | your application ready for upcoming major versions of dependencies.
    |
    */

    'deprecations' => [
        'channel' => ConfigService::getString('LOG_DEPRECATIONS_CHANNEL', 'null'),
        'trace' => ConfigService::getBoolean('LOG_DEPRECATIONS_TRACE', false),
    ],

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Laravel
    | utilizes the Monolog PHP logging library, which includes a variety
    | of powerful log handlers and formatters that you're free to use.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog", "custom", "stack"
    |
    */

    'channels' => [

        'stack' => [
            'driver' => 'stack',
            'channels' => ConfigService::getArray('LOG_STACK', ['single']),
            'ignore_exceptions' => false,
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/' . ConfigService::getString('LOG_FILE', 'laravel') . '.log'),
            'level' => ConfigService::getString('LOG_LEVEL', 'debug'),
            'replace_placeholders' => true,
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/' . ConfigService::getString('LOG_FILE', 'laravel') . '.log'),
            'level' => ConfigService::getString('LOG_LEVEL', 'debug'),
            'days' => ConfigService::getNumber('LOG_DAILY_DAYS', 14),
            'replace_placeholders' => true,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => ConfigService::getString('LOG_SLACK_WEBHOOK_URL'),
            'username' => Str::headline(ConfigService::getString('LOG_SLACK_USERNAME' . ' Log')),
            'emoji' => ConfigService::getString('LOG_SLACK_EMOJI', ':boom:'),
            'level' => ConfigService::getString('LOG_LEVEL', 'critical'),
            'replace_placeholders' => true,
        ],

        'papertrail' => [
            'driver' => 'monolog',
            'level' => ConfigService::getString('LOG_LEVEL', 'debug'),
            'handler' => ConfigService::getString('LOG_PAPERTRAIL_HANDLER', SyslogUdpHandler::class),
            'handler_with' => [
                'host' => ConfigService::getString('PAPERTRAIL_URL'),
                'port' => ConfigService::getString('PAPERTRAIL_PORT'),
                'connectionString' => 'tls://' . ConfigService::getString('PAPERTRAIL_URL') . ':' . ConfigService::getString('PAPERTRAIL_PORT'),
            ],
            'processors' => [PsrLogMessageProcessor::class],
        ],

        'stderr' => [
            'driver' => 'monolog',
            'level' => ConfigService::getString('LOG_LEVEL', 'debug'),
            'handler' => StreamHandler::class,
            'formatter' => ConfigService::getString('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
            'processors' => [PsrLogMessageProcessor::class],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => ConfigService::getString('LOG_LEVEL', 'debug'),
            'facility' => ConfigService::getNumber('LOG_SYSLOG_FACILITY', LOG_USER),
            'replace_placeholders' => true,
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => ConfigService::getString('LOG_LEVEL', 'debug'),
            'replace_placeholders' => true,
        ],

        'null' => [
            'driver' => 'monolog',
            'handler' => NullHandler::class,
        ],

        'emergency' => [
            'path' => storage_path('logs/' . ConfigService::getString('LOG_FILE', 'laravel') . '.log'),
        ],

    ],
];
