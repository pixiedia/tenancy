<?php

declare(strict_types=1);

return [
    App\Providers\AppServiceProvider::class,
    App\Providers\CommonModuleProvider::class,
    App\Providers\HorizonServiceProvider::class,
    App\Providers\TelescopeServiceProvider::class,
    Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class,
    Common\Http\Provider\HttpModuleProvider::class,
    Common\PubSub\Provider\PubSubModuleProvider::class,
    Common\Request\Provider\RequestModuleProvider::class,
    Common\Swagger\Provider\SwaggerModuleProvider::class,
    Common\Response\Provider\ResponseModuleProvider::class,
    Modules\User\Provider\UserModuleProvider::class,
    Modules\Role\Provider\RoleModuleProvider::class,
    Modules\Permission\Provider\PermissionModuleProvider::class,
    Modules\Tenant\Provider\TenantModuleProvider::class,
    Modules\Customer\Provider\CustomerModuleProvider::class,
    Modules\Team\Provider\TeamModuleProvider::class,
    OwenIt\Auditing\AuditingServiceProvider::class,
    Common\Health\Provider\HealthModuleProvider::class,
];
