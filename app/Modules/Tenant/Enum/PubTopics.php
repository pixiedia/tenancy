<?php

declare(strict_types=1);

namespace Modules\Tenant\Enum;

use BenSampo\Enum\Enum;

/**
 * Enumeration of (Pub/Sub) topics related to publish events.
 *
 * @method static static CREATED()
 * @method static static UPDATED()
 * @method static static DELETED()
 * @method static static SOFT_DELETED()
 * @method static static RECOVERED()
 */
final class PubTopics extends Enum
{
    /**
     * Topic for creation events.
     *
     * @example 'tenant.created'
     */
    const CREATED = 'tenant.created';

    /**
     * Topic for update events.
     *
     * @example 'tenant.updated'
     */
    const UPDATED = 'tenant.updated';

    /**
     * Topic for deletion events.
     *
     * @example 'tenant.deleted'
     */
    const DELETED = 'tenant.deleted';

    /**
     * Topic for soft deletion events.
     *
     * @example 'soft.tenant.deleted'
     */
    const SOFT_DELETED = 'tenant.soft.deleted';

    /**
     * Topic for recovery events.
     *
     * @example 'tenant.recovered'
     */
    const RECOVERED = 'tenant.recovered';
}
