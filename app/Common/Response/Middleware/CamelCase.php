<?php

declare(strict_types=1);

namespace Common\Response\Middleware;

use App\Util\KeyCaseConverter;
use App\Util\PHPUtil;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class CamelCase
 */
class CamelCase
{
    /**
     * Handle an incoming response.
     *
     * @param  Request  $request  The incoming request
     * @param  Closure  $next  The next middleware in the pipeline
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Call the next middleware in the pipeline and get the response
        $response = $next($request);

        // Check if the response is a JsonResponse
        if ($response instanceof JsonResponse) {
            // Convert the response data keys to camelCase
            $convertedData = resolve(KeyCaseConverter::class)->convert(
                KeyCaseConverter::CASE_CAMEL,
                PHPUtil::jsonDecode($response->content(), true)
            );

            // Set the converted data as the new response data
            $response->setData($convertedData);
        }

        // Return the modified response
        return $response;
    }
}
