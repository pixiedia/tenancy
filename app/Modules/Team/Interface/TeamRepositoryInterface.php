<?php

declare(strict_types=1);

namespace Modules\Team\Interface;

use App\Abstractions\Interface\AbstractRepositoryInterface;

/**
 * Interface TeamRepositoryInterface
 */
interface TeamRepositoryInterface extends AbstractRepositoryInterface
{
}
