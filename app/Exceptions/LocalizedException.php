<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use Util\Phraser\Phrase;
use Util\Phraser\Renderer\Placeholder;

/**
 * Localized exception
 */
class LocalizedException extends Exception
{
    /**
     * @var Phrase
     */
    protected $phrase;

    /**
     * @var string
     */
    protected $logMessage;

    /**
     * @param  int  $code
     */
    public function __construct(Phrase $phrase, ?Exception $cause = null, $code = 0)
    {
        $this->phrase = $phrase;

        parent::__construct($phrase->render(), (int) $code, $cause);
    }

    /**
     * Get the un-processed message, without the parameters filled in
     *
     * @return string
     */
    public function getRawMessage()
    {
        return $this->phrase->getText();
    }

    /**
     * Get parameters, corresponding to placeholders in raw exception message
     *
     * @return array
     */
    public function getParameters()
    {
        return $this->phrase->getArguments();
    }

    /**
     * Get the un-localized message, but with the parameters filled in
     *
     * @return string
     */
    public function getLogMessage()
    {
        if ($this->logMessage === null) {
            $renderer = new Placeholder();
            $this->logMessage = $renderer->render([$this->getRawMessage()], $this->getParameters());
        }

        return $this->logMessage;
    }
}
