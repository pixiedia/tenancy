<?php

declare(strict_types=1);

namespace Modules\Tenant\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode;
use Modules\Tenant\Exception\TenancyNotInitializedException;
use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CheckTenantForMaintenanceMode extends CheckForMaintenanceMode
{
    /**
     * The key used to retrieve the maintenance mode status from the tenant.
     */
    private const MAINTENANCE_MODE_KEY = 'maintenance_mode';

    /**
     * The HTTP status code for service unavailable responses.
     */
    private const STATUS_SERVICE_UNAVAILABLE = 503;

    /**
     * The message for service unavailable responses.
     */
    private const MESSAGE_SERVICE_UNAVAILABLE = 'Service Unavailable';

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @return mixed
     *
     * @throws TenancyNotInitializedException
     * @throws HttpException
     */
    public function handle($request, Closure $next)
    {
        // Check if tenancy is initialized
        if (! tenant()) {
            throw new TenancyNotInitializedException();
        }

        // Check if maintenance mode is enabled for the tenant
        if (tenant(self::MAINTENANCE_MODE_KEY)) {
            $data = tenant(self::MAINTENANCE_MODE_KEY);

            // Allow requests from specified IP addresses
            if (isset($data['allowed']) && IpUtils::checkIp($request->ip(), (array) $data['allowed'])) {
                return $next($request);
            }

            // Allow requests that are in the except array
            if ($this->inExceptArray($request)) {
                return $next($request);
            }

            // Throw a 503 HTTP exception for other requests
            throw new HttpException(
                self::STATUS_SERVICE_UNAVAILABLE,
                self::MESSAGE_SERVICE_UNAVAILABLE,
                null,
                isset($data['retry']) ? ['Retry-After' => $data['retry']] : []
            );
        }

        return $next($request);
    }
}
