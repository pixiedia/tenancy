<?php

declare(strict_types=1);

namespace Common\PubSub\Service;

use App\Util\PHPUtil;
use Common\PubSub\Interface\PubSubServiceInterface;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

/**
 * Class PubSubService
 *
 * A service class for interacting with PubSub in WordPress.
 */
class PubSubService implements PubSubServiceInterface
{
    /**
     * Publishes a message to a topic in PubSub with additional metadata.
     *
     * @param  string  $topic  The topic to publish to.
     * @param  mixed  $payload  The payload to publish.
     *
     * @throws Exception
     */
    public function publish($topic, $payload)
    {
        try {
            // Get the current timestamp.
            $timestamp = now()->timestamp;

            // Generate a unique message ID using UuidService.
            $messageId = Uuid::uuid4()->toString();

            // Get store name.
            $storeName = Config::get('app.name');

            // Create metadata for the message.
            $messageMetadata = [
                'X-Event-Name' => $topic,
                'X-Event-Token' => $messageId,
                'X-Timestamp' => $timestamp,
                'Content-Type' => 'application/json',
                'X-Event-Source' => Str::headline($storeName),
            ];

            // Combine metadata with the payload and encode the message.
            $messagePayload = PHPUtil::jsonEncode(
                array_merge($messageMetadata, ['payload' => $payload])
            );

            // Publish the message to the specified topic.
            Redis::publish($topic, $messagePayload);
        } catch (Exception $e) {
            // Log and re-throw any errors that occur during publishing.
            Log::error(
                '❌ Error in PubSubService::publish method: ' . $e->getMessage()
            );
            throw $e;
        }
    }

    /**
     * Subscribe to a PubSub topic and execute a callback when a message is received.
     *
     * @param  string  $topic  The topic to subscribe to.
     * @param  callable  $callback  The callback function to execute when a message is received.
     *
     * @throws Exception
     */
    public function subscribe($topic, $callback = null)
    {
        try {
            // NOTE: Create a PubSub instance.
            $pubSub = Redis::client()->pubSub();

            // NOTE: Subscribe to the specified topic.
            $pubSub->subscribe($topic);

            // NOTE: Iterate through messages received from the subscription.
            foreach ($pubSub as $message) {
                // NOTE: Check if the message is of kind 'message'.
                if ($message->kind === 'message') {
                    // NOTE: Handle the received message using the provided callback.
                    // $this->handleMessage($message->payload, $callback);
                }
            }
        } catch (Exception $e) {
            // NOTE: Log and re-throw any errors that occur during subscription.
            Log::error(
                '❌ Error in PubSubService::subscribe method: ' .
                    $e->getMessage()
            );
            throw $e;
        }
    }
}
