<?php

declare(strict_types=1);

namespace Modules\Customer\Enum;

use BenSampo\Enum\Enum;

/**
 * Enumeration of (Pub/Sub) topics related to publish events.
 *
 * @method static static CREATED()
 * @method static static UPDATED()
 * @method static static DELETED()
 * @method static static SOFT_DELETED()
 * @method static static RECOVERED()
 */
final class PubTopics extends Enum
{
    /**
     * Topic for creation events.
     *
     * @example 'customer.created'
     */
    const CREATED = 'customer.created';

    /**
     * Topic for update events.
     *
     * @example 'customer.updated'
     */
    const UPDATED = 'customer.updated';

    /**
     * Topic for deletion events.
     *
     * @example 'customer.deleted'
     */
    const DELETED = 'customer.deleted';

    /**
     * Topic for soft deletion events.
     *
     * @example 'soft.customer.deleted'
     */
    const SOFT_DELETED = 'customer.soft.deleted';

    /**
     * Topic for recovery events.
     *
     * @example 'customer.recovered'
     */
    const RECOVERED = 'customer.recovered';
}
