<?php

declare(strict_types=1);

namespace Modules\Tenant\Trait;

use Illuminate\Support\Str;

/**
 * Trait IsSubdomain
 */
trait IsSubdomain
{
    /**
     * Check if a given hostname is a subdomain.
     *
     * @param  string  $hostname  The hostname to check.
     * @return bool True if the hostname is a subdomain, false otherwise.
     */
    protected function isSubdomain(string $hostname): bool
    {
        // Check if the hostname ends with any of the central domains
        return Str::endsWith($hostname, config('multi-tenancy.central_domains'));
    }
}
