<?php

declare(strict_types=1);

namespace Common\Config\Service;

use App\Util\PHPUtil;
use Common\Config\Interface\ConfigServiceInterface;
use Exception;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

/**
 * Class ConfigService
 *
 * A service class for managing environment variables using Dotenv in WordPress.
 */
class ConfigService implements ConfigServiceInterface
{
    /**
     * Get the NODE_ENV value.
     *
     * @return string The NODE_ENV value.
     */
    public static function getEnv(): ?string
    {
        try {
            return self::get('APP_ENV');
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return null;
        }
    }

    /**
     * Check if the current environment matches the provided environment.
     *
     * @param  string  $env  The environment to check against.
     * @return bool True if the environment matches, false otherwise.
     */
    public static function checkEnvironment(string $env): bool
    {
        try {
            return self::getEnv() === $env;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return false;
        }
    }

    /**
     * Check if the current environment is production.
     *
     * @return bool True if the environment is production, false otherwise.
     */
    public static function isProduction(): bool
    {
        try {
            return self::checkEnvironment('production');
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return false;
        }
    }

    /**
     * Check if the current environment is development.
     *
     * @return bool True if the environment is development, false otherwise.
     */
    public static function isDevelopment(): bool
    {
        try {
            return self::checkEnvironment('development');
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return false;
        }
    }

    /**
     * Check if the current environment is test.
     *
     * @return bool True if the environment is test, false otherwise.
     */
    public static function isTest(): bool
    {
        try {
            return self::checkEnvironment('test');
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return false;
        }
    }

    /**
     * Get the value of a configuration variable.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  mixed  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return mixed The value of the configuration variable or the default value.
     */
    public static function get(
        string $key,
        $defaultValue = null,
        $options = null
    ) {
        try {
            // Use Env to retrieve the value of the environment variable
            $value = Env::get($key, $defaultValue);

            // Return the value if found, otherwise return the default value
            return $value !== false ? $value : $defaultValue;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return null;
        }
    }

    /**
     * Get the value of a configuration variable or throw an exception if not defined.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  mixed  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return mixed The value of the configuration variable.
     */
    public static function getOrThrow(
        string $key,
        $defaultValue = null,
        $options = null
    ) {
        try {
            $value = self::get($key, $defaultValue, $options);
            if ($value === null) {
                throw new Exception(
                    "Environment variable '{$key}' is not defined."
                );
            }

            return $value;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return null;
        }
    }

    /**
     * Get the value of a configuration variable as a string.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  string|null  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return string The value of the configuration variable as a string.
     */
    public static function getString(
        string $key,
        ?string $defaultValue = null,
        $options = null
    ): ?string {
        try {
            return self::get($key, $defaultValue, $options) ?? $defaultValue ?? '';
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return null;
        }
    }

    /**
     * Get the value of a configuration variable as a number.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  int  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return int The value of the configuration variable as a number.
     */
    public static function getNumber(
        string $key,
        $defaultValue = null,
        $options = null
    ): ?int {
        try {
            return (int) self::get($key, $defaultValue, $options);
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return null;
        }
    }

    /**
     * Get the value of a configuration variable as a float number.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  float  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return float The value of the configuration variable as a float number.
     */
    public static function getFloat(
        string $key,
        $defaultValue = null,
        $options = null
    ): ?float {
        try {
            return (float) (self::get($key, $defaultValue, $options));
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return null;
        }
    }

    /**
     * Get the value of a configuration variable as a boolean.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  bool|null  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return bool The value of the configuration variable as a boolean.
     */
    public static function getBoolean(
        string $key,
        ?bool $defaultValue = null,
        $options = null
    ): bool {
        try {
            $value = self::get($key, $defaultValue, $options);

            // Log the key and value for debugging purposes
            // dd("Retrieved boolean value for '{$key}': " . var_export($value, true));

            // NOTE: Handle the conversion from "true" and "false" strings to boolean values
            if (PHPUtil::isString($value)) {
                $lowercaseValue = Str::lower($value);
                if ($lowercaseValue === 'true') {
                    return true;
                }
                if ($lowercaseValue === 'false') {
                    return false;
                }
            }

            // NOTE: Use filter_var for standard boolean conversion
            return filter_var(
                $value,
                FILTER_VALIDATE_BOOLEAN,
                FILTER_NULL_ON_FAILURE
            ) ?? $defaultValue;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return false;
        }
    }

    /**
     * Get the value of a configuration variable as an array.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  array|null  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (e.g., ['separator' => ',']).
     * @return array The value of the configuration variable as an array.
     */
    public static function getArray(
        string $key,
        ?array $defaultValue = null,
        $options = null
    ): ?array {
        try {
            // NOTE: Get the array separator from options or use default (",")
            $separator = $options['separator'] ?? ',';

            // NOTE: Get the configuration value
            $value = self::get($key, $defaultValue, $options);

            // Check if the value is null
            if ($value === null) {
                // Return the default value if it's provided
                if ($defaultValue !== null) {
                    return $defaultValue;
                }

                // Otherwise, return an empty array
                return [];
            }

            // NOTE: Split the string into an array using the specified separator
            return explode($separator, $value);
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return null;
        }
    }

    /**
     * Get the value of a configuration variable as an object.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return mixed The value of the configuration variable as an object.
     */
    public static function getObject(string $key, $options = null)
    {
        try {
            return self::get($key, null, $options);
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return null;
        }
    }

    /**
     * Get the value of a configuration variable as a Unix timestamp.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  float|null  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return float|null The value of the configuration variable as a Unix timestamp.
     */
    public static function getUnixTimestamp(
        string $key,
        $defaultValue = null,
        $options = null
    ): ?float {
        try {
            $value = self::get($key, $defaultValue, $options);

            return $value ? strtotime($value) : null;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            return null;
        }
    }
}
