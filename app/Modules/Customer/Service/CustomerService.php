<?php

declare(strict_types=1);

namespace Modules\Customer\Service;

use App\Abstractions\AbstractService;
use Exception;
use Modules\Customer\Interface\CustomerRepositoryInterface;
use Modules\Customer\Interface\CustomerServiceInterface;
use Modules\Customer\Interface\Data\CustomerInterface;
use Modules\Customer\Repository\CustomerRepository;

/**
 * Class CustomerService
 */
class CustomerService extends AbstractService implements CustomerServiceInterface
{
    /**
     * AbstractRepository constructor.
     *
     * @param  CustomerRepository  $repository  The repository instance to use in the service.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(CustomerRepositoryInterface $repository)
    {
        parent::__construct($repository, CustomerInterface::class);
    }
}
