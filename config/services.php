<?php

declare(strict_types=1);

use Common\Config\Service\ConfigService;

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'postmark' => [
        'token' => ConfigService::getString('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => ConfigService::getString('AWS_ACCESS_KEY_ID'),
        'secret' => ConfigService::getString('AWS_SECRET_ACCESS_KEY'),
        'region' => ConfigService::getString('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'slack' => [
        'notifications' => [
            'bot_user_oauth_token' => ConfigService::getString('SLACK_BOT_USER_OAUTH_TOKEN'),
            'channel' => ConfigService::getString('SLACK_BOT_USER_DEFAULT_CHANNEL'),
        ],
    ],

];
