<?php

declare(strict_types=1);

namespace Common\Request\Middleware;

use Closure;
use Common\Config\Service\ConfigService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

/**
 * Class Language
 */
class Language
{
    /**
     * Configuration key for the supported locales.
     */
    private const CONFIG_AVAILABLE_LOCALES = 'AVAILABLE_LOCALES';

    /**
     * Configuration key for the fallback locale.
     */
    private const CONFIG_FALLBACK_LOCALE = 'app.fallback_locale';

    /**
     * Header name for the Accept-Language header.
     */
    private const HEADER_ACCEPT_LANGUAGE = 'Accept-Language';

    /**
     * Header name for the X-Language header.
     */
    private const HEADER_X_LANGUAGE = 'x-language';

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request  The incoming request
     * @param  Closure  $next  The next middleware in the pipeline
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Retrieve the allowed locales from the configuration
        $locales = ConfigService::getArray(self::CONFIG_AVAILABLE_LOCALES);
        $fallbackLocale = Config::get(self::CONFIG_FALLBACK_LOCALE);

        // Check if the X-Language header is not present
        if (! $request->headers->has(self::HEADER_X_LANGUAGE)) {
            // Get the Accept-Language header from the request
            $acceptLanguage = $request->header(self::HEADER_ACCEPT_LANGUAGE) ?: $fallbackLocale;

            // Set the default locale as fallback
            $locale = App::getLocale();

            // If Accept-Language header is present and matches a supported locale, use it
            if ($acceptLanguage && in_array($acceptLanguage, $locales)) {
                $locale = $acceptLanguage;
            }

            // Set the X-Language header with the selected locale
            $request->headers->set(self::HEADER_X_LANGUAGE, $locale);

            // Set the application locale
            App::setLocale($locale);
        }

        // Call the next middleware in the pipeline with the modified request
        return $next($request);
    }
}
