<?php

declare(strict_types=1);

namespace Modules\Role\Interface\Data;

/**
 * Interface RoleUserInterface
 *
 * This interface defines constants related to role user attributes.
 */
interface RoleUserInterface
{
    /**
     * for the role_user table.
     */
    public const TABLE = 'role_user';
}
