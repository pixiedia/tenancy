<?php

declare(strict_types=1);

namespace Modules\Team\Factory;

use App\Util\PHPUtil;
use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Team\Interface\Data\TeamInterface;
use Modules\Team\Model\Team;
use RuntimeException;

/**
 * Class TeamFactory
 */
class TeamFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Team::class;

    /**
     * Define the model's default state.
     *
     * This method defines the default attributes for the Team model when creating fake data.
     * It uses the Faker library to generate random values for the attributes.
     *
     * @return array The default attributes for the Team model.
     */
    public function definition()
    {
        // Return an array containing the default attributes for the Team model
        return [
            // Generate a random UUID for the team
            TeamInterface::UUID => $this->faker->uuid,

            // Generate a random name for the team
            TeamInterface::NAME => $this->faker->words(3, true),

            // Generate a random value between 1 and 10 for the created_by field
            TeamInterface::CREATED_BY => $this->faker->numberBetween(1, 10),

            // Generate a random value between 1 and 10 for the updated_by field
            TeamInterface::UPDATED_BY => $this->faker->numberBetween(1, 10),

            // Generate random metadata for the team
            TeamInterface::METADATA => $this->getMetadata(),
        ];
    }

    /**
     * Get a randomly generated metadata array.
     *
     * @return string The generated metadata array.
     *
     * @throws RuntimeException If the metadata array fails to encode as JSON.
     */
    private function getMetadata(): string
    {
        // Initialize an empty array to store metadata
        $metadata = [];

        // Generate metadata with random keys and values
        for ($i = 0; $i < 10; $i++) {
            // Generate a random word as the key and another random word as the value
            $metadata[$this->faker->word] = $this->faker->word;
        }

        // Encode the metadata array as JSON
        $encodedMetadata = PHPUtil::jsonEncode($metadata);

        // Check if encoding was successful
        if ($encodedMetadata === false) {
            throw new RuntimeException('Failed to encode metadata as JSON: ' . json_last_error_msg());
        }

        return $encodedMetadata;
    }
}
