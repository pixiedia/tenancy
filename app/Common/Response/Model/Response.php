<?php

declare(strict_types=1);

namespace Common\Response\Model;

use App\Traits\DataObject;
use App\Util\PHPUtil;
use Common\Http\Enum\HttpStatusCode;
use Common\Response\Interface\Data\ResponseInterface;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class Response
 *
 * Data Transfer Object representing a response with optional meta information.
 */
class Response implements ResponseInterface
{
    use DataObject;

    /**
     * @var array
     */
    private $headers = [];

    /**
     * Get the data payload.
     */
    public function getPayload(): ?array
    {
        return $this->getData(static::PAYLOAD);
    }

    /**
     * Set the data payload.
     *
     * @param  array|null  $payload  The data to be included in the response.
     * @return $this
     */
    public function setPayload(?array $payload = null): self
    {
        $this->setData(static::PAYLOAD, $payload);

        return $this;
    }

    /**
     * Get the meta information.
     */
    public function getMeta(): ?array
    {
        return $this->getData(static::META);
    }

    /**
     * Set the meta information.
     *
     * @param  array|null  $meta  The meta information.
     * @return $this
     */
    public function setMeta(?array $meta = null): self
    {
        $this->setData(static::META, $meta);

        return $this;
    }

    /**
     * Get the HTTP status code.
     */
    public function getStatusCode(): int
    {
        return $this->getData(static::STATUS_CODE) ?? HttpStatusCode::OK;
    }

    /**
     * Set the HTTP status code.
     *
     * @param  int  $statusCode  The HTTP status code.
     * @return $this
     */
    public function setStatusCode(int $statusCode): self
    {
        // If a cause is provided and its status code is within the success range, use the cause's status code
        if ($statusCode === null || $statusCode < HttpStatusCode::OK || $statusCode >= HttpStatusCode::MULTIPLE_CHOICES) {
            // If no valid status code is provided or it's outside the success range, set it to internal server error
            $statusCode = HttpStatusCode::INTERNAL_SERVER_ERROR;
        }

        $this->setData(static::STATUS_CODE, $statusCode);

        return $this;
    }

    /**
     * Get the response message.
     */
    public function getMessage(): string
    {
        return $this->getData(static::MESSAGE) ?? '';
    }

    /**
     * Set the response message.
     *
     * @param  string|null  $message  The response message.
     * @return $this
     */
    public function setMessage(?string $message = null): self
    {
        $this->setData(static::MESSAGE, $message);

        return $this;
    }

    /**
     * Get the headers.
     */
    public function getHeaders(): array
    {
        return $this->getData(static::HEADERS) ?? [];
    }

    /**
     * Set the headers.
     *
     * @param  array|string  $headers  The headers to be included in the response.
     * @return $this
     */
    public function setHeaders($headers): self
    {
        // Get the existing headers
        $existingHeaders = $this->getHeaders();

        // Merge the new headers with the existing headers
        if (PHPUtil::isArray($headers)) {
            // If headers are provided as an array, merge them with the existing headers
            // Duplicate keys will be overridden by the new values
            $mergedHeaders = $existingHeaders + $headers;
        } else {
            // If headers are provided as a string, add them as a new entry to the existing headers
            $mergedHeaders = $existingHeaders + [$headers];
        }

        // Set the merged headers as the new headers for the response
        $this->setData(static::HEADERS, $mergedHeaders);

        // Return $this to allow for method chaining
        return $this;
    }

    /**
     * Get the error.
     *
     * @return Throwable|bool The error or false if not set.
     */
    public function getCause(): Throwable|bool
    {
        return $this->getData(static::CAUSE) ?? false;
    }

    /**
     * Set the error.
     *
     * @param  Throwable|null  $cause  The error.
     * @return $this
     */
    public function setCause(?Throwable $cause): self
    {
        $this->setData(static::CAUSE, $cause);

        return $this;
    }

    /**
     * Get the errors.
     */
    public function getErrors(): ?array
    {
        return $this->getData(static::ERRORS);
    }

    /**
     * Set the errors.
     *
     * @param  array  $headers  The errors to be included in the response.
     * @return $this
     */
    public function setErrors(array $errors): self
    {
        $this->setData(static::ERRORS, $errors);

        return $this;
    }

    /**
     * Convert array of object data with to array with keys requested in $keys array
     *
     * @param  array  $keys  array of required keys
     * @return array
     */
    public function toArray(array $keys = [])
    {
        // NOTE: Get status code, cause, headers, and data from the Response object
        $cause = $this->getCause();
        $errors = $this->getErrors();
        $payload = $this->getPayload();
        $headers = $this->getHeaders();
        $statusCode = $this->getStatusCode();

        // Get App Debug Status
        // TODO: UNCOMMENT AFTER DEBUG WHY RETUEN NULL
        $isAppDebugging = (bool) Config::get('app.debug');

        // NOTE: Initialize message with default value
        $message = __('Request executed successfully');

        // NOTE: Check if cause is an Exception and update message accordingly
        if ($cause instanceof Exception) {
            $message = $cause->getMessage();
        } elseif (! empty($this->getMessage())) {
            $message = $this->getMessage();
        }

        // Set the status code key using the camel-case version of the STATUS_CODE constant
        $response[Str::camel(static::STATUS_CODE)] = $statusCode;

        // Set the message key in the response array
        $response[static::MESSAGE] = $message;

        // NOTE: Add headers if available
        if (! empty($headers)) {
            $response[static::HEADERS] = $headers;
        }

        // NOTE: Add error information if cause is present
        if ((! empty($cause) || $cause !== false) && $isAppDebugging) {
            $response[static::CAUSE] = $cause instanceof Exception ? $cause->getTrace() : __('Error in response');
        } elseif ((! empty($errors) || $errors !== null) && $isAppDebugging) {
            $response[static::ERRORS] = $errors;
        } else {
            // NOTE: Add response data if no error
            $response[static::DATA] = $payload;
        }

        // NOTE: Add meta information if available
        if ($meta = $this->getMeta()) {
            $response[static::META] = $meta;
        }

        // NOTE: Return the response array
        return $response;
    }

    /**
     * Convert the response to JSON format.
     */
    public function toJson(): string
    {
        // Convert the response array to JSON format
        return PHPUtil::jsonEncode($this->toArray(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}
