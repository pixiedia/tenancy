<?php

declare(strict_types=1);

namespace Modules\Tenant\Exception;

use Exception;

/**
 * Exception thrown when tenancy is not initialized.
 */
class TenancyNotInitializedException extends Exception
{
    /**
     * Create a new TenancyNotInitializedException instance.
     *
     * @param  string  $message  The exception message (optional, default is 'Tenancy is not initialized.')
     */
    public function __construct($message = '')
    {
        parent::__construct($message ?: 'Tenancy is not initialized.');
    }
}
