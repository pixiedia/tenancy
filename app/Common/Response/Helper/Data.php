<?php

declare(strict_types=1);

namespace Common\Response\Helper;

use App\Util\PHPUtil;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

/**
 * Class Data
 *
 * Helper class for retrieving request ID and timestamp from request headers.
 */
class Data
{
    /**
     * Header name for the X-Request-ID header.
     */
    private const REQUEST_ID_HEADER = 'x-request-id';

    /**
     * Header name for the X-Amazon-Request-ID header.
     */
    private const AMAZON_REQUEST_ID_HEADER = 'x-amazon-request-id';

    /**
     * Header name for the X-Timestamp header.
     */
    private const TIMESTAMP_HEADER = 'x-timestamp';

    /**
     * Get the request ID from the request headers.
     *
     * @param  mixed  $request  The HTTP request object.
     * @return string The request ID.
     */
    public static function getRequestId($request): string
    {
        $headers = $request->getHeaders();

        foreach ($headers as $header) {
            $fieldName = $header->getFieldName();

            // NOTE: Check for different possible header fields containing the request ID
            if ($fieldName === Str::camel(self::REQUEST_ID_HEADER) || $fieldName === Str::ucfirst(self::AMAZON_REQUEST_ID_HEADER)) {
                return $header->getFieldValue();
            }
        }

        // NOTE: If no specific request ID headers found, generate a new UUID
        return Uuid::uuid4()->toString();
    }

    /**
     * Get the timestamp from the request headers.
     *
     * @param  mixed  $request  The HTTP request object.
     * @return string The timestamp extracted from the headers.
     */
    public static function getTimestamp($request): string
    {
        $headers = $request->getHeaders();

        foreach ($headers as $header) {
            $fieldName = $header->getFieldName();

            // NOTE: Check if the current header matches the timestamp header name
            if ($fieldName === Str::ucfirst(self::TIMESTAMP_HEADER)) {
                return $header->getFieldValue();
            }
        }

        // NOTE: If no specific timestamp header is found, generate a new timestamp
        return now()->timestamp;
    }

    /**
     * Recursively get nested data from the response array.
     *
     * @param  array  $data  The array containing response information.
     * @return mixed The nested data extracted from the response array.
     */
    public function getNestedData(array $data)
    {
        $responseData = $data;
        foreach ($data as $key => $value) {
            if (PHPUtil::isArray($value)) {
                $responseData[$key] = $this->getNestedData($value);
            }
        }

        return $responseData;
    }
}
