<?php

declare(strict_types=1);

namespace Modules\User\Service;

use App\Abstractions\AbstractService;
use Exception;
use Modules\User\Interface\Data\UserInterface;
use Modules\User\Interface\UserRepositoryInterface;
use Modules\User\Interface\UserServiceInterface;
use Modules\User\Repository\UserRepository;

/**
 * Class UserService
 */
class UserService extends AbstractService implements UserServiceInterface
{
    /**
     * AbstractRepository constructor.
     *
     * @param  UserRepository  $repository  The repository instance to use in the service.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        parent::__construct($repository, UserInterface::class);
    }
}
