<?php

declare(strict_types=1);

namespace Common\Http\Enum;

use BenSampo\Enum\Enum;

/**
 * Class HttpMethod
 *
 * Defines constants for common HTTP methods.
 *
 * @method static static GET()
 * @method static static POST()
 * @method static static PUT()
 * @method static static PATCH()
 * @method static static DELETE()
 */
final class HttpMethod extends Enum
{
    /**
     * HTTP GET method.
     */
    const GET = 'GET';

    /**
     * HTTP POST method.
     */
    const POST = 'POST';

    /**
     * HTTP PUT method.
     */
    const PUT = 'PUT';

    /**
     * HTTP PATCH method.
     */
    const PATCH = 'PATCH';

    /**
     * HTTP DELETE method.
     */
    const DELETE = 'DELETE';
}
