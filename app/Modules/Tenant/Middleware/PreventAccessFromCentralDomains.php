<?php

declare(strict_types=1);

namespace Modules\Tenant\Middleware;

use Closure;
use Common\Http\Enum\HttpStatusCode;
use Common\Response\Trait\ResponseBuilder;
use Illuminate\Http\Request;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Util\Phraser\Phrase;

/**
 * Class PreventAccessFromCentralDomains
 */
class PreventAccessFromCentralDomains
{
    use ResponseBuilder;

    /**
     * Handle an incoming request.
     *
     * @param  Closure(Request): (Response)  $next
     *
     * @throws AccessDeniedException
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (in_array($request->getHost(), config('multi-tenancy.central_domains'))) {
            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('Access denied from central domain.'), HttpStatusCode::FORBIDDEN);

            return response()->json($response->toArray(), HttpStatusCode::FORBIDDEN);
        }

        return $next($request);
    }
}
