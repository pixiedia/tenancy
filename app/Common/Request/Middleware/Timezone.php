<?php

declare(strict_types=1);

namespace Common\Request\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * Class Timezone
 */
class Timezone
{
    /**
     * Header name for the X-Timezone header.
     */
    private const TIME_ZONE_HEADER = 'x-timezone';

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request  The incoming request
     * @param  Closure  $next  The next middleware in the pipeline
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Set the X-Timezone header with the current timezone
        $request->headers->set(self::TIME_ZONE_HEADER, (string) now()->timezone);

        // Call the next middleware in the pipeline with the modified request
        return $next($request);
    }
}
