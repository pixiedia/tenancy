<?php

declare(strict_types=1);

namespace App\Abstractions;

use App\Abstractions\Interface\AbstractControllerInterface;
use App\Util\PHPUtil;
use Common\Http\Enum\HttpStatusCode;
use Common\Request\Interface\Data\RequestInterface;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Util\Phraser\Phrase;

/**
 * Class AbstractController
 */
abstract class AbstractController implements AbstractControllerInterface
{
    /**
     * The entity name for the pagination.
     *
     * @var string|null
     */
    const ENTITY_NAME = null;

    /**
     * Validate pagination parameters.
     *
     * @param  int  $pageNumber  The page number.
     * @param  int  $pageSize  The page size.
     *
     * @throws InvalidArgumentException if validation fails.
     */
    public function validatePaginationParams(int $pageNumber, int $pageSize): void
    {
        try {
            // Validate page number
            if ($pageNumber < RequestInterface::MIN_PAGE_NUMBER) {
                throw new InvalidArgumentException(Phrase::__('Invalid %1 list page number.', Str::plural(static::ENTITY_NAME))->render(), HttpStatusCode::BAD_REQUEST);
            }

            // Set your maximum limit
            $maxLimit = RequestInterface::MAX_PAGE_SIZE;

            if ($pageSize > $maxLimit) {
                throw new InvalidArgumentException(Phrase::__('Limit exceeds the maximum allowed limit.')->getText(), HttpStatusCode::BAD_REQUEST);
            }
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Re-throw the exception to maintain the original behavior
            throw $e;
        }
    }

    /**
     * Get and decode JSON request body content into an array.
     *
     * @param  Request  $request  The request object.
     * @return array The decoded JSON data.
     *
     * @throws InvalidArgumentException If the request body is empty or the JSON format is invalid.
     */
    public function getBody(Request $request): array
    {
        // Get the request body content
        $content = $request->getContent();

        // Check if the request body is empty
        if (empty($content)) {
            throw new InvalidArgumentException('Empty request body.');
        }

        // Decode the JSON data
        $data = PHPUtil::jsonDecode($content, true);

        // Check if JSON decoding failed
        if ($data === null && PHPUtil::isValidJson($content)) {
            throw new InvalidArgumentException('Invalid JSON format.');
        }

        return $data;
    }

    /**
     * Get the current language/locale.
     *
     * @return string The current language/locale.
     */
    public function getCurrentLanguage(): string
    {
        return App::getLocale();
    }

    /**
     * Get the user ID from the bearer token.
     *
     * @param  Request  $request  The request object.
     * @return int|null The user ID, or null if not found.
     */
    public function getUserId(Request $request): ?int
    {
        // Retrieve the authenticated user from the request
        $user = $request->user();

        // Check if a user is authenticated
        if ($user !== null) {
            // Return the user's ID
            return $user->id;
        }

        // Return null if no user is authenticated
        return null;
    }
}
