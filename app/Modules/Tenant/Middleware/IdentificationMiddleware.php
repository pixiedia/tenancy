<?php

declare(strict_types=1);

namespace Modules\Tenant\Middleware;

use Closure;
use Common\Http\Enum\HttpStatusCode;
use Common\Response\Trait\ResponseBuilder;
use Illuminate\Http\Request;
use Modules\Tenant\Exception\TenantCouldNotBeIdentifiedException;
use Modules\Tenant\Exception\TenantCouldNotBeIdentifiedOnDomainException;
use Modules\Tenant\Interface\Data\TenantInterface;
use Modules\Tenant\Model\Tenant;
use Util\Phraser\Phrase;

/**
 * Middleware to handle tenant detection and sharing of tenant instance with the application.
 */
class IdentificationMiddleware
{
    use ResponseBuilder;

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            // Detect the tenant based on the request
            /** @var Tenant $tenant */
            $tenant = $this->detectTenant($request);

            // Share the tenant instance with the application using a singleton
            app()->singleton(TenantInterface::ENTITY_NAME, fn () => $tenant);

            // If a matching tenant is found, set it on the request
            if ($tenant || $tenant->getId()) {
                $request->attributes->set(TenantInterface::ENTITY_NAME, $tenant);
            }

            // Continue processing the request
            return $next($request);
        } catch (TenantCouldNotBeIdentifiedException $e) {
            // Return failure response with error message
            $response = $this->createFailure(Phrase::__($e->getMessage()), HttpStatusCode::NOT_FOUND, $e);

            return response()->json($response->toArray(), HttpStatusCode::NOT_FOUND);
        }
    }

    /**
     * Detect the tenant based on the request.
     *
     * @param  Request  $request
     * @return Tenant|null
     *
     * @throws TenantCouldNotBeIdentifiedOnDomainException
     */
    protected function detectTenant($request): Tenant
    {
        // Get the domain from the request
        $domain = $request->getHost();

        // Find the tenant with the given domain
        $tenant = Tenant::where(TenantInterface::DOMAIN, $domain);

        if (! $tenant || ! $tenant->getId()) {
            throw new TenantCouldNotBeIdentifiedOnDomainException($domain);
        }

        dd('Welcome ' . $tenant->getName());

        return $tenant;
    }
}
