<?php

declare(strict_types=1);

namespace Modules\Role\Interface;

use App\Abstractions\Interface\AbstractServiceInterface;

/**
 * Interface RoleServiceInterface
 */
interface RoleServiceInterface extends AbstractServiceInterface
{
}
