<?php

declare(strict_types=1);

namespace Common\Request\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * Class UserAgent
 */
class UserAgent
{
    /**
     * Header name for the User-Agent header.
     */
    private const USER_AGENT_HEADER = 'User-Agent';

    /**
     * Header name for the X-User-Agent header.
     */
    private const X_USER_AGENT_HEADER = 'x-user-agent';

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request  The incoming request
     * @param  Closure  $next  The next middleware in the pipeline
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Check if the User-Agent header is present
        if (! $request->headers->has(self::USER_AGENT_HEADER)) {
            // Set the X-User-Agent header with the current user agent
            $request->headers->set(self::X_USER_AGENT_HEADER, $request->server(self::USER_AGENT_HEADER));
        }

        // Call the next middleware in the pipeline with the modified request
        return $next($request);
    }
}
