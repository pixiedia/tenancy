<?php

declare(strict_types=1);

namespace Modules\Role\Provider;

use App\Abstractions\AbstractModuleProvider;
use Modules\Role\Interface\Data\RoleInterface;
use Modules\Role\Interface\RoleRepositoryInterface;
use Modules\Role\Interface\RoleServiceInterface;
use Modules\Role\Model\Role;
use Modules\Role\Repository\RoleRepository;
use Modules\Role\Router\V1\RoleRoutes;
use Modules\Role\Service\RoleService;

/**
 * Class RoleModuleProvider
 */
class RoleModuleProvider extends AbstractModuleProvider
{
    /**
     * RoleModuleProvider constructor.
     *
     * Initializes the module provider with route, seeder, and migration files.
     */
    public function __construct()
    {
        // Glob all route files matching the defined path
        $this->routers = [RoleRoutes::class];

        // Define the path to the seeder files for the module
        $seederPath = base_path('app/Modules/Role/Seeder/*.php');

        // Glob all seeder files matching the defined path
        $this->seeders = glob($seederPath);

        // Define the path to the migration files for the module
        $migrationPath = base_path('app/Modules/Role/Migration/*.php');

        // Glob all migration files matching the defined path
        $this->migrations = glob($migrationPath);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function registerBindings()
    {
        // Bind RoleInterface to Role
        app()->bind(RoleInterface::class, Role::class);

        // Bind RoleRepositoryInterface to RoleRepository
        app()->bind(RoleRepositoryInterface::class, fn ($app) => new RoleRepository($app->make(RoleInterface::class)));

        // Bind RoleServiceInterface to RoleService
        app()->bind(RoleServiceInterface::class, fn ($app) => new RoleService($app->make(RoleRepositoryInterface::class)));
    }
}
