<?php

declare(strict_types=1);

namespace App\Abstractions;

use App\Abstractions\Interface\AbstractModuleProviderInterface;
use App\Util\PHPUtil;
use Common\Config\Service\ConfigService;
use Exception;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

/**
 * Abstract class for module provider.
 */
abstract class AbstractModuleProvider extends ServiceProvider implements AbstractModuleProviderInterface
{
    /**
     * Class name of the router class for the module.
     */
    public array $routers = [];

    /**
     * Array of seeder files for the module.
     */
    public array $seeders = [];

    /**
     * Array of middleware classes for the module.
     */
    public array $middlewares = [];

    /**
     * Array of migration files for the module.
     */
    public array $migrations = [];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
    }

    /**
     * Bootstrap services.
     *
     * This method is called after all other service providers have been registered,
     * meaning you have access to all other services in the application here.
     */
    public function boot(): void
    {
        // Register routes for the module
        $this->registerRoutes();

        // Register middleware for the module
        $this->registerMiddlewares();

        // Register migrations for the module
        $this->registerMigrations();

        // Register seeders for the module
        $this->registerSeeders();
    }

    /**
     * Register the module's service bindings.
     *
     * This method should be implemented in concrete module providers to bind interfaces to their implementations.
     *
     * @return void
     */
    public function registerBindings()
    {
    }

    /**
     * Register the tenancy-related routes.
     *
     * This method iterates over each version of the API and defines routes for that version.
     */
    public function registerRoutes()
    {
        // Define versions for the API
        $versions = ConfigService::getArray('API_VERSIONS');

        // Define prefix for the API
        $apiPrefix = ConfigService::getString('API_PREFIX');

        // Check if there are any route files found for the current version
        if ($this->routers) {
            // Iterate over each route file for the current version
            foreach ($this->routers as $router) {
                // Loop through each version and define routes for that version
                foreach ($versions as $version) {
                    $this->registerRoutesForVersion($router, $version, $apiPrefix);
                }
            }
        }
    }

    /**
     * Register the tenancy-related middlewares.
     *
     * @return $this
     */
    public function registerMiddlewares()
    {
        // Prepend each middleware to the global middleware priority
        foreach (array_reverse($this->middlewares) as $middleware) {
            app(Kernel::class)->pushMiddleware($middleware);
        }

        return $this;
    }

    /**
     * Register the tenancy-related migrations.
     *
     * This method loads all migration files from the 'Migration' directory
     * and registers them with the Laravel migration system.
     *
     * @return void
     */
    public function registerMigrations()
    {
        // Check if any migration files were found
        if ($this->migrations !== false && ! empty($this->migrations)) {
            // Iterate over each migration file
            foreach ($this->migrations as $migration) {
                // Load the migration file
                $this->loadMigrationsFrom($migration);
            }
        }
    }

    /**
     * Register the tenancy-related seeders.
     *
     * This method loads all seeder files from the 'Seeder' directory
     * and registers them with the Laravel seeder system.
     *
     * @return void
     */
    public function registerSeeders()
    {
        // Check if any seeder files were found
        if ($this->seeders === false || empty($this->seeders)) {
            return;
        }

        // Iterate over each seeder file
        foreach ($this->seeders as $seeder) {
            // Extract the class name from the file path
            $className = PHPUtil::getFullyQualifiedClassName($seeder);

            // Register the seeder
            $this->loadSeedersFrom($className);
        }
    }

    public function loadSeedersFrom($class)
    {
        $this->callAfterResolving(Seeder::class, function ($seeder) use ($class) {
            $seeder->call($class);
        });
    }

    /**
     * Setup an after resolving listener, or fire immediately if already resolved.
     *
     * @param  string  $name
     * @param  callable  $callback
     * @return void
     */
    protected function callAfterResolving($name, $callback)
    {
        app()->afterResolving($name, $callback);

        if (app()->resolved($name)) {
            $callback(app()->make($name), app());
        }
    }

    /**
     * Register routes for a specific API version.
     *
     * @param  string  $router  The router class name.
     * @param  string  $version  The API version.
     * @param  string  $apiPrefix  The API prefix.
     */
    private function registerRoutesForVersion($router, $version, $apiPrefix)
    {
        // Extract the version from the router class namespace
        $routerNamespace = trim(substr($router, 0, strrpos($router, '\\')), '\\');
        $routerVersion = substr($routerNamespace, strrpos($routerNamespace, '\\') + 1);

        // Check if the extracted version contains the specified version
        if (! str_contains($routerVersion, $version)) {
            // Skip registration if the router version does not contain the specified version
            return;
        }

        // Instantiate the router class
        $routerInstance = new $router();

        // Check if the router instance is an instance of AbstractRouter
        if (! ($routerInstance instanceof AbstractRouter)) {
            throw new Exception("Router class '{$router}' must be an instance of AbstractRouter.");
        }

        // Define routes within a prefix group for the current version
        Route::prefix("{$apiPrefix}/{$version}")
            ->group(function () use ($routerInstance) {
                // Require the route file to define routes
                $routerInstance->getRoutes();
            });
    }
}
