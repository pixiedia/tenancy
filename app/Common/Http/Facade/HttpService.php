<?php

declare(strict_types=1);

namespace Common\Http\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * Facade for the HttpService class.
 *
 * This facade provides a static interface for accessing the HttpService functionality.
 */
class HttpService extends Facade
{
    /**
     * The key used in the service container binding.
     */
    const HTTP_SERVICE = 'httpService';

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return self::HTTP_SERVICE;
    }
}
