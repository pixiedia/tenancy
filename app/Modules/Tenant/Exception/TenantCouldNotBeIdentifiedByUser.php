<?php

declare(strict_types=1);

namespace Modules\Tenant\Exception;

use Facade\IgnitionContracts\BaseSolution;
use Facade\IgnitionContracts\ProvidesSolution;
use Facade\IgnitionContracts\Solution;

/**
 * Exception thrown when a tenant could not be identified for a user.
 */
class TenantCouldNotBeIdentifiedByUser extends TenantCouldNotBeIdentifiedException implements ProvidesSolution
{
    /**
     * Create a new exception instance.
     */
    public function __construct()
    {
        parent::__construct('Tenant could not be identified for this user');
    }

    /**
     * Get the solution for the exception.
     */
    public function getSolution(): Solution
    {
        return BaseSolution::create('Tenant could not be identified for this user')
            ->setSolutionDescription('Did you forget to create a tenant for this user?');
    }
}
