<?php

declare(strict_types=1);

namespace Modules\Permission\Seeder;

use App\Util\PHPUtil;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Modules\Permission\Interface\Data\PermissionInterface;
use Modules\Permission\Model\Permission;
use Modules\Role\Interface\Data\RoleInterface;
use Modules\Role\Model\Role;

/**
 * Class PermissionSeeder
 * Seeder class for generating permission data
 */
class PermissionSeeder extends Seeder
{
    /**
     * Default size for seeders.
     */
    private $defaultSeederSize;

    /**
     * Default size for relation seeders.
     */
    private $defaultRelationSeederSize;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->defaultSeederSize = Config::get('database.seeders.defaultSeederSize');
        $this->defaultRelationSeederSize = Config::get('database.seeders.defaultSeederRelationSize');
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        try {
            // Log the start of the seeding to the console
            PHPUtil::consoleOutput('🚀 Seeding permissions...');

            // Seed permissions
            $this->seedPermissions();

            // Associate roles with permissions
            $this->associateRoles();

            // Log the end of the seeding process to the console
            PHPUtil::consoleOutput('✅ Seeding completed.');
        } catch (QueryException $e) {
            // Handle the exception
            PHPUtil::consoleOutput("❌ Error seeding permissions: {$e->getMessage()}");
        }
    }

    /**
     * Seed permissions.
     */
    private function seedPermissions(): void
    {
        // Generate 50 permission instances
        $permissions = Permission::factory()->count($this->defaultSeederSize)->make();

        // Seed each permission
        foreach ($permissions as $permission) {
            DB::table(PermissionInterface::TABLE)->insert($permission->toArray());

            // Log details about the seeded permission
            PHPUtil::consoleOutput("🌱 Seeded permission: {$permission->getName()}");
        }
    }

    /**
     * Associate roles with permissions.
     */
    private function associateRoles(): void
    {
        // Get all existing roles
        $roles = Role::all();

        // Get all existing permissions
        $permissions = Permission::all();

        // Associate roles with each permission
        $permissions->each(function ($permission) use ($roles) {
            // Attach random roles to the current permission
            $randomRoles = $roles->random($this->defaultRelationSeederSize);

            /** @var Permission $permission */
            $permission->roles()->saveMany($randomRoles);

            // Log details about the permission and the associated roles
            PHPUtil::consoleOutput("🔗 Associated roles with permission: {$randomRoles->pluck(RoleInterface::NAME)->implode(', ')}");
        });
    }
}
