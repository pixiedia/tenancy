<?php

declare(strict_types=1);

namespace Common\Response\Interface\Data;

use Throwable;

/**
 * Interface ResponseInterface
 *
 * @api
 */
interface ResponseInterface
{
    /**
     * Key for accessing the data in the response.
     */
    public const DATA = 'data';

    /**
     * Key for accessing the payload in the response.
     */
    public const PAYLOAD = 'payload';

    /**
     * Key for accessing the meta information in the response.
     */
    public const META = 'meta';

    /**
     * Key for accessing the HTTP status code in the response.
     */
    public const STATUS_CODE = 'status_code';

    /**
     * Key for accessing the response message in the response.
     */
    public const MESSAGE = 'message';

    /**
     * Key for accessing the headers in the response.
     */
    public const HEADERS = 'headers';

    /**
     * Key for accessing the error cause in the response.
     */
    public const CAUSE = 'cause';

    /**
     * Key for accessing the errors in the response.
     */
    public const ERRORS = 'errors';

    /**
     * Represents the page number in pagination.
     */
    const PAGE_NUMBER_KEY = 'pageNumber';

    /**
     * Represents the page size in pagination.
     */
    const PAGE_SIZE_KEY = 'pageSize';

    /**
     * Represents the total count of items in pagination.
     */
    const TOTAL_COUNT_KEY = 'totalCount';

    /**
     * Get the data payload.
     */
    public function getPayload(): ?array;

    /**
     * Set the data.
     *
     * @param  array|null  $data  The data to be included in the response.
     * @return $this
     */
    public function setPayload(?array $data = null): self;

    /**
     * Get the meta information.
     */
    public function getMeta(): ?array;

    /**
     * Set the meta information.
     *
     * @param  array|null  $meta  The meta information.
     * @return $this
     */
    public function setMeta(?array $meta = null): self;

    /**
     * Get the HTTP status code.
     */
    public function getStatusCode(): int;

    /**
     * Set the HTTP status code.
     *
     * @param  int  $statusCode  The HTTP status code.
     * @return $this
     */
    public function setStatusCode(int $statusCode): self;

    /**
     * Get the response message.
     */
    public function getMessage(): string;

    /**
     * Set the response message.
     *
     * @param  string|null  $message  The response message.
     * @return $this
     */
    public function setMessage(?string $message = null): self;

    /**
     * Get the headers.
     */
    public function getHeaders(): array;

    /**
     * Set the headers.
     *
     * @param  array|string  $headers  The headers to be included in the response.
     * @return $this
     */
    public function setHeaders($headers): self;

    /**
     * Get the error.
     *
     * @return Throwable|bool The error or false if not set.
     */
    public function getCause(): Throwable|bool;

    /**
     * Set the error.
     *
     * @param  Throwable|null  $cause  The error.
     * @return $this
     */
    public function setCause(?Throwable $cause): self;

    /**
     * Get the errors.
     */
    public function getErrors(): ?array;

    /**
     * Set the errors.
     *
     * @param  array  $headers  The errors to be included in the response.
     * @return $this
     */
    public function setErrors(array $errors): self;

    /**
     * Convert array of object data with to array with keys requested in $keys array
     *
     * @param  array  $keys  array of required keys
     * @return array
     */
    public function toArray(array $keys = []);

    /**
     * Convert the response to JSON format.
     */
    public function toJson(): string;
}
