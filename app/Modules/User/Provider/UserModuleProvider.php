<?php

declare(strict_types=1);

namespace Modules\User\Provider;

use App\Abstractions\AbstractModuleProvider;
use Modules\User\Interface\Data\UserInterface;
use Modules\User\Interface\UserRepositoryInterface;
use Modules\User\Interface\UserServiceInterface;
use Modules\User\Model\User;
use Modules\User\Repository\UserRepository;
use Modules\User\Router\V1\UserRoutes;
use Modules\User\Service\UserService;

/**
 * Class UserModuleProvider
 */
class UserModuleProvider extends AbstractModuleProvider
{
    /**
     * UserModuleProvider constructor.
     *
     * Initializes the module provider with route, seeder, and migration files.
     */
    public function __construct()
    {
        // Glob all route files matching the defined path
        $this->routers = [UserRoutes::class];

        // Define the path to the seeder files for the module
        $seederPath = base_path('app/Modules/User/Seeder/*.php');

        // Glob all seeder files matching the defined path
        $this->seeders = glob($seederPath);

        // Define the path to the migration files for the module
        $migrationPath = base_path('app/Modules/User/Migration/*.php');

        // Glob all migration files matching the defined path
        $this->migrations = glob($migrationPath);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function registerBindings()
    {
        // Bind UserInterface to User
        app()->bind(UserInterface::class, User::class);

        // Bind UserRepositoryInterface to UserRepository
        app()->bind(UserRepositoryInterface::class, fn ($app) => new UserRepository($app->make(UserInterface::class)));

        // Bind UserServiceInterface to UserService
        app()->bind(UserServiceInterface::class, fn ($app) => new UserService($app->make(UserRepositoryInterface::class)));
    }
}
