<?php

declare(strict_types=1);

namespace Modules\Team\Interface;

use App\Abstractions\Interface\AbstractServiceInterface;

/**
 * Interface TeamServiceInterface
 */
interface TeamServiceInterface extends AbstractServiceInterface
{
}
