<?php

declare(strict_types=1);

namespace Common\Response\Provider;

use App\Abstractions\AbstractModuleProvider;
use Common\Response\Interface\Data\ResponseInterface;
use Common\Response\Middleware\CamelCase;
use Common\Response\Middleware\TransformResponse;
use Common\Response\Model\Response;

/**
 * Class ResponseModuleProvider
 */
class ResponseModuleProvider extends AbstractModuleProvider
{
    /**
     * ResponseModuleProvider constructor.
     *
     * Initializes the module provider with route, seeder, and migration files.
     */
    public function __construct()
    {
        // Define the middlewares for the module
        $this->middlewares = [
            TransformResponse::class,
            CamelCase::class,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function registerBindings()
    {
        // Bind ResponseInterface to Response
        app()->bind(ResponseInterface::class, fn ($app) => new Response());
    }
}
