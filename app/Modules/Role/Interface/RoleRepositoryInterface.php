<?php

declare(strict_types=1);

namespace Modules\Role\Interface;

use App\Abstractions\Interface\AbstractRepositoryInterface;

/**
 * Interface RoleRepositoryInterface
 */
interface RoleRepositoryInterface extends AbstractRepositoryInterface
{
}
