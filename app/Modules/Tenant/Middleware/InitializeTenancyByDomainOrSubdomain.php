<?php

declare(strict_types=1);

namespace Modules\Tenant\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Tenant\Trait\IsSubdomain;
use Symfony\Component\HttpFoundation\Response;

/**
 * Middleware for initializing tenancy based on domain or subdomain.
 */
class InitializeTenancyByDomainOrSubdomain
{
    use IsSubdomain;

    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): Response
    {
        // Check if the hostname is a subdomain
        if ($this->isSubdomain($request->getHost())) {
            // Initialize tenancy based on subdomain
            return app(InitializeTenancyBySubdomain::class)->handle($request, $next);
        }

        // Initialize tenancy based on domain
        return app(InitializeTenancyByDomain::class)->handle($request, $next);
    }
}
