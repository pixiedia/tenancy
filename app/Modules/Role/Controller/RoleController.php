<?php

declare(strict_types=1);

namespace Modules\Role\Controller;

use App\Abstractions\AbstractController;
use Common\Request\Interface\Data\RequestInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Role\Dto\CreateRoleDto;
use Modules\Role\Dto\UpdateRoleDto;
use Modules\Role\Interface\Data\RoleInterface;
use Modules\Role\Interface\RoleServiceInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Tag(
 *      name="Roles",
 *      description="Role API Endpoints"
 * )
 *
 * Class RoleController
 */
class RoleController extends AbstractController
{
    /**
     * The key used to store the search term in the request.
     */
    const SEARCH_TERM_KEY = 'searchTerm';

    /**
     * The entity name for the pagination.
     *
     * @var string|null
     */
    const ENTITY_NAME = RoleInterface::ENTITY_NAME;

    /**
     * The Role service interface instance.
     *
     * @var RoleServiceInterface
     */
    protected $roleService;

    /**
     * RoleController constructor.
     */
    public function __construct(RoleServiceInterface $roleService)
    {
        $this->roleService = $roleService;
    }

    /**
     * @OA\Get(
     *      path="/roles",
     *      summary="Get a list of roles",
     *      tags={"Roles"},
     *
     *      @OA\Parameter(
     *          name="page",
     *          in="query",
     *          description="Page number",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=1
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Role")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a list of roles.
     *
     * @param  int  $page  The page number.
     * @param  int  $size  The page size.
     * @return JsonResponse The JSON response containing the list of roles.
     */
    public function getList(Request $request): JsonResponse
    {
        try {
            // Get pagination parameters from the request
            $size = (int) $request->query(RequestInterface::SIZE_KEY, (string) RequestInterface::DEFAULT_PAGE_SIZE);
            $page = (int) $request->query(RequestInterface::PAGE_KEY, (string) RequestInterface::DEFAULT_PAGE_NUMBER);

            // Validate pagination parameters
            $this->validatePaginationParams($page, $size);

            // Retrieve paginated list of roles from the service
            $roles = $this->roleService->list($page, $size);

            // Return JSON response with the list of roles
            return response()->json($roles);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/roles/search/{searchTerm}",
     *      summary="Get a list of roles by search term",
     *      tags={"Roles"},
     *
     *      @OA\Parameter(
     *          name="searchTerm",
     *          in="path",
     *          description="Search Term",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="string",
     *              default="Role A"
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(
     *              required={"searchTerm", "page", "size", "filters", "sortOrder"},
     *
     *              @OA\Property(property="filters", type="object", example={}),
     *              @OA\Property(property="sortOrder", type="string", enum={"asc", "desc"}, example="asc")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Role")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a list of roles.
     *
     * @param  Request  $request  The HTTP request object containing search, pagination, filters, and sort parameters.
     * @return JsonResponse The JSON response containing the list of roles matching the search criteria.
     */
    public function search(Request $request): JsonResponse
    {
        try {
            // Get search term from the request
            $searchTerm = $request->route(static::SEARCH_TERM_KEY);

            // Get pagination parameters from the request
            $size = (int) $request->query(RequestInterface::SIZE_KEY, (string) RequestInterface::DEFAULT_PAGE_SIZE);
            $page = (int) $request->query(RequestInterface::PAGE_KEY, (string) RequestInterface::DEFAULT_PAGE_NUMBER);

            // Validate pagination parameters
            $this->validatePaginationParams($page, $size);

            // Get filters and sort criteria from the request, defaulting to an empty array and default sort field and order if not provided
            $filters = $request->input(RequestInterface::FILTERS_KEY, []);
            $sortOrder = $request->input(RequestInterface::SORT_ORDER_KEY, RequestInterface::DEFAULT_SORT_ORDER);

            // Retrieve paginated search of roles from the service based on the search term, filters, and sort criteria
            $roles = $this->roleService->search($searchTerm, $page, $size, $filters, $sortOrder);

            // Return JSON response with the list of roles matching the search criteria
            return response()->json($roles);
        } catch (Exception $e) {
            // Handle any exceptions that occur during the search and return a JSON response with an error message
            throw $e;
        }
    }

    /**
     * @OA\Get(
     *      path="/roles/{id}",
     *      summary="Get a role by ID",
     *      tags={"Roles"},
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the role to retrieve",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Role")
     *          )
     *      ),
     *
     *     @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a role by ID.
     *
     * @param  int  $id  The ID of the role to retrieve.
     * @return JsonResponse The JSON response containing the role details.
     */
    public function getById(int $id): JsonResponse
    {
        try {
            // Retrieve the role by ID from the service
            $role = $this->roleService->find($id);

            // Return JSON response with the role details
            return response()->json($role);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Get(
     *      path="/roles/uuid/{uuid}",
     *      summary="Get a role by UUID",
     *      tags={"Roles"},
     *
     *      @OA\Parameter(
     *          name="uuid",
     *          in="path",
     *          description="UUID of the role to retrieve",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Role")
     *          )
     *      ),
     *
     *     @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a role by UUID.
     *
     * @param  string  $uuid  The UUID of the role to retrieve.
     * @return JsonResponse The JSON response containing the role details.
     */
    public function getByUuid(string $uuid): JsonResponse
    {
        try {
            // Retrieve the role by ID from the service
            $role = $this->roleService->findByUuid($uuid);

            // Return JSON response with the role details
            return response()->json($role);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/roles",
     *      summary="Create a new role",
     *      tags={"Roles"},
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/CreateRoleDto")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Role")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Create a new role.
     *
     * @param  CreateRoleDto  $request  The validated request DTO containing the role data.
     * @return JsonResponse The JSON response containing the newly created role details.
     */
    public function create(CreateRoleDto $request): JsonResponse
    {
        try {
            // Get validated data from the request DTO
            $data = $request->validated();

            // Create a new role using the service
            $role = $this->roleService->create($data);

            // Return JSON response with the newly created role details
            return response()->json($role);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/roles/bulk",
     *      summary="Create multiple new roles",
     *      tags={"Roles"},
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(
     *              type="array",
     *
     *              @OA\Items(ref="#/components/schemas/Role")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Role")),
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Create multiple new roles.
     *
     * @param  Request  $request  The HTTP request object containing the list of roles to create.
     * @return JsonResponse The JSON response containing the newly created roles.
     */
    public function bulkCreate(Request $request): JsonResponse
    {
        try {
            $dataList = $request->list(); // Assuming the request contains an array of role data
            $roles = $this->roleService->bulkCreate($dataList);

            return response()->json($roles);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @OA\Put(
     *      path="/roles/{id}",
     *      summary="Update a role by ID",
     *      tags={"Roles"},
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the role to update",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/UpdateRoleDto")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Role")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Update a role by ID.
     *
     * @param  UpdateRoleDto  $request  The validated request DTO containing the updated role data.
     * @param  $id  The ID of the role to update.
     * @return JsonResponse The JSON response containing the updated role details.
     */
    public function update(UpdateRoleDto $request, int $id): JsonResponse
    {
        try {
            // Get validated data from the request DTO
            $data = $request->validated();

            // Update the role using the service
            $role = $this->roleService->update($id, $data);

            // Return JSON response with the updated role details
            return response()->json($role);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Delete(
     *      path="/roles/{id}",
     *      summary="Delete a role by ID",
     *      tags={"Roles"},
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the role to delete",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Role")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Delete a role by ID.
     *
     * @param  int  $id  The ID of the role to delete.
     * @return JsonResponse The JSON response containing the deleted role details.
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            // Delete the role using the service
            $role = $this->roleService->delete($id);

            // Return JSON response with the deleted role details
            return response()->json($role);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Put(
     *      path="/roles/{id}/restore",
     *      summary="Restore a soft-deleted role by ID",
     *      tags={"Roles"},
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the role to restore",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Role")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Restore a soft-deleted role by ID.
     *
     * @param  int  $id  The ID of the role to restore.
     * @return JsonResponse The JSON response indicating the success or failure of the restore operation.
     */
    public function restore(int $id): JsonResponse
    {
        try {
            $role = $this->roleService->restore($id);

            return response()->json($role);
        } catch (Exception $e) {
            throw $e;
        }
    }
}
