<?php

declare(strict_types=1);

namespace Modules\Customer\Provider;

use App\Abstractions\AbstractModuleProvider;
use Modules\Customer\Interface\CustomerRepositoryInterface;
use Modules\Customer\Interface\CustomerServiceInterface;
use Modules\Customer\Interface\Data\CustomerInterface;
use Modules\Customer\Model\Customer;
use Modules\Customer\Repository\CustomerRepository;
use Modules\Customer\Router\V1\CustomerRoutes;
use Modules\Customer\Service\CustomerService;

/**
 * Class CustomerModuleProvider
 */
class CustomerModuleProvider extends AbstractModuleProvider
{
    /**
     * CustomerModuleProvider constructor.
     *
     * Initializes the module provider with route, seeder, and migration files.
     */
    public function __construct()
    {
        // Glob all route files matching the defined path
        $this->routers = [CustomerRoutes::class];

        // Define the path to the seeder files for the module
        $seederPath = base_path('app/Modules/Customer/Seeder/*.php');

        // Glob all seeder files matching the defined path
        $this->seeders = glob($seederPath);

        // Define the path to the migration files for the module
        $migrationPath = base_path('app/Modules/Customer/Migration/*.php');

        // Glob all migration files matching the defined path
        $this->migrations = glob($migrationPath);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function registerBindings()
    {
        // Bind CustomerInterface to Customer
        app()->bind(CustomerInterface::class, Customer::class);

        // Bind CustomerRepositoryInterface to CustomerRepository
        app()->bind(CustomerRepositoryInterface::class, fn ($app) => new CustomerRepository($app->make(CustomerInterface::class)));

        // Bind CustomerServiceInterface to CustomerService
        app()->bind(CustomerServiceInterface::class, fn ($app) => new CustomerService($app->make(CustomerRepositoryInterface::class)));
    }
}
