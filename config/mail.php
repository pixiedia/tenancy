<?php

declare(strict_types=1);

use Common\Config\Service\ConfigService;
use Illuminate\Support\Str;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Mailer
    |--------------------------------------------------------------------------
    |
    | This option controls the default mailer that is used to send all email
    | messages unless another mailer is explicitly specified when sending
    | the message. All additional mailers can be configured within the
    | "mailers" array. Examples of each type of mailer are provided.
    |
    */

    'default' => ConfigService::getString('MAIL_MAILER', 'log'),

    'driver' => ConfigService::getString('MAIL_MAILER', 'log'),
    'url' => ConfigService::getString('MAIL_URL'),
    'host' => ConfigService::getString('MAIL_HOST', '127.0.0.1'),
    'port' => ConfigService::getNumber('MAIL_PORT', 2525),
    'encryption' => ConfigService::getString('MAIL_ENCRYPTION', 'tls'),
    'username' => ConfigService::getString('MAIL_USERNAME'),
    'password' => ConfigService::getString('MAIL_PASSWORD'),
    'timeout' => null,
    'local_domain' => ConfigService::getString('MAIL_EHLO_DOMAIN'),

    /*
    |--------------------------------------------------------------------------
    | Mailer Configurations
    |--------------------------------------------------------------------------
    |
    | Here you may configure all of the mailers used by your application plus
    | their respective settings. Several examples have been configured for
    | you and you are free to add your own as your application requires.
    |
    | Laravel supports a variety of mail "transport" drivers that can be used
    | when delivering an email. You may specify which one you're using for
    | your mailers below. You may also add additional mailers if needed.
    |
    | Supported: "smtp", "sendmail", "mailgun", "ses", "ses-v2",
    |            "postmark", "log", "array", "failover", "roundrobin"
    |
    */

    'mailers' => [

        'smtp' => [
            'transport' => 'smtp',
            'url' => ConfigService::getString('MAIL_URL'),
            'host' => ConfigService::getString('MAIL_HOST', '127.0.0.1'),
            'port' => ConfigService::getNumber('MAIL_PORT', 2525),
            'encryption' => ConfigService::getString('MAIL_ENCRYPTION', 'tls'),
            'username' => ConfigService::getString('MAIL_USERNAME'),
            'password' => ConfigService::getString('MAIL_PASSWORD'),
            'timeout' => null,
            'local_domain' => ConfigService::getString('MAIL_EHLO_DOMAIN'),
        ],

        'ses' => [
            'transport' => 'ses',
        ],

        'postmark' => [
            'transport' => 'postmark',
            // 'message_stream_id' => ConfigService::getString('POSTMARK_MESSAGE_STREAM_ID'),
            // 'client' => [
            //     'timeout' => 5,
            // ],
        ],

        'sendmail' => [
            'transport' => 'sendmail',
            'path' => ConfigService::getString('MAIL_SENDMAIL_PATH', '/usr/sbin/sendmail -bs -i'),
        ],

        'log' => [
            'transport' => 'log',
            'channel' => ConfigService::getString('MAIL_LOG_CHANNEL'),
        ],

        'array' => [
            'transport' => 'array',
        ],

        'failover' => [
            'transport' => 'failover',
            'mailers' => [
                'smtp',
                'log',
            ],
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Global "From" Address
    |--------------------------------------------------------------------------
    |
    | You may wish for all emails sent by your application to be sent from
    | the same address. Here you may specify a name and address that is
    | used globally for all emails that are sent by your application.
    |
    */

    'from' => [
        'address' => ConfigService::getString('MAIL_FROM_ADDRESS', 'hello@example.com'),
        'name' => Str::headline(ConfigService::getString('MAIL_FROM_NAME', 'Example')),
        'reply_to' => ['address' => ConfigService::getString('REPLY_TO_ADDRESS', 'hello@example.com'), 'name' => Str::headline(ConfigService::getString('REPLY_TO_NAME'))],
    ],

];
