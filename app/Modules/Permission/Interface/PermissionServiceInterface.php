<?php

declare(strict_types=1);

namespace Modules\Permission\Interface;

use App\Abstractions\Interface\AbstractServiceInterface;

/**
 * Interface PermissionServiceInterface
 */
interface PermissionServiceInterface extends AbstractServiceInterface
{
}
