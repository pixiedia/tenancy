<?php

declare(strict_types=1);

namespace Modules\Permission\Interface\Data;

/**
 * Interface PermissionRoleInterface
 *
 * This interface defines constants related to permission role attributes.
 */
interface PermissionRoleInterface
{
    /**
     * for the permission_role table.
     */
    public const TABLE = 'permission_role';
}
