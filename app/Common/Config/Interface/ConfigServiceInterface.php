<?php

declare(strict_types=1);

namespace Common\Config\Interface;

/**
 * Interface ConfigServiceInterface
 *
 * A service interface for managing environment variables using Dotenv in WordPress.
 */
interface ConfigServiceInterface
{
    /**
     * Get the NODE_ENV value.
     *
     * @return string The NODE_ENV value.
     */
    public static function getEnv(): ?string;

    /**
     * Check if the current environment matches the provided environment.
     *
     * @param  string  $env  The environment to check against.
     * @return bool True if the environment matches, false otherwise.
     */
    public static function checkEnvironment(string $env): bool;

    /**
     * Check if the current environment is production.
     *
     * @return bool True if the environment is production, false otherwise.
     */
    public static function isProduction(): bool;

    /**
     * Check if the current environment is development.
     *
     * @return bool True if the environment is development, false otherwise.
     */
    public static function isDevelopment(): bool;

    /**
     * Check if the current environment is test.
     *
     * @return bool True if the environment is test, false otherwise.
     */
    public static function isTest(): bool;

    /**
     * Get the value of a configuration variable.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  mixed  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return mixed The value of the configuration variable or the default value.
     */
    public static function get(
        string $key,
        $defaultValue = null,
        $options = null
    );

    /**
     * Get the value of a configuration variable or throw an exception if not defined.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  mixed  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return mixed The value of the configuration variable.
     *
     * @throws Exception If the configuration variable is not defined.
     */
    public static function getOrThrow(
        string $key,
        $defaultValue = null,
        $options = null
    );

    /**
     * Get the value of a configuration variable as a string.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  string|null  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return string The value of the configuration variable as a string.
     */
    public static function getString(
        string $key,
        ?string $defaultValue = null,
        $options = null
    ): ?string;

    /**
     * Get the value of a configuration variable as a number.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  int  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return int The value of the configuration variable as a number.
     */
    public static function getNumber(
        string $key,
        $defaultValue = null,
        $options = null
    ): ?int;

    /**
     * Get the value of a configuration variable as a float number.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  float  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return float The value of the configuration variable as a float number.
     */
    public static function getFloat(
        string $key,
        $defaultValue = null,
        $options = null
    ): ?float;

    /**
     * Get the value of a configuration variable as a boolean.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  bool|null  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return bool The value of the configuration variable as a boolean.
     */
    public static function getBoolean(
        string $key,
        ?bool $defaultValue = null,
        $options = null
    ): bool;

    /**
     * Get the value of a configuration variable as an array.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  array|null  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (e.g., ['separator' => ',']).
     * @return array The value of the configuration variable as an array.
     */
    public static function getArray(
        string $key,
        ?array $defaultValue = null,
        $options = null
    ): ?array;

    /**
     * Get the value of a configuration variable as an object.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return mixed The value of the configuration variable as an object.
     */
    public static function getObject(string $key, $options = null);

    /**
     * Get the value of a configuration variable as a Unix timestamp.
     *
     * @param  string  $key  The key of the configuration variable.
     * @param  float|null  $defaultValue  The default value if the key is not found.
     * @param  mixed  $options  Additional options (not used in this example).
     * @return float|null The value of the configuration variable as a Unix timestamp.
     */
    public static function getUnixTimestamp(
        string $key,
        $defaultValue = null,
        $options = null
    ): ?float;
}
