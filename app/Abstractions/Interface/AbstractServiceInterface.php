<?php

declare(strict_types=1);

namespace App\Abstractions\Interface;

use App\Exceptions\NoSuchEntityException;
use Common\Request\Interface\Data\RequestInterface;
use Common\Response\Interface\Data\ResponseInterface;
use Exception;

/**
 * Interface AbstractServiceInterface
 */
interface AbstractServiceInterface
{
    /**
     * Get all entities with pagination.
     *
     * @param  int  $pageNumber  The page number.
     * @param  int  $pageSize  The number of items per page.
     */
    public function list(int $pageNumber = RequestInterface::DEFAULT_PAGE_NUMBER, int $pageSize = RequestInterface::DEFAULT_PAGE_SIZE): ResponseInterface;

    /**
     * Search for entities based on a search term, with optional filters, sorting, and pagination.
     *
     * @param  string  $searchTerm  The term to search for.
     * @param  int  $pageNumber  The page number.
     * @param  int  $pageSize  The number of items per page.
     * @param  array  $filters  Additional filters for the search.
     * @param  string  $sort  The sorting order ('ASC' or 'DESC').
     */
    public function search(string $searchTerm, int $pageNumber = RequestInterface::DEFAULT_PAGE_NUMBER, int $pageSize = RequestInterface::DEFAULT_PAGE_SIZE, array $filters = [], string $sort = RequestInterface::DEFAULT_SORT_ORDER): ResponseInterface;

    /**
     * Find a entity by ID.
     *
     * @param  int  $id  The ID of the entity to find.
     * @return ResponseInterface|null
     */
    public function find(int $id): ResponseInterface;

    /**
     * Find a entity by UUID.
     *
     * @param  string  $uuid  The UUID of the entity to find.
     * @return ResponseInterface|null
     */
    public function findByUuid(string $uuid): ResponseInterface;

    /**
     * Find a entity by identifier.
     *
     * @param  string  $identifier  The identifier of the entity to find.
     * @return ResponseInterface|null
     */
    public function findBy(string $identifier, ?string $key = null): ResponseInterface;

    /**
     * Create a new entity.
     *
     * @param  array  $data  The data to create the entity.
     */
    public function create(array $data): ResponseInterface;

    /**
     * Create multiple new entities.
     *
     * @param  array  $dataList  List of data to create entities.
     *
     * @throws Exception
     */
    public function bulkCreate(array $dataList): ResponseInterface;

    /**
     * Update a entity by ID.
     *
     * @param  int  $id  The ID of the entity to update.
     * @param  array  $data  The data to update the entity.
     */
    public function update(int $id, array $data): ResponseInterface;

    /**
     * Delete a entity by ID.
     *
     * @param  int  $id  The ID of the entity to delete.
     * @return ResponseInterface The deleted entity.
     *
     * @throws NoSuchEntityException When the entity with the specified ID is not found.
     * @throws Exception When an error occurs during the deletion process.
     */
    public function delete(int $id): ResponseInterface;

    /**
     * Restore a soft-deleted entity by ID.
     *
     * @param  int  $id  The ID of the entity to restore.
     *
     * @throws Exception
     */
    public function restore(int $id): ResponseInterface;
}
