<?php

declare(strict_types=1);

namespace Modules\Customer\Seeder;

use App\Util\PHPUtil;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Modules\Customer\Interface\Data\CustomerInterface;
use Modules\Customer\Model\Customer;

/**
 * Class CustomerSeeder
 * Seeder class for generating customer data
 */
class CustomerSeeder extends Seeder
{
    /**
     * Default size for seeders.
     */
    private $defaultSeederSize;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->defaultSeederSize = Config::get('database.seeders.defaultSeederSize');
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        try {
            // Generate 50 customer instances
            $customers = Customer::factory()->count($this->defaultSeederSize)->make();

            // Log the start of the seeding to the console
            PHPUtil::consoleOutput('🚀 Seeding customers...');

            // Log details about each customer as it is being seeded
            foreach ($customers as $customer) {
                // Insert each customer's data into the database
                DB::table(CustomerInterface::TABLE)->insert($customer->toArray());

                // Log details about the customer
                PHPUtil::consoleOutput("🌱 Seeded customer: {$customer->getName()}");
            }

            // Log the end of the seeding process to the console
            PHPUtil::consoleOutput('✅ Seeding completed.');
        } catch (QueryException $e) {
            // Handle the exception
            PHPUtil::consoleOutput("❌ Error seeding customers: {$e->getMessage()}");
        }
    }
}
