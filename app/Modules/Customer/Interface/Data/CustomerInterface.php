<?php

declare(strict_types=1);

namespace Modules\Customer\Interface\Data;

/**
 * Interface CustomerInterface
 *
 * This interface defines constants related to customer attributes.
 */
interface CustomerInterface
{
    /**
     * for the customers table.
     */
    public const TABLE = 'customers';

    /**
     * The name of the entity related to this table.
     */
    public const ENTITY_NAME = 'customer';

    /**
     * for the customers index.
     */
    public const INDEX_NAME = 'customers_index';

    /**
     * The primary key for the customers table.
     */
    public const PRIMARY_KEY = 'id';

    /**
     * The data type of the primary key for the customers table.
     */
    public const KEY_TYPE = 'int';

    /**
     * The data type of the event object for the customers event.
     */
    public const EVENT_OBJECT = 'payload';

    /**
     * The data type of the cache tag for the customers event.
     */
    public const CACHE_TAG = 'CUSTOMERS';

    /**
     * Constant for the ID attribute.
     */
    public const ID = self::PRIMARY_KEY;

    /**
     * Constant for the UUID attribute.
     */
    public const UUID = 'uuid';

    /**
     * Constant for the name attribute.
     */
    public const NAME = 'name';

    /**
     * The name of the status column.
     */
    public const STATUS = 'status';

    /**
     * The name of the email column.
     */
    public const EMAIL = 'email';

    /**
     * The name of the email_verified_at column.
     */
    public const EMAIL_VERIFIED_AT = 'email_verified_at';

    /**
     * The name of the phone column.
     */
    public const PHONE = 'phone';

    /**
     * The name of the password column.
     */
    public const PASSWORD = 'password';

    /**
     * The name of the remember_token column.
     */
    public const REMEMBER_TOKEN = 'remember_token';

    /**
     * The name of the deleted_at column.
     */
    public const DELETED_AT = 'deleted_at';

    /**
     * Constant for the metadata attribute.
     */
    public const METADATA = 'metadata';

    /**
     * Constant for the roles attribute.
     */
    public const ROLES = 'roles';

    /**
     * Get the type of the model.
     *
     * @return string Model type.
     */
    public function getType(): string;

    /**
     * Load object data by key and field.
     *
     * @param  mixed|null  $value
     * @param  string|null  $field
     * @return $this|null
     */
    public function loadBy($value, $field = null): self;

    /**
     * Get ID.
     */
    public function getId(): int;

    /**
     * Set ID.
     *
     * @return $this
     */
    public function setId(int $id): self;

    /**
     * Get UUID.
     */
    public function getUuid(): string;

    /**
     * Set UUID.
     *
     * @return $this
     */
    public function setUuid(string $uuid): self;

    /**
     * Get the customer's name.
     */
    public function getName(): string;

    /**
     * Set the customer's name.
     *
     * @return $this
     */
    public function setName(string $name): self;

    /**
     * Get the value of the email column.
     */
    public function getEmail(): string;

    /**
     * Set the value of the email column.
     *
     * @return $this
     */
    public function setEmail(string $email): self;

    /**
     * Get the value of the phone column.
     */
    public function getPhone(): string;

    /**
     * Set the value of the phone column.
     *
     * @return $this
     */
    public function setPhone(string $phone): self;

    /**
     * Get the value of the email_verified_at column.
     */
    public function getEmailVerifiedAt(): string;

    /**
     * Set the value of the email_verified_at column.
     *
     * @return $this
     */
    public function setEmailVerifiedAt(string $emailVerifiedAt): self;

    /**
     * Get creation time.
     */
    public function getCreatedAt(): string;

    /**
     * Set creation time.
     *
     * @return $this
     */
    public function setCreatedAt(string $creationTime): self;

    /**
     * Get update time.
     */
    public function getUpdatedAt(): string;

    /**
     * Set update time.
     *
     * @return $this
     */
    public function setUpdatedAt(string $updateTime): self;

    /**
     * Get deletion time.
     */
    public function getDeletedAt(): ?string;

    /**
     * Set deletion time.
     *
     * @param  string|null  $deletionTime
     * @return $this
     */
    public function setDeletedAt($deletionTime): self;

    /**
     * Retrieves custom fields data for the entity.
     *
     * @return array|null Returns the custom fields data as an array if it's valid JSON or already an array, otherwise returns null.
     */
    public function getMetadata(): ?array;

    /**
     * Sets custom fields data for the entity.
     *
     * @param  string  $metadata  String or associative array of custom fields.
     * @return $this
     */
    public function setMetadata(string $metadata): self;

    /**
     * Get the model's attributes as an array with keys.
     *
     * @return array Returns an array with keys.
     */
    public function toArray(): array;

    /**
     * Get the model's attributes as an array with keys in camelCase format.
     *
     * @return array Returns an array with keys in camelCase format.
     */
    public function toDataArray(): array;

    /**
     * Update or initialize the model with data from an array.
     *
     * @param  array  $data  The array containing data to update or initialize the model.
     * @return $this Returns the updated or initialized model instance.
     */
    public function fromArray(array $data): self;
}
