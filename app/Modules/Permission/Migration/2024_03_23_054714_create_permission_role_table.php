<?php

declare(strict_types=1);

namespace Modules\Team\Migration;

use App\Util\PHPUtil;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Permission\Interface\Data\PermissionInterface;
use Modules\Role\Interface\Data\RoleInterface;

/**
 * Class CreatePermissionRoleTableMigration
 *
 * This migration creates the permission_role pivot table, which represents the many-to-many relationship
 * between teams and tenants.
 */
return new class() extends Migration
{
    /**
     * The name of the pivot table.
     */
    public const TABLE = 'permission_role';

    /**
     * The name of the column storing the role ID.
     */
    public const ROLE_ID = 'role_id';

    /**
     * The name of the column storing the permission ID.
     */
    public const PERMISSION_ID = 'permission_id';

    /**
     * The name of the "created at" column.
     *
     * @var string|null
     */
    const CREATED_AT = 'created_at';

    /**
     * The name of the "updated at" column.
     *
     * @var string|null
     */
    const UPDATED_AT = 'updated_at';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Creating "' . self::TABLE . '" table...');

        // Create the permission_role pivot table
        Schema::create(self::TABLE, function (Blueprint $table) {
            $table->id();

            // The ID of the tenant
            $table->unsignedBigInteger(static::PERMISSION_ID)->comment('The ID of the ' . PermissionInterface::ENTITY_NAME);

            // The ID of the team
            $table->unsignedBigInteger(static::ROLE_ID)->comment('The ID of the ' . RoleInterface::ENTITY_NAME);

            // Creation timestamp (nullable)
            $table->timestamp(static::CREATED_AT)->useCurrent()->comment('The time the relationship was created');

            // Last update timestamp (nullable)
            $table->timestamp(static::UPDATED_AT)->nullable()->default(null)->comment('The time the relationship was last updated');

            // Define foreign key constraints
            $table->foreign(static::PERMISSION_ID)->references(PermissionInterface::PRIMARY_KEY)->on(PermissionInterface::TABLE)->onDelete('cascade');
            $table->foreign(static::ROLE_ID)->references(RoleInterface::PRIMARY_KEY)->on(RoleInterface::TABLE)->onDelete('cascade');
        });

        // Add a comment to the 'team' table
        Schema::table(self::TABLE, function (Blueprint $table) {
            $table->comment(static::TABLE . ' table');
        });

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ "' . self::TABLE . '" table created successfully.');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Log the start of the rollback
        PHPUtil::consoleOutput('🗑️ Dropping "' . self::TABLE . '" table...');

        // Drop the tenants table if it exists
        Schema::dropIfExists(self::TABLE);

        // Log the completion of the rollback
        PHPUtil::consoleOutput('✅ "' . self::TABLE . '" table dropped successfully.');
    }
};
