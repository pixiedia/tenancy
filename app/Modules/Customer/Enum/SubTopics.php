<?php

declare(strict_types=1);

namespace Modules\Customer\Enum;

use BenSampo\Enum\Enum;

/**
 * Enumeration of (Pub/Sub) topics related to DNS subscription events.
 *
 * @method static static LIST()
 * @method static static GET()
 * @method static static CREATE()
 * @method static static UPDATE()
 * @method static static DELETE()
 * @method static static RECOVERED()
 */
final class SubTopics extends Enum
{
    /**
     * Topic for getting a list of DNSs.
     *
     * @example 'customer.list'
     */
    const LIST = 'customer.list';

    /**
     * Topic for getting information about a single DNS.
     *
     * @example 'customer.get'
     */
    const GET = 'customer.get';

    /**
     * Topic for creating a new DNS.
     *
     * @example 'customer.create'
     */
    const CREATE = 'customer.create';

    /**
     * Topic for updating an existing DNS.
     *
     * @example 'customer.update'
     */
    const UPDATE = 'customer.update';

    /**
     * Topic for deleting a DNS.
     *
     * @example 'customer.delete'
     */
    const DELETE = 'customer.delete';

    /**
     * Topic for recovery events.
     *
     * @example 'customer.recovered'
     */
    const RECOVERED = 'customer.recover';
}
