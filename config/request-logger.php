<?php

declare(strict_types=1);

return [
    /*
    |--------------------------------------------------------------------------
    | HTTP Request Logger Configuration
    |--------------------------------------------------------------------------
    |
    | This configuration file allows you to enable/disable the HTTP request logger
    | and configure its behavior.
    |
    */

    'logger' => [
        'enabled' => true, // Whether the logger is enabled
        'handlers' => ['Prettus\RequestLogger\Handler\HttpLoggerHandler'], // Handlers for the logger
        'file' => storage_path('logs/http.log'), // File path for logging
        'level' => 'info', // Log level
        'format' => 'combined', // Log format
    ],
];
