<?php

declare(strict_types=1);

namespace App\Enums;

/**
 * Enum representing cache time-to-live values in seconds.
 */
enum CacheTTL: int
{
    /**
     * The value for one second.
     */
    case SECOND = 1;

    /**
     * The value for one minute (60 seconds).
     */
    case MINUTE = 60;

    /**
     * The value for one hour (3600 seconds).
     */
    case HOUR = 3600;

    /**
     * The value for one day (86400 seconds).
     */
    case DAY = 86400;

    /**
     * The value for one week (604800 seconds, 7 days).
     */
    case WEEK = 604800;

    /**
     * The value for one month (2592000 seconds, 30 days).
     */
    case MONTH = 2592000;

    /**
     * The value for one year (31536000 seconds, 365 days).
     */
    case YEAR = 31536000;

    /**
     * Get the value for one second.
     */
    public static function second(): int
    {
        return self::SECOND->value;
    }

    /**
     * Get the value for the specified number of seconds.
     *
     * @param  int  $seconds  The number of seconds.
     */
    public static function seconds(int $seconds): int
    {
        return $seconds * self::SECOND->value;
    }

    /**
     * Get the value for one minute.
     */
    public static function minute(): int
    {
        return self::MINUTE->value;
    }

    /**
     * Get the value for the specified number of minutes.
     *
     * @param  int  $minutes  The number of minutes.
     */
    public static function minutes(int $minutes): int
    {
        return $minutes * self::MINUTE->value;
    }

    /**
     * Get the value for one hour.
     */
    public static function hour(): int
    {
        return self::HOUR->value;
    }

    /**
     * Get the value for the specified number of hours.
     *
     * @param  int  $hours  The number of hours.
     */
    public static function hours(int $hours): int
    {
        return $hours * self::HOUR->value;
    }

    /**
     * Get the value for one day.
     */
    public static function day(): int
    {
        return self::DAY->value;
    }

    /**
     * Get the value for the specified number of days.
     *
     * @param  int  $days  The number of days.
     */
    public static function days(int $days): int
    {
        return $days * self::DAY->value;
    }

    /**
     * Get the value for one week (7 days).
     */
    public static function week(): int
    {
        return self::WEEK->value;
    }

    /**
     * Get the value for the specified number of weeks.
     *
     * @param  int  $weeks  The number of weeks.
     */
    public static function weeks(int $weeks): int
    {
        return $weeks * self::WEEK->value;
    }

    /**
     * Get the value for one month (30 days).
     */
    public static function month(): int
    {
        return self::MONTH->value;
    }

    /**
     * Get the value for the specified number of months.
     *
     * @param  int  $months  The number of months.
     */
    public static function months(int $months): int
    {
        return $months * self::MONTH->value;
    }

    /**
     * Get the value for one year (365 days).
     */
    public static function year(): int
    {
        return self::YEAR->value;
    }

    /**
     * Get the value for the specified number of years.
     *
     * @param  int  $years  The number of years.
     */
    public static function years(int $years): int
    {
        return $years * self::YEAR->value;
    }
}
