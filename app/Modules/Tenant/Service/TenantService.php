<?php

declare(strict_types=1);

namespace Modules\Tenant\Service;

use App\Abstractions\AbstractService;
use App\Exceptions\NoSuchEntityException;
use Common\Http\Enum\HttpStatusCode;
use Common\Response\Interface\Data\ResponseInterface;
use Exception;
use Illuminate\Support\Facades\Log;
use Modules\Team\Interface\Data\TeamTenantInterface;
use Modules\Team\Model\Team;
use Modules\Tenant\Interface\Data\TenantInterface;
use Modules\Tenant\Interface\TenantRepositoryInterface;
use Modules\Tenant\Interface\TenantServiceInterface;
use Modules\Tenant\Repository\TenantRepository;
use Util\Phraser\Phrase;

/**
 * Class TenantService
 */
class TenantService extends AbstractService implements TenantServiceInterface
{
    /**
     * AbstractRepository constructor.
     *
     * @param  TenantRepository  $repository  The repository instance to use in the service.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(TenantRepositoryInterface $repository)
    {
        parent::__construct($repository, TenantInterface::class);
    }

    /**
     * Create a new entity.
     *
     * @param  array  $data  The data to create the entity.
     * @return Model
     *
     * @throws Exception
     */
    public function create(array $data): ResponseInterface
    {
        try {
            // Fetch entities using the repository
            $entity = $this->repository->create($data);

            // If the entity is not found, throw a NoSuchEntityException
            if (! $entity || ! $entity->getId()) {
                throw new NoSuchEntityException(Phrase::__('No %1 found after creating.', $this->modelInterface::ENTITY_NAME));
            }

            // Attach teams to the tenant if team_id is provided in the data
            if (isset($data[TeamTenantInterface::TEAM_ID])) {
                $team = Team::find($data[TeamTenantInterface::TEAM_ID]);

                if ($team) {
                    $entity->teams()->attach($team->getId());
                }
            }

            // Creating a success response
            $response = $this->createSuccess($entity->toArray(), HttpStatusCode::OK, Phrase::__('Successfully created a %1.', $this->modelInterface::ENTITY_NAME));

            return $response;
        } catch (NoSuchEntityException $e) {
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while creating a %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::NOT_FOUND, $e);

            return $response;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while creating a %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        }
    }

    /**
     * Assign a team to a tenant.
     *
     * @param  int  $tenantId  The ID of the tenant.
     * @param  int  $teamId  The ID of the team to assign.
     * @return Model
     *
     * @throws Exception
     */
    public function assignTeam(int $tenantId, int $teamId): ResponseInterface
    {
        try {
            // Fetch entities using the repository
            $entity = $this->repository->find($tenantId);

            // If the entity is not found, throw a NoSuchEntityException
            if (! $entity || ! $entity->getId()) {
                throw new NoSuchEntityException(Phrase::__('No %1 found after creating.', $this->modelInterface::ENTITY_NAME));
            }

            // Attach teams to the tenant if team_id is provided in the data
            if ($teamId) {
                $team = Team::find($teamId);

                if ($team) {
                    $entity->teams()->attach($team->getId());
                }
            }

            // Creating a success response
            $response = $this->createSuccess($entity->toArray(), HttpStatusCode::OK, Phrase::__('Successfully created a %1.', $this->modelInterface::ENTITY_NAME));

            return $response;
        } catch (NoSuchEntityException $e) {
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while creating a %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::NOT_FOUND, $e);

            return $response;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while creating a %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        }
    }

    /**
     * Detach a team to a tenant.
     *
     * @param  int  $tenantId  The ID of the tenant.
     * @param  int  $teamId  The ID of the team to assign.
     * @return Model
     *
     * @throws Exception
     */
    public function detachTeam(int $tenantId, int $teamId): ResponseInterface
    {
        try {
            // Fetch entities using the repository
            $entity = $this->repository->find($tenantId);

            // If the entity is not found, throw a NoSuchEntityException
            if (! $entity || ! $entity->getId()) {
                throw new NoSuchEntityException(Phrase::__('No %1 found after creating.', $this->modelInterface::ENTITY_NAME));
            }

            // Detach teams to the tenant if team_id is provided in the data
            if ($teamId) {
                $team = Team::find($teamId);

                if ($team) {
                    $entity->teams()->detach($team->getId());
                }
            }

            // Creating a success response
            $response = $this->createSuccess($entity->toArray(), HttpStatusCode::OK, Phrase::__('Successfully created a %1.', $this->modelInterface::ENTITY_NAME));

            return $response;
        } catch (NoSuchEntityException $e) {
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while creating a %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::NOT_FOUND, $e);

            return $response;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while creating a %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        }
    }
}
