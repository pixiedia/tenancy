<?php

declare(strict_types=1);

namespace Modules\Tenant\Interface\Data;

/**
 * Interface TenantInterface
 *
 * This interface defines constants related to tenant attributes.
 */
interface TenantInterface
{
    /**
     * for the tenants table.
     */
    public const TABLE = 'tenants';

    /**
     * The name of the entity related to this table.
     */
    public const ENTITY_NAME = 'tenant';

    /**
     * for the tenants index.
     */
    public const INDEX_NAME = 'tenants_index';

    /**
     * The primary key for the tenants table.
     */
    public const PRIMARY_KEY = 'id';

    /**
     * The data type of the primary key for the tenants table.
     */
    public const KEY_TYPE = 'int';

    /**
     * The data type of the event object for the tenants event.
     */
    public const EVENT_OBJECT = 'payload';

    /**
     * The data type of the cache tag for the tenants event.
     */
    public const CACHE_TAG = 'TEAMS';

    /**
     * Constant for the ID attribute.
     */
    public const ID = self::PRIMARY_KEY;

    /**
     * Constant for the UUID attribute.
     */
    public const UUID = 'uuid';

    /**
     * Constant for the name attribute.
     */
    public const NAME = 'name';

    /**
     * Constant for the slug attribute.
     */
    public const SLUG = 'slug';

    /**
     * The name of the status column.
     */
    public const STATUS = 'status';

    /**
     * The name of the domain column.
     */
    public const DOMAIN = 'domain';

    /**
     * The name of the teams column.
     */
    public const TEAMS = 'teams';

    /**
     * The name of the deleted_at column.
     */
    public const DELETED_AT = 'deleted_at';

    /**
     * The name of the created_by column.
     */
    public const CREATED_BY = 'created_by';

    /**
     * The name of the updated_by column.
     */
    public const UPDATED_BY = 'updated_by';

    /**
     * The name of the deleted_by column.
     */
    public const DELETED_BY = 'deleted_by';

    /**
     * Constant for the metadata attribute.
     */
    public const METADATA = 'metadata';

    /**
     * Get the type of the model.
     *
     * @return string Model type.
     */
    public function getType(): string;

    /**
     * Load object data by key and field.
     *
     * @param  mixed|null  $value
     * @param  string|null  $field
     * @return $this|null
     */
    public function loadBy($value, $field = null): self;

    /**
     * Get ID.
     */
    public function getId(): int;

    /**
     * Set ID.
     *
     * @param  int  $id
     * @return $this
     */
    public function setId($id): self;

    /**
     * Get UUID.
     */
    public function getUuid(): string;

    /**
     * Set UUID.
     *
     * @param  string  $uuid
     * @return $this
     */
    public function setUuid($uuid): self;

    /**
     * Get creation time.
     */
    public function getCreatedAt(): string;

    /**
     * Set creation time.
     *
     * @param  string  $creationTime
     * @return $this
     */
    public function setCreatedAt($creationTime): self;

    /**
     * Get update time.
     */
    public function getUpdatedAt(): string;

    /**
     * Set update time.
     *
     * @param  string  $updateTime
     * @return $this
     */
    public function setUpdatedAt($updateTime): self;

    /**
     * Get deletion time.
     */
    public function getDeletedAt(): ?string;

    /**
     * Set deletion time.
     *
     * @param  string|null  $deletionTime
     * @return $this
     */
    public function setDeletedAt($deletionTime): self;

    /**
     * Get the tenant's name.
     */
    public function getName(): string;

    /**
     * Set the tenant's name.
     *
     * @return $this
     */
    public function setName(string $name): self;

    /**
     * Get the value of the status column.
     */
    public function getStatus(): string;

    /**
     * Set the value of the status column.
     *
     * @return $this
     */
    public function setStatus(string $status): self;

    /**
     * Get the value of the domain column.
     */
    public function getDomain(): string;

    /**
     * Set the value of the domain column.
     *
     * @return $this
     */
    public function setDomain(string $domain): self;

    /**
     * Get the value of the teams column.
     */
    public function getTeams(): array;

    /**
     * Retrieves custom fields data for the entity.
     *
     * @return array|null Returns the custom fields data as an array if it's valid JSON or already an array, otherwise returns null.
     */
    public function getMetadata(): ?array;

    /**
     * Sets custom fields data for the entity.
     *
     * @param  string  $metadata  String or associative array of custom fields.
     * @return $this
     */
    public function setMetadata(string $metadata): self;

    /**
     * Get the model's attributes as an array with keys.
     *
     * @return array Returns an array with keys.
     */
    public function toArray(): array;

    /**
     * Get the model's attributes as an array with keys in camelCase format.
     *
     * @return array Returns an array with keys in camelCase format.
     */
    public function toDataArray(): array;

    /**
     * Update or initialize the model with data from an array.
     *
     * @param  array  $data  The array containing data to update or initialize the model.
     * @return $this Returns the updated or initialized model instance.
     */
    public function fromArray(array $data): self;
}
