<?php

declare(strict_types=1);

namespace Modules\Team\Migration;

use App\Util\PHPUtil;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Team\Interface\Data\TeamInterface;
use Modules\Team\Interface\Data\TeamTenantInterface;
use Modules\Team\Model\TeamTenant;
use Modules\Tenant\Interface\Data\TenantInterface;

/**
 * Class CreateTeamTenantTableMigration
 *
 * This migration creates the team_tenant pivot table, which represents the many-to-many relationship
 * between teams and tenants.
 */
return new class() extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Creating "' . TeamTenantInterface::TABLE . '" table...');

        // Create the team_tenant pivot table
        Schema::create(TeamTenantInterface::TABLE, function (Blueprint $table) {
            $table->id();

            // The ID of the team
            $table->unsignedBigInteger(TeamTenantInterface::TEAM_ID)->comment('The ID of the team');

            // The ID of the tenant
            $table->unsignedBigInteger(TeamTenantInterface::TENANT_ID)->comment('The ID of the tenant');

            // Creation timestamp (nullable)
            $table->timestamp(TeamTenant::CREATED_AT)->useCurrent()->comment('The time the relationship was created');

            // Last update timestamp (nullable)
            $table->timestamp(TeamTenant::UPDATED_AT)->nullable()->default(null)->comment('The time the relationship was last updated');

            // Define foreign key constraints
            $table->foreign(TeamTenantInterface::TEAM_ID)->references(TeamInterface::PRIMARY_KEY)->on(TeamInterface::TABLE)->onDelete('cascade');
            $table->foreign(TeamTenantInterface::TENANT_ID)->references(TenantInterface::PRIMARY_KEY)->on(TenantInterface::TABLE)->onDelete('cascade');
        });

        // Add a comment to the 'team' table
        Schema::table(TeamTenantInterface::TABLE, function (Blueprint $table) {
            $table->comment(TeamTenantInterface::TABLE . ' table');
        });

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ "' . TeamTenantInterface::TABLE . '" table created successfully.');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Log the start of the rollback
        PHPUtil::consoleOutput('🗑️ Dropping "' . TeamTenantInterface::TABLE . '" table...');

        // Drop the tenants table if it exists
        Schema::dropIfExists(TeamTenantInterface::TABLE);

        // Log the completion of the rollback
        PHPUtil::consoleOutput('✅ "' . TeamTenantInterface::TABLE . '" table dropped successfully.');
    }
};
