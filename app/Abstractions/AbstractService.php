<?php

declare(strict_types=1);

namespace App\Abstractions;

use App\Abstractions\Interface\AbstractServiceInterface;
use App\Exceptions\NoSuchEntityException;
use Common\Http\Enum\HttpStatusCode;
use Common\Request\Interface\Data\RequestInterface;
use Common\Response\Interface\Data\ResponseInterface;
use Common\Response\Trait\ResponseBuilder;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Modules\Model\Model\Model;
use Ramsey\Uuid\Uuid;
use Util\Phraser\Phrase;

/**
 * Class AbstractService
 */
abstract class AbstractService implements AbstractServiceInterface
{
    use ResponseBuilder;

    /**
     * The underlying repository implementation.
     *
     * @var AbstractRepository
     */
    protected $repository;

    /**
     * The model interface instance.
     *
     * @var mixed
     */
    protected $modelInterface;

    /**
     * AbstractRepository constructor.
     *
     * @param  AbstractRepository  $repository  The repository instance to use in the service.
     * @param  mixed  $modelInterface  The model interface instance to use in the repository.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(AbstractRepository $repository, $modelInterface)
    {
        $this->repository = $repository;

        // Check if $modelInterface is null, and throw an exception if it is
        if ($modelInterface === null) {
            throw new Exception('ModelInterface cannot be null.');
        }

        // Assign the $modelInterface to the class property
        $this->modelInterface = $modelInterface;
    }

    /**
     * Get all entities based on pagination criteria.
     *
     * @param  int  $pageNumber  The page number.
     * @param  int  $pageSize  The number of items per page.
     *
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function list(int $pageNumber = RequestInterface::DEFAULT_PAGE_NUMBER, int $pageSize = RequestInterface::DEFAULT_PAGE_SIZE): ResponseInterface
    {
        try {
            // Validate page number
            if ($pageNumber < RequestInterface::MIN_PAGE_NUMBER) {
                throw new InvalidArgumentException('Page number must be greater than or equal to ' . RequestInterface::MIN_PAGE_NUMBER);
            }

            // Validate page size
            if ($pageSize < 1) {
                throw new InvalidArgumentException('Page size must be greater than 0');
            }

            // Set your maximum limit
            $maxLimit = RequestInterface::MAX_PAGE_SIZE;

            if ($pageSize > $maxLimit) {
                throw new InvalidArgumentException('Page size must not exceed ' . $maxLimit);
            }

            // Prepare criteria for fetching entities
            // TODO: CREATE "SEARCHCRITERIA" CLASS SAME AS "MAGENTO"
            $criteria = [
                RequestInterface::SIZE_KEY => $pageSize,
                RequestInterface::PAGE_KEY => $pageNumber,
            ];

            // Fetch entities using the repository
            $entities = $this->repository->list($criteria);

            // Get total count for pagination
            $totalCount = $entities->count();

            // Iterate through each entity and map its data
            /** @var Model $entity */
            foreach ($entities as $entity) {
                $entitiesData[] = $entity->toArray();
            }

            // Creating a success response
            $response = $this->createSuccess($entitiesData, HttpStatusCode::OK, Phrase::__('Successfully retrieved %1 list', Str::plural($this->modelInterface::ENTITY_NAME)), [$pageNumber, $pageSize, $totalCount]);

            return $response;
        } catch (InvalidArgumentException $e) {
            // Log and re-throw the exception
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while retrieving the %1 list.', Str::plural($this->modelInterface::ENTITY_NAME)), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        } catch (Exception $e) {
            // Log any other exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while retrieving the %1 list.', Str::plural($this->modelInterface::ENTITY_NAME)), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        }
    }

    /**
     * Search for entities based on a search term, with optional filters, sorting, and pagination.
     *
     * @param  string  $searchTerm  The term to search for.
     * @param  int  $pageNumber  The page number.
     * @param  int  $pageSize  The number of items per page.
     * @param  array  $filters  Additional filters for the search.
     * @param  string  $sort  The sorting order ('ASC' or 'DESC').
     *
     * @throws Exception
     */
    public function search(string $searchTerm, int $pageNumber = RequestInterface::DEFAULT_PAGE_NUMBER, int $pageSize = RequestInterface::DEFAULT_PAGE_SIZE, array $filters = [], string $sort = RequestInterface::DEFAULT_SORT_ORDER): ResponseInterface
    {
        try {
            // TODO: CREATE "SEARCHCRITERIA" CLASS SAME AS "MAGENTO"
            // Prepare criteria for search
            $criteria = [
                'searchTerm' => $searchTerm,
                RequestInterface::SORT_KEY => $sort,
                RequestInterface::FILTERS_KEY => $filters,
                RequestInterface::SIZE_KEY => $pageSize,
                RequestInterface::PAGE_KEY => $pageNumber,
            ];

            // Perform search using the repository
            $entities = ${$this}->repository->search($criteria);

            // Get total count for pagination
            $totalCount = $entities->count();

            // Iterate through each entity and map its data
            /** @var Model $entity */
            foreach ($entities as $entity) {
                $entitiesData[] = $entity->toArray();
            }

            // Creating a success response
            $response = $this->createSuccess($entitiesData, HttpStatusCode::OK, Phrase::__('Successfully retrieved %1 list', Str::plural($this->modelInterface::ENTITY_NAME)), [$pageNumber, $pageSize, $totalCount]);

            return $response;
        } catch (Exception $e) {
            // Log and re-throw the exception
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while searching the %1 list.', Str::plural($this->modelInterface::ENTITY_NAME)), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        }
    }

    /**
     * Find a entity by ID.
     *
     * @param  int  $id  The ID of the entity to find.
     * @return Model|null
     *
     * @throws Exception
     */
    public function find(int $id): ResponseInterface
    {
        try {
            // Validate ID
            if ($id <= 0) {
                throw new InvalidArgumentException('ID must be greater than 0');
            }

            // Fetch entities using the repository
            $entity = $this->repository->find($id);

            // If the entity is not found, throw a NoSuchEntityException
            if (! $entity || ! $entity->getId()) {
                throw new NoSuchEntityException(Phrase::__('No %1 found with the given ID.', Str::plural($this->modelInterface::ENTITY_NAME)));
            }

            // Creating a success response
            $response = $this->createSuccess($entity->toArray(), HttpStatusCode::OK, Phrase::__('Successfully found %1 by criteria', Str::plural($this->modelInterface::ENTITY_NAME)));

            return $response;
        } catch (NoSuchEntityException $e) {
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while finding the %1 by criteria.', Str::plural($this->modelInterface::ENTITY_NAME)), HttpStatusCode::NOT_FOUND, $e);

            return $response;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while finding the %1 by criteria.', Str::plural($this->modelInterface::ENTITY_NAME)), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        }
    }

    /**
     * Find a entity by UUID.
     *
     * @param  string  $uuid  The UUID of the entity to find.
     * @return Model|null
     *
     * @throws InvalidArgumentException When the UUID is not a valid UUID.
     * @throws NoSuchEntityException When the entity with the specified UUID is not found.
     * @throws Exception
     */
    public function findByUuid(string $uuid): ResponseInterface
    {
        try {
            // Validate UUID
            if (! Uuid::isValid($uuid)) {
                throw new InvalidArgumentException('Invalid UUID format');
            }

            // Fetch entities using the repository
            $entity = $this->repository->findByUuid($uuid);

            // If the entity is not found, throw a NoSuchEntityException
            if (! $entity || ! $entity->getId()) {
                throw new NoSuchEntityException(Phrase::__('No %1 found with the given UUID.', $this->modelInterface::ENTITY_NAME));
            }

            // Creating a success response
            $response = $this->createSuccess($entity->toArray(), HttpStatusCode::OK, Phrase::__('Successfully found %1 by uuid', $this->modelInterface::ENTITY_NAME));

            return $response;
        } catch (NoSuchEntityException $e) {
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while finding the %1 by uuid.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::NOT_FOUND, $e);

            return $response;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while finding the %1 by uuid.', Str::plural($this->modelInterface::ENTITY_NAME)), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        }
    }

    /**
     * Find a entity by Identifier.
     *
     * @param  string  $identifier  The identifier of the entity to find.
     * @param  string  $key  The column to search for the identifier.
     * @return Model|null
     *
     * @throws Exception
     */
    public function findBy(string $identifier, ?string $key = null): ResponseInterface
    {
        try {
            // Validate identifier value
            if (empty($identifier)) {
                throw new InvalidArgumentException('Identifier cannot be null or empty');
            }

            // Validate identifier type
            if (! is_string($identifier)) {
                throw new InvalidArgumentException('Identifier must be a string');
            }

            // Fetch entities using the repository
            $entity = $this->repository->findBy($key, $identifier);

            // If the entity is not found, throw a NoSuchEntityException
            if (! $entity || ! $entity->getId()) {
                throw new NoSuchEntityException(Phrase::__('No %1 found with the given identifier.', $this->modelInterface::ENTITY_NAME));
            }

            // Creating a success response
            $response = $this->createSuccess($entity->toArray(), HttpStatusCode::OK, Phrase::__('Successfully found %1 by identifier', $this->modelInterface::ENTITY_NAME));

            return $response;
        } catch (NoSuchEntityException $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while finding the %1 by identifier.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::NOT_FOUND, $e);

            return $response;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while finding the %1 by identifier.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        }
    }

    /**
     * Create a new entity.
     *
     * @param  array  $data  The data to create the entity.
     * @return Model
     *
     * @throws Exception
     */
    public function create(array $data): ResponseInterface
    {
        try {
            // Fetch entities using the repository
            $entity = $this->repository->create($data);

            // If the entity is not found, throw a NoSuchEntityException
            if (! $entity || ! $entity->getId()) {
                throw new NoSuchEntityException(Phrase::__('No %1 found after creating.', $this->modelInterface::ENTITY_NAME));
            }

            // Creating a success response
            $response = $this->createSuccess($entity->toArray(), HttpStatusCode::OK, Phrase::__('Successfully created a %1.', $this->modelInterface::ENTITY_NAME));

            return $response;
        } catch (NoSuchEntityException $e) {
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while creating a %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::NOT_FOUND, $e);

            return $response;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while creating a %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        }
    }

    /**
     * Create multiple new entities.
     *
     * @param  array  $dataList  List of data to create entities.
     *
     * @throws Exception
     */
    public function bulkCreate(array $dataList): ResponseInterface
    {
        try {
            // Fetch entities using the repository
            $entity = $this->repository->bulkCreate($dataList);

            // Creating a success response
            $response = $this->createSuccess($entity->toArray(), HttpStatusCode::OK, Phrase::__('Successfully bulk created %1', Str::plural($this->modelInterface::ENTITY_NAME)));

            return $response;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while bulk creating the %1.', Str::plural($this->modelInterface::ENTITY_NAME)), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        }
    }

    /**
     * Update a entity by ID.
     *
     * @param  int  $id  The ID of the entity to update.
     * @param  array  $data  The data to update the entity.
     * @return Model The updated entity.
     *
     * @throws NoSuchEntityException When the entity with the specified ID is not found.
     * @throws Exception When an error occurs during the update process.
     */
    public function update(int $id, array $data): ResponseInterface
    {
        try {
            // Validate ID
            if ($id <= 0) {
                throw new InvalidArgumentException('ID must be greater than 0');
            }

            // Find the entity by ID
            $existingModel = $this->repository->find($id);

            // If the entity is not found, throw a NoSuchEntityException
            if (! $existingModel->getId()) {
                throw new NoSuchEntityException(Phrase::__('No %1 found while updating.', $this->modelInterface::ENTITY_NAME));
            }

            // Update the entity
            $this->repository->update($id, $data);

            // Get the updated entity
            $entity = $this->repository->find($id);

            // Creating a success response
            $response = $this->createSuccess($entity->toArray(), HttpStatusCode::OK, Phrase::__('Successfully updated %1', $this->modelInterface::ENTITY_NAME));

            return $response;
        } catch (NoSuchEntityException $e) {
            // Log the exception
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while updating the %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::NOT_FOUND, $e);

            return $response;
        } catch (Exception $e) {
            // Log any other exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while updating the %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        }
    }

    /**
     * Delete a entity by ID.
     *
     * @param  int  $id  The ID of the entity to delete.
     * @return Model The deleted entity.
     *
     * @throws NoSuchEntityException When the entity with the specified ID is not found.
     * @throws Exception When an error occurs during the deletion process.
     */
    public function delete(int $id): ResponseInterface
    {
        try {
            // Validate ID
            if ($id <= 0) {
                throw new InvalidArgumentException('ID must be greater than 0');
            }

            // Find the entity by ID
            $entity = $this->repository->find($id);

            // If the entity is not found, throw a NoSuchEntityException
            if (! $entity || ! $entity->getId()) {
                throw new NoSuchEntityException(Phrase::__('No %1 found while deleting.', $this->modelInterface::ENTITY_NAME));
            }

            // Delete the entity
            $this->repository->delete($id);

            // Creating a success response
            $response = $this->createSuccess($entity->toArray(), HttpStatusCode::OK, Phrase::__('Successfully deleted %1', $this->modelInterface::ENTITY_NAME));

            return $response;
        } catch (NoSuchEntityException $e) {
            // Log the exception
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while deleting the %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::NOT_FOUND, $e);

            return $response;
        } catch (Exception $e) {
            // Log any other exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while deleting the %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        }
    }

    /**
     * Restore a soft-deleted entity by ID.
     *
     * @param  int  $id  The ID of the entity to restore.
     * @return Model The restored entity.
     *
     * @throws NoSuchEntityException When the entity with the specified ID is not found.
     * @throws Exception When an error occurs during the deletion process.
     */
    public function restore(int $id): ResponseInterface
    {
        try {
            // Validate ID
            if ($id <= 0) {
                throw new InvalidArgumentException('ID must be greater than 0');
            }

            // Find the entity by ID
            $entity = $this->repository->find($id);

            // If the entity is not found, throw a NoSuchEntityException
            if (! $entity || ! $entity->getId()) {
                throw new NoSuchEntityException(Phrase::__('No %1 found while restoring.', $this->modelInterface::ENTITY_NAME));
            }

            // Restore the entity
            $this->repository->restore($id);

            // Creating a success response
            $response = $this->createSuccess($entity->toArray(), HttpStatusCode::OK, Phrase::__('Successfully restored %1', $this->modelInterface::ENTITY_NAME));

            return $response;
        } catch (NoSuchEntityException $e) {
            // Log the exception
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while restoring the %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::NOT_FOUND, $e);

            return $response;
        } catch (Exception $e) {
            // Log any other exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Return failure response with error message
            $response = $this->createFailure(Phrase::__('An error occurred while restoring the %1.', $this->modelInterface::ENTITY_NAME), HttpStatusCode::INTERNAL_SERVER_ERROR, $e);

            return $response;
        }
    }
}
