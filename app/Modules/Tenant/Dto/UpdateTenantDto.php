<?php

declare(strict_types=1);

namespace Modules\Tenant\Dto;

use App\Sanitizers\JsonEncode;
use ArondeParon\RequestSanitizer\Traits\SanitizesInputs;
use Common\Http\Enum\HttpStatusCode;
use Common\Response\Trait\ResponseBuilder;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Modules\Team\Interface\Data\TeamTenantInterface;
use Modules\Tenant\Interface\Data\TenantInterface;
use Modules\Tenant\Model\Tenant;
use Util\Phraser\Phrase;

/**
 * @OA\Schema(
 *      schema="UpdateTenantDto",
 *      required={"name", "description"},
 *
 *      @OA\Property(
 *          property="name",
 *          type="string",
 *          example="Tenant A"
 *      ),
 *      @OA\Property(
 *          property="description",
 *          type="string",
 *          example="Description of Tenant A"
 *      ),
 *      @OA\Property(
 *          property="domain",
 *          type="string",
 *          example="tenant@example.com"
 *      ),
 *      @OA\Property(
 *          property="updated_at",
 *          type="string",
 *          format="date-time",
 *          example="2024-03-25 10:00:00"
 *      ),
 *      @OA\Property(
 *          property="deleted_at",
 *          type="string",
 *          format="date-time",
 *          example="2024-03-25 10:00:00"
 *      ),
 *      @OA\Property(
 *          property="updated_by",
 *          type="integer",
 *          example=1
 *      ),
 *      @OA\Property(
 *          property="deleted_by",
 *          type="integer",
 *          example=1
 *      ),
 *      @OA\Property(
 *          property="metadata",
 *          type="array",
 *
 *          @OA\Items(
 *              type="string",
 *              example="value1"
 *          )
 *      )
 * )
 */
class UpdateTenantDto extends FormRequest
{
    use ResponseBuilder, SanitizesInputs;

    /**
     * The array of sanitizers to apply to specific fields.
     *
     * @var array
     */
    protected $sanitizers = [
        TenantInterface::METADATA => [
            JsonEncode::class,
        ],
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Update this based on your authorization logic
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            TenantInterface::NAME => 'required|string',
            TenantInterface::STATUS => 'nullable|string|in:active,inactive,suspended,archived',
            TenantInterface::DOMAIN => 'required|string',
            Tenant::UPDATED_AT => 'nullable|date_format:Y-m-d H:i:s',
            TenantInterface::DELETED_AT => 'nullable|date_format:Y-m-d H:i:s',
            TenantInterface::UPDATED_BY => 'nullable|integer',
            TenantInterface::DELETED_BY => 'nullable|integer',
            TenantInterface::METADATA => ['array'], // Assuming metadata should be an array
            TenantInterface::METADATA . '.*' => 'string', // Assuming each value in metadata should be a string
            TeamTenantInterface::TEAM_ID => 'nullable|integer',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  Validator  $validator  The validator instance containing the validation errors.
     * @return void
     *
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        // Create a failure response with a generic error message
        $response = $this->createFailure(Phrase::__('An error occurred while validating request.'), HttpStatusCode::UNPROCESSABLE_ENTITY);

        // Set the validation errors from the validator instance into the response
        $response->setErrors($validator->errors()->jsonSerialize());

        // Throw a ValidationException with the validator instance and the failure response as the response
        throw new ValidationException($validator, response()->json($response->toArray(), HttpStatusCode::UNPROCESSABLE_ENTITY));
    }
}
