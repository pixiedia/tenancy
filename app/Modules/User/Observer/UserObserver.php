<?php

declare(strict_types=1);

namespace Modules\User\Observer;

use Common\PubSub\Facade\PubSubService;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\User\Enum\PubTopics;
use Modules\User\Mail\UserCreated;
use Modules\User\Model\User;
use Ramsey\Uuid\Uuid;

/**
 * Class UserObserver
 */
class UserObserver
{
    /**
     * Handle the User "creating" event.
     *
     * @param  User  $user  The newly creating user.
     */
    public function creating(User $user): void
    {
        // Log the creation of the user
        Log::info("User creating: {$user->getId()}");

        // Generate a UUID for the user
        $uuid = Uuid::uuid4()->toString();

        // Set the generated UUID for the user
        $user->setUuid($uuid);

        // Flush Cache
        $user->flushCache();
    }

    /**
     * Handle the User "creating" event.
     *
     * @param  User  $user  The newly creating user.
     */
    public function created(User $user): void
    {
        // Log the creation of the user
        Log::info("User created: {$user->getId()}");

        // Dispatch the created event
        Event::dispatch(PubTopics::CREATED, $user->toDataArray());

        // Pubsub the created event
        PubSubService::publish(PubTopics::CREATED, $user->toDataArray());

        // Send an email
        Mail::to('pixiedia@gmail.com')->send(new UserCreated($user));
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  User  $user  The updated user.
     */
    public function updated(User $user): void
    {
        // Log the update of the user
        Log::info("User updated: {$user->getId()}");

        // Set the updated at timestamp
        $user->setUpdatedAt(now());

        // Flush Cache
        $user->flushCache();

        // Dispatch the updated event
        Event::dispatch(PubTopics::UPDATED, $user->toDataArray());

        // Pubsub the updated event
        PubSubService::publish(PubTopics::UPDATED, $user->toDataArray());
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  User  $user  The deleted user.
     */
    public function deleted(User $user): void
    {
        // Log the deletion of the user
        Log::info("User deleted: {$user->getId()}");

        // Set the deleted at timestamp
        $user->setDeletedAt(now());

        // Flush Cache
        $user->flushCache();

        // Dispatch the deleted event
        Event::dispatch(PubTopics::SOFT_DELETED, $user->toDataArray());

        // Pubsub the deleted event
        PubSubService::publish(PubTopics::SOFT_DELETED, $user->toDataArray());
    }

    /**
     * Handle the User "forceDeleted" event.
     *
     * @param  User  $user  The force deleted user.
     */
    public function forceDeleted(User $user): void
    {
        // Log the force deletion of the user
        Log::info("User force deleted: {$user->getId()}");

        // Set the deleted at timestamp
        $user->setDeletedAt(now());

        // Flush Cache
        $user->flushCache();

        // Dispatch the force deleted event
        Event::dispatch(PubTopics::DELETED, $user->toDataArray());

        // Pubsub the deleted event
        PubSubService::publish(PubTopics::DELETED, $user->toDataArray());
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  User  $user  The restored user.
     */
    public function restored(User $user): void
    {
        // Log the restoration of the user
        Log::info("User restored: {$user->getId()}");

        // Reset the deleted at timestamp
        $user->setDeletedAt(null);

        // Flush Cache
        $user->flushCache();

        // Dispatch the restored event
        Event::dispatch(PubTopics::RECOVERED, $user->toDataArray());

        // Pubsub the restored event
        PubSubService::publish(PubTopics::RECOVERED, $user->toDataArray());
    }
}
