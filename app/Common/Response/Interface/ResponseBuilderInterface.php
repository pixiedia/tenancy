<?php

declare(strict_types=1);

namespace Common\Response\Interface;

use Common\Http\Enum\HttpStatusCode;
use Common\Http\Enum\HttpStatusType;
use Common\Response\Interface\Data\ResponseInterface;
use Exception;

/**
 * Interface ResponseBuilderInterface
 *
 * Interface for building API responses.
 */
interface ResponseBuilderInterface
{
    /**
     * Create a success response.
     *
     * @param  array  $data  The data to be included in the response.
     * @param  int|null  $statusCode  The HTTP status code (optional).
     * @param  string  $message  The response message (optional).
     * @param  array|null  $meta  The meta information (optional).
     * @return ResponseInterface An instance of the Response class.
     */
    public function createSuccess(array $data = [], int $statusCode = HttpStatusCode::OK, string $message = HttpStatusType::OK, ?array $meta = null): ResponseInterface;

    /**
     * Create a failure response.
     *
     * @param  string  $message  The error message.
     * @param  int|null  $statusCode  The error code (optional).
     * @param  Exception|null  $cause  The cause of the failure (optional).
     * @return ResponseInterface An instance of the Response class.
     */
    public function createFailure(string $message, ?int $statusCode = HttpStatusCode::BAD_REQUEST, ?Exception $cause = null): ResponseInterface;

    /**
     * Set the headers.
     *
     * @param  array|string  $headers  The headers to be included in the response.
     */
    public function setHeaders($headers): void;
}
