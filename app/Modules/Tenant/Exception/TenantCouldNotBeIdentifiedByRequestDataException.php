<?php

declare(strict_types=1);

namespace Modules\Tenant\Exception;

use Facade\IgnitionContracts\BaseSolution;
use Facade\IgnitionContracts\ProvidesSolution;
use Facade\IgnitionContracts\Solution;

/**
 * Exception thrown when a tenant cannot be identified by request data.
 */
class TenantCouldNotBeIdentifiedByRequestDataException extends TenantCouldNotBeIdentifiedException implements ProvidesSolution
{
    /**
     * Create a new exception instance.
     *
     * @param  string  $tenantId  The ID of the tenant.
     */
    public function __construct(string $tenantId)
    {
        parent::__construct("Tenant could not be identified by request data with payload: {$tenantId}");
    }

    /**
     * Get a solution for the exception.
     *
     * @return Solution The solution for the exception.
     */
    public function getSolution(): Solution
    {
        return BaseSolution::create('Tenant could not be identified with this request data')
            ->setSolutionDescription('Did you forget to create a tenant with this id?');
    }
}
