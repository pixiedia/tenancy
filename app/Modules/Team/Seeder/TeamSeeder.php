<?php

declare(strict_types=1);

namespace Modules\Team\Seeder;

use App\Util\PHPUtil;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Modules\Team\Interface\Data\TeamInterface;
use Modules\Team\Model\Team;
use Modules\Tenant\Interface\Data\TenantInterface;
use Modules\Tenant\Model\Tenant;

/**
 * Class TeamSeeder
 * Seeder class for generating team data
 */
class TeamSeeder extends Seeder
{
    /**
     * Default size for seeders.
     */
    private $defaultSeederSize;

    /**
     * Default size for relation seeders.
     */
    private $defaultRelationSeederSize;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->defaultSeederSize = Config::get('database.seeders.defaultSeederSize');
        $this->defaultRelationSeederSize = Config::get('database.seeders.defaultSeederRelationSize');
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        try {
            // Log the start of the seeding to the console
            PHPUtil::consoleOutput('🚀 Seeding tenants...');

            // Generate and seed tenants
            $this->seedTeams();

            // Seed tenants and associate them with teams
            $this->associateTenants();

            // Log the end of the seeding process to the console
            PHPUtil::consoleOutput('✅ Seeding completed.');
        } catch (QueryException $e) {
            // Handle the exception
            PHPUtil::consoleOutput("❌ Error seeding teams: {$e->getMessage()}");
        }
    }

    /**
     * Run the database seeds.
     */
    public function seedTeams(): void
    {
        try {
            // Generate 50 team instances
            $teams = Team::factory()->count($this->defaultSeederSize)->make();

            // Log the start of the seeding to the console
            PHPUtil::consoleOutput('🚀 Seeding teams...');

            // Log details about each team as it is being seeded
            foreach ($teams as $team) {
                // Insert each team's data into the database
                DB::table(TeamInterface::TABLE)->insert($team->toArray());

                // Log details about the team
                PHPUtil::consoleOutput("🌱 Seeded team: {$team->getName()}");
            }

            // Log the end of the seeding process to the console
            PHPUtil::consoleOutput('✅ Seeding completed.');
        } catch (QueryException $e) {
            // Handle the exception
            PHPUtil::consoleOutput("❌ Error seeding teams: {$e->getMessage()}");
        }
    }

    /**
     * Associate teams with tenants.
     */
    private function associateTenants(): void
    {
        // Get all existing teams
        $teams = Team::all();

        // Get all existing tenants
        $tenants = Tenant::all();

        // Associate tenants with each team
        $teams->each(function ($teams) use ($tenants) {
            // Attach random teams to the current tenant
            $randomTenants = $tenants->random($this->defaultRelationSeederSize);

            /** @var Team $team */
            $teams->tenants()->saveMany($randomTenants);

            // Log details about the tenant and the associated teams
            PHPUtil::consoleOutput("🔗 Associated teams with tenant: {$randomTenants->pluck(TenantInterface::NAME)->implode(', ')}");
        });
    }
}
