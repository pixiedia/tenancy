<?php

declare(strict_types=1);

namespace App\Util;

use Illuminate\Support\Str;
use InvalidArgumentException;

/**
 * Class KeyCaseConverter
 */
class KeyCaseConverter
{
    /**
     * The case constant representing snake_case.
     */
    public const CASE_SNAKE = 'snake';

    /**
     * The case constant representing camelCase.
     */
    public const CASE_CAMEL = 'camel';

    /**
     * Constant for the metadata attribute.
     */
    public const METADATA = 'metadata';

    /**
     * Convert an array keys to a given case.
     *
     * @param  string  $case  The case to convert keys to (snake_case or camelCase)
     * @param  mixed  $data  The data to convert keys of
     * @return array The data with keys converted to the specified case
     *
     * @throws InvalidArgumentException if the case is not supported
     */
    public function convert(string $case, $data): array
    {
        // Ensure the provided case is either snake or camel
        if (! in_array($case, [self::CASE_CAMEL, self::CASE_SNAKE])) {
            throw new InvalidArgumentException('Case must be either snake or camel');
        }

        // If the data is not an array, return it as is
        if (! PHPUtil::isArray($data)) {
            return $data;
        }

        $array = [];

        // Iterate over each key-value pair in the data array
        foreach ($data as $key => $value) {
            // Check if the key is a subarray and is not metadata
            if (PHPUtil::isArray($value) && ! $this->isMetadata($key)) {
                // If it's a subarray and not metadata, recursively convert its keys
                $array[$key] = $this->convert($case, $value);
            } else {
                // Convert the key to the specified case
                $array[Str::{$case}($key)] = $value;
            }
        }

        return $array;
    }

    /**
     * Check if a key is metadata.
     *
     * @param  mixed  $key  The key to check
     * @return bool True if the key is metadata, false otherwise
     */
    private function isMetadata($key): bool
    {
        // Check if the key is "metadata"
        if ($key === self::METADATA) {
            // The key is metadata
            return true;
        }

        // The key is not metadata
        return false;
    }
}
