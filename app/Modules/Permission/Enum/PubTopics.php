<?php

declare(strict_types=1);

namespace Modules\Permission\Enum;

use BenSampo\Enum\Enum;

/**
 * Enumeration of (Pub/Sub) topics related to publish events.
 *
 * @method static static CREATED()
 * @method static static UPDATED()
 * @method static static DELETED()
 * @method static static SOFT_DELETED()
 * @method static static RECOVERED()
 */
final class PubTopics extends Enum
{
    /**
     * Topic for creation events.
     *
     * @example 'permission.created'
     */
    const CREATED = 'permission.created';

    /**
     * Topic for update events.
     *
     * @example 'permission.updated'
     */
    const UPDATED = 'permission.updated';

    /**
     * Topic for deletion events.
     *
     * @example 'permission.deleted'
     */
    const DELETED = 'permission.deleted';

    /**
     * Topic for soft deletion events.
     *
     * @example 'soft.permission.deleted'
     */
    const SOFT_DELETED = 'permission.soft.deleted';

    /**
     * Topic for recovery events.
     *
     * @example 'permission.recovered'
     */
    const RECOVERED = 'permission.recovered';
}
