<?php

declare(strict_types=1);

namespace Modules\User\Enum;

use BenSampo\Enum\Enum;

/**
 * Enumeration of (Pub/Sub) topics related to DNS subscription events.
 *
 * @method static static LIST()
 * @method static static GET()
 * @method static static CREATE()
 * @method static static UPDATE()
 * @method static static DELETE()
 * @method static static RECOVERED()
 */
final class SubTopics extends Enum
{
    /**
     * Topic for getting a list of DNSs.
     *
     * @example 'user.list'
     */
    const LIST = 'user.list';

    /**
     * Topic for getting information about a single DNS.
     *
     * @example 'user.get'
     */
    const GET = 'user.get';

    /**
     * Topic for creating a new DNS.
     *
     * @example 'user.create'
     */
    const CREATE = 'user.create';

    /**
     * Topic for updating an existing DNS.
     *
     * @example 'user.update'
     */
    const UPDATE = 'user.update';

    /**
     * Topic for deleting a DNS.
     *
     * @example 'user.delete'
     */
    const DELETE = 'user.delete';

    /**
     * Topic for recovery events.
     *
     * @example 'user.recovered'
     */
    const RECOVERED = 'user.recover';
}
