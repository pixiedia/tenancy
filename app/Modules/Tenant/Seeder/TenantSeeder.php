<?php

declare(strict_types=1);

namespace Modules\Tenant\Seeder;

use App\Util\PHPUtil;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Modules\Tenant\Interface\Data\TenantInterface;
use Modules\Tenant\Model\Tenant;

/**
 * Class TenantSeeder
 * Seeder class for generating tenant data
 */
class TenantSeeder extends Seeder
{
    /**
     * Default size for seeders.
     */
    private $defaultSeederSize;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->defaultSeederSize = Config::get('database.seeders.defaultSeederSize');
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        try {
            // Log the start of the seeding to the console
            PHPUtil::consoleOutput('🚀 Seeding tenants...');

            // Generate and seed tenants
            $this->seedTenants();

            // Log the end of the seeding process to the console
            PHPUtil::consoleOutput('✅ Seeding completed.');
        } catch (QueryException $e) {
            // Handle the exception
            PHPUtil::consoleOutput("❌ Error seeding teams: {$e->getMessage()}");
        }
    }

    /**
     * Generate and seed tenant instances.
     */
    private function seedTenants(): void
    {
        // Generate 50 tenant instances
        $tenants = Tenant::factory()->count($this->defaultSeederSize)->make();

        // Seed each tenant
        foreach ($tenants as $tenant) {
            DB::table(TenantInterface::TABLE)->insert($tenant->toArray());

            // Log details about the seeded tenant
            PHPUtil::consoleOutput("🌱 Seeded tenant: {$tenant->getName()}");
        }
    }
}
