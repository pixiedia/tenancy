<?php

declare(strict_types=1);

return [

    /*
    |-------------------------------------------------------------------
    | Cross-Origin Resource Sharing (CORS) Configuration
    |-------------------------------------------------------------------
    |
    | Here you may configure your settings for cross-origin resource sharing
    | or "CORS". This determines what cross-origin operations may execute
    | in web browsers. You are free to adjust these settings as needed.
    |
    | To learn more: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
    |
    */

    'paths' => ['api/*', 'docs/*', 'sanctum/csrf-cookie'],

    /**
     * The paths that should be accessible by CORS requests.
     */
    'allowed_methods' => ['*'],

    /**
     * The HTTP methods that should be allowed for CORS requests.
     */
    'allowed_origins' => ['*'],

    /**
     * The origins that should be allowed to access the API. '*' allows all origins.
     */
    'allowed_origins_patterns' => [],

    /**
     * The allowed origins patterns for CORS requests.
     */
    'allowed_headers' => ['*'],

    /**
     * The headers that should be allowed for CORS requests.
     */
    'exposed_headers' => [],

    /**
     * The headers that should be exposed to the browser.
     */
    'max_age' => 0,

    /**
     * The maximum number of seconds the CORS response may be cached.
     */
    'supports_credentials' => false,

];
