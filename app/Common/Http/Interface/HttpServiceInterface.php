<?php

declare(strict_types=1);

namespace Common\Http\Interface;

/**
 * Interface HttpServiceInterface
 *
 * A common interface for interacting with HTTP.
 */
interface HttpServiceInterface
{
    /**
     * Perform a GET request.
     *
     * @param  string  $url  The URL to send the GET request to.
     * @param  array  $headers  Optional. Additional headers for the request.
     * @param  array  $options  Optional. Additional options for the request.
     * @return mixed The response data.
     */
    public function get($url, $headers = [], $options = []);

    /**
     * Perform a POST request.
     *
     * @param  string  $url  The URL to send the POST request to.
     * @param  array  $data  Optional. Data to include in the POST request.
     * @param  array  $headers  Optional. Additional headers for the request.
     * @param  array  $options  Optional. Additional options for the request.
     * @return mixed The response data.
     */
    public function post($url, $data = [], $headers = [], $options = []);

    /**
     * Perform a PUT request.
     *
     * @param  string  $url  The URL to send the PUT request to.
     * @param  array  $data  Optional. Data to include in the PUT request.
     * @param  array  $headers  Optional. Additional headers for the request.
     * @param  array  $options  Optional. Additional options for the request.
     * @return mixed The response data.
     */
    public function put($url, $data = [], $headers = [], $options = []);

    /**
     * Perform a DELETE request.
     *
     * @param  string  $url  The URL to send the DELETE request to.
     * @param  array  $headers  Optional. Additional headers for the request.
     * @param  array  $options  Optional. Additional options for the request.
     * @return mixed The response data.
     */
    public function delete($url, $headers = [], $options = []);
}
