<?php

declare(strict_types=1);

namespace Common\PubSub\Interface;

/**
 * Interface PubSubServiceInterface
 *
 * Interface for PubSub service classes.
 */
interface PubSubServiceInterface
{
    /**
     * Publishes a message to a topic in PubSub with additional metadata.
     *
     * @param  string  $topic  The topic to publish to.
     * @param  mixed  $payload  The payload to publish.
     */
    public function publish($topic, $payload);

    /**
     * Subscribe to a PubSub topic and execute a callback when a message is received.
     *
     * @param  string  $topic  The topic to subscribe to.
     * @param  callable  $callback  The callback function to execute when a message is received.
     */
    public function subscribe($topic, $callback);
}
