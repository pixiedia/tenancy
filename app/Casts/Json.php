<?php

declare(strict_types=1);

namespace App\Casts;

use App\Util\PHPUtil;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;

class Json implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  array<string, mixed>  $attributes
     */
    public function get(Model $model, string $key, mixed $value, array $attributes): mixed
    {
        if (is_string($value)) {
            return PHPUtil::jsonDecode($value);
        }
        if (PHPUtil::isArray($value)) {
            return $value;
        }

        return null; // or throw an exception if the value is not a string or array
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  array<string, mixed>  $attributes
     */
    public function set(Model $model, string $key, mixed $value, array $attributes): mixed
    {
        return PHPUtil::jsonEncode($value);
    }
}
