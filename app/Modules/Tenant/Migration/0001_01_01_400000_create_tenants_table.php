<?php

declare(strict_types=1);

namespace Modules\Tenant\Migration;

use App\Util\PHPUtil;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Tenant\Interface\Data\TenantInterface;
use Modules\Tenant\Model\Tenant;
use Modules\User\Interface\Data\UserInterface;

/**
 * Class CreateTenantsTableMigration
 */
return new class() extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Creating "' . TenantInterface::TABLE . '" table...');

        // Create the tenants table
        Schema::create(TenantInterface::TABLE, function (Blueprint $table) {
            // Primary key
            $table->id();

            // Universally Unique Identifier
            $table->uuid(TenantInterface::UUID)->comment('Universally Unique Identifier');

            // Tenant name
            $table->string(TenantInterface::NAME)->comment(TenantInterface::ENTITY_NAME . ' name');

            // Slug for the tenant
            $table->string(TenantInterface::SLUG)->unique()->nullable()->comment('Slug for the ' . TenantInterface::ENTITY_NAME);

            // Tenant status
            $table->string(TenantInterface::STATUS)->comment(TenantInterface::ENTITY_NAME . ' status');

            // Tenant domain (nullable)
            $table->string(TenantInterface::DOMAIN)->nullable()->comment(TenantInterface::ENTITY_NAME . ' domain');

            // Creation timestamp (nullable)
            $table->timestamp(Tenant::CREATED_AT)->useCurrent()->comment('Creation timestamp');

            // Last update timestamp (nullable)
            $table->timestamp(Tenant::UPDATED_AT)->nullable()->comment('Last update timestamp');

            // Deletion timestamp (nullable)
            $table->timestamp(TenantInterface::DELETED_AT)->nullable()->comment('Deletion timestamp');

            // User who last updated the tenant (nullable)
            $table->bigInteger(TenantInterface::CREATED_BY)->unsigned()->nullable()->comment('User who last updated the ' . TenantInterface::ENTITY_NAME);

            // User who created the tenant (nullable)
            $table->bigInteger(TenantInterface::UPDATED_BY)->nullable()->comment('User who created the ' . TenantInterface::ENTITY_NAME);

            // User who deleted the tenant (nullable)
            $table->bigInteger(TenantInterface::DELETED_BY)->nullable()->comment('User who deleted the ' . TenantInterface::ENTITY_NAME);

            // Tenant metadata (nullable JSON)
            $table->json(TenantInterface::METADATA)->nullable()->comment(TenantInterface::ENTITY_NAME . ' metadata');

            // Indexes
            $table->index(TenantInterface::UUID);
            $table->index(TenantInterface::STATUS);
            $table->index(TenantInterface::DELETED_AT);

            // Foreign keys
            $table->foreign(TenantInterface::CREATED_BY)->references(TenantInterface::PRIMARY_KEY)->on(UserInterface::TABLE)->onDelete('set null');
            $table->foreign(TenantInterface::UPDATED_BY)->references(TenantInterface::PRIMARY_KEY)->on(UserInterface::TABLE)->onDelete('set null');
            $table->foreign(TenantInterface::DELETED_BY)->references(TenantInterface::PRIMARY_KEY)->on(UserInterface::TABLE)->onDelete('set null');
        });

        // Add a comment to the 'tenant' table
        Schema::table(TenantInterface::TABLE, function (Blueprint $table) {
            $table->comment(TenantInterface::TABLE . ' table');
        });

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ "' . TenantInterface::TABLE . '" table created successfully.');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Log the start of the rollback
        PHPUtil::consoleOutput('🗑️ Dropping "' . TenantInterface::TABLE . '" table...');

        // Drop the tenants table if it exists
        Schema::dropIfExists(TenantInterface::TABLE);

        // Log the completion of the rollback
        PHPUtil::consoleOutput('✅ "' . TenantInterface::TABLE . '" table dropped successfully.');
    }
};
