<?php

declare(strict_types=1);

namespace Modules\Permission\Factory;

use App\Util\PHPUtil;
use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Permission\Interface\Data\PermissionInterface;
use Modules\Permission\Model\Permission;
use RuntimeException;

/**
 * Class PermissionFactory
 */
class PermissionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Permission::class;

    /**
     * Define the model's default state.
     *
     * This method defines the default state for the Permission model using Faker to generate fake data.
     *
     * @return array The default state array for the Permission model.
     */
    public function definition()
    {
        // Return the definition array for the Permission model
        return [
            // Generate a UUID for the permission
            PermissionInterface::UUID => $this->faker->uuid,

            // Generate a random name for the permission
            PermissionInterface::NAME => $this->faker->words(3, true),

            // Generate a random status for the permission
            PermissionInterface::IS_ACTIVE => $this->faker->boolean(),

            // Generate a random description for the permission
            PermissionInterface::DESCRIPTION => $this->faker->text(100),

            // Generate a random value between 1 and 10 for the created_by field
            PermissionInterface::CREATED_BY => $this->faker->numberBetween(1, 10),

            // Generate a random value between 1 and 10 for the updated_by field
            PermissionInterface::UPDATED_BY => $this->faker->numberBetween(1, 10),

            // Generate random metadata for the permission
            PermissionInterface::METADATA => $this->getMetadata(),
        ];
    }

    /**
     * Get a randomly generated metadata array.
     *
     * @return string The generated metadata array.
     *
     * @throws RuntimeException If the metadata array fails to encode as JSON.
     */
    private function getMetadata(): string
    {
        // Initialize an empty array to store metadata
        $metadata = [];

        // Generate metadata with random keys and values
        for ($i = 0; $i < 10; $i++) {
            // Generate a random word as the key and another random word as the value
            $metadata[$this->faker->word] = $this->faker->word;
        }

        // Encode the metadata array as JSON
        $encodedMetadata = PHPUtil::jsonEncode($metadata);

        // Check if encoding was successful
        if ($encodedMetadata === false) {
            throw new RuntimeException('Failed to encode metadata as JSON: ' . json_last_error_msg());
        }

        return $encodedMetadata;
    }
}
