<?php

declare(strict_types=1);

namespace Modules\Customer\Router\V1;

use App\Abstractions\AbstractRouter;
use Common\Http\Enum\HttpMethod;
use Modules\Customer\Controller\CustomerController;
use Modules\Customer\Interface\Data\CustomerInterface;

/**
 * Router class for defining routes related to the Customer module.
 */
class CustomerRoutes extends AbstractRouter
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->apiPrefix = CustomerInterface::ENTITY_NAME;
        $this->controllerClass = CustomerController::class;
    }

    /**
     * Get the routes configuration for the router.
     *
     * @return array The routes configuration.
     */
    public function registerRoutes(): array
    {
        return [
            HttpMethod::GET => [
                // Route to get a list of customers
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR,
                    AbstractRouter::KEY_ACTION => 'getList',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'getList',
                    AbstractRouter::KEY_PREFIX => '',
                ],

                // Route to get a customer by ID
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR . '{' . CustomerInterface::ID . '}',
                    AbstractRouter::KEY_ACTION => 'getById',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'getById',
                    AbstractRouter::KEY_PREFIX => '',
                ],

                // Route to get a customer by UUID
                [
                    AbstractRouter::KEY_URI => '/' . CustomerInterface::UUID . '/{' . CustomerInterface::UUID . '}',
                    AbstractRouter::KEY_ACTION => 'getByUuid',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'getByUuid',
                    AbstractRouter::KEY_PREFIX => '',
                ],
            ],
            HttpMethod::POST => [
                // Route to create a new customer
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR,
                    AbstractRouter::KEY_ACTION => 'create',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'create',
                    AbstractRouter::KEY_PREFIX => '',
                ],

                // Route to search for customers
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR . 'search',
                    AbstractRouter::KEY_ACTION => 'search',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'search',
                    AbstractRouter::KEY_PREFIX => '',
                ],

                // Route to bulk create customers
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR . 'bulk',
                    AbstractRouter::KEY_ACTION => 'bulkCreate',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'bulkCreate',
                    AbstractRouter::KEY_PREFIX => '',
                ],
            ],
            HttpMethod::PUT => [
                // Route to update a customer
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR . '{' . CustomerInterface::ID . '}',
                    AbstractRouter::KEY_ACTION => 'update',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'update',
                    AbstractRouter::KEY_PREFIX => '',
                ],

                // Route to restore a customer
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR . 'restore/{' . CustomerInterface::ID . '}',
                    AbstractRouter::KEY_ACTION => 'restore',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'restore',
                    AbstractRouter::KEY_PREFIX => '',
                ],
            ],
            HttpMethod::DELETE => [
                // Route to delete a customer
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR . '{' . CustomerInterface::ID . '}',
                    AbstractRouter::KEY_ACTION => 'destroy',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'destroy',
                    AbstractRouter::KEY_PREFIX => '',
                ],
            ],
        ];
    }
}
