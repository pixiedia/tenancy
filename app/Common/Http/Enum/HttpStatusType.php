<?php

declare(strict_types=1);

namespace Common\Http\Enum;

use BenSampo\Enum\Enum;

/**
 * Class HttpStatusType
 *
 * Represents HTTP status types and provides methods to get status names.
 *
 * @method static static CONTINUE()
 * @method static static SWITCHING_PROTOCOLS()
 * @method static static PROCESSING()
 * @method static static OK()
 * @method static static CREATED()
 * @method static static ACCEPTED()
 * @method static static NON_AUTHORITATIVE_INFORMATION()
 * @method static static NO_CONTENT()
 * @method static static RESET_CONTENT()
 * @method static static PARTIAL_CONTENT()
 * @method static static MULTI_STATUS()
 * @method static static ALREADY_REPORTED()
 * @method static static IM_USED()
 * @method static static MULTIPLE_CHOICES()
 * @method static static MOVED_PERMANENTLY()
 * @method static static FOUND()
 * @method static static SEE_OTHER()
 * @method static static NOT_MODIFIED()
 * @method static static USE_PROXY()
 * @method static static RESERVED()
 * @method static static TEMPORARY_REDIRECT()
 * @method static static PERMANENT_REDIRECT()
 * @method static static BAD_REQUEST()
 * @method static static UNAUTHORIZED()
 * @method static static PAYMENT_REQUIRED()
 * @method static static FORBIDDEN()
 * @method static static NOT_FOUND()
 * @method static static METHOD_NOT_ALLOWED()
 * @method static static NOT_ACCEPTABLE()
 * @method static static PROXY_AUTHENTICATION_REQUIRED()
 * @method static static REQUEST_TIMEOUT()
 * @method static static CONFLICT()
 * @method static static GONE()
 * @method static static LENGTH_REQUIRED()
 * @method static static PRECONDITION_FAILED()
 * @method static static PAYLOAD_TOO_LARGE()
 * @method static static URI_TOO_LONG()
 * @method static static UNSUPPORTED_MEDIA_TYPE()
 * @method static static RANGE_NOT_SATISFIABLE()
 * @method static static EXPECTATION_FAILED()
 * @method static static IM_A_TEAPOT()
 * @method static static MISDIRECTED_REQUEST()
 * @method static static UNPROCESSABLE_ENTITY()
 * @method static static LOCKED()
 * @method static static FAILED_DEPENDENCY()
 * @method static static RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL()
 * @method static static TOO_EARLY()
 * @method static static UPGRADE_REQUIRED()
 * @method static static PRECONDITION_REQUIRED()
 * @method static static TOO_MANY_REQUESTS()
 * @method static static REQUEST_HEADER_FIELDS_TOO_LARGE()
 * @method static static UNAVAILABLE_FOR_LEGAL_REASONS()
 * @method static static INTERNAL_SERVER_ERROR()
 * @method static static NOT_IMPLEMENTED()
 * @method static static BAD_GATEWAY()
 * @method static static SERVICE_UNAVAILABLE()
 * @method static static GATEWAY_TIMEOUT()
 * @method static static VERSION_NOT_SUPPORTED()
 * @method static static VARIANT_ALSO_NEGOTIATES()
 * @method static static INSUFFICIENT_STORAGE()
 * @method static static LOOP_DETECTED()
 * @method static static NOT_EXTENDED()
 * @method static static NETWORK_AUTHENTICATION_REQUIRED()
 */
final class HttpStatusType extends Enum
{
    /**
     * Continue (100)
     *
     * @var string
     */
    const CONTINUE = 'CONTINUE';

    /**
     * Switching Protocols (101)
     *
     * @var string
     */
    const SWITCHING_PROTOCOLS = 'SWITCHING_PROTOCOLS';

    /**
     * Processing (RFC2518) (102)
     *
     * @var string
     */
    const PROCESSING = 'PROCESSING';

    /**
     * OK (200)
     *
     * @var string
     */
    const OK = 'OK';

    /**
     * Created (201)
     *
     * @var string
     */
    const CREATED = 'CREATED';

    /**
     * Accepted (202)
     *
     * @var string
     */
    const ACCEPTED = 'ACCEPTED';

    /**
     * Non-Authoritative Information (203)
     *
     * @var string
     */
    const NON_AUTHORITATIVE_INFORMATION = 'NON_AUTHORITATIVE_INFORMATION';

    /**
     * No Content (204)
     *
     * @var string
     */
    const NO_CONTENT = 'NO_CONTENT';

    /**
     * Reset Content (205)
     *
     * @var string
     */
    const RESET_CONTENT = 'RESET_CONTENT';

    /**
     * Partial Content (206)
     *
     * @var string
     */
    const PARTIAL_CONTENT = 'PARTIAL_CONTENT';

    /**
     * Multi-Status (RFC4918) (207)
     *
     * @var string
     */
    const MULTI_STATUS = 'MULTI_STATUS';

    /**
     * Already Reported (RFC5842) (208)
     *
     * @var string
     */
    const ALREADY_REPORTED = 'ALREADY_REPORTED';

    /**
     * IM Used (RFC3229) (226)
     *
     * @var string
     */
    const IM_USED = 'IM_USED';

    /**
     * Multiple Choices (300)
     *
     * @var string
     */
    const MULTIPLE_CHOICES = 'MULTIPLE_CHOICES';

    /**
     * Moved Permanently (301)
     *
     * @var string
     */
    const MOVED_PERMANENTLY = 'MOVED_PERMANENTLY';

    /**
     * Found (302)
     *
     * @var string
     */
    const FOUND = 'FOUND';

    /**
     * See Other (303)
     *
     * @var string
     */
    const SEE_OTHER = 'SEE_OTHER';

    /**
     * Not Modified (304)
     *
     * @var string
     */
    const NOT_MODIFIED = 'NOT_MODIFIED';

    /**
     * Use Proxy (305)
     *
     * @var string
     */
    const USE_PROXY = 'USE_PROXY';

    /**
     * Reserved (306)
     *
     * @var string
     */
    const RESERVED = 'RESERVED';

    /**
     * Temporary Redirect (307)
     *
     * @var string
     */
    const TEMPORARY_REDIRECT = 'TEMPORARY_REDIRECT';

    /**
     * Permanent Redirect (RFC7238) (308)
     *
     * @var string
     */
    const PERMANENT_REDIRECT = 'PERMANENT_REDIRECT';

    /**
     * Bad Request (400)
     *
     * @var string
     */
    const BAD_REQUEST = 'BAD_REQUEST';

    /**
     * Unauthorized (401)
     *
     * @var string
     */
    const UNAUTHORIZED = 'UNAUTHORIZED';

    /**
     * Payment Required (402)
     *
     * @var string
     */
    const PAYMENT_REQUIRED = 'PAYMENT_REQUIRED';

    /**
     * Forbidden (403)
     *
     * @var string
     */
    const FORBIDDEN = 'FORBIDDEN';

    /**
     * Not Found (404)
     *
     * @var string
     */
    const NOT_FOUND = 'NOT_FOUND';

    /**
     * Method Not Allowed (405)
     *
     * @var string
     */
    const METHOD_NOT_ALLOWED = 'METHOD_NOT_ALLOWED';

    /**
     * Not Acceptable (406)
     *
     * @var string
     */
    const NOT_ACCEPTABLE = 'NOT_ACCEPTABLE';

    /**
     * Proxy Authentication Required (407)
     *
     * @var string
     */
    const PROXY_AUTHENTICATION_REQUIRED = 'PROXY_AUTHENTICATION_REQUIRED';

    /**
     * Request Timeout (408)
     *
     * @var string
     */
    const REQUEST_TIMEOUT = 'REQUEST_TIMEOUT';

    /**
     * Conflict (409)
     *
     * @var string
     */
    const CONFLICT = 'CONFLICT';

    /**
     * Gone (410)
     *
     * @var string
     */
    const GONE = 'GONE';

    /**
     * Length Required (411)
     *
     * @var string
     */
    const LENGTH_REQUIRED = 'LENGTH_REQUIRED';

    /**
     * Precondition Failed (412)
     *
     * @var string
     */
    const PRECONDITION_FAILED = 'PRECONDITION_FAILED';

    /**
     * Payload Too Large (413)
     *
     * @var string
     */
    const PAYLOAD_TOO_LARGE = 'PAYLOAD_TOO_LARGE';

    /**
     * URI Too Long (414)
     *
     * @var string
     */
    const URI_TOO_LONG = 'URI_TOO_LONG';

    /**
     * Unsupported Media Type (415)
     *
     * @var string
     */
    const UNSUPPORTED_MEDIA_TYPE = 'UNSUPPORTED_MEDIA_TYPE';

    /**
     * Range Not Satisfiable (416)
     *
     * @var string
     */
    const RANGE_NOT_SATISFIABLE = 'RANGE_NOT_SATISFIABLE';

    /**
     * Expectation Failed (417)
     *
     * @var string
     */
    const EXPECTATION_FAILED = 'EXPECTATION_FAILED';

    /**
     * I'm a teapot (RFC2324) (418)
     *
     * @var string
     */
    const IM_A_TEAPOT = 'IM_A_TEAPOT';

    /**
     * Misdirected Request (421)
     *
     * @var string
     */
    const MISDIRECTED_REQUEST = 'MISDIRECTED_REQUEST';

    /**
     * Unprocessable Entity (RFC4918) (422)
     *
     * @var string
     */
    const UNPROCESSABLE_ENTITY = 'UNPROCESSABLE_ENTITY';

    /**
     * Locked (RFC4918) (423)
     *
     * @var string
     */
    const LOCKED = 'LOCKED';

    /**
     * Failed Dependency (RFC4918) (424)
     *
     * @var string
     */
    const FAILED_DEPENDENCY = 'FAILED_DEPENDENCY';

    /**
     * Deprecated as of RFC8470 specification.
     * The 425 HTTP status code has become experimental and has been given a name.
     * Use `TOO_EARLY` instead.
     */

    /**
     * Reserved for WebDAV Advanced Collections Expired Proposal (RFC2817) (425)
     *
     * @deprecated As of RFC8470 specification, use `TOO_EARLY` instead.
     *
     * @var string
     */
    const RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL = 'RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL';

    /**
     * Too Early (RFC8470) (425)
     *
     * @var string
     */
    const TOO_EARLY = 'TOO_EARLY';

    /**
     * Upgrade Required (RFC2817) (426)
     *
     * @var string
     */
    const UPGRADE_REQUIRED = 'UPGRADE_REQUIRED';

    /**
     * Precondition Required (RFC6585) (428)
     *
     * @var string
     */
    const PRECONDITION_REQUIRED = 'PRECONDITION_REQUIRED';

    /**
     * Too Many Requests (RFC6585) (429)
     *
     * @var string
     */
    const TOO_MANY_REQUESTS = 'TOO_MANY_REQUESTS';

    /**
     * Request Header Fields Too Large (RFC6585) (431)
     *
     * @var string
     */
    const REQUEST_HEADER_FIELDS_TOO_LARGE = 'REQUEST_HEADER_FIELDS_TOO_LARGE';

    /**
     * Unavailable For Legal Reasons (451)
     *
     * @var string
     */
    const UNAVAILABLE_FOR_LEGAL_REASONS = 'UNAVAILABLE_FOR_LEGAL_REASONS';

    /**
     * Internal Server Error (500)
     *
     * @var string
     */
    const INTERNAL_SERVER_ERROR = 'INTERNAL_SERVER_ERROR';

    /**
     * Not Implemented (501)
     *
     * @var string
     */
    const NOT_IMPLEMENTED = 'NOT_IMPLEMENTED';

    /**
     * Bad Gateway (502)
     *
     * @var string
     */
    const BAD_GATEWAY = 'BAD_GATEWAY';

    /**
     * Service Unavailable (503)
     *
     * @var string
     */
    const SERVICE_UNAVAILABLE = 'SERVICE_UNAVAILABLE';

    /**
     * Gateway Timeout (504)
     *
     * @var string
     */
    const GATEWAY_TIMEOUT = 'GATEWAY_TIMEOUT';

    /**
     * HTTP Version Not Supported (505)
     *
     * @var string
     */
    const VERSION_NOT_SUPPORTED = 'VERSION_NOT_SUPPORTED';

    /**
     * Variant Also Negotiates (RFC2295) (506)
     *
     * @var string
     */
    const VARIANT_ALSO_NEGOTIATES = 'VARIANT_ALSO_NEGOTIATES';

    /**
     * Insufficient Storage (RFC4918) (507)
     *
     * @var string
     */
    const INSUFFICIENT_STORAGE = 'INSUFFICIENT_STORAGE';

    /**
     * Loop Detected (RFC5842) (508)
     *
     * @var string
     */
    const LOOP_DETECTED = 'LOOP_DETECTED';

    /**
     * Not Extended (RFC2774) (510)
     *
     * @var string
     */
    const NOT_EXTENDED = 'NOT_EXTENDED';

    /**
     * Network Authentication Required (RFC6585) (511)
     *
     * @var string
     */
    const NETWORK_AUTHENTICATION_REQUIRED = 'NETWORK_AUTHENTICATION_REQUIRED';
}
