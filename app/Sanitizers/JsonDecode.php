<?php

declare(strict_types=1);

namespace App\Sanitizers;

use App\Util\PHPUtil;
use ArondeParon\RequestSanitizer\Contracts\Sanitizer;

/**
 * Class JsonDecode
 */
class JsonDecode implements Sanitizer
{
    /**
     * Decode JSON data into an associative array.
     *
     * @param  mixed  $input  The input data to sanitize (JSON string).
     * @return array|null The decoded array or null if decoding fails.
     */
    public function sanitize($input)
    {
        // Decode JSON data
        return PHPUtil::jsonDecode($input, true);
    }
}
