<?php

declare(strict_types=1);

namespace Modules\Customer\Repository;

use App\Abstractions\AbstractRepository;
use Exception;
use Modules\Customer\Interface\CustomerRepositoryInterface;
use Modules\Customer\Interface\Data\CustomerInterface;
use Modules\Customer\Model\Customer;

/**
 * Class CustomerRepository
 *
 * This class implements the CustomerRepositoryInterface and provides methods to interact with the Customer model.
 */
class CustomerRepository extends AbstractRepository implements CustomerRepositoryInterface
{
    /**
     * CustomerRepository constructor.
     *
     * @param  Customer  $model  The Customer model instance to use.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(CustomerInterface $model)
    {
        // Call the parent constructor with the model and the model interface
        parent::__construct($model, CustomerInterface::class);

        // Assign the model instance to the class property
        $this->model = $model;
    }
}
