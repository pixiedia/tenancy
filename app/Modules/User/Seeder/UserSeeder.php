<?php

declare(strict_types=1);

namespace Modules\User\Seeder;

use App\Util\PHPUtil;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Modules\User\Interface\Data\UserInterface;
use Modules\User\Model\User;

/**
 * Class UserSeeder
 * Seeder class for generating user data
 */
class UserSeeder extends Seeder
{
    /**
     * Default size for seeders.
     */
    private $defaultSeederSize;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->defaultSeederSize = Config::get('database.seeders.defaultSeederSize');
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        try {
            // Generate 50 user instances
            $users = User::factory()->count($this->defaultSeederSize)->make();

            // Log the start of the seeding to the console
            PHPUtil::consoleOutput('🚀 Seeding users...');

            // Log details about each user as it is being seeded
            foreach ($users as $user) {
                // Insert each user's data into the database
                DB::table(UserInterface::TABLE)->insert($user->toArray());

                // Log details about the user
                PHPUtil::consoleOutput("🌱 Seeded user: {$user->getName()}");
            }

            // Log the end of the seeding process to the console
            PHPUtil::consoleOutput('✅ Seeding completed.');
        } catch (QueryException $e) {
            // Handle the exception
            PHPUtil::consoleOutput("❌ Error seeding users: {$e->getMessage()}");
        }
    }
}
