<?php

declare(strict_types=1);

namespace Util\Phraser;

/**
 * Translated phrase renderer
 *
 * @api
 *
 * @since 100.0.2
 */
interface RendererInterface
{
    /**
     * Render source text
     *
     * @param [] $source
     * @param [] $arguments
     * @return string
     */
    public function render(array $source, array $arguments);
}
