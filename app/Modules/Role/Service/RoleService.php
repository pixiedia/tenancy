<?php

declare(strict_types=1);

namespace Modules\Role\Service;

use App\Abstractions\AbstractService;
use Exception;
use Modules\Role\Interface\Data\RoleInterface;
use Modules\Role\Interface\RoleRepositoryInterface;
use Modules\Role\Interface\RoleServiceInterface;
use Modules\Role\Repository\RoleRepository;

/**
 * Class RoleService
 */
class RoleService extends AbstractService implements RoleServiceInterface
{
    /**
     * AbstractRepository constructor.
     *
     * @param  RoleRepository  $repository  The repository instance to use in the service.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(RoleRepositoryInterface $repository)
    {
        parent::__construct($repository, RoleInterface::class);
    }
}
