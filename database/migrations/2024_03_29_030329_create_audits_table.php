<?php

declare(strict_types=1);

use App\Util\PHPUtil;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditsTable extends Migration
{
    /**
     * The ID column name.
     */
    const ID = 'id';

    /**
     * The morph type column name.
     */
    const MORPH_TYPE = 'type';

    /**
     * The morph ID column name.
     */
    const MORPH_ID = 'id';

    /**
     * The event column name.
     */
    const EVENT = 'event';

    /**
     * The auditable column name.
     */
    const AUDITABLE = 'auditable';

    /**
     * The old values column name.
     */
    const OLD_VALUES = 'old_values';

    /**
     * The new values column name.
     */
    const NEW_VALUES = 'new_values';

    /**
     * The URL column name.
     */
    const URL = 'url';

    /**
     * The IP address column name.
     */
    const IP_ADDRESS = 'ip_address';

    /**
     * The user agent column name.
     */
    const USER_AGENT = 'user_agent';

    /**
     * The tags column name.
     */
    const TAGS = 'tags';

    /**
     * Run the migrations.
     *
     * This method creates the 'audits' table with the specified schema.
     *
     * @return void
     */
    public function up()
    {
        // Get the database connection and table name from the configuration
        $tableName = config('audit.drivers.database.table', 'audits');
        $connection = config('audit.drivers.database.connection', config('database.default'));

        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Creating "' . $tableName . '" table...');

        // Create the 'audits' table
        Schema::connection($connection)->create($tableName, function (Blueprint $table) {
            // Get the prefix for the user morph
            $morphPrefix = config('audit.user.morph_prefix', 'user');

            // The ID of the audit entry
            $table->bigIncrements(self::ID)->comment('The ID of the audit entry');

            // The type of the morphed entity (e.g., 'user', 'product')
            $table->string($morphPrefix . '_' . self::MORPH_TYPE)->nullable()->comment('The type of the morphed entity (e.g., \'user\', \'product\')');

            // The ID of the morphed entity
            $table->unsignedBigInteger($morphPrefix . '_' . self::MORPH_ID)->nullable()->comment('The ID of the morphed entity');

            // The event type (e.g., 'created', 'updated', 'deleted')
            $table->string(self::EVENT)->comment('The event type (e.g., \'created\', \'updated\', \'deleted\')');

            // Morphs relationship to the auditable entity
            $table->morphs(self::AUDITABLE);

            // The old values of the audited changes
            $table->text(self::OLD_VALUES)->nullable()->comment('The old values of the audited changes');

            // The new values of the audited changes
            $table->text(self::NEW_VALUES)->nullable()->comment('The new values of the audited changes');

            // The URL related to the audit
            $table->text(self::URL)->nullable()->comment('The URL related to the audit');

            // The IP address of the user triggering the audit
            $table->ipAddress(self::IP_ADDRESS)->nullable()->comment('The IP address of the user triggering the audit');

            // The user agent of the user triggering the audit
            $table->string(self::USER_AGENT, 1023)->nullable()->comment('The user agent of the user triggering the audit');

            // Tags associated with the audit entry
            $table->string(self::TAGS)->nullable()->comment('Tags associated with the audit entry');

            // Add timestamps to track creation and update times
            $table->timestamps();

            // Index the morph ID and type for faster lookups
            $table->index([$morphPrefix . '_' . self::MORPH_ID, $morphPrefix . '_' . self::MORPH_TYPE]);
        });

        // Add a comment to the 'audits' table
        Schema::table($tableName, function (Blueprint $table) use ($tableName) {
            $table->comment($tableName . ' table');
        });

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ "' . $tableName . '" table created successfully.');
    }

    /**
     * Reverse the migrations.
     *
     * Drop the 'users' tables if they exist.
     */
    public function down(): void
    {
        // Get the database connection and table name from the configuration
        $tableName = config('audit.drivers.database.table', 'audits');
        $connection = config('audit.drivers.database.connection', config('database.default'));

        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Dropping tables: ' . $tableName . '...');

        // Drop the 'users' table if it exists
        Schema::connection($connection)->drop($tableName);

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ Tables dropped: ' . $tableName . '.');
    }
}
