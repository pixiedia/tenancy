<?php

declare(strict_types=1);

namespace Util\Phraser;

use App\Util\PHPUtil;
use JsonSerializable;
use ReturnTypeWillChange;
use Throwable;
use Util\Phraser\Renderer\Placeholder as RendererPlaceholder;

/**
 * Phrase (for replacing Data Value with Object)
 *
 * @api
 *
 * @since 100.0.2
 */
class Phrase implements JsonSerializable
{
    /**
     * Default phrase renderer. Allows stacking renderers that "don't know about each other"
     *
     * @var RendererInterface
     */
    private static $renderer;

    /**
     * String for rendering
     *
     * @var string
     */
    private $text;

    /**
     * Arguments for placeholder values
     *
     * @var array
     */
    private $arguments;

    /**
     * Phrase construct
     *
     * @param  string  $text
     */
    public function __construct($text, array $arguments = [])
    {
        $this->text = (string) $text;
        $this->arguments = $arguments;
    }

    /**
     * Create value-object Phrase
     *
     * @SuppressWarnings(PHPMD.ShortMethodName)
     * phpcs:disable Squiz.Functions.GlobalFunction
     *
     * @param  array  $argc
     * @return Phrase
     */
    public static function __(...$argc)
    {
        $text = array_shift($argc);
        if (! empty($argc) && PHPUtil::isArray($argc[0])) {
            $argc = $argc[0];
        }

        return new Phrase($text, $argc);
    }

    /**
     * Defers rendering to the last possible moment (when converted to string)
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * Set default Phrase renderer
     *
     * @return void
     */
    public static function setRenderer(RendererInterface $renderer)
    {
        self::$renderer = $renderer;
    }

    /**
     * Get default Phrase renderer
     *
     * @return RendererInterface
     */
    public static function getRenderer()
    {
        if (! self::$renderer) {
            self::$renderer = new RendererPlaceholder();
        }

        return self::$renderer;
    }

    /**
     * Get phrase base text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Get phrase message arguments
     *
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * Render phrase
     *
     * @return string
     */
    public function render()
    {
        try {
            return self::getRenderer()->render([$this->text], $this->getArguments());
        } catch (Throwable $e) {
            return $this->getText();
        }
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @return string
     */
    #[ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return $this->render();
    }
}
