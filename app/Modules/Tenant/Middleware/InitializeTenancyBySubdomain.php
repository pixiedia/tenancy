<?php

declare(strict_types=1);

namespace Modules\Tenant\Middleware;

use Closure;
use Common\Http\Enum\HttpStatusCode;
use Common\Response\Trait\ResponseBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Modules\Tenant\Exception\NotASubdomainException;
use Modules\Tenant\Interface\Data\TenantInterface;
use Modules\Tenant\Model\Tenant;
use Symfony\Component\HttpFoundation\Response;
use Util\Phraser\Phrase;

/**
 * Class InitializeTenancyBySubdomain
 */
class InitializeTenancyBySubdomain
{
    use ResponseBuilder;

    /**
     * Handle an incoming request.
     *
     * @param  Closure(Request): (Response)  $next
     *
     * @throws NotASubdomainException If the hostname is not a subdomain
     */
    public function handle(Request $request, Closure $next): Response
    {
        try {
            // Validate that the hostname is a subdomain
            $this->validateSubdomain($request->getHost());
        } catch (NotASubdomainException $e) {
            // Return failure response with error message
            $response = $this->createFailure(Phrase::__($e->getMessage()), HttpStatusCode::BAD_REQUEST, $e);

            return response()->json($response->toArray(), HttpStatusCode::BAD_REQUEST);
        }

        // Get the domain from the request
        $domain = $request->getHost();

        // Query the Tenant model to find a matching tenant for the domain
        $tenant = Tenant::where(TenantInterface::DOMAIN, $domain)->first();

        // If a matching tenant is found, set it on the request
        if ($tenant || $tenant->getId()) {
            $request->attributes->set(TenantInterface::ENTITY_NAME, $tenant);
        }

        // Call the next middleware in the pipeline with the modified request
        return $next($request);
    }

    /**
     * Check if the hostname is a subdomain or not
     *
     * @throws NotASubdomainException If the hostname is not a valid subdomain
     */
    protected function validateSubdomain(string $hostname): void
    {
        $parts = explode('.', $hostname);

        // Check if the hostname is localhost or an IP address
        $isLocalhost = count($parts) === 1;
        $isIpAddress = count(array_filter($parts, 'is_numeric')) === count($parts);

        // Check if the hostname is a central domain or a third-party domain
        $isCentralDomain = in_array($hostname, config('multi-tenancy.central_domains'), true);
        $isThirdPartyDomain = ! Str::endsWith($hostname, config('multi-tenancy.central_domains'));

        // If the hostname is localhost, an IP address, a central domain, or a third-party domain, it's not a valid subdomain
        if ($isLocalhost || $isIpAddress || $isCentralDomain || $isThirdPartyDomain) {
            throw new NotASubdomainException($hostname);
        }
    }
}
