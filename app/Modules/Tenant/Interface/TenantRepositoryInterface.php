<?php

declare(strict_types=1);

namespace Modules\Tenant\Interface;

use App\Abstractions\Interface\AbstractRepositoryInterface;

/**
 * Interface TenantRepositoryInterface
 */
interface TenantRepositoryInterface extends AbstractRepositoryInterface
{
}
