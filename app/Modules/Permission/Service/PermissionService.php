<?php

declare(strict_types=1);

namespace Modules\Permission\Service;

use App\Abstractions\AbstractService;
use Exception;
use Modules\Permission\Interface\Data\PermissionInterface;
use Modules\Permission\Interface\PermissionRepositoryInterface;
use Modules\Permission\Interface\PermissionServiceInterface;
use Modules\Permission\Repository\PermissionRepository;

/**
 * Class PermissionService
 */
class PermissionService extends AbstractService implements PermissionServiceInterface
{
    /**
     * AbstractRepository constructor.
     *
     * @param  PermissionRepository  $repository  The repository instance to use in the service.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(PermissionRepositoryInterface $repository)
    {
        parent::__construct($repository, PermissionInterface::class);
    }
}
