<?php

declare(strict_types=1);

namespace Common\PubSub\Helper;

use Magento\Framework\App\DeploymentConfig;

/**
 * Helper class for managing Redis configurations.
 *
 * This class provides methods to retrieve specific configurations for Redis.
 */
class Data
{
    /**
     * The configuration key for Redis.
     */
    const REDIS_CONFIG = 'cache';

    /**
     * Redis server configuration key.
     */
    const REDIS_SERVER = 'server';

    /**
     * Redis host configuration key.
     */
    const REDIS_HOST = 'host';

    /**
     * Redis port configuration key.
     */
    const REDIS_PORT = 'port';

    /**
     * Redis password configuration key.
     */
    const REDIS_PASSWORD = 'password';

    /**
     * @var DeploymentConfig The DeploymentConfig instance.
     */
    protected $deploymentConfig;

    /**
     * RedisData constructor.
     */
    public function __construct(DeploymentConfig $deploymentConfig)
    {
        $this->deploymentConfig = $deploymentConfig;
    }

    /**
     * Get the Redis configuration from Magento 2 deployment configuration (env.php).
     *
     * @return array|null The Redis configuration.
     */
    public function getEnv(): ?array
    {
        $redisConfig = null;

        // NOTE: Check if Redis configuration is present in env.php
        if (! empty($this->deploymentConfig->getConfigData(self::REDIS_CONFIG))) {
            $cacheConfig = $this->deploymentConfig->getConfigData(self::REDIS_CONFIG);
            $redisConfig = $cacheConfig['frontend']['default']['backend_options'];
        }

        return $redisConfig;
    }

    /**
     * Get the Redis host configuration.
     *
     * @return string|null The Redis host configuration.
     */
    public function getHost(): ?string
    {
        return $this->getConfigValue(self::REDIS_SERVER);
    }

    /**
     * Get the Redis port configuration.
     *
     * @return int|null The Redis port configuration.
     */
    public function getPort(): ?string
    {
        return $this->getConfigValue(self::REDIS_PORT);
    }

    /**
     * Get the Redis password configuration.
     *
     * @return string|null The Redis password configuration.
     */
    public function getPassword(): ?string
    {
        return $this->getConfigValue(self::REDIS_PASSWORD);
    }

    /**
     * Get all Redis configurations.
     *
     * @return array Associative array of all Redis configurations.
     */
    public function getConfig(): array
    {
        return [
            self::REDIS_HOST => $this->getHost(),
            self::REDIS_PORT => $this->getPort(),
            self::REDIS_PASSWORD => $this->getPassword(),
        ];
    }

    /**
     * Get a specific configuration value from the deployment configuration.
     *
     * @param  string  $key  The configuration key.
     * @return mixed|null The configuration value if found, null otherwise.
     */
    private function getConfigValue(string $key)
    {
        $redisConfig = $this->getEnv();

        return $redisConfig[$key] ?? null;
    }
}
