<?php

declare(strict_types=1);

namespace Modules\Team\Repository;

use App\Abstractions\AbstractRepository;
use Exception;
use Modules\Team\Interface\Data\TeamInterface;
use Modules\Team\Interface\TeamRepositoryInterface;
use Modules\Team\Model\Team;

/**
 * Class TeamRepository
 *
 * This class implements the TeamRepositoryInterface and provides methods to interact with the Team model.
 */
class TeamRepository extends AbstractRepository implements TeamRepositoryInterface
{
    /**
     * TeamRepository constructor.
     *
     * @param  Team  $model  The Team model instance to use.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(TeamInterface $model)
    {
        // Call the parent constructor with the model and the model interface
        parent::__construct($model, TeamInterface::class);

        // Assign the model instance to the class property
        $this->model = $model;
    }
}
