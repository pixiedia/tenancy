<?php

declare(strict_types=1);

use Common\Config\Service\ConfigService;
use Illuminate\Support\Str;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for database operations. This is
    | the connection which will be utilized unless another connection
    | is explicitly specified when you execute a query / statement.
    |
    */

    'default' => ConfigService::getString('DB_CONNECTION', 'sqlite'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Below are all of the database connections defined for your application.
    | An example configuration is provided for each database system which
    | is supported by Laravel. You're free to add / remove connections.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'url' => ConfigService::getString('DB_URL'),
            'database' => ConfigService::getString('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => ConfigService::getBoolean('DB_FOREIGN_KEYS', true),
        ],

        'mysql' => [
            'driver' => 'mysql',
            'url' => ConfigService::getString('DB_URL'),
            'host' => ConfigService::getString('DB_HOST', '127.0.0.1'),
            'port' => ConfigService::getString('DB_PORT', '3306'),
            'database' => ConfigService::getString('DB_DATABASE', 'laravel'),
            'username' => ConfigService::getString('DB_USERNAME', 'root'),
            'password' => ConfigService::getString('DB_PASSWORD', ''),
            'unix_socket' => ConfigService::getString('DB_SOCKET', ''),
            'charset' => ConfigService::getString('DB_CHARSET', 'utf8mb4'),
            'collation' => ConfigService::getString('DB_COLLATION', 'utf8mb4_unicode_ci'),
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => ConfigService::getString('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

        'mariadb' => [
            'driver' => 'mariadb',
            'url' => ConfigService::getString('DB_URL'),
            'host' => ConfigService::getString('DB_HOST', '127.0.0.1'),
            'port' => ConfigService::getString('DB_PORT', '3306'),
            'database' => ConfigService::getString('DB_DATABASE', 'laravel'),
            'username' => ConfigService::getString('DB_USERNAME', 'root'),
            'password' => ConfigService::getString('DB_PASSWORD', ''),
            'unix_socket' => ConfigService::getString('DB_SOCKET', ''),
            'charset' => ConfigService::getString('DB_CHARSET', 'utf8mb4'),
            'collation' => ConfigService::getString('DB_COLLATION', 'utf8mb4_unicode_ci'),
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => ConfigService::getString('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'url' => ConfigService::getString('DB_URL'),
            'host' => ConfigService::getString('DB_HOST', '127.0.0.1'),
            'port' => ConfigService::getString('DB_PORT', '5432'),
            'database' => ConfigService::getString('DB_DATABASE', 'laravel'),
            'username' => ConfigService::getString('DB_USERNAME', 'root'),
            'password' => ConfigService::getString('DB_PASSWORD', ''),
            'charset' => ConfigService::getString('DB_CHARSET', 'utf8'),
            'prefix' => '',
            'prefix_indexes' => true,
            'search_path' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'url' => ConfigService::getString('DB_URL'),
            'host' => ConfigService::getString('DB_HOST', 'localhost'),
            'port' => ConfigService::getString('DB_PORT', '1433'),
            'database' => ConfigService::getString('DB_DATABASE', 'laravel'),
            'username' => ConfigService::getString('DB_USERNAME', 'root'),
            'password' => ConfigService::getString('DB_PASSWORD', ''),
            'charset' => ConfigService::getString('DB_CHARSET', 'utf8'),
            'prefix' => '',
            'prefix_indexes' => true,
            // 'encrypt' => ConfigService::getString('DB_ENCRYPT', 'yes'),
            // 'trust_server_certificate' => ConfigService::getString('DB_TRUST_SERVER_CERTIFICATE', 'false'),
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run on the database.
    |
    */

    'migrations' => [
        'table' => 'migrations',
        'update_date_on_publish' => true,
    ],

    /*
    |--------------------------------------------------------------------------
    | Seeder Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the default sizes for seeders and their relations.
    | These values will be used when seeding your database.
    |
    */

    'seeders' => [
        'defaultSeederSize' => ConfigService::getNumber('DEFAULT_SEEDER_SIZE', 10),
        'defaultSeederRelationSize' => ConfigService::getNumber('DEFAULT_RELATION_SEEDER_SIZE', 10),
    ],

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as Memcached. You may define your connection settings here.
    |
    */

    'redis' => [

        'client' => ConfigService::getString('REDIS_CLIENT', 'phpredis'),

        'options' => [
            'cluster' => ConfigService::getString('REDIS_CLUSTER', 'redis'),
            // 'prefix' => ConfigService::getString('REDIS_PREFIX', Str::slug(ConfigService::getString('APP_NAME', 'laravel'), '_').'_database_'),
        ],

        'default' => [
            'url' => ConfigService::getString('REDIS_URL'),
            'host' => ConfigService::getString('REDIS_HOST', '127.0.0.1'),
            'username' => ConfigService::getString('REDIS_USERNAME'),
            'password' => ConfigService::getString('REDIS_PASSWORD'),
            'port' => ConfigService::getString('REDIS_PORT', '6379'),
            'database' => ConfigService::getString('REDIS_DB', '0'),
        ],

        'cache' => [
            'url' => ConfigService::getString('REDIS_URL'),
            'host' => ConfigService::getString('REDIS_HOST', '127.0.0.1'),
            'username' => ConfigService::getString('REDIS_USERNAME'),
            'password' => ConfigService::getString('REDIS_PASSWORD'),
            'port' => ConfigService::getString('REDIS_PORT', '6379'),
            'database' => ConfigService::getString('REDIS_CACHE_DB', '1'),
        ],

    ],

];
