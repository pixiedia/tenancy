<?php

declare(strict_types=1);

namespace Modules\Tenant\Controller;

use App\Abstractions\AbstractController;
use Common\Request\Interface\Data\RequestInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Tenant\Dto\CreateTenantDto;
use Modules\Tenant\Dto\UpdateTenantDto;
use Modules\Tenant\Interface\Data\TenantInterface;
use Modules\Tenant\Interface\TenantServiceInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Tag(
 *      name="Tenants",
 *      description="Tenant API Endpoints"
 * )
 * Class TenantController
 */
class TenantController extends AbstractController
{
    /**
     * The key used to store the search term in the request.
     */
    const SEARCH_TERM_KEY = 'searchTerm';

    /**
     * The entity name for the pagination.
     *
     * @var string|null
     */
    const ENTITY_NAME = TenantInterface::ENTITY_NAME;

    protected $tenantService;

    /**
     * TenantController constructor.
     */
    public function __construct(TenantServiceInterface $tenantService)
    {
        $this->tenantService = $tenantService;
    }

    /**
     * @OA\Get(
     *      path="/tenants",
     *      summary="Get a list of tenants",
     *      tags={"Tenants"},
     *
     *      @OA\Parameter(
     *          name="page",
     *          in="query",
     *          description="Page number",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=1
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Tenant")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a list of tenants.
     *
     * @param  int  $page  The page number.
     * @param  int  $size  The page size.
     * @return JsonResponse The JSON response containing the list of tenants.
     */
    public function getList(Request $request): JsonResponse
    {
        try {
            // Get pagination parameters from the request
            $size = (int) $request->query(RequestInterface::SIZE_KEY, (string) RequestInterface::DEFAULT_PAGE_SIZE);
            $page = (int) $request->query(RequestInterface::PAGE_KEY, (string) RequestInterface::DEFAULT_PAGE_NUMBER);

            // Validate pagination parameters
            $this->validatePaginationParams($page, $size);

            // Retrieve paginated list of tenants from the service
            $tenants = $this->tenantService->list($page, $size);

            // Return JSON response with the list of tenants
            return response()->json($tenants);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/tenants/search/{searchTerm}",
     *      summary="Get a list of tenants by search term",
     *      tags={"Tenants"},
     *
     *      @OA\Parameter(
     *          name="searchTerm",
     *          in="path",
     *          description="Search Term",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="string",
     *              default="Tenant A"
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(
     *              required={"searchTerm", "page", "size", "filters", "sortOrder"},
     *
     *              @OA\Property(property="filters", type="object", example={}),
     *              @OA\Property(property="sortOrder", type="string", enum={"asc", "desc"}, example="asc")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Tenant")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a list of tenants.
     *
     * @param  Request  $request  The HTTP request object containing search, pagination, filters, and sort parameters.
     * @return JsonResponse The JSON response containing the list of tenants matching the search criteria.
     */
    public function search(Request $request): JsonResponse
    {
        try {
            // Get search term from the request
            $searchTerm = $request->route(static::SEARCH_TERM_KEY);

            // Get pagination parameters from the request
            $size = (int) $request->query(RequestInterface::SIZE_KEY, (string) RequestInterface::DEFAULT_PAGE_SIZE);
            $page = (int) $request->query(RequestInterface::PAGE_KEY, (string) RequestInterface::DEFAULT_PAGE_NUMBER);

            // Validate pagination parameters
            $this->validatePaginationParams($page, $size);

            // Get filters and sort criteria from the request, defaulting to an empty array and default sort field and order if not provided
            $filters = $request->input(RequestInterface::FILTERS_KEY, []);
            $sortOrder = $request->input(RequestInterface::SORT_ORDER_KEY, RequestInterface::DEFAULT_SORT_ORDER);

            // Retrieve paginated search of tenants from the service based on the search term, filters, and sort criteria
            $tenants = $this->tenantService->search($searchTerm, $page, $size, $filters, $sortOrder);

            // Return JSON response with the list of tenants matching the search criteria
            return response()->json($tenants);
        } catch (Exception $e) {
            // Handle any exceptions that occur during the search and return a JSON response with an error message
            throw $e;
        }
    }

    /**
     * @OA\Get(
     *      path="/tenants/{id}",
     *      summary="Get a tenant by ID",
     *      tags={"Tenants"},
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the tenant to retrieve",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Tenant")
     *          )
     *      ),
     *
     *     @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a tenant by ID.
     *
     * @param  int  $id  The ID of the tenant to retrieve.
     * @return JsonResponse The JSON response containing the tenant details.
     */
    public function getById(int $id): JsonResponse
    {
        try {
            // Retrieve the tenant by ID from the service
            $tenant = $this->tenantService->find($id);

            // Return JSON response with the tenant details
            return response()->json($tenant);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Get(
     *      path="/tenants/uuid/{uuid}",
     *      summary="Get a tenant by UUID",
     *      tags={"Tenants"},
     *
     *      @OA\Parameter(
     *          name="uuid",
     *          in="path",
     *          description="UUID of the tenant to retrieve",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Tenant")
     *          )
     *      ),
     *
     *     @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a tenant by UUID.
     *
     * @param  string  $uuid  The UUID of the tenant to retrieve.
     * @return JsonResponse The JSON response containing the tenant details.
     */
    public function getByUuid(string $uuid): JsonResponse
    {
        try {
            // Retrieve the tenant by ID from the service
            $tenant = $this->tenantService->findByUuid($uuid);

            // Return JSON response with the tenant details
            return response()->json($tenant);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Get(
     *      path="/tenants/domain/{domain}",
     *      summary="Get a tenant by domain",
     *      tags={"Tenants"},
     *
     *      @OA\Parameter(
     *          name="domain",
     *          in="path",
     *          description="Domain of the tenant to retrieve",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Tenant")
     *          )
     *      ),
     *
     *     @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a tenant by ID.
     *
     * @param  string  $domain  The Domain of the tenant to retrieve.
     * @return JsonResponse The JSON response containing the tenant details.
     */
    public function getByDomain(string $domain): JsonResponse
    {
        try {
            // Retrieve the tenant by ID from the service
            $tenant = $this->tenantService->findBy($domain, TenantInterface::DOMAIN);

            // Return JSON response with the tenant details
            return response()->json($tenant);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/tenants",
     *      summary="Create a new tenant",
     *      tags={"Tenants"},
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/CreateTenantDto")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Tenant")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Create a new tenant.
     *
     * @param  CreateTenantDto  $request  The validated request DTO containing the tenant data.
     * @return JsonResponse The JSON response containing the newly created tenant details.
     */
    public function create(CreateTenantDto $request): JsonResponse
    {
        try {
            // Get validated data from the request DTO
            $data = $request->validated();

            // Create a new tenant using the service
            $tenant = $this->tenantService->create($data);

            // Return JSON response with the newly created tenant details
            return response()->json($tenant);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/tenants/{tenantId}/teams/{teamId}",
     *      summary="Assign a team to a tenant",
     *      tags={"Tenants"},
     *
     *      @OA\Parameter(
     *          name="tenantId",
     *          in="path",
     *          required=true,
     *          description="ID of the tenant",
     *
     *          @OA\Schema(type="integer")
     *      ),
     *
     *      @OA\Parameter(
     *          name="teamId",
     *          in="path",
     *          required=true,
     *          description="ID of the team",
     *
     *          @OA\Schema(type="integer")
     *      ),
     *
     *       @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Tenant")),
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Assign a team to a tenant.
     *
     * @param  int  $tenantId  The ID of the tenant.
     * @param  int  $teamId  The ID of the team to assign.
     *
     * @throws Exception
     */
    public function assignTeam(int $tenantId, int $teamId): JsonResponse
    {
        try {
            // Call the service method to assign the team to the tenant
            $tenant = $this->tenantService->assignTeam($tenantId, $teamId);

            // Return JSON response with the newly created tenant details
            return response()->json($tenant);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Delete(
     *      path="/tenants/{tenantId}/teams/{teamId}",
     *      summary="Detach a team from a tenant",
     *      tags={"Tenants"},
     *
     *      @OA\Parameter(
     *          name="tenantId",
     *          in="path",
     *          required=true,
     *          description="ID of the tenant",
     *
     *          @OA\Schema(type="integer")
     *      ),
     *
     *      @OA\Parameter(
     *          name="teamId",
     *          in="path",
     *          required=true,
     *          description="ID of the team",
     *
     *          @OA\Schema(type="integer")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="integer", example=200),
     *              @OA\Property(property="message", type="string", example="Success"),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Tenant")),
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="integer", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Detach a team from a tenant.
     *
     * @param  int  $tenantId  The ID of the tenant.
     * @param  int  $teamId  The ID of the team to detach.
     *
     * @throws Exception
     */
    public function detachTeam(int $tenantId, int $teamId): JsonResponse
    {
        try {
            // Call the service method to detach the team from the tenant
            $tenant = $this->tenantService->detachTeam($tenantId, $teamId);

            // Return JSON response with the updated tenant details
            return response()->json($tenant);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/tenants/bulk",
     *      summary="Create multiple new tenants",
     *      tags={"Tenants"},
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(
     *              type="array",
     *
     *              @OA\Items(ref="#/components/schemas/Tenant")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Tenant")),
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Create multiple new tenants.
     *
     * @param  Request  $request  The HTTP request object containing the list of tenants to create.
     * @return JsonResponse The JSON response containing the newly created tenants.
     */
    public function bulkCreate(Request $request): JsonResponse
    {
        try {
            $dataList = $request->list(); // Assuming the request contains an array of tenant data
            $tenants = $this->tenantService->bulkCreate($dataList);

            return response()->json($tenants);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @OA\Put(
     *      path="/tenants/{id}",
     *      summary="Update a tenant by ID",
     *      tags={"Tenants"},
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the tenant to update",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/UpdateTenantDto")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Tenant")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Update a tenant by ID.
     *
     * @param  UpdateTenantDto  $request  The validated request DTO containing the updated tenant data.
     * @param  $id  The ID of the tenant to update.
     * @return JsonResponse The JSON response containing the updated tenant details.
     */
    public function update(UpdateTenantDto $request, int $id): JsonResponse
    {
        try {
            // Get validated data from the request DTO
            $data = $request->validated();

            // Update the tenant using the service
            $tenant = $this->tenantService->update($id, $data);

            // Return JSON response with the updated tenant details
            return response()->json($tenant);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Delete(
     *      path="/tenants/{id}",
     *      summary="Delete a tenant by ID",
     *      tags={"Tenants"},
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the tenant to delete",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Tenant")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Delete a tenant by ID.
     *
     * @param  int  $id  The ID of the tenant to delete.
     * @return JsonResponse The JSON response containing the deleted tenant details.
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            // Delete the tenant using the service
            $tenant = $this->tenantService->delete($id);

            // Return JSON response with the deleted tenant details
            return response()->json($tenant);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Put(
     *      path="/tenants/{id}/restore",
     *      summary="Restore a soft-deleted tenant by ID",
     *      tags={"Tenants"},
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the tenant to restore",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Tenant")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Restore a soft-deleted tenant by ID.
     *
     * @param  int  $id  The ID of the tenant to restore.
     * @return JsonResponse The JSON response indicating the success or failure of the restore operation.
     */
    public function restore(int $id): JsonResponse
    {
        try {
            $tenant = $this->tenantService->restore($id);

            return response()->json($tenant);
        } catch (Exception $e) {
            throw $e;
        }
    }
}
