<?php

declare(strict_types=1);

namespace Common\Request\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * Class Timestamp
 */
class Timestamp
{
    /**
     * Header name for the X-Timestamp header.
     */
    private const TIME_STAMP_HEADER = 'x-timestamp';

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request  The incoming request
     * @param  Closure  $next  The next middleware in the pipeline
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Set the X-Timestamp header with the current timestamp
        $request->headers->set(self::TIME_STAMP_HEADER, (string) now()->timestamp);

        // Call the next middleware in the pipeline with the modified request
        return $next($request);
    }
}
