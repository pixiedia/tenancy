<?php

declare(strict_types=1);

namespace Modules\Role\Interface\Data;

/**
 * Interface RoleInterface
 *
 * This interface defines constants related to role attributes.
 */
interface RoleInterface
{
    /**
     * for the roles table.
     */
    public const TABLE = 'roles';

    /**
     * for the user role table.
     */
    public const USER_ROLE_TABLE = 'user_role';

    /**
     * The name of the entity related to this table.
     */
    public const ENTITY_NAME = 'role';

    /**
     * for the roles index.
     */
    public const INDEX_NAME = 'roles_index';

    /**
     * The primary key for the roles table.
     */
    public const PRIMARY_KEY = 'id';

    /**
     * The data type of the primary key for the roles table.
     */
    public const KEY_TYPE = 'int';

    /**
     * The data type of the event object for the roles event.
     */
    public const EVENT_OBJECT = 'payload';

    /**
     * The data type of the cache tag for the roles event.
     */
    public const CACHE_TAG = 'ROLES';

    /**
     * Constant for the ID attribute.
     */
    public const ID = self::PRIMARY_KEY;

    /**
     * Constant for the UUID attribute.
     */
    public const UUID = 'uuid';

    /**
     * Constant for the name attribute.
     */
    public const NAME = 'name';

    /**
     * Constant for the slug attribute.
     */
    public const SLUG = 'slug';

    /**
     * The name of the is_active column.
     */
    public const IS_ACTIVE = 'is_active';

    /**
     * The name of the description column.
     */
    public const DESCRIPTION = 'description';

    /**
     * The name of the deleted_at column.
     */
    public const DELETED_AT = 'deleted_at';

    /**
     * The name of the created_by column.
     */
    public const CREATED_BY = 'created_by';

    /**
     * The name of the updated_by column.
     */
    public const UPDATED_BY = 'updated_by';

    /**
     * The name of the deleted_by column.
     */
    public const DELETED_BY = 'deleted_by';

    /**
     * Constant for the metadata attribute.
     */
    public const METADATA = 'metadata';

    /**
     * Constant for the permissions attribute.
     */
    public const PERMISSIONS = 'permissions';

    /**
     * Get the type of the model.
     *
     * @return string Model type.
     */
    public function getType(): string;

    /**
     * Load object data by key and field.
     *
     * @param  mixed|null  $value
     * @param  string|null  $field
     * @return $this|null
     */
    public function loadBy($value, $field = null): self;

    /**
     * Get ID.
     */
    public function getId(): int;

    /**
     * Set ID.
     *
     * @return $this
     */
    public function setId(int $id): self;

    /**
     * Get UUID.
     */
    public function getUuid(): string;

    /**
     * Set UUID.
     *
     * @return $this
     */
    public function setUuid(string $uuid): self;

    /**
     * Get the role's name.
     */
    public function getName(): string;

    /**
     * Set the role's name.
     *
     * @return $this
     */
    public function setName(string $name): self;

    /**
     * Get the value of the is_active column.
     */
    public function isActive(): bool;

    /**
     * Set the value of the is_active column.
     *
     * @return $this
     */
    public function setIsActive(bool $isActive): self;

    /**
     * Get the value of the description column.
     */
    public function getDescription(): string;

    /**
     * Set the value of the description column.
     *
     * @return $this
     */
    public function setDescription(string $description): self;

    /**
     * Get creation time.
     */
    public function getCreatedAt(): string;

    /**
     * Set creation time.
     *
     * @return $this
     */
    public function setCreatedAt(string $creationTime): self;

    /**
     * Get update time.
     */
    public function getUpdatedAt(): string;

    /**
     * Set update time.
     *
     * @return $this
     */
    public function setUpdatedAt(string $updateTime): self;

    /**
     * Get deletion time.
     */
    public function getDeletedAt(): ?string;

    /**
     * Set deletion time.
     *
     * @param  string|null  $deletionTime
     * @return $this
     */
    public function setDeletedAt($deletionTime): self;

    /**
     * Get the value of the users column.
     */
    public function getUser(): ?array;

    /**
     * Get the value of the permissions column.
     */
    public function getPermissions(): ?array;

    /**
     * Retrieves custom fields data for the entity.
     *
     * @return array|null Returns the custom fields data as an array if it's valid JSON or already an array, otherwise returns null.
     */
    public function getMetadata(): ?array;

    /**
     * Sets custom fields data for the entity.
     *
     * @param  string  $metadata  String or associative array of custom fields.
     * @return $this
     */
    public function setMetadata(string $metadata): self;

    /**
     * Get the model's attributes as an array with keys in camelCase format.
     *
     * @return array Returns an array with keys in camelCase format.
     */
    public function toDataArray(): array;

    /**
     * Update or initialize the model with data from an array.
     *
     * @param  array  $data  The array containing data to update or initialize the model.
     * @return $this Returns the updated or initialized model instance.
     */
    public function fromArray(array $data): self;
}
