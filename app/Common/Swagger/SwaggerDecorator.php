<?php

declare(strict_types=1);

namespace Common\Swagger;

/**
 * @OA\Info(
 *      title="Tenancy Service API Documentation",
 *      version="1.0.0",
 *      description="
🚀 Welcome to the Tenancy Service API! The Tenancy Service API empowers you to manage Tenancy Service effortlessly with its robust endpoints, all fortified by Bearer token authentication.

🔐 **Authentication**  
Ensure secure access to the API by including a valid Bearer token in the Authorization header of your requests.

🛣️ **API Endpoints**  
Navigate the Tenancy Service universe with these key endpoints:  
- **GET /common-service:** Retrieve a curated list of Tenancy Service.  
- **GET /common-service/{id}:** Uncover intricate details of a specific API.  
- **ENTITY /common-service:** Forge a new API.  
- **PUT /common-service/{id}:** Sculpt an existing API to perfection.  
- **DELETE /common-service/{id}:** Eliminate an API gracefully.

👤 **User Credentials**  
By default, an admin user account is configured:  
- **Username:** admin  
- **Password:** admin

🛡️ **Security Best Practices**  
Ensure a fortress around your deployment:  
- Update admin credentials for heightened security.  
- Implement encryption and follow industry security best practices.

🤝 **Support and Contact**  
Need assistance or have questions? Reach out to our support team at [support@common-service.example.com](mailto:support@common-service.example.com).

🔗 **Useful Links:**  
- [JSON Documentation](https://admin.tenancy-service.orb.local/docs/api-docs.json)  
- [YAML Documentation](https://admin.tenancy-service.orb.local/docs/api-docs.yaml)  
- [The source API definition](https://github.com/swagger-api/swagger-petstore/blob/master/src/main/resources/openapi.yaml)

🌟 **Note:**  
Dive into the Tenancy Service API with confidence and craftsmanship!"
 * )
 * @OA\Servers({
 *      @OA\Server(
 *          url="http://admin.tenancy-service.orb.local/api/V1",
 *          description="Local server"
 *      ),
 *      @OA\Server(
 *          url="https://your-production-url.com",
 *          description="Production server"
 *      )
 * })
 */
class SwaggerDecorator
{
}
