<?php

declare(strict_types=1);

namespace App\Abstractions\Interface;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface AbstractRepositoryInterface
 */
interface AbstractRepositoryInterface
{
    /**
     * Get all entities based on provided criteria.
     *
     * @param  array  $criteria  An array containing where, sort, and pagination criteria.
     *
     * @throws Exception
     */
    public function list(array $criteria = []): Collection;

    /**
     * Search for entities based on a search term, with optional filters, sorting, and pagination.
     *
     * @param  array  $criteria  The search criteria.
     */
    public function search(array $criteria = []): Collection;

    /**
     * Find a entity by ID.
     *
     * @param  int  $id  The ID of the entity to find.
     */
    public function find(int $id): ?Model;

    /**
     * Find a entity by UUID.
     *
     * @param  string  $uuid  The UUID of the entity to find.
     */
    public function findByUuid(string $uuid): ?Model;

    /**
     * Find a entity by identifier.
     *
     * @param  string  $identifier  The identifier of the entity to find.
     */
    public function findBy(string $identifier, ?string $key = null): ?Model;

    /**
     * Create a new entity.
     *
     * @param  array  $data  The data to create the entity.
     */
    public function create(array $data): Model;

    /**
     * Create multiple new entities.
     *
     * @param  array  $dataList  List of data to create entities.
     *
     * @throws Exception
     */
    public function bulkCreate(array $dataList): Collection;

    /**
     * Update a entity by ID.
     *
     * @param  int  $id  The ID of the entity to update.
     * @param  array  $data  The data to update the entity.
     */
    public function update(int $id, array $data): bool;

    /**
     * Delete a entity by ID.
     *
     * @param  int  $id  The ID of the entity to delete.
     */
    public function delete(int $id): bool;

    /**
     * Restore a soft-deleted entity by ID.
     *
     * @param  int  $id  The ID of the entity to restore.
     *
     * @throws Exception
     */
    public function restore(int $id): bool;
}
