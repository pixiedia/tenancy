<?php

declare(strict_types=1);

namespace Modules\Tenant\Enum;

use BenSampo\Enum\Enum;

/**
 * Enumeration of (Pub/Sub) topics related to DNS subscription events.
 *
 * @method static static LIST()
 * @method static static GET()
 * @method static static CREATE()
 * @method static static UPDATE()
 * @method static static DELETE()
 * @method static static RECOVERED()
 */
final class SubTopics extends Enum
{
    /**
     * Topic for getting a list of DNSs.
     *
     * @example 'tenant.list'
     */
    const LIST = 'tenant.list';

    /**
     * Topic for getting information about a single DNS.
     *
     * @example 'tenant.get'
     */
    const GET = 'tenant.get';

    /**
     * Topic for creating a new DNS.
     *
     * @example 'tenant.create'
     */
    const CREATE = 'tenant.create';

    /**
     * Topic for updating an existing DNS.
     *
     * @example 'tenant.update'
     */
    const UPDATE = 'tenant.update';

    /**
     * Topic for deleting a DNS.
     *
     * @example 'tenant.delete'
     */
    const DELETE = 'tenant.delete';

    /**
     * Topic for recovery events.
     *
     * @example 'tenant.recovered'
     */
    const RECOVERED = 'tenant.recover';
}
