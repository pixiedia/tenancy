<?php

declare(strict_types=1);

return [
    /*
    |--------------------------------------------------------------------------
    | Revision Model
    |--------------------------------------------------------------------------
    */
    'model' => Venturecraft\Revisionable\Revision::class,

    'additional_fields' => [],

];
