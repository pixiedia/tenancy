<?php

declare(strict_types=1);

use Common\Config\Service\ConfigService;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application for file storage.
    |
    */

    'default' => ConfigService::getString('FILESYSTEM_DISK', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Below you may configure as many filesystem disks as necessary, and you
    | may even configure multiple disks for the same driver. Examples for
    | most supported storage drivers are configured here for reference.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
            'throw' => false,
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => ConfigService::getString('APP_URL') . '/storage',
            'visibility' => 'public',
            'throw' => false,
        ],

        's3' => [
            'driver' => 's3',
            'key' => ConfigService::getString('AWS_ACCESS_KEY_ID'),
            'secret' => ConfigService::getString('AWS_SECRET_ACCESS_KEY'),
            'region' => ConfigService::getString('AWS_DEFAULT_REGION'),
            'bucket' => ConfigService::getString('AWS_BUCKET'),
            'url' => ConfigService::getString('AWS_URL'),
            'endpoint' => ConfigService::getString('AWS_ENDPOINT'),
            'use_path_style_endpoint' => ConfigService::getBoolean('AWS_USE_PATH_STYLE_ENDPOINT', false),
            'throw' => false,
        ],

        'minio' => [
            'driver' => 's3',
            'key' => ConfigService::getString('MINIO_ACCESS_KEY_ID'),
            'secret' => ConfigService::getString('MINIO_SECRET_ACCESS_KEY'),
            'region' => ConfigService::getString('MINIO_DEFAULT_REGION'),
            'bucket' => ConfigService::getString('MINIO_BUCKET'),
            'url' => ConfigService::getString('MINIO_URL'),
            'endpoint' => ConfigService::getString('MINIO_ENDPOINT'),
            'use_path_style_endpoint' => ConfigService::getBoolean('MINIO_USE_PATH_STYLE_ENDPOINT', false),
            'throw' => false,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('storage') => storage_path('app/public'),
    ],

];
