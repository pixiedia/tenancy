<?php

declare(strict_types=1);

namespace Modules\Team\Provider;

use App\Abstractions\AbstractModuleProvider;
use Modules\Team\Interface\Data\TeamInterface;
use Modules\Team\Interface\TeamRepositoryInterface;
use Modules\Team\Interface\TeamServiceInterface;
use Modules\Team\Model\Team;
use Modules\Team\Repository\TeamRepository;
use Modules\Team\Router\V1\TeamRoutes;
use Modules\Team\Service\TeamService;

/**
 * Class TeamModuleProvider
 */
class TeamModuleProvider extends AbstractModuleProvider
{
    /**
     * TeamModuleProvider constructor.
     *
     * Initializes the module provider with route, seeder, and migration files.
     */
    public function __construct()
    {
        // Glob all route files matching the defined path
        $this->routers = [TeamRoutes::class];

        // Define the path to the seeder files for the module
        $seederPath = base_path('app/Modules/Team/Seeder/*.php');

        // Glob all seeder files matching the defined path
        //  $this->seeders = glob($seederPath);

        // Define the path to the migration files for the module
        $migrationPath = base_path('app/Modules/Team/Migration/*.php');

        // Glob all migration files matching the defined path
        $this->migrations = glob($migrationPath);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function registerBindings()
    {
        // Bind TeamInterface to Team
        app()->bind(TeamInterface::class, Team::class);

        // Bind TeamRepositoryInterface to TeamRepository
        app()->bind(TeamRepositoryInterface::class, fn ($app) => new TeamRepository($app->make(TeamInterface::class)));

        // Bind TeamServiceInterface to TeamService
        app()->bind(TeamServiceInterface::class, fn ($app) => new TeamService($app->make(TeamRepositoryInterface::class)));
    }
}
