<?php

declare(strict_types=1);

namespace Modules\Tenant\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Tenant\Interface\Data\TenantInterface;
use Modules\Tenant\Model\Tenant;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class InitializeTenancyByDomain
 */
class InitializeTenancyByDomain
{
    /**
     * Handle an incoming request.
     *
     * @param  Closure(Request): (Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // Get the domain from the request
        $domain = $request->getHost();

        // Query the Tenant model to find a matching tenant for the domain
        $tenant = Tenant::where(TenantInterface::DOMAIN, $domain)->first();

        // If a matching tenant is found, set it on the request
        if ($tenant || $tenant->getId()) {
            $request->attributes->set(TenantInterface::ENTITY_NAME, $tenant);
        }

        // Continue processing the request
        return $next($request);
    }
}
