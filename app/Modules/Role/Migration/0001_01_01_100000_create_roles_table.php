<?php

declare(strict_types=1);

use App\Util\PHPUtil;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Role\Interface\Data\RoleInterface;
use Modules\Role\Model\Role;
use Modules\User\Interface\Data\UserInterface;

/**
 * Class CreateRolesTableMigration
 */
return new class() extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Creating "' . RoleInterface::TABLE . '" table...');

        // Create the roles table
        Schema::create(RoleInterface::TABLE, function (Blueprint $table) {
            // Primary key
            $table->id();

            // Universally Unique Identifier
            $table->uuid(RoleInterface::UUID)->comment('Universally Unique Identifier');

            // Role's name
            $table->string(RoleInterface::NAME)->comment('The name of the ' . RoleInterface::ENTITY_NAME);

            // Slug for the role
            $table->string(RoleInterface::SLUG)->unique()->nullable()->comment('Slug for the ' . RoleInterface::ENTITY_NAME);

            // Tenant status
            $table->boolean(RoleInterface::IS_ACTIVE)->comment('Status of ' . RoleInterface::ENTITY_NAME);

            // Tenant status
            $table->string(RoleInterface::DESCRIPTION)->comment(RoleInterface::ENTITY_NAME . ' description');

            // Creation timestamp (nullable)
            $table->timestamp(Role::CREATED_AT)->useCurrent()->comment('Creation timestamp');

            // Last update timestamp (nullable)
            $table->timestamp(Role::UPDATED_AT)->nullable()->comment('Last update timestamp');

            // Deletion timestamp (nullable)
            $table->timestamp(RoleInterface::DELETED_AT)->nullable()->comment('Deletion timestamp');

            // Role who last updated the tenant (nullable)
            $table->bigInteger(RoleInterface::CREATED_BY)->unsigned()->nullable()->comment('Role who last updated the ' . RoleInterface::ENTITY_NAME);

            // Role who created the tenant (nullable)
            $table->bigInteger(RoleInterface::UPDATED_BY)->nullable()->comment('Role who created the ' . RoleInterface::ENTITY_NAME);

            // Role who deleted the tenant (nullable)
            $table->bigInteger(RoleInterface::DELETED_BY)->nullable()->comment('Role who deleted the ' . RoleInterface::ENTITY_NAME);

            // Tenant metadata (nullable JSON)
            $table->json(RoleInterface::METADATA)->nullable()->comment(RoleInterface::ENTITY_NAME . ' metadata');

            // Indexes
            $table->index(RoleInterface::UUID);
            $table->index(RoleInterface::IS_ACTIVE);
            $table->index(RoleInterface::DELETED_AT);

            // Foreign keys
            $table->foreign(RoleInterface::CREATED_BY)->references(RoleInterface::PRIMARY_KEY)->on(UserInterface::TABLE)->onDelete('set null');
            $table->foreign(RoleInterface::UPDATED_BY)->references(RoleInterface::PRIMARY_KEY)->on(UserInterface::TABLE)->onDelete('set null');
            $table->foreign(RoleInterface::DELETED_BY)->references(RoleInterface::PRIMARY_KEY)->on(UserInterface::TABLE)->onDelete('set null');
        });

        // Add a comment to the 'roles' table
        Schema::table(RoleInterface::TABLE, function (Blueprint $table) {
            $table->comment(RoleInterface::TABLE . ' table');
        });

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ "' . RoleInterface::TABLE . '" table created successfully.');
    }

    /**
     * Reverse the migrations.
     *
     * Drop the 'roles' tables if they exist.
     */
    public function down(): void
    {
        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Dropping tables: ' . RoleInterface::TABLE . '...');

        // Drop the 'roles' table if it exists
        Schema::dropIfExists(RoleInterface::TABLE);

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ Tables dropped: ' . RoleInterface::TABLE . '.');
    }
};
