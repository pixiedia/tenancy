<?php

declare(strict_types=1);

namespace Modules\Role\Observer;

use Common\PubSub\Facade\PubSubService;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Modules\Role\Enum\PubTopics;
use Modules\Role\Model\Role;
use Ramsey\Uuid\Uuid;

/**
 * Class RoleObserver
 */
class RoleObserver
{
    /**
     * Handle the Role "creating" event.
     *
     * @param  Role  $role  The newly creating role.
     */
    public function creating(Role $role): void
    {
        // Log the creation of the role
        Log::info("Role creating: {$role->getId()}");

        // Generate a UUID for the role
        $uuid = Uuid::uuid4()->toString();

        // Set the generated UUID for the role
        $role->setUuid($uuid);
    }

    /**
     * Handle the Role "creating" event.
     *
     * @param  Role  $role  The newly creating role.
     */
    public function created(Role $role): void
    {
        // Log the creation of the role
        Log::info("Role created: {$role->getId()}");

        // Dispatch the created event
        Event::dispatch(PubTopics::CREATED, $role->toDataArray());

        // Pubsub the created event
        PubSubService::publish(PubTopics::CREATED, $role->toDataArray());
    }

    /**
     * Handle the Role "updated" event.
     *
     * @param  Role  $role  The updated role.
     */
    public function updated(Role $role): void
    {
        // Log the update of the role
        Log::info("Role updated: {$role->getId()}");

        // Set the updated at timestamp
        $role->setUpdatedAt(now());

        // Dispatch the updated event
        Event::dispatch(PubTopics::UPDATED, $role->toDataArray());

        // Pubsub the updated event
        PubSubService::publish(PubTopics::UPDATED, $role->toDataArray());
    }

    /**
     * Handle the Role "deleted" event.
     *
     * @param  Role  $role  The deleted role.
     */
    public function deleted(Role $role): void
    {
        // Log the deletion of the role
        Log::info("Role deleted: {$role->getId()}");

        // Set the deleted at timestamp
        $role->setDeletedAt(now());

        // Dispatch the deleted event
        Event::dispatch(PubTopics::SOFT_DELETED, $role->toDataArray());

        // Pubsub the deleted event
        PubSubService::publish(PubTopics::SOFT_DELETED, $role->toDataArray());
    }

    /**
     * Handle the Role "forceDeleted" event.
     *
     * @param  Role  $role  The force deleted role.
     */
    public function forceDeleted(Role $role): void
    {
        // Log the force deletion of the role
        Log::info("Role force deleted: {$role->getId()}");

        // Set the deleted at timestamp
        $role->setDeletedAt(now());

        // Dispatch the force deleted event
        Event::dispatch(PubTopics::DELETED, $role->toDataArray());

        // Pubsub the deleted event
        PubSubService::publish(PubTopics::DELETED, $role->toDataArray());
    }

    /**
     * Handle the Role "restored" event.
     *
     * @param  Role  $role  The restored role.
     */
    public function restored(Role $role): void
    {
        // Log the restoration of the role
        Log::info("Role restored: {$role->getId()}");

        // Reset the deleted at timestamp
        $role->setDeletedAt(null);

        // Dispatch the restored event
        Event::dispatch(PubTopics::RECOVERED, $role->toDataArray());

        // Pubsub the restored event
        PubSubService::publish(PubTopics::RECOVERED, $role->toDataArray());
    }
}
