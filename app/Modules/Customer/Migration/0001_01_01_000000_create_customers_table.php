<?php

declare(strict_types=1);

use App\Util\PHPUtil;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Customer\Interface\Data\CustomerInterface;
use Modules\Customer\Model\Customer;

/**
 * Class CreateCustomersTableMigration
 */
return new class() extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Creating "' . CustomerInterface::TABLE . '" table...');

        // Create the customers table
        Schema::create(CustomerInterface::TABLE, function (Blueprint $table) {
            // Primary key
            $table->id();

            // Universally Unique Identifier
            $table->uuid(CustomerInterface::UUID)->comment('Universally Unique Identifier');

            // Customer's name
            $table->string(CustomerInterface::NAME)->comment('The name of the ' . CustomerInterface::ENTITY_NAME);

            // Tenant status
            $table->string(CustomerInterface::STATUS)->comment(CustomerInterface::ENTITY_NAME . ' status');

            // Unique email address
            $table->string(CustomerInterface::EMAIL)->unique()->comment('The email address of the ' . CustomerInterface::ENTITY_NAME);

            // Unique phone
            $table->string(CustomerInterface::PHONE)->unique()->comment('The phone of the ' . CustomerInterface::ENTITY_NAME);

            // Timestamp for email verification
            $table->timestamp(CustomerInterface::EMAIL_VERIFIED_AT)->nullable()->comment('The timestamp when the email was verified');

            // Customer's password
            $table->string(CustomerInterface::PASSWORD)->comment('The password of the ' . CustomerInterface::ENTITY_NAME);

            // Remember token for "remember me" functionality
            $table->rememberToken();

            // Creation timestamp (nullable)
            $table->timestamp(Customer::CREATED_AT)->useCurrent()->comment('Creation timestamp');

            // Last update timestamp (nullable)
            $table->timestamp(Customer::UPDATED_AT)->nullable()->comment('Last update timestamp');

            // Deletion timestamp (nullable)
            $table->timestamp(CustomerInterface::DELETED_AT)->nullable()->comment('Deletion timestamp');

            // Tenant metadata (nullable JSON)
            $table->json(CustomerInterface::METADATA)->nullable()->comment(CustomerInterface::ENTITY_NAME . ' metadata');

            // Indexes
            $table->index(CustomerInterface::UUID);
            $table->index(CustomerInterface::STATUS);
            $table->index(CustomerInterface::DELETED_AT);
        });

        // Add a comment to the 'customers' table
        Schema::table(CustomerInterface::TABLE, function (Blueprint $table) {
            $table->comment(CustomerInterface::TABLE . ' table');
        });

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ "' . CustomerInterface::TABLE . '" table created successfully.');
    }

    /**
     * Reverse the migrations.
     *
     * Drop the 'customers' tables if they exist.
     */
    public function down(): void
    {
        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Dropping tables: ' . CustomerInterface::TABLE . '...');

        // Drop the 'customers' table if it exists
        Schema::dropIfExists(CustomerInterface::TABLE);

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ Tables dropped: ' . CustomerInterface::TABLE . '.');
    }
};
