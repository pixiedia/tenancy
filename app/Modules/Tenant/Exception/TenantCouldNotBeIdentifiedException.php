<?php

declare(strict_types=1);

namespace Modules\Tenant\Exception;

use Exception;

/**
 * Abstract base class for exceptions related to identifying tenants.
 */
abstract class TenantCouldNotBeIdentifiedException extends Exception
{
}
