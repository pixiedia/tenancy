<?php

declare(strict_types=1);

namespace Modules\Tenant\Exception;

use Exception;

/**
 * Class NotASubdomainException
 */
class NotASubdomainException extends Exception
{
    /**
     * Create a new NotASubdomainException instance.
     *
     * @param  string  $hostname  The hostname that is not a subdomain.
     */
    public function __construct(string $hostname)
    {
        // Call the parent constructor with a formatted message
        parent::__construct("Hostname {$hostname} does not include a subdomain.");
    }
}
