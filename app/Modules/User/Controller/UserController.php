<?php

declare(strict_types=1);

namespace Modules\User\Controller;

use App\Abstractions\AbstractController;
use Common\Request\Interface\Data\RequestInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\User\Dto\CreateUserDto;
use Modules\User\Dto\UpdateUserDto;
use Modules\User\Interface\Data\UserInterface;
use Modules\User\Interface\UserServiceInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Tag(
 *      name="Users",
 *      description="User API Endpoints"
 * )
 *
 * Class UserController
 */
class UserController extends AbstractController
{
    /**
     * The key used to store the search term in the request.
     */
    const SEARCH_TERM_KEY = 'searchTerm';

    /**
     * The entity name for the pagination.
     *
     * @var string|null
     */
    const ENTITY_NAME = UserInterface::ENTITY_NAME;

    /**
     * The User service interface instance.
     *
     * @var UserServiceInterface
     */
    protected $userService;

    /**
     * UserController constructor.
     */
    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @OA\Get(
     *      path="/users",
     *      summary="Get a list of users",
     *      tags={"Users"},
     *      security={ "jwt": {} },
     *
     *      @OA\Parameter(
     *          name="page",
     *          in="query",
     *          description="Page number",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=1
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/User")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a list of users.
     *
     * @param  int  $page  The page number.
     * @param  int  $size  The page size.
     * @return JsonResponse The JSON response containing the list of users.
     */
    public function getList(Request $request): JsonResponse
    {
        try {
            // Get pagination parameters from the request
            $size = (int) $request->query(RequestInterface::SIZE_KEY, (string) RequestInterface::DEFAULT_PAGE_SIZE);
            $page = (int) $request->query(RequestInterface::PAGE_KEY, (string) RequestInterface::DEFAULT_PAGE_NUMBER);

            // Validate pagination parameters
            $this->validatePaginationParams($page, $size);

            // Retrieve paginated list of users from the service
            $users = $this->userService->list($page, $size);

            // Return JSON response with the list of users
            return response()->json($users);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/users/search/{searchTerm}",
     *      summary="Get a list of users by search term",
     *      tags={"Users"},
     *      security={ "jwt": {} },
     *
     *      @OA\Parameter(
     *          name="searchTerm",
     *          in="path",
     *          description="Search Term",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="string",
     *              default="User A"
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(
     *              required={"searchTerm", "page", "size", "filters", "sortOrder"},
     *
     *              @OA\Property(property="filters", type="object", example={}),
     *              @OA\Property(property="sortOrder", type="string", enum={"asc", "desc"}, example="asc")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/User")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a list of users.
     *
     * @param  Request  $request  The HTTP request object containing search, pagination, filters, and sort parameters.
     * @return JsonResponse The JSON response containing the list of users matching the search criteria.
     */
    public function search(Request $request): JsonResponse
    {
        try {
            // Get search term from the request
            $searchTerm = $request->route(static::SEARCH_TERM_KEY);

            // Get pagination parameters from the request
            $size = (int) $request->query(RequestInterface::SIZE_KEY, (string) RequestInterface::DEFAULT_PAGE_SIZE);
            $page = (int) $request->query(RequestInterface::PAGE_KEY, (string) RequestInterface::DEFAULT_PAGE_NUMBER);

            // Validate pagination parameters
            $this->validatePaginationParams($page, $size);

            // Get filters and sort criteria from the request, defaulting to an empty array and default sort field and order if not provided
            $filters = $request->input(RequestInterface::FILTERS_KEY, []);
            $sortOrder = $request->input(RequestInterface::SORT_ORDER_KEY, RequestInterface::DEFAULT_SORT_ORDER);

            // Retrieve paginated search of users from the service based on the search term, filters, and sort criteria
            $users = $this->userService->search($searchTerm, $page, $size, $filters, $sortOrder);

            // Return JSON response with the list of users matching the search criteria
            return response()->json($users);
        } catch (Exception $e) {
            // Handle any exceptions that occur during the search and return a JSON response with an error message
            throw $e;
        }
    }

    /**
     * @OA\Get(
     *      path="/users/{id}",
     *      summary="Get a user by ID",
     *      tags={"Users"},
     *      security={ "jwt": {} },
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the user to retrieve",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/User")
     *          )
     *      ),
     *
     *     @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a user by ID.
     *
     * @param  int  $id  The ID of the user to retrieve.
     * @return JsonResponse The JSON response containing the user details.
     */
    public function getById(int $id): JsonResponse
    {
        try {
            // Retrieve the user by ID from the service
            $user = $this->userService->find($id);

            // Return JSON response with the user details
            return response()->json($user);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Get(
     *      path="/users/uuid/{uuid}",
     *      summary="Get a user by UUID",
     *      tags={"Users"},
     *      security={ "jwt": {} },
     *
     *      @OA\Parameter(
     *          name="uuid",
     *          in="path",
     *          description="UUID of the user to retrieve",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/User")
     *          )
     *      ),
     *
     *     @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a user by UUID.
     *
     * @param  string  $uuid  The UUID of the user to retrieve.
     * @return JsonResponse The JSON response containing the user details.
     */
    public function getByUuid(string $uuid): JsonResponse
    {
        try {
            // Retrieve the user by ID from the service
            $user = $this->userService->findByUuid($uuid);

            // Return JSON response with the user details
            return response()->json($user);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/users",
     *      summary="Create a new user",
     *      tags={"Users"},
     *      security={ "jwt": {} },
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/CreateUserDto")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/User")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Create a new user.
     *
     * @param  CreateUserDto  $request  The validated request DTO containing the user data.
     * @return JsonResponse The JSON response containing the newly created user details.
     */
    public function create(CreateUserDto $request): JsonResponse
    {
        try {
            // Get validated data from the request DTO
            $data = $request->validated();

            // Create a new user using the service
            $user = $this->userService->create($data);

            // Return JSON response with the newly created user details
            return response()->json($user);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/users/bulk",
     *      summary="Create multiple new users",
     *      tags={"Users"},
     *      security={ "jwt": {} },
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(
     *              type="array",
     *
     *              @OA\Items(ref="#/components/schemas/User")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/User")),
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Create multiple new users.
     *
     * @param  Request  $request  The HTTP request object containing the list of users to create.
     * @return JsonResponse The JSON response containing the newly created users.
     */
    public function bulkCreate(Request $request): JsonResponse
    {
        try {
            $dataList = $request->list(); // Assuming the request contains an array of user data
            $users = $this->userService->bulkCreate($dataList);

            return response()->json($users);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @OA\Put(
     *      path="/users/{id}",
     *      summary="Update a user by ID",
     *      tags={"Users"},
     *      security={ "jwt": {} },
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the user to update",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/UpdateUserDto")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/User")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Update a user by ID.
     *
     * @param  UpdateUserDto  $request  The validated request DTO containing the updated user data.
     * @param  $id  The ID of the user to update.
     * @return JsonResponse The JSON response containing the updated user details.
     */
    public function update(UpdateUserDto $request, int $id): JsonResponse
    {
        try {
            // Get validated data from the request DTO
            $data = $request->validated();

            // Update the user using the service
            $user = $this->userService->update($id, $data);

            // Return JSON response with the updated user details
            return response()->json($user);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Delete(
     *      path="/users/{id}",
     *      summary="Delete a user by ID",
     *      tags={"Users"},
     *      security={ "jwt": {} },
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the user to delete",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/User")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Delete a user by ID.
     *
     * @param  int  $id  The ID of the user to delete.
     * @return JsonResponse The JSON response containing the deleted user details.
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            // Delete the user using the service
            $user = $this->userService->delete($id);

            // Return JSON response with the deleted user details
            return response()->json($user);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Put(
     *      path="/users/{id}/restore",
     *      summary="Restore a soft-deleted user by ID",
     *      tags={"Users"},
     *      security={ "jwt": {} },
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the user to restore",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/User")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Restore a soft-deleted user by ID.
     *
     * @param  int  $id  The ID of the user to restore.
     * @return JsonResponse The JSON response indicating the success or failure of the restore operation.
     */
    public function restore(int $id): JsonResponse
    {
        try {
            $user = $this->userService->restore($id);

            return response()->json($user);
        } catch (Exception $e) {
            throw $e;
        }
    }
}
