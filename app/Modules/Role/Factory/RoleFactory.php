<?php

declare(strict_types=1);

namespace Modules\Role\Factory;

use App\Util\PHPUtil;
use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Role\Interface\Data\RoleInterface;
use Modules\Role\Model\Role;
use RuntimeException;

/**
 * Class RoleFactory
 */
class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * This method defines the default attributes for the Role model when creating fake data.
     * It uses the Faker library to generate random values for the attributes.
     *
     * @return array The default attributes for the Role model.
     */
    public function definition()
    {
        // Return an array containing the default attributes for the Role model
        return [
            // Generate a random UUID for the role
            RoleInterface::UUID => $this->faker->uuid,

            // Generate a random name for the role
            RoleInterface::NAME => $this->faker->words(3, true),

            // Generate a random status for the role (active or inactive)
            RoleInterface::IS_ACTIVE => $this->faker->boolean(),

            // Generate a random description for the role
            RoleInterface::DESCRIPTION => $this->faker->text(100),

            // Generate a random value between 1 and 10 for the created_by field
            RoleInterface::CREATED_BY => $this->faker->numberBetween(1, 10),

            // Generate a random value between 1 and 10 for the updated_by field
            RoleInterface::UPDATED_BY => $this->faker->numberBetween(1, 10),

            // Generate random metadata for the role
            RoleInterface::METADATA => $this->getMetadata(),
        ];
    }

    /**
     * Get a randomly generated metadata array.
     *
     * @return string The generated metadata array.
     *
     * @throws RuntimeException If the metadata array fails to encode as JSON.
     */
    private function getMetadata(): string
    {
        // Initialize an empty array to store metadata
        $metadata = [];

        // Generate metadata with random keys and values
        for ($i = 0; $i < 10; $i++) {
            // Generate a random word as the key and another random word as the value
            $metadata[$this->faker->word] = $this->faker->word;
        }

        // Encode the metadata array as JSON
        $encodedMetadata = PHPUtil::jsonEncode($metadata);

        // Check if encoding was successful
        if ($encodedMetadata === false) {
            throw new RuntimeException('Failed to encode metadata as JSON: ' . json_last_error_msg());
        }

        return $encodedMetadata;
    }
}
