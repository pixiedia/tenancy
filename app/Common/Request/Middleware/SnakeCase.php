<?php

declare(strict_types=1);

namespace Common\Request\Middleware;

use App\Util\KeyCaseConverter;
use Closure;
use Illuminate\Http\Request;

/**
 * Class SnakeCase
 */
class SnakeCase
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request  The incoming request
     * @param  Closure  $next  The next middleware in the pipeline
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Resolve an instance of KeyCaseConverter from the service container
        $keyCaseConverter = resolve(KeyCaseConverter::class);

        // Convert the request data keys to snake_case
        $convertedData = $keyCaseConverter->convert(
            KeyCaseConverter::CASE_SNAKE,
            $request->all()
        );

        // Replace the request data with the converted data
        $request->replace($convertedData);

        // Call the next middleware in the pipeline with the modified request
        return $next($request);
    }
}
