<?php

declare(strict_types=1);

namespace Modules\Team\Migration;

use App\Util\PHPUtil;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Team\Interface\Data\TeamInterface;
use Modules\Team\Model\Team;
use Modules\User\Interface\Data\UserInterface;

/**
 * Class CreateTeamsTableMigration
 */
return new class() extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Log the start of the migration
        PHPUtil::consoleOutput('🔨 Creating "' . TeamInterface::TABLE . '" table... ');

        // Create the teams table
        Schema::create(TeamInterface::TABLE, function (Blueprint $table) {
            $table->id();
            // Universally Unique Identifier
            $table->uuid(TeamInterface::UUID)->comment('Universally Unique Identifier');

            // Team name
            $table->string(TeamInterface::NAME)->comment(TeamInterface::ENTITY_NAME . ' name');

            // Slug for the team
            $table->string(TeamInterface::SLUG)->unique()->nullable()->comment('Slug for the ' . TeamInterface::ENTITY_NAME);

            // Creation timestamp (nullable)
            $table->timestamp(Team::CREATED_AT)->useCurrent()->comment('Creation timestamp');

            // Last update timestamp (nullable)
            $table->timestamp(Team::UPDATED_AT)->nullable()->comment('Last update timestamp');

            // Deletion timestamp (nullable)
            $table->softDeletes();

            // $table->timestamp(TeamInterface::DELETED_AT)->nullable()->comment('Deletion timestamp');

            // User who last updated the team (nullable)
            $table->bigInteger(TeamInterface::CREATED_BY)->unsigned()->nullable()->comment('User who last updated the ' . TeamInterface::ENTITY_NAME);

            // User who created the team (nullable)
            $table->bigInteger(TeamInterface::UPDATED_BY)->nullable()->comment('User who created the ' . TeamInterface::ENTITY_NAME);

            // User who deleted the team (nullable)
            $table->bigInteger(TeamInterface::DELETED_BY)->nullable()->comment('User who deleted the ' . TeamInterface::ENTITY_NAME);

            // Team metadata (nullable JSON)
            $table->json(TeamInterface::METADATA)->nullable()->comment(TeamInterface::ENTITY_NAME . ' metadata');

            // Indexes
            $table->index(TeamInterface::UUID);
            $table->index(TeamInterface::DELETED_AT);

            // Foreign keys
            $table->foreign(TeamInterface::CREATED_BY)->references(TeamInterface::PRIMARY_KEY)->on(UserInterface::TABLE)->onDelete('set null');
            $table->foreign(TeamInterface::UPDATED_BY)->references(TeamInterface::PRIMARY_KEY)->on(UserInterface::TABLE)->onDelete('set null');
            $table->foreign(TeamInterface::DELETED_BY)->references(TeamInterface::PRIMARY_KEY)->on(UserInterface::TABLE)->onDelete('set null');
        });

        // Add a comment to the 'team' table
        Schema::table(TeamInterface::TABLE, function (Blueprint $table) {
            $table->comment(TeamInterface::TABLE . ' table');
        });

        // Log the completion of the migration
        PHPUtil::consoleOutput('✅ "' . TeamInterface::TABLE . '" table created successfully.');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Log the start of the rollback
        PHPUtil::consoleOutput('🗑️ Dropping "' . TeamInterface::TABLE . '" table...');

        // Drop the teams table if it exists
        Schema::dropIfExists(TeamInterface::TABLE);

        // Log the completion of the rollback
        PHPUtil::consoleOutput('✅ "' . TeamInterface::TABLE . '" table dropped successfully.');
    }
};
