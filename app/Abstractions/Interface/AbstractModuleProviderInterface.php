<?php

declare(strict_types=1);

namespace App\Abstractions\Interface;

/**
 * Interface AbstractModuleProviderInterface
 *
 * This interface defines the contract for a module provider that registers routes, middlewares, migrations, and seeders for a module.
 */
interface AbstractModuleProviderInterface
{
    /**
     * Register the module's service bindings.
     *
     * This method should be implemented in concrete module providers to bind interfaces to their implementations.
     *
     * @return void
     */
    public function registerBindings();

    /**
     * Register the tenancy-related routes.
     *
     * This method iterates over each version of the API and defines routes for that version.
     *
     * @return void
     */
    public function registerRoutes();

    /**
     * Register the tenancy-related middlewares.
     *
     * @return $this
     */
    public function registerMiddlewares();

    /**
     * Register the tenancy-related migrations.
     *
     * This method loads all migration files and registers them with the Laravel migration system.
     *
     * @return void
     */
    public function registerMigrations();

    /**
     * Register the tenancy-related seeders.
     *
     * This method loads all seeder files and registers them with the Laravel seeder system.
     *
     * @return void
     */
    public function registerSeeders();
}
