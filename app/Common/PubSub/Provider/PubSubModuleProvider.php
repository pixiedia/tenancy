<?php

declare(strict_types=1);

namespace Common\PubSub\Provider;

use App\Abstractions\AbstractModuleProvider;
use Common\PubSub\Facade\PubSubService as PubSubServiceFacade;
use Common\PubSub\Service\PubSubService;

/**
 * Class PubSubModuleProvider
 */
class PubSubModuleProvider extends AbstractModuleProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function registerBindings()
    {
        // Bind PubSubServiceFacade to HttpService
        app()->bind(PubSubServiceFacade::PUB_SUB_SERVICE, fn ($app) => new PubSubService());
    }
}
