#!/usr/bin/env bash

###########################################################
# Script for tagging releases in a Git repository         #
#                                                         #
# Usage:                                                  #
#   tag-release.sh <version>                              #
#                                                         #
#   <version>: Version number to tag the release with.    #
###########################################################

# Check if there are any uncommitted changes
if [[ $(git status --porcelain) ]]; then
  echo 'ERROR: Please commit all changes before tagging a new version.'
  exit 1
fi

# Check if a version number is provided
if [ -z "$1" ]; then
  echo 'ERROR: You must supply a version number.'
  exit 1
fi

# Assign the version number provided as the tag
TAG=$1
echo "$TAG" > .version

# Add all changes, commit with version number, and push to the repository
git -c color.ui=always add .
git -c color.ui=always commit -m "chore(release): bump version to ${TAG}"
git -c color.ui=always push

# Tag the commit with the version number and force update the 'latest' tag
git -c color.ui=always tag "$TAG"
git -c color.ui=always tag latest -f
git -c color.ui=always push --tags -f

# Print a message indicating the tagging is complete
echo "${TAG} tagged. Now go to ${REPO_URL} and finish the draft release."
