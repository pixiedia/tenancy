<?php

declare(strict_types=1);

namespace Modules\Permission\Repository;

use App\Abstractions\AbstractRepository;
use Exception;
use Modules\Permission\Interface\Data\PermissionInterface;
use Modules\Permission\Interface\PermissionRepositoryInterface;
use Modules\Permission\Model\Permission;

/**
 * Class PermissionRepository
 *
 * This class implements the PermissionRepositoryInterface and provides methods to interact with the Permission model.
 */
class PermissionRepository extends AbstractRepository implements PermissionRepositoryInterface
{
    /**
     * PermissionRepository constructor.
     *
     * @param  Permission  $model  The Permission model instance to use.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(PermissionInterface $model)
    {
        // Call the parent constructor with the model and the model interface
        parent::__construct($model, PermissionInterface::class);

        // Assign the model instance to the class property
        $this->model = $model;
    }
}
