<?php

declare(strict_types=1);

namespace Modules\Role\Model;

use App\Casts\Json;
use App\Util\PHPUtil;
use Cviebrock\EloquentSluggable\Sluggable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Attributes\ObservedBy;
use Illuminate\Database\Eloquent\BroadcastsEvents;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Modules\Permission\Model\Permission;
use Modules\Role\Factory\RoleFactory;
use Modules\Role\Interface\Data\RoleInterface;
use Modules\Role\Observer\RoleObserver;
use Modules\User\Model\User;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable;
use Venturecraft\Revisionable\RevisionableTrait;
use Wildside\Userstamps\Userstamps;

/**
 * @OA\Schema(
 *      schema="Role",
 *      required={"id", "uuid", "name", "created_at", "updated_at", "deleted_at", "created_by", "updated_by", "deleted_by", "metadata"},
 *
 *      @OA\Property(
 *          property="id",
 *          type="integer",
 *          format="int64",
 *          example=1
 *      ),
 *      @OA\Property(
 *          property="uuid",
 *          type="string",
 *          example="d3d29d70-1d25-11e3-8591-034165a3a613"
 *      ),
 *      @OA\Property(
 *          property="name",
 *          type="string",
 *          example="Role A"
 *      ),
 *      @OA\Property(
 *          property="is_active",
 *          type="boolean",
 *          example="false"
 *      ),
 *      @OA\Property(
 *          property="created_at",
 *          type="string",
 *          format="date-time",
 *          example="2024-03-25 10:00:00"
 *      ),
 *      @OA\Property(
 *          property="updated_at",
 *          type="string",
 *          format="date-time",
 *          example="2024-03-25 10:00:00"
 *      ),
 *      @OA\Property(
 *          property="deleted_at",
 *          type="string",
 *          format="date-time",
 *          example="2024-03-25 10:00:00"
 *      ),
 *      @OA\Property(
 *          property="created_by",
 *          type="integer",
 *          example=1
 *      ),
 *      @OA\Property(
 *          property="updated_by",
 *          type="integer",
 *          example=1
 *      ),
 *      @OA\Property(
 *          property="deleted_by",
 *          type="integer",
 *          example=1
 *      ),
 *     @OA\Property(
 *          property="metadata",
 *          type="object",
 *          additionalProperties=true,
 *          example={"key1": "value1", "key2": "value2"}
 *      )
 * )
 *
 * Class Role
 *
 * @property int $id
 * @property string $uuid Universally Unique Identifier
 * @property string $name tenant name
 * @property string $status tenant status
 * @property string|null $domain tenant domain
 * @property \Illuminate\Support\Carbon $created_at Creation timestamp
 * @property \Illuminate\Support\Carbon|null $updated_at Last update timestamp
 * @property \Illuminate\Support\Carbon|null $deleted_at Deletion timestamp
 * @property int|null $created_by User who last updated the tenant
 * @property int|null $updated_by User who created the tenant
 * @property int|null $deleted_by User who deleted the tenant
 * @property mixed|null $metadata tenant metadata
 * @property bool $is_active Status of role
 * @property string $description role description
 */
#[ObservedBy([RoleObserver::class])]
class Role extends Model implements Auditable, RoleInterface
{
    use AuditableTrait, BroadcastsEvents, Cachable, HasFactory, RevisionableTrait, Sluggable, SoftDeletes, Userstamps;

    /**
     * The name of the "created at" column.
     *
     * @var string|null
     */
    const CREATED_AT = 'created_at';

    /**
     * The name of the "updated at" column.
     *
     * @var string|null
     */
    const UPDATED_AT = 'updated_at';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The table name for the model.
     *
     * @var string
     */
    protected $table = self::TABLE;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = self::PRIMARY_KEY;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = self::KEY_TYPE;

    /**
     * Parameter name in event.
     *
     * In observe method you can use "eventObject" in this case.
     *
     * @var string
     */
    protected $eventObject = self::EVENT_OBJECT;

    /**
     * The cache prefix for the cache key.
     *
     * @var string
     */
    protected $cachePrefix = self::CACHE_TAG;

    /**
     * The number of seconds for the cache cooldown (1 hour).
     *
     * @var int
     */
    protected $cacheCooldownSeconds = 3600; // 1 hour

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::UUID,
        self::NAME,
        self::DESCRIPTION,
        self::IS_ACTIVE,
        self::CREATED_AT,
        self::UPDATED_AT,
        self::DELETED_AT,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::DELETED_BY,
        self::METADATA,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::METADATA => Json::class,
    ];

    /**
     * Return the sluggable configuration array for this model.
     */
    public function sluggable(): array
    {
        return [
            static::SLUG => [
                'source' => static::NAME,
            ],
        ];
    }

    /**
     * Get the type of the model.
     *
     * @return string Model type.
     */
    public function getType(): string
    {
        return Str::lower(PHPUtil::getClassName(Role::class));
    }

    /**
     * Define a many-to-many relationship between the role and users.
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, RoleInterface::USER_ROLE_TABLE);
    }

    /**
     * Define a many-to-many relationship between the role and permissions.
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * Load object data by key and field.
     *
     * @param  mixed|null  $value
     * @param  string|null  $field
     * @return $this|null
     */
    public function loadBy($value, $field = null): self
    {
        // NOTE: Load the object data by field using the model
        $this->firstWhere($field, '=', $value);

        // NOTE: Return the object for method chaining
        return $this;
    }

    /**
     * Find a model by its primary key or return null if not found.
     *
     * @param  mixed  $id
     * @param  array|string  $columns
     * @return Model|Collection|static|static[]|null
     */
    public function findOrFail($id, $columns = ['*'])
    {
        $result = $this->find($id, $columns);

        $id = $id instanceof Arrayable ? $id->toArray() : $id;

        if (PHPUtil::isArray($id)) {
            if (count($result) !== count(array_unique($id))) {
                return null;
            }

            return $result;
        }

        return $result;
    }

    /**
     * Get ID.
     */
    public function getId(): int
    {
        return (int) $this->__get($this->primaryKey);
    }

    /**
     * Set ID.
     *
     * @param  int  $id
     * @return $this
     */
    public function setId($id): self
    {
        $this->__set($this->primaryKey, $id);

        return $this;
    }

    /**
     * Get UUID.
     */
    public function getUuid(): string
    {
        return $this->__get(static::UUID);
    }

    /**
     * Set UUID.
     *
     * @param  string  $uuid
     * @return $this
     */
    public function setUuid($uuid): self
    {
        $this->__set(static::UUID, $uuid);

        return $this;
    }

    /**
     * Get the role's name.
     */
    public function getName(): string
    {
        return $this->__get(static::NAME);
    }

    /**
     * Set the role's name.
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->__set(static::NAME, $name);

        return $this;
    }

    /**
     * Get the value of the description column.
     */
    public function getDescription(): string
    {
        return $this->__get(static::DESCRIPTION);
    }

    /**
     * Set the value of the description column.
     *
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->__set(static::DESCRIPTION, $description);

        return $this;
    }

    /**
     * Get the value of the is_active column.
     */
    public function isActive(): bool
    {
        return (bool) $this->__get(static::IS_ACTIVE);
    }

    /**
     * Set the value of the is_active column.
     *
     * @return $this
     */
    public function setIsActive(bool $isActive): self
    {
        $this->__set(static::IS_ACTIVE, $isActive);

        return $this;
    }

    /**
     * Get creation time.
     */
    public function getCreatedAt(): string
    {
        return $this->__get(static::CREATED_AT);
    }

    /**
     * Set creation time.
     *
     * @param  string  $creationTime
     * @return $this
     */
    public function setCreatedAt($creationTime): self
    {
        $this->__set(static::CREATED_AT, $creationTime);

        return $this;
    }

    /**
     * Get update time.
     */
    public function getUpdatedAt(): string
    {
        return $this->__get(static::UPDATED_AT);
    }

    /**
     * Set update time.
     *
     * @param  string  $updateTime
     * @return $this
     */
    public function setUpdatedAt($updateTime): self
    {
        $this->__set(static::UPDATED_AT, $updateTime);

        return $this;
    }

    /**
     * Get deletion time.
     */
    public function getDeletedAt(): ?string
    {
        return $this->__get(static::DELETED_AT);
    }

    /**
     * Set deletion time.
     *
     * @param  string|null  $deletionTime
     * @return $this
     */
    public function setDeletedAt($deletionTime): self
    {
        $this->__set(static::DELETED_AT, $deletionTime);

        return $this;
    }

    /**
     * Get the value of the users column.
     */
    public function getUser(): ?array
    {
        return $this->users;
    }

    /**
     * Get the value of the permissions column.
     */
    public function getPermissions(): ?array
    {
        return []; //$this->permissions;
    }

    /**
     * Retrieves custom fields data for the entity.
     *
     * @return array|null Returns the custom fields data as an array if it's valid JSON or already an array, otherwise returns null.
     */
    public function getMetadata(): ?array
    {
        // If $metadata is not provided, use the value of the METADATA attribute
        $metadata = $this->__get(static::METADATA);

        // Return null if $metadata is not a valid JSON string or array
        return PHPUtil::isString($metadata) ? (array) PHPUtil::jsonDecode($metadata) : (array) $metadata;
    }

    /**
     * Sets custom fields data for the entity.
     *
     * @param  string  $metadata  String or associative array of custom fields.
     * @return $this
     */
    public function setMetadata(string $metadata): self
    {
        $this->__set(static::METADATA, $metadata);

        return $this;
    }

    /**
     * Get the model's attributes as an array with keys in camelCase format.
     *
     * @return array Returns an array with keys in camelCase format.
     */
    public function toDataArray(): array
    {
        // Initialize an empty array to store the converted data
        $dataArray = [];

        // Iterate over each attribute in the model
        foreach ($this->toArray() as $key => $value) {
            // Convert the attribute key to camelCase
            $camelCaseKey = Str::camel($key);

            // Add the attribute to the new array with the camelCase key
            $dataArray[$camelCaseKey] = $value;
        }

        // Add the metadata key and value pair to the dataArray
        $metadata = $this->getMetadata();
        if (! is_null($metadata)) {
            $dataArray[RoleInterface::METADATA] = $metadata;
        }

        // Return the array with keys in camelCase format
        return $dataArray;
    }

    /**
     * Update or initialize the model with data from an array.
     *
     * @param  array  $data  The array containing data to update or initialize the model.
     * @return $this Returns the updated or initialized model instance.
     */
    public function fromArray(array $data): self
    {
        // Iterate over each key-value pair in the $data array
        foreach ($data as $key => $value) {
            // Use setter methods if available or set data directly

            // Generate the setter method name based on the key
            $setterMethod = 'set' . Str::camel($key);

            // Check if the setter method exists in the model
            if (method_exists($this, $setterMethod)) {
                // Call the setter method to set the value
                $this->{$setterMethod}($value);
            } else {
                // If no setter method exists, set the data directly
                $this->__set($key, $value);
            }
        }

        // Return the updated or initialized model instance
        return $this;
    }

    /**
     * Create a new factory instance for the model.
     */
    protected static function newFactory(): Factory
    {
        return RoleFactory::new();
    }
}
