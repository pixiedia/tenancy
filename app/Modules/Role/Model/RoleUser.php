<?php

declare(strict_types=1);

namespace Modules\Role\Model;

use Illuminate\Database\Eloquent\Model;
use Modules\Role\Interface\Data\RoleUserInterface;

/**
 * Class RoleUser
 */
class RoleUser extends Model implements RoleUserInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = RoleUserInterface::TABLE;
}
