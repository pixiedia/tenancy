<?php

declare(strict_types=1);

namespace Modules\Team\Enum;

use BenSampo\Enum\Enum;

/**
 * Enumeration of (Pub/Sub) topics related to publish events.
 *
 * @method static static CREATED()
 * @method static static UPDATED()
 * @method static static DELETED()
 * @method static static SOFT_DELETED()
 * @method static static RECOVERED()
 */
final class PubTopics extends Enum
{
    /**
     * Topic for creation events.
     *
     * @example 'team.created'
     */
    const CREATED = 'team.created';

    /**
     * Topic for update events.
     *
     * @example 'team.updated'
     */
    const UPDATED = 'team.updated';

    /**
     * Topic for deletion events.
     *
     * @example 'team.deleted'
     */
    const DELETED = 'team.deleted';

    /**
     * Topic for soft deletion events.
     *
     * @example 'soft.team.deleted'
     */
    const SOFT_DELETED = 'team.soft.deleted';

    /**
     * Topic for recovery events.
     *
     * @example 'team.recovered'
     */
    const RECOVERED = 'team.recovered';
}
