<?php

declare(strict_types=1);

namespace Modules\User\Enum;

use BenSampo\Enum\Enum;

/**
 * Enumeration of (Pub/Sub) topics related to publish events.
 *
 * @method static static CREATED()
 * @method static static UPDATED()
 * @method static static DELETED()
 * @method static static SOFT_DELETED()
 * @method static static RECOVERED()
 */
final class PubTopics extends Enum
{
    /**
     * Topic for creation events.
     *
     * @example 'user.created'
     */
    const CREATED = 'user.created';

    /**
     * Topic for update events.
     *
     * @example 'user.updated'
     */
    const UPDATED = 'user.updated';

    /**
     * Topic for deletion events.
     *
     * @example 'user.deleted'
     */
    const DELETED = 'user.deleted';

    /**
     * Topic for soft deletion events.
     *
     * @example 'soft.user.deleted'
     */
    const SOFT_DELETED = 'user.soft.deleted';

    /**
     * Topic for recovery events.
     *
     * @example 'user.recovered'
     */
    const RECOVERED = 'user.recovered';
}
