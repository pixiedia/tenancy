<?php

declare(strict_types=1);

namespace Common\Http\Enum;

use BenSampo\Enum\Enum;

/**
 * Interface HttpStatusCode
 *
 * Represents standard HTTP status codes as constants.
 *
 * @method static static CONTINUE()
 * @method static static SWITCHING_PROTOCOLS()
 * @method static static PROCESSING()
 * @method static static OK()
 * @method static static CREATED()
 * @method static static ACCEPTED()
 * @method static static NON_AUTHORITATIVE_INFORMATION()
 * @method static static NO_CONTENT()
 * @method static static RESET_CONTENT()
 * @method static static PARTIAL_CONTENT()
 * @method static static MULTI_STATUS()
 * @method static static ALREADY_REPORTED()
 * @method static static IM_USED()
 * @method static static MULTIPLE_CHOICES()
 * @method static static MOVED_PERMANENTLY()
 * @method static static FOUND()
 * @method static static SEE_OTHER()
 * @method static static NOT_MODIFIED()
 * @method static static USE_PROXY()
 * @method static static RESERVED()
 * @method static static TEMPORARY_REDIRECT()
 * @method static static PERMANENT_REDIRECT()
 * @method static static BAD_REQUEST()
 * @method static static UNAUTHORIZED()
 * @method static static PAYMENT_REQUIRED()
 * @method static static FORBIDDEN()
 * @method static static NOT_FOUND()
 * @method static static METHOD_NOT_ALLOWED()
 * @method static static NOT_ACCEPTABLE()
 * @method static static PROXY_AUTHENTICATION_REQUIRED()
 * @method static static REQUEST_TIMEOUT()
 * @method static static CONFLICT()
 * @method static static GONE()
 * @method static static LENGTH_REQUIRED()
 * @method static static PRECONDITION_FAILED()
 * @method static static PAYLOAD_TOO_LARGE()
 * @method static static URI_TOO_LONG()
 * @method static static UNSUPPORTED_MEDIA_TYPE()
 * @method static static RANGE_NOT_SATISFIABLE()
 * @method static static EXPECTATION_FAILED()
 * @method static static IM_A_TEAPOT()
 * @method static static MISDIRECTED_REQUEST()
 * @method static static UNPROCESSABLE_ENTITY()
 * @method static static LOCKED()
 * @method static static FAILED_DEPENDENCY()
 * @method static static RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL()
 * @method static static TOO_EARLY()
 * @method static static UPGRADE_REQUIRED()
 * @method static static PRECONDITION_REQUIRED()
 * @method static static TOO_MANY_REQUESTS()
 * @method static static REQUEST_HEADER_FIELDS_TOO_LARGE()
 * @method static static UNAVAILABLE_FOR_LEGAL_REASONS()
 * @method static static INTERNAL_SERVER_ERROR()
 * @method static static NOT_IMPLEMENTED()
 * @method static static BAD_GATEWAY()
 * @method static static SERVICE_UNAVAILABLE()
 * @method static static GATEWAY_TIMEOUT()
 * @method static static VERSION_NOT_SUPPORTED()
 * @method static static VARIANT_ALSO_NEGOTIATES()
 * @method static static INSUFFICIENT_STORAGE()
 * @method static static LOOP_DETECTED()
 * @method static static NOT_EXTENDED()
 * @method static static NETWORK_AUTHENTICATION_REQUIRED()
 */
final class HttpStatusCode extends Enum
{
    /**
     * Informational 1xx
     */

    /**
     * Continue (100)
     *
     * @var int
     */
    const CONTINUE = 100;

    /**
     * Switching Protocols (101)
     *
     * @var int
     */
    const SWITCHING_PROTOCOLS = 101;

    /**
     * Processing (RFC2518) (102)
     *
     * @var int
     */
    const PROCESSING = 102;

    /**
     * Successful 2xx
     */

    /**
     * OK (200)
     *
     * @var int
     */
    const OK = 200;

    /**
     * Created (201)
     *
     * @var int
     */
    const CREATED = 201;

    /**
     * Accepted (202)
     *
     * @var int
     */
    const ACCEPTED = 202;

    /**
     * Non-Authoritative Information (203)
     *
     * @var int
     */
    const NON_AUTHORITATIVE_INFORMATION = 203;

    /**
     * No Content (204)
     *
     * @var int
     */
    const NO_CONTENT = 204;

    /**
     * Reset Content (205)
     *
     * @var int
     */
    const RESET_CONTENT = 205;

    /**
     * Partial Content (206)
     *
     * @var int
     */
    const PARTIAL_CONTENT = 206;

    /**
     * Multi-Status (RFC4918) (207)
     *
     * @var int
     */
    const MULTI_STATUS = 207;

    /**
     * Already Reported (RFC5842) (208)
     *
     * @var int
     */
    const ALREADY_REPORTED = 208;

    /**
     * IM Used (RFC3229) (226)
     *
     * @var int
     */
    const IM_USED = 226;

    /**
     * Redirection 3xx
     */

    /**
     * Multiple Choices (300)
     *
     * @var int
     */
    const MULTIPLE_CHOICES = 300;

    /**
     * Moved Permanently (301)
     *
     * @var int
     */
    const MOVED_PERMANENTLY = 301;

    /**
     * Found (302)
     *
     * @var int
     */
    const FOUND = 302;

    /**
     * See Other (303)
     *
     * @var int
     */
    const SEE_OTHER = 303;

    /**
     * Not Modified (304)
     *
     * @var int
     */
    const NOT_MODIFIED = 304;

    /**
     * Use Proxy (305)
     *
     * @var int
     */
    const USE_PROXY = 305;

    /**
     * Reserved (306)
     *
     * @var int
     */
    const RESERVED = 306;

    /**
     * Temporary Redirect (307)
     *
     * @var int
     */
    const TEMPORARY_REDIRECT = 307;

    /**
     * Permanent Redirect (RFC7238) (308)
     *
     * @var int
     */
    const PERMANENT_REDIRECT = 308;

    /**
     * Client Error 4xx
     */

    /**
     * Bad Request (400)
     *
     * @var int
     */
    const BAD_REQUEST = 400;

    /**
     * Unauthorized (401)
     *
     * @var int
     */
    const UNAUTHORIZED = 401;

    /**
     * Payment Required (402)
     *
     * @var int
     */
    const PAYMENT_REQUIRED = 402;

    /**
     * Forbidden (403)
     *
     * @var int
     */
    const FORBIDDEN = 403;

    /**
     * Not Found (404)
     *
     * @var int
     */
    const NOT_FOUND = 404;

    /**
     * Method Not Allowed (405)
     *
     * @var int
     */
    const METHOD_NOT_ALLOWED = 405;

    /**
     * Not Acceptable (406)
     *
     * @var int
     */
    const NOT_ACCEPTABLE = 406;

    /**
     * Proxy Authentication Required (407)
     *
     * @var int
     */
    const PROXY_AUTHENTICATION_REQUIRED = 407;

    /**
     * Request Timeout (408)
     *
     * @var int
     */
    const REQUEST_TIMEOUT = 408;

    /**
     * Conflict (409)
     *
     * @var int
     */
    const CONFLICT = 409;

    /**
     * Gone (410)
     *
     * @var int
     */
    const GONE = 410;

    /**
     * Length Required (411)
     *
     * @var int
     */
    const LENGTH_REQUIRED = 411;

    /**
     * Precondition Failed (412)
     *
     * @var int
     */
    const PRECONDITION_FAILED = 412;

    /**
     * Payload Too Large (413)
     *
     * @var int
     */
    const PAYLOAD_TOO_LARGE = 413;

    /**
     * URI Too Long (414)
     *
     * @var int
     */
    const URI_TOO_LONG = 414;

    /**
     * Unsupported Media Type (415)
     *
     * @var int
     */
    const UNSUPPORTED_MEDIA_TYPE = 415;

    /**
     * Range Not Satisfiable (416)
     *
     * @var int
     */
    const RANGE_NOT_SATISFIABLE = 416;

    /**
     * Expectation Failed (417)
     *
     * @var int
     */
    const EXPECTATION_FAILED = 417;

    /**
     * I'm a teapot (RFC2324) (418)
     *
     * @var int
     */
    const IM_A_TEAPOT = 418;

    /**
     * Misdirected Request (421)
     *
     * @var int
     */
    const MISDIRECTED_REQUEST = 421;

    /**
     * Unprocessable Entity (RFC4918) (422)
     *
     * @var int
     */
    const UNPROCESSABLE_ENTITY = 422;

    /**
     * Locked (RFC4918) (423)
     *
     * @var int
     */
    const LOCKED = 423;

    /**
     * Failed Dependency (RFC4918) (424)
     *
     * @var int
     */
    const FAILED_DEPENDENCY = 424;

    /**
     * Deprecated as of RFC8470 specification.
     * The 425 HTTP status code has become experimental and has been given a name.
     * Use `TOO_EARLY` instead.
     */

    /**
     * Reserved for WebDAV Advanced Collections Expired Proposal (RFC2817) (425)
     *
     * @deprecated As of RFC8470 specification, use `TOO_EARLY` instead.
     *
     * @var int
     */
    const RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL = 425;

    /**
     * Too Early (RFC8470) (425)
     *
     * @var int
     */
    const TOO_EARLY = 425;

    /**
     * Upgrade Required (RFC2817) (426)
     *
     * @var int
     */
    const UPGRADE_REQUIRED = 426;

    /**
     * Precondition Required (RFC6585) (428)
     *
     * @var int
     */
    const PRECONDITION_REQUIRED = 428;

    /**
     * Too Many Requests (RFC6585) (429)
     *
     * @var int
     */
    const TOO_MANY_REQUESTS = 429;

    /**
     * Request Header Fields Too Large (RFC6585) (431)
     *
     * @var int
     */
    const REQUEST_HEADER_FIELDS_TOO_LARGE = 431;

    /**
     * Unavailable For Legal Reasons (451)
     *
     * @var int
     */
    const UNAVAILABLE_FOR_LEGAL_REASONS = 451;

    /**
     * Internal Server Error (500)
     *
     * @var int
     */
    const INTERNAL_SERVER_ERROR = 500;

    /**
     * Not Implemented (501)
     *
     * @var int
     */
    const NOT_IMPLEMENTED = 501;

    /**
     * Bad Gateway (502)
     *
     * @var int
     */
    const BAD_GATEWAY = 502;

    /**
     * Service Unavailable (503)
     *
     * @var int
     */
    const SERVICE_UNAVAILABLE = 503;

    /**
     * Gateway Timeout (504)
     *
     * @var int
     */
    const GATEWAY_TIMEOUT = 504;

    /**
     * HTTP Version Not Supported (505)
     *
     * @var int
     */
    const VERSION_NOT_SUPPORTED = 505;

    /**
     * Variant Also Negotiates (RFC2295) (506)
     *
     * @var int
     */
    const VARIANT_ALSO_NEGOTIATES = 506;

    /**
     * Insufficient Storage (RFC4918) (507)
     *
     * @var int
     */
    const INSUFFICIENT_STORAGE = 507;

    /**
     * Loop Detected (RFC5842) (508)
     *
     * @var int
     */
    const LOOP_DETECTED = 508;

    /**
     * Not Extended (RFC2774) (510)
     *
     * @var int
     */
    const NOT_EXTENDED = 510;

    /**
     * Network Authentication Required (RFC6585) (511)
     *
     * @var int
     */
    const NETWORK_AUTHENTICATION_REQUIRED = 511;
}
