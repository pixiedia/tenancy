<?php

declare(strict_types=1);

namespace App\Abstractions\Interface;

/**
 * Interface AbstractRouterInterface
 */
interface AbstractRouterInterface
{
    /**
     * Get the routes configuration for the router.
     *
     * @return array The routes configuration.
     */
    public function registerRoutes(): array;

    /**
     * Register the routes with Laravel's route system.
     */
    public function getRoutes();
}
