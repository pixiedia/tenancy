<?php

declare(strict_types=1);

namespace Common\Request\Interface\Data;

/**
 * Interface RequestInterface
 *
 * @api
 */
interface RequestInterface
{
    /**
     * Key for accessing the headers in the response.
     */
    public const HEADERS_KEY = 'headers';

    /**
     * Represents the page number in pagination.
     */
    const PAGE_KEY = 'page';

    /**
     * Represents the page size in pagination.
     */
    const SIZE_KEY = 'size';

    /**
     * Represents the sort key for sorting in pagination.
     */
    const SORT_KEY = 'sort';

    /**
     * Represents the filters key for filtering in pagination.
     */
    const FILTERS_KEY = 'filters';

    /**
     * Represents the sortOrder key for sorting.
     */
    const SORT_ORDER_KEY = 'sortOrder';

    /**
     * Minimum limit for the number of pages to retrieve.
     */
    public const MIN_PAGE_NUMBER = 1;

    /**
     * Maximum limit for the number of items to retrieve.
     */
    public const MAX_PAGE_SIZE = 1000;

    /**
     * Default page number.
     */
    public const DEFAULT_PAGE_NUMBER = 1;

    /**
     * Default page size.
     */
    public const DEFAULT_PAGE_SIZE = 50;

    /**
     * Default sort order.
     */
    public const DEFAULT_SORT_ORDER = 'ASC';
}
