<?php

declare(strict_types=1);

namespace Modules\Customer\Interface;

use App\Abstractions\Interface\AbstractRepositoryInterface;

/**
 * Interface CustomerRepositoryInterface
 */
interface CustomerRepositoryInterface extends AbstractRepositoryInterface
{
}
