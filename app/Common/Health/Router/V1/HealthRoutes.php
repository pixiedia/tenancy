<?php

declare(strict_types=1);

namespace Common\Health\Router\V1;

use App\Abstractions\AbstractRouter;
use Common\Http\Enum\HttpMethod;
use Spatie\Health\Http\Controllers\HealthCheckResultsController;

/**
 * Router class for defining routes related to the Health module.
 */
class HealthRoutes extends AbstractRouter
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->disablePrefixing = true;
        $this->controllerClass = HealthCheckResultsController::class;
    }

    /**
     * Get the routes configuration for the router.
     *
     * @return array The routes configuration.
     */
    public function registerRoutes(): array
    {
        return [
            HttpMethod::GET => [
                // Route to get a list of health checks
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR . "health",
                    AbstractRouter::KEY_ACTION => '',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'health',
                    AbstractRouter::KEY_PREFIX => '',
                ]
            ]
        ];
    }
}
