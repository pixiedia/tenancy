<?php

declare(strict_types=1);

namespace Modules\Role\Enum;

use BenSampo\Enum\Enum;

/**
 * Enumeration of (Pub/Sub) topics related to DNS subscription events.
 *
 * @method static static LIST()
 * @method static static GET()
 * @method static static CREATE()
 * @method static static UPDATE()
 * @method static static DELETE()
 * @method static static RECOVERED()
 */
final class SubTopics extends Enum
{
    /**
     * Topic for getting a list of DNSs.
     *
     * @example 'role.list'
     */
    const LIST = 'role.list';

    /**
     * Topic for getting information about a single DNS.
     *
     * @example 'role.get'
     */
    const GET = 'role.get';

    /**
     * Topic for creating a new DNS.
     *
     * @example 'role.create'
     */
    const CREATE = 'role.create';

    /**
     * Topic for updating an existing DNS.
     *
     * @example 'role.update'
     */
    const UPDATE = 'role.update';

    /**
     * Topic for deleting a DNS.
     *
     * @example 'role.delete'
     */
    const DELETE = 'role.delete';

    /**
     * Topic for recovery events.
     *
     * @example 'role.recovered'
     */
    const RECOVERED = 'role.recover';
}
