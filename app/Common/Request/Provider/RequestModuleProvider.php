<?php

declare(strict_types=1);

namespace Common\Request\Provider;

use App\Abstractions\AbstractModuleProvider;
use Common\Request\Middleware\Language;
use Common\Request\Middleware\RequestId;
use Common\Request\Middleware\SnakeCase;
use Common\Request\Middleware\Timestamp;
use Common\Request\Middleware\Timezone;
use Common\Request\Middleware\UserAgent;

/**
 * Class RequestModuleProvider
 */
class RequestModuleProvider extends AbstractModuleProvider
{
    /**
     * PermissionModuleProvider constructor.
     *
     * Initializes the module provider with route, seeder, and migration files.
     */
    public function __construct()
    {
        // Define the middlewares for the module
        $this->middlewares = [
            Language::class,
            Timezone::class,
            RequestId::class,
            Timestamp::class,
            UserAgent::class,
            SnakeCase::class,
        ];
    }
}
