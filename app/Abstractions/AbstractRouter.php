<?php

declare(strict_types=1);

namespace App\Abstractions;

use App\Abstractions\Interface\AbstractRouterInterface;
use App\Util\PHPUtil;
use Common\Http\Enum\HttpMethod;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use InvalidArgumentException;

/**
 * Abstract class for defining routers.
 */
abstract class AbstractRouter implements AbstractRouterInterface
{
    /**
     * The key for the URI.
     */
    public const KEY_URI = 'uri';

    /**
     * The key for the name.
     */
    public const KEY_NAME = 'name';

    /**
     * The key for the action.
     */
    public const KEY_ACTION = 'action';

    /**
     * The key for the prefix.
     */
    public const KEY_PREFIX = 'prefix';

    /**
     * The key for the middleware.
     */
    public const KEY_MIDDLEWARE = 'middleware';

    /**
     * Route prefix for internal routes.
     */
    public const PREFIX_INTERNAL = 'internal';

    /**
     * Route prefix for public routes.
     */
    public const PREFIX_PUBLIC = 'public';

    /**
     * Route prefix for private routes.
     */
    public const PREFIX_PRIVATE = 'private';

    /**
     * Indicates if the controller should disable API prefixing.
     *
     * @var bool
     */
    protected $disablePrefixing;

    /**
     * The controller api perfix.
     *
     * @var string
     */
    protected $apiPrefix;

    /**
     * The controller class.
     *
     * @var string
     */
    protected $controllerClass;

    /**
     * Constructor.
     *
     * @param  string  $controllerClass  The controller class for the router.
     */
    public function __construct(string $controllerClass)
    {
        $this->controllerClass = $controllerClass;
    }

    /**
     * Get the routes configuration for the router.
     *
     * @return array The routes configuration.
     */
    abstract public function registerRoutes(): array;

    /**
     * Register the routes with Laravel's route system.
     * This method iterates over the defined routes and registers them with Laravel's router.
     */
    public function getRoutes()
    {
        // Get the routes configuration
        $routes = $this->registerRoutes();

        // Get the API prefix
        $apiPrefix = Str::plural($this->apiPrefix);

        // Check if API prefix is set
        // if (empty($apiPrefix)) {
        //     throw new InvalidArgumentException('API prefix is not set');
        // }

        // Iterate over the routes
        foreach ($routes as $method => $routeItems) {
            foreach ($routeItems as $route) {
                // Validate required keys
                if (!isset($route[self::KEY_NAME], $route[self::KEY_URI], $route[self::KEY_ACTION])) {
                    throw new InvalidArgumentException('Route configuration is missing required keys');
                }

                // Validate route method
                if (!in_array($method, [HttpMethod::GET, HttpMethod::POST, HttpMethod::PUT, HttpMethod::DELETE])) {
                    throw new InvalidArgumentException("Invalid route method specified: {$method}");
                }

                // Validate middleware
                $middleware = $route[self::KEY_MIDDLEWARE] ?? [];
                if (!PHPUtil::isArray($middleware)) {
                    throw new InvalidArgumentException('Middleware must be an array');
                }

                // Register the route
                $this->registerRoute($apiPrefix, $method, $route);
            }
        }
    }

    /**
     * Register a single route with Laravel's router.
     * This method handles the registration of a single route.
     *
     * @param string $apiPrefix The API prefix for the route.
     * @param string $method The HTTP method of the route.
     * @param array $route The route configuration array.
     */
    private function registerRoute($apiPrefix, $method, $route)
    {
        // Determine the route prefix
        $prefix = !empty($route[self::KEY_PREFIX]) ? '/' . $route[self::KEY_PREFIX] : '';

        // Define the route group
        $routeBuilder = Route::as($apiPrefix . ':');

        // If prefixing is not disabled, apply the API prefix to the route group
        if (!$this->disablePrefixing) {
            $routeBuilder->prefix($apiPrefix);
        }

        // Define the route group with the configured prefix
        $routeBuilder->group(function () use ($method, $route, $prefix) {
            // Register the route
            $this->registerRouteWithMethod($method, $route, $prefix);
        });
    }

    /**
     * Register a route with a specific HTTP method.
     * This method registers a route with a specific HTTP method.
     */
    private function registerRouteWithMethod($method, $route, $prefix)
    {
        $action = !empty($route[self::KEY_ACTION])
            ? [$this->controllerClass, $route[self::KEY_ACTION]]
            : $this->controllerClass;

        // Define the route builder
        Route::$method($prefix . $route[self::KEY_URI], $action)
            ->name($route[self::KEY_NAME])
            ->middleware($route[self::KEY_MIDDLEWARE] ?? []);
    }
}
