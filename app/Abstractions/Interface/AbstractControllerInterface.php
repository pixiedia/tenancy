<?php

declare(strict_types=1);

namespace App\Abstractions\Interface;

use Illuminate\Http\Request;
use InvalidArgumentException;

/**
 * Interface for abstract service controllers.
 */
interface AbstractControllerInterface
{
    /**
     * Validate pagination parameters.
     *
     * @param  int  $pageNumber  The page number.
     * @param  int  $pageSize  The page size.
     *
     * @throws InvalidArgumentException if validation fails.
     */
    public function validatePaginationParams(int $pageNumber, int $pageSize): void;

    /**
     * Get and decode JSON request body content into an array.
     *
     * @param  Request  $request  The request object.
     * @return array The decoded JSON data.
     *
     * @throws InvalidArgumentException If the request body is empty or the JSON format is invalid.
     */
    public function getBody(Request $request): array;

    /**
     * Get the current language/locale.
     *
     * @return string The current language/locale.
     */
    public function getCurrentLanguage(): string;

    /**
     * Get the user ID from the bearer token.
     *
     * @param  Request  $request  The request object.
     * @return int|null The user ID, or null if not found.
     */
    public function getUserId(Request $request): ?int;
}
