<?php

declare(strict_types=1);

namespace Modules\Tenant\Provider;

use App\Abstractions\AbstractModuleProvider;
use Modules\Team\Interface\TeamRepositoryInterface;
use Modules\Tenant\Interface\Data\TenantInterface;
use Modules\Tenant\Interface\TenantRepositoryInterface;
use Modules\Tenant\Interface\TenantServiceInterface;
use Modules\Tenant\Model\Tenant;
use Modules\Tenant\Repository\TenantRepository;
use Modules\Tenant\Router\V1\TenantRoutes;
use Modules\Tenant\Service\TenantService;

/**
 * Class TenantModuleProvider
 */
class TenantModuleProvider extends AbstractModuleProvider
{
    /**
     * TenantModuleProvider constructor.
     *
     * Initializes the module provider with route, seeder, and migration files.
     */
    public function __construct()
    {
        // Glob all route files matching the defined path
        $this->routers = [TenantRoutes::class];

        // Define the path to the seeder files for the module
        $seederPath = base_path('app/Modules/Tenant/Seeder/*.php');

        // Glob all seeder files matching the defined path
        $this->seeders = glob($seederPath);

        // Define the path to the migration files for the module
        $migrationPath = base_path('app/Modules/Tenant/Migration/*.php');

        // Glob all migration files matching the defined path
        $this->migrations = glob($migrationPath);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function registerBindings()
    {
        // Bind TenantInterface to Tenant
        app()->bind(TenantInterface::class, Tenant::class);

        // Bind TenantRepositoryInterface to TenantRepository
        app()->bind(TenantRepositoryInterface::class, fn ($app) => new TenantRepository($app->make(TenantInterface::class), $app->make(TeamRepositoryInterface::class)));

        // Bind TenantServiceInterface to TenantService
        app()->bind(TenantServiceInterface::class, fn ($app) => new TenantService($app->make(TenantRepositoryInterface::class)));
    }
}
