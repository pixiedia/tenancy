<?php

declare(strict_types=1);

namespace Modules\Team\Interface\Data;

/**
 * Interface TeamTenantInterface
 *
 * This interface defines constants related to permission role attributes.
 */
interface TeamTenantInterface
{
    /**
     * The name of the pivot table.
     */
    public const TABLE = 'team_tenant';

    /**
     * The name of the column storing the team ID.
     */
    public const TEAM_ID = 'team_id';

    /**
     * The name of the column storing the tenant ID.
     */
    public const TENANT_ID = 'tenant_id';
}
