<?php

declare(strict_types=1);

namespace Modules\Tenant\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Modules\Tenant\Exception\TenantCouldNotBeIdentifiedByPathException;
use Modules\Tenant\Exception\TenantCouldNotBeIdentifiedByRequestDataException;
use Modules\Tenant\Interface\Data\TenantInterface;
use Modules\Tenant\Model\Tenant;
use Symfony\Component\HttpFoundation\Response;

/**
 * Middleware for initializing tenancy based on request data.
 */
class InitializeTenancyByRequestData
{
    /**
     * The name of the header to use for tenant identification.
     *
     * @var string|null
     */
    private static $header = 'X-Tenant-ID';

    /**
     * The name of the query parameter to use for tenant identification.
     *
     * @var string|null
     */
    private static $queryParameter = 'tenant_id';

    /**
     * The name of the route parameter to use for tenant identification.
     *
     * @var string|null
     */
    private static $tenantParameterName = 'tenant_id';

    /**
     * Find the tenant based on the request.
     *
     * @param  Request  $request  The request object.
     * @return Tenant The found tenant.
     *
     * @throws Exception If the tenant cannot be identified.
     */
    public static function find(Request $request): Tenant
    {
        // Get the tenant ID and the method of identification
        [$id, $byRoute] = (new self())->getPayload($request);

        // Throw an exception if the ID is null
        throw_if(is_null($id), 'The id is null given');

        // Find the tenant based on the ID
        $tenant = Tenant::query()
            ->where(TenantInterface::ID, $id)
            ->firstOr(function () use ($id, $byRoute) {
                // Throw a specific exception based on the method of identification
                if ($byRoute) {
                    throw new TenantCouldNotBeIdentifiedByPathException($id);
                }
                throw new TenantCouldNotBeIdentifiedByRequestDataException($id);
            });

        // If a matching tenant is found, set it on the request
        if ($tenant || $tenant->getId()) {
            $request->attributes->set(TenantInterface::ENTITY_NAME, $tenant);
        }

        return $tenant;
    }

    /**
     * Handle an incoming request.
     *
     * @param  Closure(Request): (Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // Find the tenant associated by request data
        // $this->find($request);

        // Continue with the next middleware in the pipeline
        return $next($request);
    }

    /**
     * Get the tenant ID and the method of identification from the request.
     *
     * @param  Request  $request  The request object.
     * @return array The tenant ID and the method of identification.
     */
    private function getPayload(Request $request): array
    {
        // Initialize variables
        $tenant = null;
        $byRoute = false;

        // Check if the tenant ID is present in the header
        if (self::$header && $request->hasHeader(self::$header)) {

            $tenant = (int) $request->header(self::$header);
        } elseif (self::$queryParameter && $request->has(self::$queryParameter)) {
            // Check if the tenant ID is present in the query parameter
            $tenant = $request->get(self::$queryParameter);
        } else {
            // Check if the tenant ID is present in the route parameter
            $tenant = $request->route(self::$tenantParameterName);

            // Remove the route parameter from the request
            // $request->route()->forgetParameter(self::$tenantParameterName);

            $byRoute = true;
        }

        return [$tenant, $byRoute];
    }
}
