<?php

declare(strict_types=1);

namespace Modules\Tenant\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Modules\Team\Interface\Data\TeamInterface;
use Modules\Tenant\Exception\TenantCouldNotBeIdentifiedByUser;
use Modules\Tenant\Model\Tenant;
use Symfony\Component\HttpFoundation\Response;

/**
 * Middleware for initializing tenancy based on the tenant associated with the authenticated user.
 */
class InitializeTenancyByTenantUser
{
    /**
     * Find the tenant associated with the authenticated user.
     *
     *
     * @throws AuthenticationException
     * @throws TenantCouldNotBeIdentifiedByUser
     */
    public static function find(Request $request, Closure $next): Tenant|Response
    {
        // Get the authenticated user from the request
        $user = $request->user();

        // If user is null, continue with the next middleware
        if ($user === null) {
            return $next($request);
        }

        // Ensure that the user is an instance of Authenticatable
        throw_unless($user instanceof Authenticatable, 'The user is not an instance of Authenticatable');

        // Throw an authentication exception if the user is null
        throw_if($user === null, new AuthenticationException());

        // Find the tenant where the team ID matches the user's team ID
        $tenant = Tenant::whereRelation(TeamInterface::TABLE, TeamInterface::PRIMARY_KEY, $user->team_id)->first();

        // If a tenant is found, return it
        if (! $tenant || ! $tenant->getId()) {
            // If no tenant is found, throw an exception
            throw new TenantCouldNotBeIdentifiedByUser();
        }

        return $tenant;
    }

    /**
     * Handle an incoming request.
     *
     *
     * @throws AuthenticationException
     * @throws TenantCouldNotBeIdentifiedByUser
     */
    public function handle(Request $request, Closure $next): Response
    {
        // Find the tenant associated with the authenticated user
        $this->find($request, $next);

        // Continue with the next middleware in the pipeline
        return $next($request);
    }
}
