<?php

declare(strict_types=1);

namespace Modules\Team\Observer;

use Common\PubSub\Facade\PubSubService;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\Team\Enum\PubTopics;
use Modules\Team\Mail\TeamCreated;
use Modules\Team\Model\Team;
use Ramsey\Uuid\Uuid;

/**
 * Class TeamObserver
 */
class TeamObserver
{
    /**
     * Handle the Team "creating" event.
     *
     * @param  Team  $team  The newly creating team.
     */
    public function creating(Team $team): void
    {
        // Log the creation of the team
        Log::info("Team creating: {$team->getId()}");

        // Generate a UUID for the team
        $uuid = Uuid::uuid4()->toString();

        // Set the generated UUID for the team
        $team->setUuid($uuid);
    }

    /**
     * Handle the Team "creating" event.
     *
     * @param  Team  $team  The newly creating team.
     */
    public function created(Team $team): void
    {
        // Log the creation of the team
        Log::info("Team created: {$team->getId()}");

        // Dispatch the created event
        Event::dispatch(PubTopics::CREATED, $team->toDataArray());

        // Pubsub the created event
        PubSubService::publish(PubTopics::CREATED, $team->toDataArray());

        // Send an email
        Mail::to('pixiedia@gmail.com')->send(new TeamCreated($team));
    }

    /**
     * Handle the Team "updated" event.
     *
     * @param  Team  $team  The updated team.
     */
    public function updated(Team $team): void
    {
        // Log the update of the team
        Log::info("Team updated: {$team->getId()}");

        // Set the updated at timestamp
        $team->setUpdatedAt(now());

        // Dispatch the updated event
        Event::dispatch(PubTopics::UPDATED, $team->toDataArray());

        // Pubsub the updated event
        PubSubService::publish(PubTopics::UPDATED, $team->toDataArray());
    }

    /**
     * Handle the Team "deleted" event.
     *
     * @param  Team  $team  The deleted team.
     */
    public function deleted(Team $team): void
    {
        // Log the deletion of the team
        Log::info("Team deleted: {$team->getId()}");

        // Set the deleted at timestamp
        $team->setDeletedAt(now());

        // Dispatch the deleted event
        Event::dispatch(PubTopics::SOFT_DELETED, $team->toDataArray());

        // Pubsub the deleted event
        PubSubService::publish(PubTopics::SOFT_DELETED, $team->toDataArray());
    }

    /**
     * Handle the Team "forceDeleted" event.
     *
     * @param  Team  $team  The force deleted team.
     */
    public function forceDeleted(Team $team): void
    {
        // Log the force deletion of the team
        Log::info("Team force deleted: {$team->getId()}");

        // Set the deleted at timestamp
        $team->setDeletedAt(now());

        // Dispatch the force deleted event
        Event::dispatch(PubTopics::DELETED, $team->toDataArray());

        // Pubsub the deleted event
        PubSubService::publish(PubTopics::DELETED, $team->toDataArray());
    }

    /**
     * Handle the Team "restored" event.
     *
     * @param  Team  $team  The restored team.
     */
    public function restored(Team $team): void
    {
        // Log the restoration of the team
        Log::info("Team restored: {$team->getId()}");

        // Reset the deleted at timestamp
        $team->setDeletedAt(null);

        // Dispatch the restored event
        Event::dispatch(PubTopics::RECOVERED, $team->toDataArray());

        // Pubsub the restored event
        PubSubService::publish(PubTopics::RECOVERED, $team->toDataArray());
    }
}
