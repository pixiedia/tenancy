<?php

declare(strict_types=1);

namespace Modules\User\Factory;

use App\Util\PHPUtil;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Modules\User\Interface\Data\UserInterface;
use Modules\User\Model\User;
use RuntimeException;

/**
 * Class UserFactory
 */
class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // Return the definition array for the User model
        return [
            // Generate a UUID for the user
            UserInterface::UUID => $this->faker->uuid,

            // Generate a full name for the user
            UserInterface::NAME => $this->faker->name,

            // Set the user status to active or inactive randomly
            UserInterface::STATUS => $this->faker->randomElement(['active', 'inactive']),

            // Generate a unique email address for the user
            UserInterface::EMAIL => $this->faker->unique()->safeEmail,

            // Set the email_verified_at attribute to a random date within this year
            UserInterface::EMAIL_VERIFIED_AT => $this->faker->dateTimeThisYear,

            // Generate a hashed password using the getPassword() method
            UserInterface::PASSWORD => Hash::make(UserInterface::PASSWORD),

            // Generate a remember_token
            UserInterface::REMEMBER_TOKEN => Str::random(10),

            // Generate a random phone number for the user
            UserInterface::PHONE => $this->faker->phoneNumber,

            // Generate random metadata for the user
            UserInterface::METADATA => $this->getMetadata(),
        ];
    }

    /**
     * Get a randomly generated metadata array.
     *
     * @return string The generated metadata array.
     *
     * @throws RuntimeException If the metadata array fails to encode as JSON.
     */
    private function getMetadata(): string
    {
        // Initialize an empty array to store metadata
        $metadata = [];

        // Generate metadata with random keys and values
        for ($i = 0; $i < 10; $i++) {
            // Generate a random word as the key and another random word as the value
            $metadata[$this->faker->word] = $this->faker->word;
        }

        // Encode the metadata array as JSON
        $encodedMetadata = PHPUtil::jsonEncode($metadata);

        // Check if encoding was successful
        if ($encodedMetadata === false) {
            throw new RuntimeException('Failed to encode metadata as JSON: ' . json_last_error_msg());
        }

        return $encodedMetadata;
    }
}
