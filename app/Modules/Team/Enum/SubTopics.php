<?php

declare(strict_types=1);

namespace Modules\Team\Enum;

use BenSampo\Enum\Enum;

/**
 * Enumeration of (Pub/Sub) topics related to DNS subscription events.
 *
 * @method static static LIST()
 * @method static static GET()
 * @method static static CREATE()
 * @method static static UPDATE()
 * @method static static DELETE()
 * @method static static RECOVERED()
 */
final class SubTopics extends Enum
{
    /**
     * Topic for getting a list of DNSs.
     *
     * @example 'team.list'
     */
    const LIST = 'team.list';

    /**
     * Topic for getting information about a single DNS.
     *
     * @example 'team.get'
     */
    const GET = 'team.get';

    /**
     * Topic for creating a new DNS.
     *
     * @example 'team.create'
     */
    const CREATE = 'team.create';

    /**
     * Topic for updating an existing DNS.
     *
     * @example 'team.update'
     */
    const UPDATE = 'team.update';

    /**
     * Topic for deleting a DNS.
     *
     * @example 'team.delete'
     */
    const DELETE = 'team.delete';

    /**
     * Topic for recovery events.
     *
     * @example 'team.recovered'
     */
    const RECOVERED = 'team.recover';
}
