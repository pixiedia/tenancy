<?php

declare(strict_types=1);

namespace Modules\User\Repository;

use App\Abstractions\AbstractRepository;
use Exception;
use Modules\User\Interface\Data\UserInterface;
use Modules\User\Interface\UserRepositoryInterface;
use Modules\User\Model\User;

/**
 * Class UserRepository
 *
 * This class implements the UserRepositoryInterface and provides methods to interact with the User model.
 */
class UserRepository extends AbstractRepository implements UserRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param  User  $model  The User model instance to use.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(UserInterface $model)
    {
        // Call the parent constructor with the model and the model interface
        parent::__construct($model, UserInterface::class);

        // Assign the model instance to the class property
        $this->model = $model;
    }
}
