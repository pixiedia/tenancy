<?php

declare(strict_types=1);

namespace App\Abstractions;

use App\Abstractions\Interface\AbstractRepositoryInterface;
use Common\Request\Interface\Data\RequestInterface;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

/**
 * Class AbstractRepository
 *
 * This abstract class implements common repository methods for interacting with Eloquent models.
 */
abstract class AbstractRepository implements AbstractRepositoryInterface
{
    /**
     * The model instance.
     *
     * @var Model
     */
    protected $model;

    /**
     * The model interface instance.
     *
     * @var mixed
     */
    protected $modelInterface;

    /**
     * AbstractRepository constructor.
     *
     * @param  Model  $model  The model instance to use in the repository.
     * @param  mixed  $modelInterface  The model interface instance to use in the repository.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(Model $model, $modelInterface)
    {
        $this->model = $model;

        // Check if $modelInterface is null, and throw an exception if it is
        if ($modelInterface === null) {
            throw new Exception('ModelInterface cannot be null.');
        }

        // Assign the $modelInterface to the class property
        $this->modelInterface = $modelInterface;
    }

    /**
     * Get all entities based on provided criteria.
     *
     * @param  array  $criteria  An array containing where, sort, and pagination criteria.
     *
     * @throws Exception
     */
    public function list(array $criteria = []): Collection
    {
        try {
            $query = $this->model->newQuery();

            // Apply where conditions
            if (isset($criteria['where'])) {
                foreach ($criteria['where'] as $condition) {
                    $query->where(...$condition);
                }
            }

            // Apply sorting
            if (isset($criteria['sort'])) {
                $query->orderBy(...$criteria['sort']);
            }

            // Apply pagination
            $pageSize = $criteria[RequestInterface::SIZE_KEY];
            $pageNumber = $criteria[RequestInterface::PAGE_KEY];

            $query->forPage($pageNumber, $pageSize);

            // Get the results
            $results = $query->get();

            return $results;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Re-throw the exception to be caught by the caller
            throw $e;
        }
    }

    /**
     * Search for entities based on a search term, with optional filters, sorting, and pagination.
     *
     * @param  array  $criteria  The search criteria.
     */
    public function search(array $criteria = []): Collection
    {
        try {
            // Prepare the base query
            $query = $this->model->search($criteria['searchTerm']);

            // Apply filters
            foreach ($criteria['filters'] as $key => $value) {
                $query->where($key, $value);
            }

            // Apply sorting
            $query->orderBy('created_at', $criteria['sort']);

            // Paginate the results
            $results = $query->paginate($criteria[RequestInterface::SIZE_KEY], $criteria[RequestInterface::PAGE_KEY]);

            // Get the results
            $results = $query->get();

            return $results->map(function ($result) {
                $result->setData($result->getAttributes());

                return $result;
            });
        } catch (Exception $e) {
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Re-throw the exception to be caught by the caller
            throw $e;
        }
    }

    /**
     * Find a entity by ID.
     *
     * @param  int  $id  The ID of the entity to find.
     *
     * @throws Exception
     */
    public function find(int $id): ?Model
    {
        try {
            return $this->model->findOrFail($id);
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Re-throw the exception to be caught by the caller
            throw $e;
        }
    }

    /**
     * Find a entity by UUID.
     *
     * @param  string  $uuid  The UUID of the entity to find.
     *
     * @throws Exception
     */
    public function findByUuid(string $uuid): ?Model
    {
        try {
            return $this->model->where($this->modelInterface::UUID, $uuid)->firstOrFail();
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Re-throw the exception to be caught by the caller
            throw $e;
        }
    }

    /**
     * Find a entity by Identifier.
     *
     * @param  string  $identifier  The identifier of the entity to find.
     * @param  string  $key  The column to search for the identifier.
     *
     * @throws Exception
     */
    public function findBy(string $identifier, ?string $key = null): ?Model
    {
        try {
            return $this->model->where($identifier, $key)->firstOrFail();
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Re-throw the exception to be caught by the caller
            throw $e;
        }
    }

    /**
     * Create a new entity.
     *
     * @param  array  $data  The data to create the entity.
     *
     * @throws Exception
     */
    public function create(array $data): Model
    {
        try {
            return $this->model->create($data);
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Re-throw the exception to be caught by the caller
            throw $e;
        }
    }

    /**
     * Create multiple new entities.
     *
     * @param  array  $dataList  List of data to create entities.
     *
     * @throws Exception
     */
    public function bulkCreate(array $dataList): Collection
    {
        try {
            // Insert the data into the database
            $this->model->insert($dataList);

            // Retrieve the IDs of the created records
            $createdIds = collect($dataList)->pluck($this->modelInterface::PRIMARY_KEY)->toArray();

            // Retrieve the created records from the database
            $createdModels = $this->model->whereIn($this->modelInterface::PRIMARY_KEY, $createdIds)->get();

            // Return the created records
            return $createdModels;
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Re-throw the exception to be caught by the caller
            throw $e;
        }
    }

    /**
     * Update a entity by ID.
     *
     * @param  int  $id  The ID of the entity to update.
     * @param  array  $data  The data to update the entity.
     *
     * @throws Exception
     */
    public function update(int $id, array $data): bool
    {
        try {
            $entity = $this->model->findOrFail($id);

            return $entity->update($data);
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Re-throw the exception to be caught by the caller
            throw $e;
        }
    }

    /**
     * Delete a entity by ID.
     *
     * @param  int  $id  The ID of the entity to delete.
     *
     * @throws Exception
     */
    public function delete(int $id): bool
    {
        try {
            $entity = $this->model->findOrFail($id);

            return $entity->delete();
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Re-throw the exception to be caught by the caller
            throw $e;
        }
    }

    /**
     * Restore a soft-deleted entity by ID.
     *
     * @param  int  $id  The ID of the entity to restore.
     *
     * @throws Exception
     */
    public function restore(int $id): bool
    {
        try {
            $entity = $this->model->onlyTrashed()->findOrFail($id);

            return $entity->restore();
        } catch (Exception $e) {
            // Log any exceptions
            Log::error('An error occurred in ' . __METHOD__ . ': ' . $e->getMessage(), [
                'class' => __CLASS__,
                'method' => __FUNCTION__,
                'exception_code' => $e->getCode(),
                'exception_file' => $e->getFile(),
                'exception_line' => $e->getLine(),
            ]);

            // Re-throw the exception to be caught by the caller
            throw $e;
        }
    }
}
