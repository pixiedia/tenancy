<?php

declare(strict_types=1);

namespace Modules\Customer\Controller;

use App\Abstractions\AbstractController;
use Common\Request\Interface\Data\RequestInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Customer\Dto\CreateCustomerDto;
use Modules\Customer\Dto\UpdateCustomerDto;
use Modules\Customer\Interface\CustomerServiceInterface;
use Modules\Customer\Interface\Data\CustomerInterface;
use OpenApi\Annotations as OA;
use OpenApi\Attributes as OAttributes;

/**
 * @OA\Tag(
 *      name="Customers",
 *      description="Customer API Endpoints"
 * )
 *
 * Class CustomerController
 */
class CustomerController extends AbstractController
{
    /**
     * The key used to store the search term in the request.
     */
    const SEARCH_TERM_KEY = 'searchTerm';

    /**
     * The entity name for the pagination.
     *
     * @var string|null
     */
    const ENTITY_NAME = CustomerInterface::ENTITY_NAME;

    /**
     * The Customer service interface instance.
     *
     * @var CustomerServiceInterface
     */
    protected $customerService;

    /**
     * CustomerController constructor.
     */
    public function __construct(CustomerServiceInterface $customerService)
    {
        $this->customerService = $customerService;
    }

    #[OAttributes\Get(
        path: '/v1/users',
        description: 'Get users list',
        security: [['jwt' => []]],
        tags: ['Users Management'],
    )]

    #[OAttributes\Parameter(name: 'page', in: 'query', required: false, schema: new OAttributes\Schema(type: 'integer', default: 1))]
    #[OAttributes\Parameter(name: 'per_page', in: 'query', required: false, schema: new OAttributes\Schema(type: 'integer', default: 20))]
    #[OAttributes\Parameter(name: 'sort_field', in: 'query', required: false, schema: new OAttributes\Schema(type: 'string'))]
    #[OAttributes\Parameter(name: 'sort_order', in: 'query', required: false, schema: new OAttributes\Schema(type: 'string', enum: ['asc', 'desc']))]
    #[OAttributes\Parameter(name: 'search', in: 'query', required: false, schema: new OAttributes\Schema(type: 'string'))]

    #[OAttributes\Response(
        response: '200',
        description: 'The list of users',
        content: new OAttributes\JsonContent(properties: [
            new OAttributes\Property(property: 'data', type: 'array', items: new OAttributes\Items(ref: '#/components/schemas/User')),
            // new OAttributes\Property(property: 'links', ref: '#/components/schemas/pagination-links'),
            // new OAttributes\Property(property: 'meta', ref: '#/components/schemas/pagination-meta'),
        ]),
    )]
    #[OAttributes\Response(response: '401', description: 'Unauthorized')]
    #[OAttributes\Response(response: '403', description: 'Forbidden')]
    #[OAttributes\Response(response: '500', description: 'Server Error')]
    public function getList(Request $request): JsonResponse
    {
        try {
            // Get pagination parameters from the request
            $size = (int) $request->query(RequestInterface::SIZE_KEY, (string) RequestInterface::DEFAULT_PAGE_SIZE);
            $page = (int) $request->query(RequestInterface::PAGE_KEY, (string) RequestInterface::DEFAULT_PAGE_NUMBER);

            // Validate pagination parameters
            $this->validatePaginationParams($page, $size);

            // Retrieve paginated list of customers from the service
            $customers = $this->customerService->list($page, $size);

            // Return JSON response with the list of customers
            return response()->json($customers);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/customers/search/{searchTerm}",
     *      summary="Get a list of customers by search term",
     *      security={ "jwt": {} },
     *      tags={"Customers"},
     *
     *      @OA\Parameter(
     *          name="searchTerm",
     *          in="path",
     *          description="Search Term",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="string",
     *              default="Customer A"
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(
     *              required={"searchTerm", "page", "size", "filters", "sortOrder"},
     *
     *              @OA\Property(property="filters", type="object", example={}),
     *              @OA\Property(property="sortOrder", type="string", enum={"asc", "desc"}, example="asc")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Customer")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a list of customers.
     *
     * @param  Request  $request  The HTTP request object containing search, pagination, filters, and sort parameters.
     * @return JsonResponse The JSON response containing the list of customers matching the search criteria.
     */
    public function search(Request $request): JsonResponse
    {
        try {
            // Get search term from the request
            $searchTerm = $request->route(static::SEARCH_TERM_KEY);

            // Get pagination parameters from the request
            $size = (int) $request->query(RequestInterface::SIZE_KEY, (string) RequestInterface::DEFAULT_PAGE_SIZE);
            $page = (int) $request->query(RequestInterface::PAGE_KEY, (string) RequestInterface::DEFAULT_PAGE_NUMBER);

            // Validate pagination parameters
            $this->validatePaginationParams($page, $size);

            // Get filters and sort criteria from the request, defaulting to an empty array and default sort field and order if not provided
            $filters = $request->input(RequestInterface::FILTERS_KEY, []);
            $sortOrder = $request->input(RequestInterface::SORT_ORDER_KEY, RequestInterface::DEFAULT_SORT_ORDER);

            // Retrieve paginated search of customers from the service based on the search term, filters, and sort criteria
            $customers = $this->customerService->search($searchTerm, $page, $size, $filters, $sortOrder);

            // Return JSON response with the list of customers matching the search criteria
            return response()->json($customers);
        } catch (Exception $e) {
            // Handle any exceptions that occur during the search and return a JSON response with an error message
            throw $e;
        }
    }

    /**
     * @OA\Get(
     *      path="/customers/{id}",
     *      summary="Get a customer by ID",
     *      tags={"Customers"},
     *      security={ "jwt": {} },
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the customer to retrieve",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Customer")
     *          )
     *      ),
     *
     *     @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a customer by ID.
     *
     * @param  int  $id  The ID of the customer to retrieve.
     * @return JsonResponse The JSON response containing the customer details.
     */
    public function getById(int $id): JsonResponse
    {
        try {
            // Retrieve the customer by ID from the service
            $customer = $this->customerService->find($id);

            // Return JSON response with the customer details
            return response()->json($customer);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Get(
     *      path="/customers/uuid/{uuid}",
     *      summary="Get a customer by UUID",
     *      tags={"Customers"},
     *      security={ "jwt": {} },
     *
     *      @OA\Parameter(
     *          name="uuid",
     *          in="path",
     *          description="UUID of the customer to retrieve",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Customer")
     *          )
     *      ),
     *
     *     @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a customer by UUID.
     *
     * @param  string  $uuid  The UUID of the customer to retrieve.
     * @return JsonResponse The JSON response containing the customer details.
     */
    public function getByUuid(string $uuid): JsonResponse
    {
        try {
            // Retrieve the customer by ID from the service
            $customer = $this->customerService->findByUuid($uuid);

            // Return JSON response with the customer details
            return response()->json($customer);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/customers",
     *      summary="Create a new customer",
     *      tags={"Customers"},
     *      security={ "jwt": {} },
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/CreateCustomerDto")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Customer")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Create a new customer.
     *
     * @param  CreateCustomerDto  $request  The validated request DTO containing the customer data.
     * @return JsonResponse The JSON response containing the newly created customer details.
     */
    public function create(CreateCustomerDto $request): JsonResponse
    {
        try {
            // Get validated data from the request DTO
            $data = $request->validated();

            // Create a new customer using the service
            $customer = $this->customerService->create($data);

            // Return JSON response with the newly created customer details
            return response()->json($customer);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/customers/bulk",
     *      summary="Create multiple new customers",
     *      tags={"Customers"},
     *      security={ "jwt": {} },
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(
     *              type="array",
     *
     *              @OA\Items(ref="#/components/schemas/Customer")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Customer")),
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Create multiple new customers.
     *
     * @param  Request  $request  The HTTP request object containing the list of customers to create.
     * @return JsonResponse The JSON response containing the newly created customers.
     */
    public function bulkCreate(Request $request): JsonResponse
    {
        try {
            $dataList = $request->list(); // Assuming the request contains an array of customer data
            $customers = $this->customerService->bulkCreate($dataList);

            return response()->json($customers);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @OA\Put(
     *      path="/customers/{id}",
     *      summary="Update a customer by ID",
     *      tags={"Customers"},
     *      security={ "jwt": {} },
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the customer to update",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/UpdateCustomerDto")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Customer")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Update a customer by ID.
     *
     * @param  UpdateCustomerDto  $request  The validated request DTO containing the updated customer data.
     * @param  $id  The ID of the customer to update.
     * @return JsonResponse The JSON response containing the updated customer details.
     */
    public function update(UpdateCustomerDto $request, int $id): JsonResponse
    {
        try {
            // Get validated data from the request DTO
            $data = $request->validated();

            // Update the customer using the service
            $customer = $this->customerService->update($id, $data);

            // Return JSON response with the updated customer details
            return response()->json($customer);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Delete(
     *      path="/customers/{id}",
     *      summary="Delete a customer by ID",
     *      tags={"Customers"},
     *      security={ "jwt": {} },
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the customer to delete",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Customer")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Delete a customer by ID.
     *
     * @param  int  $id  The ID of the customer to delete.
     * @return JsonResponse The JSON response containing the deleted customer details.
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            // Delete the customer using the service
            $customer = $this->customerService->delete($id);

            // Return JSON response with the deleted customer details
            return response()->json($customer);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Put(
     *      path="/customers/{id}/restore",
     *      summary="Restore a soft-deleted customer by ID",
     *      tags={"Customers"},
     *      security={ "jwt": {} },
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the customer to restore",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Customer")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Restore a soft-deleted customer by ID.
     *
     * @param  int  $id  The ID of the customer to restore.
     * @return JsonResponse The JSON response indicating the success or failure of the restore operation.
     */
    public function restore(int $id): JsonResponse
    {
        try {
            $customer = $this->customerService->restore($id);

            return response()->json($customer);
        } catch (Exception $e) {
            throw $e;
        }
    }
}
