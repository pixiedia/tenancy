<?php

declare(strict_types=1);

namespace Modules\Team\Service;

use App\Abstractions\AbstractService;
use Exception;
use Modules\Team\Interface\Data\TeamInterface;
use Modules\Team\Interface\TeamRepositoryInterface;
use Modules\Team\Interface\TeamServiceInterface;
use Modules\Team\Repository\TeamRepository;

/**
 * Class TeamService
 */
class TeamService extends AbstractService implements TeamServiceInterface
{
    /**
     * AbstractRepository constructor.
     *
     * @param  TeamRepository  $repository  The repository instance to use in the service.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(TeamRepositoryInterface $repository)
    {
        parent::__construct($repository, TeamInterface::class);
    }
}
