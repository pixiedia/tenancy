<?php

declare(strict_types=1);

namespace Common\Request\Middleware;

use Closure;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

/**
 * Class RequestId
 */
class RequestId
{
    /**
     * Header name for the X-Request-ID header.
     */
    private const REQUEST_ID_HEADER = 'x-request-id';

    /**
     * Header name for the X-Amazon-Request-ID header.
     */
    private const AMAZON_REQUEST_ID_HEADER = 'x-amazon-request-id';

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request  The incoming request
     * @param  Closure  $next  The next middleware in the pipeline
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Check if the X-Request-ID header is set
        if (! $request->headers->has(self::REQUEST_ID_HEADER) && ! $request->headers->has(self::AMAZON_REQUEST_ID_HEADER)) {
            // Generate a UUID
            $uuid = Uuid::uuid4()->toString();
            // Set the X-Request-ID header with the generated UUID
            $request->headers->set(self::REQUEST_ID_HEADER, $uuid);
        }

        // Call the next middleware in the pipeline with the modified request
        return $next($request);
    }
}
