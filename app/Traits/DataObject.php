<?php

declare(strict_types=1);

namespace App\Traits;

use App\Util\PHPUtil;
use Exception;
use InvalidArgumentException;

/**
 * Universal data container with array access implementation
 */
trait DataObject
{
    /**
     * Object attributes
     *
     * @var array
     */
    protected $_data = [];

    /**
     * Setter/Getter underscore transformation cache
     *
     * @var array
     */
    protected static $_underscoreCache = [];

    /**
     * Constructor
     *
     * By default is looking for first argument as array and assigns it as object attributes
     * This behavior may change in child classes
     */
    public function __construct(array $data = [])
    {
        $this->_data = $data;
    }

    /**
     * Set/Get attribute wrapper
     *
     * @param  string  $method
     * @param  array  $args
     * @return mixed
     *
     * @throws Exception
     */
    public function __call($method, $args)
    {
        switch (substr((string) $method, 0, 3)) {
            case 'get':
                $key = $this->_underscore(substr($method, 3));
                $index = $args[0] ?? null;

                return $this->getData($key, $index);
            case 'set':
                $key = $this->_underscore(substr($method, 3));
                $value = $args[0] ?? null;

                return $this->setData($key, $value);
            case 'uns':
                $key = $this->_underscore(substr($method, 3));

                return $this->unsetData($key);
            case 'has':
                $key = $this->_underscore(substr($method, 3));

                return isset($this->_data[$key]);
            default:
                // Handle other method calls not related to data handling
                return parent::__call($method, $args);
        }
    }

    /**
     * Add data to the object.
     *
     * Retains previous data in the object.
     *
     * @return $this
     */
    public function addData(array $arr)
    {
        if ($this->_data === []) {
            $this->setData($arr);

            return $this;
        }

        foreach ($arr as $index => $value) {
            $this->setData($index, $value);
        }

        return $this;
    }

    /**
     * Overwrite data in the object.
     *
     * The $key parameter can be string or array.
     * If $key is string, the attribute value will be overwritten by $value
     *
     * If $key is an array, it will overwrite all the data in the object.
     *
     * @param  string|array  $key
     * @param  mixed  $value
     * @return $this
     */
    public function setData($key, $value = null)
    {
        if ($key === (array) $key) {
            $this->_data = $key;
        } else {
            $this->_data[$key] = $value;
        }

        return $this;
    }

    /**
     * Unset data from the object.
     *
     * @param  null|string|array  $key
     * @return $this
     */
    public function unsetData($key = null)
    {
        if ($key === null) {
            $this->setData([]);
        } elseif (is_string($key)) {
            if (isset($this->_data[$key]) || array_key_exists($key, $this->_data)) {
                unset($this->_data[$key]);
            }
        } elseif ($key === (array) $key) {
            foreach ($key as $element) {
                $this->unsetData($element);
            }
        }

        return $this;
    }

    /**
     * Object data getter
     *
     * If $key is not defined will return all the data as an array.
     * Otherwise it will return value of the element specified by $key.
     * It is possible to use keys like a/b/c for access nested array data
     *
     * If $index is specified it will assume that attribute data is an array
     * and retrieve corresponding member. If data is the string - it will be explode
     * by new line character and converted to array.
     *
     * @param  string  $key
     * @param  string|int  $index
     * @return mixed
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getData($key = '', $index = null)
    {
        if ($key === '') {
            return $this->_data;
        }

        /* process a/b/c key as ['a']['b']['c'] */
        if ($key !== null && str_contains($key, DIRECTORY_SEPARATOR)) {
            $data = $this->getDataByPath($key);
        } else {
            $data = $this->_getData($key);
        }

        if ($index !== null) {
            if ($data === (array) $data) {
                $data = $data[$index] ?? null;
            } elseif (is_string($data)) {
                $data = explode(PHP_EOL, $data);
                $data = $data[$index] ?? null;
            } elseif ($data instanceof DataObject) {
                $data = $data->getData($index);
            } else {
                $data = null;
            }
        }

        return $data;
    }

    /**
     * Get object data by path
     *
     * Method consider the path as chain of keys: a/b/c => ['a']['b']['c']
     *
     * @param  string  $path
     * @return mixed
     */
    public function getDataByPath($path)
    {
        $keys = explode(DIRECTORY_SEPARATOR, (string) $path);

        $data = $this->_data;
        foreach ($keys as $key) {
            if ((array) $data === $data && isset($data[$key])) {
                $data = $data[$key];
            } elseif ($data instanceof DataObject) {
                $data = $data->getDataByKey($key);
            } else {
                return null;
            }
        }

        return $data;
    }

    /**
     * Get object data by particular key
     *
     * @param  string  $key
     * @return mixed
     */
    public function getDataByKey($key)
    {
        return $this->_getData($key);
    }

    /**
     * Set object data with calling setter method
     *
     * @param  string  $key
     * @param  mixed  $args
     * @return $this
     */
    public function setDataUsingMethod($key, $args = [])
    {
        $method = 'set' . ($key !== null ? str_replace('_', '', ucwords($key, '_')) : '');
        $this->{$method}($args);

        return $this;
    }

    /**
     * Get object data by key with calling getter method
     *
     * @param  string  $key
     * @param  mixed  $args
     * @return mixed
     */
    public function getDataUsingMethod($key, $args = null)
    {
        $method = 'get' . ($key !== null ? str_replace('_', '', ucwords($key, '_')) : '');

        return $this->{$method}($args);
    }

    /**
     * If $key is empty, checks whether there's any data in the object
     *
     * Otherwise checks if the specified attribute is set.
     *
     * @param  string  $key
     * @return bool
     */
    public function hasData($key = '')
    {
        if (empty($key) || ! is_string($key)) {
            return ! empty($this->_data);
        }

        return array_key_exists($key, $this->_data);
    }

    /**
     * Convert array of object data with to array with keys requested in $keys array
     *
     * @param  array  $keys  array of required keys
     * @return array
     */
    public function toArray(array $keys = [])
    {
        if (empty($keys)) {
            return $this->_data;
        }

        $result = [];
        foreach ($keys as $key) {
            if (isset($this->_data[$key])) {
                $result[$key] = $this->_data[$key];
            } else {
                $result[$key] = null;
            }
        }

        return $result;
    }

    /**
     * The "__" style wrapper for toArray method
     *
     * @return array
     */
    public function convertToArray(array $keys = [])
    {
        return $this->toArray($keys);
    }

    /**
     * Convert object data into XML string
     *
     * @param  array  $keys  array of keys that must be represented
     * @param  string  $rootName  root node name
     * @param  bool  $addOpenTag  flag that allow to add initial xml node
     * @param  bool  $addCdata  flag that require wrap all values in CDATA
     * @return string
     */
    public function toXml(array $keys = [], $rootName = 'item', $addOpenTag = false, $addCdata = true)
    {
        $xml = '';
        $data = $this->toArray($keys);
        foreach ($data as $fieldName => $fieldValue) {
            if ($addCdata === true) {
                $fieldValue = "<![CDATA[{$fieldValue}]]>";
            } else {
                $fieldValue = $fieldValue !== null ? str_replace(
                    ['&', '"', "'", '<', '>'],
                    ['&amp;', '&quot;', '&apos;', '&lt;', '&gt;'],
                    $fieldValue
                ) : '';
            }
            $xml .= "<{$fieldName}>{$fieldValue}</{$fieldName}>\n";
        }
        if ($rootName) {
            $xml = "<{$rootName}>\n{$xml}</{$rootName}>\n";
        }
        if ($addOpenTag) {
            $xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . $xml;
        }

        return $xml;
    }

    /**
     * The "__" style wrapper for toXml method
     *
     * @param  array  $arrAttributes  array of keys that must be represented
     * @param  string  $rootName  root node name
     * @param  bool  $addOpenTag  flag that allow to add initial xml node
     * @param  bool  $addCdata  flag that require wrap all values in CDATA
     * @return string
     */
    public function convertToXml(
        array $arrAttributes = [],
        $rootName = 'item',
        $addOpenTag = false,
        $addCdata = true
    ) {
        return $this->toXml($arrAttributes, $rootName, $addOpenTag, $addCdata);
    }

    /**
     * The "__" style wrapper for toJson
     *
     * @return bool|string
     *
     * @throws InvalidArgumentException
     */
    public function convertToJson(array $keys = [])
    {
        $data = $this->toArray($keys);

        return PHPUtil::jsonEncode($data);
    }

    /**
     * Convert object data into string with predefined format
     *
     * Will use $format as an template and substitute {{key}} for attributes
     *
     * @param  string  $format
     * @return string
     */
    public function toString($format = '')
    {
        if (empty($format)) {
            $result = implode(', ', $this->getData());
        } else {
            preg_match_all('/\{\{([a-z0-9_]+)\}\}/is', $format, $matches);
            foreach ($matches[1] as $var) {
                $data = $this->getData($var) ?? '';
                $format = str_replace('{{' . $var . '}}', $data, $format);
            }
            $result = $format;
        }

        return $result;
    }

    /**
     * Checks whether the object is empty
     *
     * @return bool
     */
    public function isEmpty()
    {
        if (empty($this->_data)) {
            return true;
        }

        return false;
    }

    /**
     * Convert object data into string with defined keys and values.
     *
     * Example: key1="value1" key2="value2" ...
     *
     * @param  array  $keys  array of accepted keys
     * @param  string  $valueSeparator  separator between key and value
     * @param  string  $fieldSeparator  separator between key/value pairs
     * @param  string  $quote  quoting sign
     * @return string
     */
    public function serialize($keys = [], $valueSeparator = '=', $fieldSeparator = ' ', $quote = '"')
    {
        $data = [];
        if (empty($keys)) {
            $keys = array_keys($this->_data);
        }

        foreach ($this->_data as $key => $value) {
            if (in_array($key, $keys)) {
                $data[] = $key . $valueSeparator . $quote . $value . $quote;
            }
        }
        $res = implode($fieldSeparator, $data);

        return $res;
    }

    /**
     * Recursively presents object data as a string in debug mode.
     *
     * @param  mixed  $data  The data to debug.
     * @param  array  $processedObjects  An array to keep track of processed objects to avoid recursion.
     * @return array The debugged data.
     */
    public function debug($data = null, &$processedObjects = [])
    {
        if ($data === null) {
            // Handle initial call
            $hash = spl_object_hash($this);
            if (! empty($processedObjects[$hash])) {
                return '*** RECURSION ***';
            }
            $processedObjects[$hash] = true;
            $data = $this->getData();
        }

        $debuggedData = [];
        foreach ($data as $key => $value) {
            if (is_scalar($value)) {
                // Scalar values can be directly added
                $debuggedData[$key] = $value;
            } elseif (PHPUtil::isArray($value)) {
                // Recursively debug arrays
                $debuggedData[$key] = $this->debug($value, $processedObjects);
            } elseif ($value instanceof DataObject) {
                // Recursively debug DataObject instances
                $debuggedData[$key . ' (' . get_class($value) . ')'] = $value->debug(null, $processedObjects);
            }
        }

        return $debuggedData;
    }

    /**
     * Assigns a value to the specified offset.
     *
     * @param  mixed  $offset  The offset to assign the value to.
     * @param  mixed  $value  The value to set.
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->_data[$offset] = $value;
    }

    /**
     * Checks whether an offset exists.
     *
     * @param  mixed  $offset  The offset to check for.
     * @return bool Returns `true` if the offset exists, `false` otherwise.
     */
    public function offsetExists(mixed $offset): bool
    {
        return isset($this->_data[$offset]) || array_key_exists($offset, $this->_data);
    }

    /**
     * Unsets an offset.
     *
     * @param  mixed  $offset  The offset to unset.
     */
    public function offsetUnset(mixed $offset): void
    {
        unset($this->_data[$offset]);
    }

    /**
     * Retrieves the value at the specified offset.
     *
     * @param  mixed  $offset  The offset to retrieve.
     * @return mixed|null The value at the specified offset, or `null` if the offset does not exist.
     */
    public function offsetGet(mixed $offset): mixed
    {
        return $this->_data[$offset] ?? null;
    }

    /**
     * Get value from _data array without parse key
     *
     * @param  string  $key
     * @return mixed
     */
    protected function _getData($key)
    {
        if (isset($this->_data[$key])) {
            return $this->_data[$key];
        }

        return null;
    }

    /**
     * Converts field names for setters and getters
     *
     * $this->setMyField($value) === $this->setData('my_field', $value)
     * Uses cache to eliminate unnecessary preg_replace
     *
     * @param  string  $name
     * @return string
     */
    protected function _underscore($name)
    {
        if (isset(static::$_underscoreCache[$name])) {
            return static::$_underscoreCache[$name];
        }
        $result = strtolower(trim(preg_replace('/([A-Z]|[0-9]+)/', '_$1', $name), '_'));
        static::$_underscoreCache[$name] = $result;

        return $result;
    }
}
