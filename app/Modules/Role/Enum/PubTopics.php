<?php

declare(strict_types=1);

namespace Modules\Role\Enum;

use BenSampo\Enum\Enum;

/**
 * Enumeration of (Pub/Sub) topics related to publish events.
 *
 * @method static static CREATED()
 * @method static static UPDATED()
 * @method static static DELETED()
 * @method static static SOFT_DELETED()
 * @method static static RECOVERED()
 */
final class PubTopics extends Enum
{
    /**
     * Topic for creation events.
     *
     * @example 'role.created'
     */
    const CREATED = 'role.created';

    /**
     * Topic for update events.
     *
     * @example 'role.updated'
     */
    const UPDATED = 'role.updated';

    /**
     * Topic for deletion events.
     *
     * @example 'role.deleted'
     */
    const DELETED = 'role.deleted';

    /**
     * Topic for soft deletion events.
     *
     * @example 'soft.role.deleted'
     */
    const SOFT_DELETED = 'role.soft.deleted';

    /**
     * Topic for recovery events.
     *
     * @example 'role.recovered'
     */
    const RECOVERED = 'role.recovered';
}
