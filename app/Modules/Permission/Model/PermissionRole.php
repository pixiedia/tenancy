<?php

declare(strict_types=1);

namespace Modules\Permission\Model;

use Illuminate\Database\Eloquent\Model;
use Modules\Permission\Interface\Data\PermissionRoleInterface;

/**
 * Class PermissionRole
 */
class PermissionRole extends Model implements PermissionRoleInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = PermissionRoleInterface::TABLE;
}
