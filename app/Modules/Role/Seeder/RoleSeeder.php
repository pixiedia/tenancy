<?php

declare(strict_types=1);

namespace Modules\Role\Seeder;

use App\Util\PHPUtil;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Modules\Role\Interface\Data\RoleInterface;
use Modules\Role\Model\Role;
use Modules\User\Interface\Data\UserInterface;
use Modules\User\Model\User;

/**
 * Class RoleSeeder
 * Seeder class for generating role data
 */
class RoleSeeder extends Seeder
{
    /**
     * Default size for seeders.
     */
    private $defaultSeederSize;

    /**
     * Default size for relation seeders.
     */
    private $defaultRelationSeederSize;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->defaultSeederSize = Config::get('database.seeders.defaultSeederSize');
        $this->defaultRelationSeederSize = Config::get('database.seeders.defaultSeederRelationSize');
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        try {
            // Log the start of the seeding to the console
            PHPUtil::consoleOutput('🚀 Seeding roles...');

            // Seed roles
            $this->seedRoles();

            // Associate roles with users
            $this->associateRoles();

            // Log the end of the seeding process to the console
            PHPUtil::consoleOutput('✅ Seeding completed.');
        } catch (QueryException $e) {
            // Handle the exception
            PHPUtil::consoleOutput("❌ Error seeding roles: {$e->getMessage()}");
        }
    }

    /**
     * Seed roles.
     */
    private function seedRoles(): void
    {
        // Generate 50 role instances
        $roles = Role::factory()->count($this->defaultSeederSize)->make();

        // Seed each role
        foreach ($roles as $role) {
            DB::table(RoleInterface::TABLE)->insert($role->toArray());

            // Log details about the seeded role
            PHPUtil::consoleOutput("🌱 Seeded role: {$role->getName()}");
        }
    }

    /**
     * Associate roles with users.
     */
    private function associateRoles(): void
    {
        // Get all existing roles
        $roles = Role::all();

        // Get all existing users
        $users = User::all();

        // Associate roles with each user
        $roles->each(function ($role) use ($users) {
            // Attach random roles to the current user
            $randomUsers = $users->random($this->defaultRelationSeederSize);

            /** @var Role $role */
            $role->users()->saveMany($randomUsers);

            // Log details about the user and the associated roles
            PHPUtil::consoleOutput("🔗 Associated roles with user: {$randomUsers->pluck(UserInterface::NAME)->implode(', ')}");
        });
    }
}
