<?php

declare(strict_types=1);

namespace Modules\Permission\Controller;

use App\Abstractions\AbstractController;
use Common\Request\Interface\Data\RequestInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Permission\Dto\CreatePermissionDto;
use Modules\Permission\Dto\UpdatePermissionDto;
use Modules\Permission\Interface\Data\PermissionInterface;
use Modules\Permission\Interface\PermissionServiceInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Tag(
 *      name="Permissions",
 *      description="Permission API Endpoints"
 * )
 *
 * Class PermissionController
 */
class PermissionController extends AbstractController
{
    /**
     * The key used to store the search term in the request.
     */
    const SEARCH_TERM_KEY = 'searchTerm';

    /**
     * The entity name for the pagination.
     *
     * @var string|null
     */
    const ENTITY_NAME = PermissionInterface::ENTITY_NAME;

    /**
     * The Permission service interface instance.
     *
     * @var PermissionServiceInterface
     */
    protected $permissionService;

    /**
     * PermissionController constructor.
     */
    public function __construct(PermissionServiceInterface $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * @OA\Get(
     *      path="/permissions",
     *      summary="Get a list of permissions",
     *      tags={"Permissions"},
     *
     *      @OA\Parameter(
     *          name="page",
     *          in="query",
     *          description="Page number",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=1
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Permission")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a list of permissions.
     *
     * @param  int  $page  The page number.
     * @param  int  $size  The page size.
     * @return JsonResponse The JSON response containing the list of permissions.
     */
    public function getList(Request $request): JsonResponse
    {
        try {
            // Get pagination parameters from the request
            $size = (int) $request->query(RequestInterface::SIZE_KEY, (string) RequestInterface::DEFAULT_PAGE_SIZE);
            $page = (int) $request->query(RequestInterface::PAGE_KEY, (string) RequestInterface::DEFAULT_PAGE_NUMBER);

            // Validate pagination parameters
            $this->validatePaginationParams($page, $size);

            // Retrieve paginated list of permissions from the service
            $permissions = $this->permissionService->list($page, $size);

            // Return JSON response with the list of permissions
            return response()->json($permissions);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/permissions/search/{searchTerm}",
     *      summary="Get a list of permissions by search term",
     *      tags={"Permissions"},
     *
     *      @OA\Parameter(
     *          name="searchTerm",
     *          in="path",
     *          description="Search Term",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="string",
     *              default="Permission A"
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\Parameter(
     *          name="size",
     *          in="query",
     *          description="Page size",
     *          required=false,
     *
     *          @OA\Schema(
     *              type="integer",
     *              default=10
     *          )
     *      ),
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(
     *              required={"searchTerm", "page", "size", "filters", "sortOrder"},
     *
     *              @OA\Property(property="filters", type="object", example={}),
     *              @OA\Property(property="sortOrder", type="string", enum={"asc", "desc"}, example="asc")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Permission")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a list of permissions.
     *
     * @param  Request  $request  The HTTP request object containing search, pagination, filters, and sort parameters.
     * @return JsonResponse The JSON response containing the list of permissions matching the search criteria.
     */
    public function search(Request $request): JsonResponse
    {
        try {
            // Get search term from the request
            $searchTerm = $request->route(static::SEARCH_TERM_KEY);

            // Get pagination parameters from the request
            $size = (int) $request->query(RequestInterface::SIZE_KEY, (string) RequestInterface::DEFAULT_PAGE_SIZE);
            $page = (int) $request->query(RequestInterface::PAGE_KEY, (string) RequestInterface::DEFAULT_PAGE_NUMBER);

            // Validate pagination parameters
            $this->validatePaginationParams($page, $size);

            // Get filters and sort criteria from the request, defaulting to an empty array and default sort field and order if not provided
            $filters = $request->input(RequestInterface::FILTERS_KEY, []);
            $sortOrder = $request->input(RequestInterface::SORT_ORDER_KEY, RequestInterface::DEFAULT_SORT_ORDER);

            // Retrieve paginated search of permissions from the service based on the search term, filters, and sort criteria
            $permissions = $this->permissionService->search($searchTerm, $page, $size, $filters, $sortOrder);

            // Return JSON response with the list of permissions matching the search criteria
            return response()->json($permissions);
        } catch (Exception $e) {
            // Handle any exceptions that occur during the search and return a JSON response with an error message
            throw $e;
        }
    }

    /**
     * @OA\Get(
     *      path="/permissions/{id}",
     *      summary="Get a permission by ID",
     *      tags={"Permissions"},
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the permission to retrieve",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Permission")
     *          )
     *      ),
     *
     *     @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a permission by ID.
     *
     * @param  int  $id  The ID of the permission to retrieve.
     * @return JsonResponse The JSON response containing the permission details.
     */
    public function getById(int $id): JsonResponse
    {
        try {
            // Retrieve the permission by ID from the service
            $permission = $this->permissionService->find($id);

            // Return JSON response with the permission details
            return response()->json($permission);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Get(
     *      path="/permissions/uuid/{uuid}",
     *      summary="Get a permission by UUID",
     *      tags={"Permissions"},
     *
     *      @OA\Parameter(
     *          name="uuid",
     *          in="path",
     *          description="UUID of the permission to retrieve",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Permission")
     *          )
     *      ),
     *
     *     @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Get a permission by UUID.
     *
     * @param  string  $uuid  The UUID of the permission to retrieve.
     * @return JsonResponse The JSON response containing the permission details.
     */
    public function getByUuid(string $uuid): JsonResponse
    {
        try {
            // Retrieve the permission by ID from the service
            $permission = $this->permissionService->findByUuid($uuid);

            // Return JSON response with the permission details
            return response()->json($permission);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/permissions",
     *      summary="Create a new permission",
     *      tags={"Permissions"},
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/CreatePermissionDto")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Permission")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Create a new permission.
     *
     * @param  CreatePermissionDto  $request  The validated request DTO containing the permission data.
     * @return JsonResponse The JSON response containing the newly created permission details.
     */
    public function create(CreatePermissionDto $request): JsonResponse
    {
        try {
            // Get validated data from the request DTO
            $data = $request->validated();

            // Create a new permission using the service
            $permission = $this->permissionService->create($data);

            // Return JSON response with the newly created permission details
            return response()->json($permission);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Post(
     *      path="/permissions/bulk",
     *      summary="Create multiple new permissions",
     *      tags={"Permissions"},
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(
     *              type="array",
     *
     *              @OA\Items(ref="#/components/schemas/Permission")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Permission")),
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Create multiple new permissions.
     *
     * @param  Request  $request  The HTTP request object containing the list of permissions to create.
     * @return JsonResponse The JSON response containing the newly created permissions.
     */
    public function bulkCreate(Request $request): JsonResponse
    {
        try {
            $dataList = $request->list(); // Assuming the request contains an array of permission data
            $permissions = $this->permissionService->bulkCreate($dataList);

            return response()->json($permissions);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @OA\Put(
     *      path="/permissions/{id}",
     *      summary="Update a permission by ID",
     *      tags={"Permissions"},
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the permission to update",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/UpdatePermissionDto")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Permission")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Update a permission by ID.
     *
     * @param  UpdatePermissionDto  $request  The validated request DTO containing the updated permission data.
     * @param  $id  The ID of the permission to update.
     * @return JsonResponse The JSON response containing the updated permission details.
     */
    public function update(UpdatePermissionDto $request, int $id): JsonResponse
    {
        try {
            // Get validated data from the request DTO
            $data = $request->validated();

            // Update the permission using the service
            $permission = $this->permissionService->update($id, $data);

            // Return JSON response with the updated permission details
            return response()->json($permission);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Delete(
     *      path="/permissions/{id}",
     *      summary="Delete a permission by ID",
     *      tags={"Permissions"},
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the permission to delete",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Permission")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Delete a permission by ID.
     *
     * @param  int  $id  The ID of the permission to delete.
     * @return JsonResponse The JSON response containing the deleted permission details.
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            // Delete the permission using the service
            $permission = $this->permissionService->delete($id);

            // Return JSON response with the deleted permission details
            return response()->json($permission);
        } catch (Exception $e) {
            // Handle exceptions and return JSON response with error message
            throw $e;
        }
    }

    /**
     * @OA\Put(
     *      path="/permissions/{id}/restore",
     *      summary="Restore a soft-deleted permission by ID",
     *      tags={"Permissions"},
     *
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the permission to restore",
     *          required=true,
     *
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=200),
     *              @OA\Property(property="message", type="string", example="success"),
     *              @OA\Property(property="data", ref="#/components/schemas/Permission")
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="status_code", type="number", example=400),
     *              @OA\Property(property="message", type="string", example="Bad request")
     *          )
     *      )
     * )
     *
     * Restore a soft-deleted permission by ID.
     *
     * @param  int  $id  The ID of the permission to restore.
     * @return JsonResponse The JSON response indicating the success or failure of the restore operation.
     */
    public function restore(int $id): JsonResponse
    {
        try {
            $permission = $this->permissionService->restore($id);

            return response()->json($permission);
        } catch (Exception $e) {
            throw $e;
        }
    }
}
