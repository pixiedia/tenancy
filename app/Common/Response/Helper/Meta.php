<?php

declare(strict_types=1);

namespace Common\Response\Helper;

use App\Util\PHPUtil;
use Common\Response\Interface\Data\ResponseInterface;

/**
 * Class Meta
 *
 * Model class for handling meta-related operations.
 */
class Meta
{
    /**
     * Constant for 'on' value.
     */
    private const ON = 'on';

    /**
     * Constant for HTTPS protocol.
     */
    private const HTTPS_PROTOCOL = 'https';

    /**
     * Constant for HTTP protocol.
     */
    private const HTTP_PROTOCOL = 'http';

    /**
     * Constant for query string separator.
     */
    private const QUERY_SEPARATOR = '?';

    /**
     * Constant for directory separator.
     */
    private const DIRECTORY_SEPARATOR = DIRECTORY_SEPARATOR;

    /**
     * Constant for the 'query' key.
     */
    private const QUERY = 'query';

    /**
     * Constant for the 'HTTPS' key.
     */
    private const HTTPS = 'HTTPS';

    /**
     * Constant for the 'HTTP_HOST' key.
     */
    private const HTTP_HOST = 'HTTP_HOST';

    /**
     * Constant for the 'REQUEST_URI' key.
     */
    private const REQUEST_URI = 'REQUEST_URI';

    /**
     * Get meta meta data.
     *
     * @param  array  $data  The data to be paginated.
     * @return array Meta meta data if applicable.
     */
    public static function getMeta(array $data): array
    {
        // NOTE: Define the required keys for pagination meta data
        $requiredKeys = [RequestInterface::SIZE_KEY, RequestInterface::PAGE_KEY, ResponseInterface::TOTAL_COUNT];

        // NOTE: Check if all required keys are present in the data and the meta data is valid
        if (self::validateKeys($data, $requiredKeys) && self::isValidMetaData($data)) {
            // NOTE: Extract data for pagination meta
            $currentPage = (int) $data[RequestInterface::SIZE_KEY];
            $perPage = (int) $data[RequestInterface::PAGE_KEY];
            $totalItems = $data[ResponseInterface::TOTAL_COUNT];
            $totalPages = (int) ceil($totalItems / $perPage);

            // NOTE: Get the full request URL
            $requestUrl = self::getUrl();

            // NOTE: Construct and return pagination meta data array
            return [
                'currentPage' => $currentPage,
                'perPage' => $perPage,
                'totalItems' => $totalItems,
                'totalPages' => $totalPages,
                'isFirstPage' => $currentPage === 1,
                'isLastPage' => $currentPage === $totalPages,
                'nextPageUrl' => $currentPage < $totalPages
                    ? self::getPageUrl($requestUrl, $currentPage + 1, $perPage) // NOTE: Get URL for the next page
                    : null,
                'prevPageUrl' => $currentPage > 1
                    ? self::getPageUrl($requestUrl, $currentPage - 1, $perPage) // NOTE: Get URL for the previous page
                    : null,
                'firstPageUrl' => self::getPageUrl($requestUrl, 1, $perPage), // NOTE: Get URL for the first page
                'lastPageUrl' => self::getPageUrl($requestUrl, $totalPages, $perPage), // NOTE: Get URL for the last page
            ];
        }

        // NOTE: Return an empty array if pagination meta data cannot be generated
        return [];
    }

    /**
     * Validate if all required keys are present in the data array.
     *
     * @param  array  $data  The data array.
     * @param  array  $requiredKeys  The required keys.
     * @return bool True if all required keys are present, false otherwise.
     */
    private static function validateKeys(array $data, array $requiredKeys): bool
    {
        return count(array_intersect($requiredKeys, array_keys($data))) === count($requiredKeys);
    }

    /**
     * Validate if the meta data is valid.
     *
     * @param  array  $data  The data array.
     * @return bool True if meta data is valid, false otherwise.
     */
    private static function isValidMetaData(array $data): bool
    {
        return $data[RequestInterface::PAGE_KEY] > 0 && $data[RequestInterface::SIZE_KEY] > 0 && $data[ResponseInterface::TOTAL_COUNT] > 0;
    }

    /**
     * Helper method to retrieve the full request URL.
     *
     * @return string The full request URL.
     */
    private static function getUrl(): string
    {
        // NOTE: Determine the protocol (HTTP or HTTPS)
        $protocol = PHPUtil::isset($_SERVER[self::HTTPS]) && $_SERVER[self::HTTPS] === self::ON
            ? self::HTTPS_PROTOCOL
            : self::HTTP_PROTOCOL;

        // NOTE: Get the host name
        $host = $_SERVER[self::HTTP_HOST];

        // NOTE: Get the original request URI
        $originalUrl = $_SERVER[self::REQUEST_URI];

        // NOTE: Combine the protocol, host, and original URL to form the full request URL
        return "{$protocol}://{$host}{$originalUrl}";
    }

    /**
     * Helper method to generate a URL for a specific page.
     *
     * @param  string  $requestUrl  The full request URL.
     * @param  int  $pageNumber  The page number.
     * @param  int  $pageSize  The page limit.
     * @return string The URL for the specified page.
     */
    private static function getPageUrl(string $requestUrl, int $pageNumber, int $pageSize): string
    {
        // NOTE: Parse the request URL to extract query parameters
        $parsedUrl = PHPUtil::parseUrl($requestUrl);

        // NOTE: Get the query string
        $query = $parsedUrl[self::QUERY] ?? '';

        // NOTE: Determine the protocol (HTTP or HTTPS)
        $protocol = PHPUtil::isset($_SERVER[self::HTTPS]) && $_SERVER[self::HTTPS] === self::ON
            ? self::HTTPS_PROTOCOL
            : self::HTTP_PROTOCOL;

        // NOTE: Get the host name
        $host = $_SERVER[self::HTTP_HOST];

        // NOTE: Explode the request URI to get its segments
        $uri = explode(self::DIRECTORY_SEPARATOR, $_SERVER[self::REQUEST_URI]);

        // NOTE: Construct the URL for the specified page
        $url = "{$protocol}://{$host}/" .
            implode(self::DIRECTORY_SEPARATOR, array_slice($uri, 1, 3)) .
            "{$pageNumber}/{$pageSize}";

        // NOTE: Append the query string if it exists
        if ($query) {
            $url .= self::QUERY_SEPARATOR . $query;
        }

        return $url;
    }
}
