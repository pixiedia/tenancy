<?php

declare(strict_types=1);

namespace Util\Phraser\Renderer;

use Util\Phraser\RendererInterface;

class Placeholder implements RendererInterface
{
    /**
     * The case constant representing keyToPlaceholder.
     */
    public const KEY_TO_PLACEHOLDER = 'keyToPlaceholder';

    /**
     * Render source text
     *
     * @param [] $source
     * @param [] $arguments
     * @return string
     */
    public function render(array $source, array $arguments)
    {
        $text = end($source);

        if ($arguments) {
            $placeholders = array_map([$this, self::KEY_TO_PLACEHOLDER], array_keys($arguments));
            $pairs = array_combine($placeholders, $arguments);
            $text = strtr($text, $pairs);
        }

        return $text;
    }

    /**
     * Get key to placeholder
     *
     * @param  string|int  $key
     * @return string
     *
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function keyToPlaceholder($key)
    {
        return '%' . (is_int($key) ? (string) ($key + 1) : $key);
    }
}
