<?php

declare(strict_types=1);

use Common\Config\Service\ConfigService;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Queue Connection Name
    |--------------------------------------------------------------------------
    |
    | Laravel's queue supports a variety of backends via a single, unified
    | API, giving you convenient access to each backend using identical
    | syntax for each. The default queue connection is defined below.
    |
    */

    'default' => ConfigService::getString('QUEUE_CONNECTION', 'database'),

    /*
    |--------------------------------------------------------------------------
    | Queue Connections
    |--------------------------------------------------------------------------
    |
    | Here you may configure the connection options for every queue backend
    | used by your application. An example configuration is provided for
    | each backend supported by Laravel. You're also free to add more.
    |
    | Drivers: "sync", "database", "beanstalkd", "sqs", "redis", "null"
    |
    */

    'connections' => [

        'sync' => [
            'driver' => 'sync',
        ],

        'database' => [
            'driver' => 'database',
            'connection' => ConfigService::getString('DB_QUEUE_CONNECTION', null),
            'table' => ConfigService::getString('DB_QUEUE_TABLE', 'jobs'),
            'queue' => ConfigService::getString('DB_QUEUE', 'default'),
            'retry_after' => ConfigService::getNumber('DB_QUEUE_RETRY_AFTER', 90),
            'after_commit' => false,
        ],

        'beanstalkd' => [
            'driver' => 'beanstalkd',
            'host' => ConfigService::getString('BEANSTALKD_QUEUE_HOST', 'localhost'),
            'queue' => ConfigService::getString('BEANSTALKD_QUEUE', 'default'),
            'retry_after' => ConfigService::getNumber('BEANSTALKD_QUEUE_RETRY_AFTER', 90),
            'block_for' => 0,
            'after_commit' => false,
        ],

        'sqs' => [
            'driver' => 'sqs',
            'key' => ConfigService::getString('AWS_ACCESS_KEY_ID'),
            'secret' => ConfigService::getString('AWS_SECRET_ACCESS_KEY'),
            'prefix' => ConfigService::getString('SQS_PREFIX', 'https://sqs.us-east-1.amazonaws.com/your-account-id'),
            'queue' => ConfigService::getString('SQS_QUEUE', 'default'),
            'suffix' => ConfigService::getString('SQS_SUFFIX'),
            'region' => ConfigService::getString('AWS_DEFAULT_REGION', 'us-east-1'),
            'after_commit' => false,
        ],

        'redis' => [
            'driver' => 'redis',
            'connection' => ConfigService::getString('REDIS_QUEUE_CONNECTION', 'default'),
            'queue' => ConfigService::getString('REDIS_QUEUE', 'default'),
            'retry_after' => ConfigService::getNumber('REDIS_QUEUE_RETRY_AFTER', 90),
            'block_for' => null,
            'after_commit' => false,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Job Batching
    |--------------------------------------------------------------------------
    |
    | The following options configure the database and table that store job
    | batching information. These options can be updated to any database
    | connection and table which has been defined by your application.
    |
    */

    'batching' => [
        'database' => ConfigService::getString('DB_CONNECTION', 'sqlite'),
        'table' => 'job_batches',
    ],

    /*
    |--------------------------------------------------------------------------
    | Failed Queue Jobs
    |--------------------------------------------------------------------------
    |
    | These options configure the behavior of failed queue job logging so you
    | can control how and where failed jobs are stored. Laravel ships with
    | support for storing failed jobs in a simple file or in a database.
    |
    | Supported drivers: "database-uuids", "dynamodb", "file", "null"
    |
    */

    'failed' => [
        'driver' => ConfigService::getString('QUEUE_FAILED_DRIVER', 'database-uuids'),
        'database' => ConfigService::getString('DB_CONNECTION', 'sqlite'),
        'table' => 'failed_jobs',
    ],
];
