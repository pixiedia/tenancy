<?php

declare(strict_types=1);

namespace Modules\Role\Repository;

use App\Abstractions\AbstractRepository;
use Exception;
use Modules\Role\Interface\Data\RoleInterface;
use Modules\Role\Interface\RoleRepositoryInterface;
use Modules\Role\Model\Role;

/**
 * Class RoleRepository
 *
 * This class implements the RoleRepositoryInterface and provides methods to interact with the Role model.
 */
class RoleRepository extends AbstractRepository implements RoleRepositoryInterface
{
    /**
     * RoleRepository constructor.
     *
     * @param  Role  $model  The Role model instance to use.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(RoleInterface $model)
    {
        // Call the parent constructor with the model and the model interface
        parent::__construct($model, RoleInterface::class);

        // Assign the model instance to the class property
        $this->model = $model;
    }
}
