<?php

declare(strict_types=1);

namespace Modules\Tenant\Observer;

use Common\PubSub\Facade\PubSubService;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\Tenant\Enum\PubTopics;
use Modules\Tenant\Mail\TenantCreated;
use Modules\Tenant\Model\Tenant;
use Ramsey\Uuid\Uuid;

/**
 * Class TenantObserver
 */
class TenantObserver
{
    /**
     * Handle the Tenant "creating" event.
     *
     * @param  Tenant  $tenant  The newly creating tenant.
     */
    public function creating(Tenant $tenant): void
    {
        // Log the creation of the tenant
        Log::info("Tenant creating: {$tenant->getId()}");

        // Generate a UUID for the tenant
        $uuid = Uuid::uuid4()->toString();

        // Set the generated UUID for the tenant
        $tenant->setUuid($uuid);
    }

    /**
     * Handle the Tenant "creating" event.
     *
     * @param  Tenant  $tenant  The newly creating tenant.
     */
    public function created(Tenant $tenant): void
    {
        // Log the creation of the tenant
        Log::info("Tenant created: {$tenant->getId()}");

        // Dispatch the created event
        Event::dispatch(PubTopics::CREATED, $tenant->toDataArray());

        // Pubsub the created event
        PubSubService::publish(PubTopics::CREATED, $tenant->toDataArray());

        // Send an email
        Mail::to('pixiedia@gmail.com')->send(new TenantCreated($tenant));
    }

    /**
     * Handle the Tenant "updated" event.
     *
     * @param  Tenant  $tenant  The updated tenant.
     */
    public function updated(Tenant $tenant): void
    {
        // Log the update of the tenant
        Log::info("Tenant updated: {$tenant->getId()}");

        // Set the updated at timestamp
        $tenant->setUpdatedAt(now());

        // Dispatch the updated event
        Event::dispatch(PubTopics::UPDATED, $tenant->toDataArray());

        // Pubsub the updated event
        PubSubService::publish(PubTopics::UPDATED, $tenant->toDataArray());
    }

    /**
     * Handle the Tenant "deleted" event.
     *
     * @param  Tenant  $tenant  The deleted tenant.
     */
    public function deleted(Tenant $tenant): void
    {
        // Log the deletion of the tenant
        Log::info("Tenant deleted: {$tenant->getId()}");

        // Set the deleted at timestamp
        $tenant->setDeletedAt(now());

        // Dispatch the deleted event
        Event::dispatch(PubTopics::SOFT_DELETED, $tenant->toDataArray());

        // Pubsub the deleted event
        PubSubService::publish(PubTopics::SOFT_DELETED, $tenant->toDataArray());
    }

    /**
     * Handle the Tenant "forceDeleted" event.
     *
     * @param  Tenant  $tenant  The force deleted tenant.
     */
    public function forceDeleted(Tenant $tenant): void
    {
        // Log the force deletion of the tenant
        Log::info("Tenant force deleted: {$tenant->getId()}");

        // Set the deleted at timestamp
        $tenant->setDeletedAt(now());

        // Dispatch the force deleted event
        Event::dispatch(PubTopics::DELETED, $tenant->toDataArray());

        // Pubsub the deleted event
        PubSubService::publish(PubTopics::DELETED, $tenant->toDataArray());
    }

    /**
     * Handle the Tenant "restored" event.
     *
     * @param  Tenant  $tenant  The restored tenant.
     */
    public function restored(Tenant $tenant): void
    {
        // Log the restoration of the tenant
        Log::info("Tenant restored: {$tenant->getId()}");

        // Reset the deleted at timestamp
        $tenant->setDeletedAt(null);

        // Dispatch the restored event
        Event::dispatch(PubTopics::RECOVERED, $tenant->toDataArray());

        // Pubsub the restored event
        PubSubService::publish(PubTopics::RECOVERED, $tenant->toDataArray());
    }
}
