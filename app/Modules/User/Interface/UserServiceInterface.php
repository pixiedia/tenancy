<?php

declare(strict_types=1);

namespace Modules\User\Interface;

use App\Abstractions\Interface\AbstractServiceInterface;

/**
 * Interface UserServiceInterface
 */
interface UserServiceInterface extends AbstractServiceInterface
{
}
