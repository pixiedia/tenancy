<?php

declare(strict_types=1);

namespace Modules\User\Interface;

use App\Abstractions\Interface\AbstractRepositoryInterface;

/**
 * Interface UserRepositoryInterface
 */
interface UserRepositoryInterface extends AbstractRepositoryInterface
{
}
