<?php

declare(strict_types=1);

namespace Modules\Tenant\Exception;

use Facade\IgnitionContracts\BaseSolution;
use Facade\IgnitionContracts\ProvidesSolution;
use Facade\IgnitionContracts\Solution;

/**
 * Exception thrown when a tenant could not be identified based on a domain.
 */
class TenantCouldNotBeIdentifiedOnDomainException extends TenantCouldNotBeIdentifiedException implements ProvidesSolution
{
    /**
     * Create a new exception instance.
     *
     * @param  string  $domain  The domain that could not be identified.
     */
    public function __construct(string $domain)
    {
        parent::__construct("Tenant could not be identified on domain {$domain}");
    }

    /**
     * Get the solution for this exception.
     *
     * @return Solution The solution for the exception.
     */
    public function getSolution(): Solution
    {
        // Create a base solution with a title and description
        return BaseSolution::create('Tenant could not be identified on this domain')
            ->setSolutionDescription('Did you forget to create a tenant for this domain?');
    }
}
