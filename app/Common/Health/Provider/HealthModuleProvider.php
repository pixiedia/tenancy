<?php

declare(strict_types=1);

namespace Common\Health\Provider;

use App\Abstractions\AbstractModuleProvider;
use Common\Config\Service\ConfigService;
use Common\Health\Router\V1\HealthRoutes;
use Spatie\Health\Checks\Checks\BackupsCheck;
use Spatie\Health\Checks\Checks\CacheCheck;
use Spatie\Health\Facades\Health;
use Spatie\Health\Checks\Checks\UsedDiskSpaceCheck;
use Spatie\Health\Checks\Checks\DatabaseCheck;
use Spatie\Health\Checks\Checks\DatabaseConnectionCountCheck;
use Spatie\Health\Checks\Checks\DebugModeCheck;
use Spatie\Health\Checks\Checks\EnvironmentCheck;
use Spatie\Health\Checks\Checks\HorizonCheck;
use Spatie\Health\Checks\Checks\MeiliSearchCheck;
use Spatie\Health\Checks\Checks\PingCheck;
use Spatie\Health\Checks\Checks\QueueCheck;
use Spatie\Health\Checks\Checks\RedisCheck;
use Spatie\Health\Checks\Checks\ScheduleCheck;
use Spatie\SecurityAdvisoriesHealthCheck\SecurityAdvisoriesCheck;
use Spatie\CpuLoadHealthCheck\CpuLoadCheck;

/**
 * Class HealthModuleProvider
 */
class HealthModuleProvider extends AbstractModuleProvider
{
    /**
     * CustomerModuleProvider constructor.
     *
     * Initializes the module provider with route, seeder, and migration files.
     */
    public function __construct()
    {
        // Glob all route files matching the defined path
        $this->routers = [HealthRoutes::class];
    }

    /**
     * Bootstrap services.
     *
     * This method is called after all other service providers have been registered,
     * meaning you have access to all other services in the application here.
     */
    public function boot(): void
    {
        parent::boot();

        Health::checks([
            DatabaseCheck::new(),
            CacheCheck::new(),
            BackupsCheck::new()->locatedAt('/path/to/backups/*.zip'),
            CpuLoadCheck::new()->failWhenLoadIsHigherInTheLast5Minutes(2.0)->failWhenLoadIsHigherInTheLast15Minutes(1.5),
            DatabaseConnectionCountCheck::new()
                ->warnWhenMoreConnectionsThan(50)
                ->failWhenMoreConnectionsThan(100),
            DebugModeCheck::new(),
            EnvironmentCheck::new()->expectEnvironment('local'),
            HorizonCheck::new(),
            MeiliSearchCheck::new()->url(ConfigService::getString("MEILISEARCH_HOST")),
            PingCheck::new()->url('https://spatie.be')->name('Spatie'),
            UsedDiskSpaceCheck::new()
                ->warnWhenUsedSpaceIsAbovePercentage(60)
                ->failWhenUsedSpaceIsAbovePercentage(80),
            SecurityAdvisoriesCheck::new(),
            ScheduleCheck::new()->useCacheStore('redis'),
            RedisCheck::new(),
            QueueCheck::new()->onQueue(['default']),
        ]);
    }
}
