<?php

declare(strict_types=1);

namespace Modules\Tenant\Factory;

use App\Util\PHPUtil;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Modules\Tenant\Interface\Data\TenantInterface;
use Modules\Tenant\Model\Tenant;
use RuntimeException;

/**
 * Class TenantFactory
 */
class TenantFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tenant::class;

    /**
     * Define the model's default state.
     *
     * This method defines the default attributes for the Tenant model when creating fake data.
     *
     * @return array The default attributes for the Tenant model.
     */
    public function definition()
    {
        // Get the base domain from the application URL
        $domain = $this->getDomain();

        // Return an array containing the default attributes for the Tenant model
        return [
            // Generate a random UUID for the tenant
            TenantInterface::UUID => $this->faker->uuid,

            // Generate a random name for the tenant
            TenantInterface::NAME => $this->faker->name,

            // Combine a random subdomain with the base domain to create a full domain for the tenant
            TenantInterface::DOMAIN => $this->getSubdomain() . '.' . $domain,

            // Generate a random value between 1 and 10 for the created_by field
            TenantInterface::CREATED_BY => $this->faker->numberBetween(1, 10),

            // Generate a random value between 1 and 10 for the updated_by field
            TenantInterface::UPDATED_BY => $this->faker->numberBetween(1, 10),

            // Generate a random status for the tenant (active, inactive, suspended, or archived)
            TenantInterface::STATUS => $this->faker->randomElement(['active', 'inactive', 'suspended', 'archived']),

            // Generate random metadata for the tenant
            TenantInterface::METADATA => $this->getMetadata(),
        ];
    }

    /**
     * Configure the model factory.
     *
     * This method defines an after creating hook that attaches random teams to the tenant after it's created.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (Tenant $tenant) {
            // Attach random teams to the tenant
            $teamIds = Tenant::inRandomOrder()->limit(rand(1, 3))->pluck('id')->toArray();
            $tenant->teams()->attach($teamIds);
        });
    }

    /**
     * Get the domain from the application URL.
     *
     * @return string
     */
    private function getDomain()
    {
        $parsedUrl = parse_url(Config::get('app.url'));

        // Remove 'www.' from the host
        return Str::replaceFirst('www.', '', $parsedUrl['host']);
    }

    /**
     * Generate a random subdomain.
     *
     * @return string
     */
    private function getSubdomain()
    {
        // Generate a random subdomain
        return $this->faker->unique()->domainWord;
    }

    /**
     * Get a randomly generated metadata array.
     *
     * @return string The generated metadata array.
     *
     * @throws RuntimeException If the metadata array fails to encode as JSON.
     */
    private function getMetadata(): string
    {
        // Initialize an empty array to store metadata
        $metadata = [
            'username' => $this->faker->userName,
            'avatar' => $this->faker->imageUrl(),
            'mobile' => $this->faker->unique()->phoneNumber,
            'status' => $this->faker->randomElement(['active', 'inactive']),
            'plan' => $this->faker->randomElement(['basic', 'pro', 'premium']),
            'store_location' => $this->faker->latitude . ',' . $this->faker->longitude,
            'tax_number' => $this->faker->unique()->numberBetween(100000000, 999999999),
            'commercial_number' => $this->faker->unique()->numberBetween(1000000000, 9999999999),
        ];

        // Encode the metadata array as JSON
        $encodedMetadata = PHPUtil::jsonEncode($metadata);

        // Check if encoding was successful
        if ($encodedMetadata === false) {
            throw new RuntimeException('Failed to encode metadata as JSON: ' . json_last_error_msg());
        }

        return $encodedMetadata;
    }
}
