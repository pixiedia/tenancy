<?php

declare(strict_types=1);

namespace App\Util;

use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Class PHPUtil
 *
 * General-purpose utility class.
 */
class PHPUtil
{
    /**
     * Constructor for the PHPUtil class.
     */
    public function __construct()
    {
    }

    /**
     * Dump variables and end script execution.
     *
     * @param  mixed  ...$variables  The variables to dump.
     */
    public static function dd(...$variables)
    {
        foreach ($variables as $variable) {
            echo "\n";
            VarDumper::dump($variable, '👉');
        }
        exit;
    }

    /**
     * Output a message to the console.
     *
     * @param  string  $message  The message to output.
     * @param  int  $level  The output level (one of the OutputInterface constants).
     */
    public static function consoleOutput(string $message, int $level = OutputInterface::OUTPUT_NORMAL)
    {
        $output = new ConsoleOutput();
        echo "\n";
        $output->writeln("<fg=green;options=bold>{$message}</>");
        echo "\n";
    }

    /**
     * Dump variables and end script execution.
     *
     * @param  mixed  ...$variables  The variables to dump.
     */
    public static function ddhtml(...$variables)
    {
        $cloner = new VarCloner();
        $dumper = new HtmlDumper();

        $dumper->setStyles([
            'default' => 'background-color:#f6f8fa; color:#24292e; line-height:1.3em; font:12px Menlo, Monaco, Consolas, monospace; word-wrap: break-word; white-space: pre-wrap; position:relative; z-index:99999; word-break: normal',
            'public' => 'color:#22863a',
            'protected' => 'color:#aa0d91',
            'private' => 'color:#aa0d91',
        ]);

        foreach ($variables as $variable) {
            $dumper->dump($cloner->cloneVar($variable));
        }

        exit(1);
    }

    /**
     * Generate HMAC using SHA-256.
     *
     * @param  string  $data  The data to be hashed.
     * @param  string  $key  The secret key for HMAC.
     * @return string The HMAC hash.
     */
    public static function HMAC(string $data, string $key): string
    {
        return hash_hmac('sha256', $data, $key);
    }

    /**
     * Hash the data using a specified algorithm.
     *
     * @param  string  $data  The data to be hashed.
     * @param  string|null  $algorithm  The hashing algorithm.
     * @return string|null Hashed data or null if data is not provided.
     */
    public static function HASH(string $data, ?string $algorithm = 'sha256'): ?string
    {
        if ($data) {
            return hash($algorithm, (string) $data);
        }

        return null;
    }

    /**
     * Checks if a string is valid JSON.
     *
     * @param  string  $json  The JSON string to validate.
     * @return bool Returns true if the string is valid JSON, otherwise returns false.
     */
    public static function isValidJson(string $json): bool
    {
        json_decode($json);

        return json_last_error() === JSON_ERROR_NONE;
    }

    /**
     * Check if a class exists.
     *
     * @param  string  $class  The class name to check.
     */
    public static function isClassExists(string $class): bool
    {
        if (! class_exists($class)) {
            // NOTE: NOTE: Attempt to add ::class suffix if it's not present
            return class_exists($class . '::class');
        }

        return true;
    }

    /**
     * Create an instance of the specified class.
     *
     * @param  string  $class  The class name.
     * @return object|null An instance of the class or null if the class doesn't exist.
     */
    public static function createInstance(string $class): ?object
    {
        if (! class_exists($class)) {
            // NOTE: NOTE: Attempt to add ::class suffix if it's not present
            $classWithSuffix = class_exists($class . '::class') ? $class . '::class' : null;

            if (! $classWithSuffix) {
                return null; // NOTE: NOTE: Class doesn't exist even with ::class suffix
            }

            $class = $classWithSuffix;
        }

        // NOTE: NOTE: Check if the class has a static getInstance method
        if (method_exists($class, 'getInstance')) {
            return $class::getInstance();
        }

        return new $class();

    }

    /**
     * Check if a function exists.
     *
     * @param  string  $functionName  The function name to check.
     */
    public static function isFunctionExists(string $functionName): bool
    {
        return function_exists($functionName);
    }

    /**
     * Check if a constant is defined.
     *
     * @param  string  $constantName  The constant name to check.
     */
    public static function isDefined(string $constantName): bool
    {
        return defined($constantName);
    }

    /**
     * Get the value of a defined constant.
     *
     * @param  string  $constantName  The constant name.
     * @return mixed|null The value of the constant or null if not defined.
     */
    public static function getDefinedValue(string $constantName)
    {
        return static::isDefined($constantName) ? constant($constantName) : null;
    }

    /**
     * Check if a property exists in an object.
     *
     * @param  object  $object  The object to check.
     * @param  string  $property  The property name.
     */
    public static function propertyExists(object $object, string $property): bool
    {
        return property_exists($object, $property);
    }

    /**
     * Check if a key exists in an array.
     *
     * @param  array  $array  The array to check.
     * @param  string|int  $key  The key to check.
     */
    public static function arrayKeyExists(array $array, $key): bool
    {
        return array_key_exists($key, $array);
    }

    /**
     * Check if a file exists.
     *
     * @param  string  $filePath  The path to the file.
     */
    public static function isFileExists(string $filePath): bool
    {
        return file_exists($filePath);
    }

    /**
     * Check if a directory exists.
     *
     * @param  string  $directoryPath  The path to the directory.
     */
    public static function isDirectoryExists(string $directoryPath): bool
    {
        return is_dir($directoryPath);
    }

    /**
     * Check if a variable is set and is not null.
     *
     * @param  mixed  $variable  The variable to check.
     */
    public static function isset($variable): bool
    {
        return isset($variable);
    }

    /**
     * Check if a variable is an array.
     *
     * @param  mixed  $variable  The variable to check.
     */
    public static function isArray($variable): bool
    {
        return is_array($variable);
    }

    /**
     * Check if a variable is null.
     *
     * @param  mixed  $variable  The variable to check.
     */
    public static function isNull($variable): bool
    {
        return is_null($variable);
    }

    /**
     * Check if a variable is of type integer.
     *
     * @param  mixed  $variable  The variable to check.
     */
    public static function isInt($variable): int|bool
    {
        return is_int($variable) ? (int) $variable : false;
    }

    /**
     * Check if a variable is of type string.
     *
     * @param  mixed  $variable  The variable to check.
     */
    public static function isString($variable): bool
    {
        return is_string($variable);
    }

    /**
     * Check if a variable is empty.
     *
     * @param  mixed  $variable  The variable to check.
     */
    public static function empty($variable): bool
    {
        return empty($variable);
    }

    /**
     * Convert a string to lowercase.
     *
     * @param  string  $string  The string to convert.
     */
    public static function strtolower(string $string): string
    {
        return strtolower($string);
    }

    /**
     * Convert a string to uppercase.
     *
     * @param  string  $string  The string to convert.
     */
    public static function strtoupper(string $string): string
    {
        return strtoupper($string);
    }

    /**
     * Convert a string to lowercase using multibyte encoding.
     *
     * @param  string  $str  The input string.
     * @param  string|null  $encoding  The character encoding. If not provided, it will use the internal encoding.
     * @return string The lowercase string.
     */
    public static function mbStrToLower(string $str, ?string $encoding = null): string
    {
        return mb_strtolower($str, $encoding);
    }

    /**
     * Wrapper for mb_strtoupper function.
     *
     * @param  string  $str  The input string to convert.
     * @return string The converted uppercase string.
     */
    public static function mbStrToUpper($str)
    {
        return mb_strtoupper($str);
    }

    /**
     * Check if the given value is callable.
     *
     * @param  mixed  $value  The value to check.
     */
    public static function isCallable($value): bool
    {
        return is_callable($value);
    }

    /**
     * Check if the given value is numeric.
     *
     * @param  mixed  $value  The value to check.
     */
    public static function isNumeric($value): bool
    {
        return is_numeric($value);
    }

    /**
     * Perform a regular expression match.
     *
     * @param  string  $pattern  The regular expression pattern.
     * @param  string  $subject  The subject string to search.
     * @return int|false The number of times the pattern matches (which may be zero), or false if an error occurred.
     */
    public static function pregMatch(string $pattern, string $subject): int|false
    {
        return preg_match($pattern, $subject);
    }

    /**
     * Perform a global regular expression match and store the matches.
     *
     * @param  string  $pattern  The regular expression pattern.
     * @param  string  $subject  The subject string to search.
     * @param  array|null  $matches  An array to store the matches.
     * @return int The number of full pattern matches.
     */
    public static function pregMatchAll(string $pattern, string $subject, ?array &$matches): int
    {
        // NOTE: NOTE: Perform a global regular expression match and store the matches.
        return preg_match_all($pattern, $subject, $matches);
    }

    /**
     * Decode HTML entities in a string.
     *
     * @param  string  $string  The string containing HTML entities.
     * @return string The decoded string.
     */
    public static function htmlEntityDecode(string $string): string
    {
        return html_entity_decode($string);
    }

    /**
     * Trim words in a string to a specified length.
     *
     * @param  string  $text  The input string.
     * @param  int  $length  The maximum number of words.
     * @param  string  $suffix  The optional suffix to append.
     * @return string Trimmed string.
     */
    public static function trimWords($text, $length, $suffix = '...')
    {
        $words = explode(' ', $text, $length + 1);

        if (count($words) > $length) {
            array_pop($words);
            $text = implode(' ', $words) . $suffix;
        }

        return $text;
    }

    /**
     * Get the class name without the namespace.
     *
     * @param  object|string  $class  The object or class name
     * @return string
     */
    public static function getClassName($class)
    {
        if (is_object($class)) {
            return get_class($class);
        }
        if (is_string($class) && class_exists($class)) {
            $parts = explode('\\', $class);

            return end($parts);
        }

        return $class; // NOTE: Return the provided parameter if it's neither an object nor a class

    }

    /**
     * Alias for json_encode with additional options parameter.
     *
     * @param  mixed  $value  The value being encoded. Can be any type except a resource.
     * @param  int  $options  Bitmask consisting of JSON_FORCE_OBJECT, JSON_HEX_QUOT, JSON_HEX_TAG, JSON_HEX_AMP, JSON_HEX_APOS, JSON_INVALID_UTF8_IGNORE, JSON_INVALID_UTF8_SUBSTITUTE, JSON_NUMERIC_CHECK, JSON_PARTIAL_OUTPUT_ON_ERROR, JSON_PRESERVE_ZERO_FRACTION, JSON_PRETTY_PRINT, JSON_UNESCAPED_LINE_TERMINATORS, JSON_UNESCAPED_SLASHES, JSON_THROW_ON_ERROR.
     * @param  int  $depth  Set the maximum depth. Must be greater than zero.
     * @return string|null Returns the JSON string if encoding is successful, otherwise returns null.
     */
    public static function jsonEncode($value, int $options = 0, int $depth = 512): ?string
    {
        $json = json_encode($value, $options, $depth);

        return static::isValidJson($json) ? $json : null;
    }

    /**
     * Alias for json_decode with additional associative and depth parameters.
     *
     * @param  string  $json  The json string being decoded.
     * @param  bool  $assoc  When true, returned objects will be converted into associative arrays.
     * @param  int  $depth  User specified recursion depth.
     * @param  int  $options  Bitmask of JSON_BIGINT_AS_STRING, JSON_INVALID_UTF8_IGNORE, JSON_INVALID_UTF8_SUBSTITUTE, JSON_OBJECT_AS_ARRAY, JSON_THROW_ON_ERROR.
     * @return mixed|null Returns the decoded value if successful, otherwise returns null.
     */
    public static function jsonDecode(string $json, bool $assoc = false, int $depth = 512, int $options = 0)
    {
        return static::isValidJson($json) ? json_decode($json, $assoc, $depth, $options) : null;
    }

    /**
     * Get all directories in the specified path.
     *
     * @param  string  $path  The path to scan for directories.
     * @return array An array of directory names.
     */
    public static function getModules($path = 'Modules')
    {
        $directories = [];
        // Scan the specified path for directories
        $items = scandir(app_path($path));

        foreach ($items as $item) {
            // Exclude the current directory (.) and parent directory (..)
            if ($item !== '.' && $item !== '..') {
                $itemPath = app_path($path) . DIRECTORY_SEPARATOR . $item;
                // Check if the item is a directory
                if (is_dir($itemPath)) {
                    // Add the directory to the list
                    $directories[] = $item;
                }
            }
        }

        return $directories;
    }

    /**
     * Get the domain from a URL.
     *
     * @param  string  $url  The URL to extract the domain from.
     * @return string The extracted domain.
     */
    public static function parseUrl($url)
    {
        return parse_url($url);
    }

    /**
     * Get the domain from a URL.
     *
     * @param  string  $url  The URL to extract the domain from.
     * @return string The extracted domain.
     */
    public static function getDomainFromUrl($url)
    {
        $parsedUrl = parse_url($url);

        // Check if the 'host' key exists in the parsed URL array
        if (isset($parsedUrl['host'])) {
            // Remove 'www.' from the host
            $parsedUrl['host'] = Str::replaceFirst('www.', '', $parsedUrl['host']);

            return $parsedUrl['host'];
        }

        return '';
    }

    /**
     * Parses a PHP file and retrieves the fully qualified class name with namespace.
     *
     * @param  string  $file_path  The path to the PHP file.
     * @return string The fully qualified class name.
     */
    public static function getFullyQualifiedClassName(string $file_path): string
    {
        // Read the file contents
        $file_contents = file_get_contents($file_path);

        // Use regular expressions to find the namespace and class name
        $namespace_pattern = '/namespace\s+(.*?);/i';
        $class_pattern = '/class\s+(.*?)\s+/i';

        $namespace = '';
        $class_name = '';

        // Find the namespace
        if (preg_match($namespace_pattern, $file_contents, $matches)) {
            $namespace = trim($matches[1]);
        }

        // Find the class name
        if (preg_match($class_pattern, $file_contents, $matches)) {
            $class_name = trim($matches[1]);
        }

        // Combine the namespace and class name to get the fully qualified class name
        $fully_qualified_class_name = $namespace . '\\' . $class_name;

        return $fully_qualified_class_name;
    }
}
