<?php

declare(strict_types=1);

namespace Common\Response\Trait;

use Common\Http\Enum\HttpStatusCode;
use Common\Http\Enum\HttpStatusType;
use Common\Response\Interface\Data\ResponseInterface;
use Common\Response\Model\Response;
use Exception;
use Throwable;
use Util\Phraser\Phrase;

/**
 * Class ResponseBuilder
 *
 * Builds API responses.
 */
trait ResponseBuilder
{
    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * Create a success response.
     *
     * @param  array  $payload  The data to be included in the response.
     * @param  int|null  $statusCode  The HTTP status code (optional).
     * @param  string|Phrase  $message  The response message (optional).
     * @param  array|null  $meta  The meta information (optional).
     * @return ResponseInterface An instance of the Response class.
     */
    public function createSuccess(array $payload = [], int $statusCode = HttpStatusCode::OK, string|Phrase $message = HttpStatusType::OK, ?array $meta = null): ResponseInterface
    {
        // Instantiate the Response class directly
        $this->response = new Response();

        // NOTE: Set status code
        $this->response->setStatusCode($statusCode);

        // NOTE: Set message
        if ($message instanceof Phrase) {
            $this->response->setMessage($message->render());
        } else {
            $this->response->setMessage($message);
        }

        // Set the response payload
        $this->response->setPayload($payload);

        // Check if meta is not null and contains exactly 3 elements
        if ($meta !== null && count($meta) === 3) {
            // Extract the values from the meta array
            [$pageNumber, $pageSize, $totalCount] = $meta;

            // Check if pageNumber, pageSize, and totalCount are greater than zero
            if ($pageNumber > 0 && $pageSize > 0 && $totalCount > 0) {
                // Set the meta values in the response
                $this->response->setMeta(compact(ResponseInterface::PAGE_NUMBER_KEY, ResponseInterface::PAGE_SIZE_KEY, ResponseInterface::TOTAL_COUNT_KEY));
            }
        }

        return $this->response;
    }

    /**
     * Create a failure response.
     *
     * @param  string|Phrase  $message  The error message.
     * @param  int|null  $statusCode  The error code (optional).
     * @param  Exception|null  $cause  The cause of the failure (optional).
     * @return ResponseInterface An instance of the Response class.
     */
    public function createFailure(string|Phrase $message, ?int $statusCode = HttpStatusCode::BAD_REQUEST, ?Exception $cause = null): ResponseInterface
    {
        // Instantiate a new Response object
        $this->response = new Response();

        // Initialize the payload with an empty array
        $this->response->setPayload([]);

        // Determine the status code based on the cause or provided status code
        if ($cause instanceof Throwable && $cause->getCode() >= HttpStatusCode::OK && $cause->getCode() < HttpStatusCode::MULTIPLE_CHOICES) {
            $statusCode = $cause->getCode();
        }

        // Set the determined status code in the response
        $this->response->setStatusCode($statusCode);

        // Set the cause of the failure in the response
        $this->response->setCause($cause);

        // Set the error message in the response, rendering it if it's a Phrase object
        $this->response->setMessage($message instanceof Phrase ? $message->render() : $message);

        // Return the created response
        return $this->response;
    }

    /**
     * Set the headers.
     *
     * @param  array|string  $headers  The headers to be included in the response.
     */
    public function setHeaders($headers): void
    {
        $this->response->setHeaders($headers);
    }

    /**
     * Set the errors.
     *
     * @param  array  $headers  The errors to be included in the response.
     */
    public function setErrors(array $errors): void
    {
        $this->response->setErrors($errors);
    }
}
