<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Create the password_reset_tokens table
        Schema::create('password_reset_tokens', function (Blueprint $table) {
            // Primary key (email)
            $table->string('email')->primary()->comment('The email address associated with the token');

            // Password reset token
            $table->string('token')->comment('The password reset token');

            // Timestamp when the token was created
            $table->timestamp('created_at')->nullable()->comment('The timestamp when the token was created');
        });

        // Add a comment to the 'password_reset_tokens' table
        Schema::table('password_reset_tokens', function (Blueprint $table) {
            $table->comment('Password reset tokens table');
        });
    }

    /**
     * Reverse the migrations.
     *
     * Drop the 'customers', 'password_reset_tokens', and 'sessions' tables if they exist.
     */
    public function down(): void
    {
        // Drop the 'password_reset_tokens' table if it exists
        Schema::dropIfExists('password_reset_tokens');
    }
};
