<?php

declare(strict_types=1);

namespace Modules\Tenant\Repository;

use App\Abstractions\AbstractRepository;
use Exception;
use Modules\Tenant\Interface\Data\TenantInterface;
use Modules\Tenant\Interface\TenantRepositoryInterface;
use Modules\Tenant\Model\Tenant;

/**
 * Class TenantRepository
 *
 * This class implements the TenantRepositoryInterface and provides methods to interact with the Tenant model.
 */
class TenantRepository extends AbstractRepository implements TenantRepositoryInterface
{
    /**
     * TenantRepository constructor.
     *
     * @param  Tenant  $model  The Tenant model instance to use.
     *
     * @throws Exception If $modelInterface is null.
     */
    public function __construct(TenantInterface $model)
    {
        // Call the parent constructor with the model and the model interface
        parent::__construct($model, TenantInterface::class);

        // Assign the model instance to the class property
        $this->model = $model;
    }
}
