<?php

declare(strict_types=1);

namespace Modules\Tenant\Interface;

use App\Abstractions\Interface\AbstractServiceInterface;
use Common\Response\Interface\Data\ResponseInterface;

/**
 * Interface TenantServiceInterface
 */
interface TenantServiceInterface extends AbstractServiceInterface
{
    /**
     * Assign a team to a tenant.
     *
     * @param  int  $tenantId  The ID of the tenant.
     * @param  int  $teamId  The ID of the team to assign.
     * @return Model
     *
     * @throws Exception
     */
    public function assignTeam(int $tenantId, int $teamId): ResponseInterface;

    /**
     * Detach a team to a tenant.
     *
     * @param  int  $tenantId  The ID of the tenant.
     * @param  int  $teamId  The ID of the team to assign.
     * @return Model
     *
     * @throws Exception
     */
    public function detachTeam(int $tenantId, int $teamId): ResponseInterface;
}
