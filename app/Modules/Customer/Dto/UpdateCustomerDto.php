<?php

declare(strict_types=1);

namespace Modules\Customer\Dto;

use App\Sanitizers\JsonEncode;
use ArondeParon\RequestSanitizer\Traits\SanitizesInputs;
use Common\Http\Enum\HttpStatusCode;
use Common\Response\Trait\ResponseBuilder;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Modules\Customer\Interface\Data\CustomerInterface;
use Util\Phraser\Phrase;

/**
 * @OA\Schema(
 *      schema="UpdateCustomerDto",
 *      required={"name", "description"},
 *
 *      @OA\Property(
 *          property="name",
 *          type="string",
 *          example="Customer A"
 *      ),
 *      @OA\Property(
 *          property="description",
 *          type="string",
 *          example="Description of Customer A"
 *      ),
 *      @OA\Property(
 *          property="updated_at",
 *          type="string",
 *          format="date-time",
 *          example="2024-03-25 10:00:00"
 *      ),
 *      @OA\Property(
 *          property="deleted_at",
 *          type="string",
 *          format="date-time",
 *          example="2024-03-25 10:00:00"
 *      ),
 *      @OA\Property(
 *          property="updated_by",
 *          type="integer",
 *          example=1
 *      ),
 *      @OA\Property(
 *          property="deleted_by",
 *          type="integer",
 *          example=1
 *      ),
 *      @OA\Property(
 *          property="metadata",
 *          type="array",
 *
 *          @OA\Items(
 *              type="string",
 *              example="value1"
 *          )
 *      )
 * )
 */
class UpdateCustomerDto extends FormRequest
{
    use ResponseBuilder, SanitizesInputs;

    /**
     * The array of sanitizers to apply to specific fields.
     *
     * @var array
     */
    protected $sanitizers = [
        CustomerInterface::METADATA => [
            JsonEncode::class,
        ],
    ];

    /**
     * Determine if the customer is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Update this based on your authorization logic
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            CustomerInterface::NAME => 'required|string',
            CustomerInterface::UPDATED_AT => 'nullable|date_format:Y-m-d H:i:s',
            CustomerInterface::DELETED_AT => 'nullable|date_format:Y-m-d H:i:s',
            CustomerInterface::UPDATED_BY => 'nullable|integer',
            CustomerInterface::DELETED_BY => 'nullable|integer',
            CustomerInterface::METADATA => ['array'], // Assuming metadata should be an array
            CustomerInterface::METADATA . '.*' => 'string', // Assuming each value in metadata should be a string
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  Validator  $validator  The validator instance containing the validation errors.
     * @return void
     *
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        // Create a failure response with a generic error message
        $response = $this->createFailure(Phrase::__('An error occurred while validating request.'), HttpStatusCode::UNPROCESSABLE_ENTITY);

        // Set the validation errors from the validator instance into the response
        $response->setErrors($validator->errors()->jsonSerialize());

        // Throw a ValidationException with the validator instance and the failure response as the response
        throw new ValidationException($validator, response()->json($response->toArray(), HttpStatusCode::UNPROCESSABLE_ENTITY));
    }
}
