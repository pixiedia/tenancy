<?php

declare(strict_types=1);

namespace Modules\Permission\Router\V1;

use App\Abstractions\AbstractRouter;
use Common\Http\Enum\HttpMethod;
use Modules\Permission\Controller\PermissionController;
use Modules\Permission\Interface\Data\PermissionInterface;

/**
 * Router class for defining routes related to the Permission module.
 */
class PermissionRoutes extends AbstractRouter
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->apiPrefix = PermissionInterface::ENTITY_NAME;
        $this->controllerClass = PermissionController::class;
    }

    /**
     * Get the routes configuration for the router.
     *
     * @return array The routes configuration.
     */
    public function registerRoutes(): array
    {
        return [
            HttpMethod::GET => [
                // Route to get a list of permissions
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR,
                    AbstractRouter::KEY_ACTION => 'getList',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'getList',
                    AbstractRouter::KEY_PREFIX => '',
                ],

                // Route to get a permission by ID
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR . '{' . PermissionInterface::ID . '}',
                    AbstractRouter::KEY_ACTION => 'getById',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'getById',
                    AbstractRouter::KEY_PREFIX => '',
                ],

                // Route to get a permission by UUID
                [
                    AbstractRouter::KEY_URI => '/' . PermissionInterface::UUID . '/{' . PermissionInterface::UUID . '}',
                    AbstractRouter::KEY_ACTION => 'getByUuid',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'getByUuid',
                    AbstractRouter::KEY_PREFIX => '',
                ],
            ],
            HttpMethod::POST => [
                // Route to create a new permission
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR,
                    AbstractRouter::KEY_ACTION => 'create',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'create',
                    AbstractRouter::KEY_PREFIX => '',
                ],

                // Route to search for permissions
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR . 'search',
                    AbstractRouter::KEY_ACTION => 'search',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'search',
                    AbstractRouter::KEY_PREFIX => '',
                ],

                // Route to bulk create permissions
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR . 'bulk',
                    AbstractRouter::KEY_ACTION => 'bulkCreate',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'bulkCreate',
                    AbstractRouter::KEY_PREFIX => '',
                ],
            ],
            HttpMethod::PUT => [
                // Route to update a permission
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR . '{' . PermissionInterface::ID . '}',
                    AbstractRouter::KEY_ACTION => 'update',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'update',
                    AbstractRouter::KEY_PREFIX => '',
                ],

                // Route to restore a permission
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR . 'restore/{' . PermissionInterface::ID . '}',
                    AbstractRouter::KEY_ACTION => 'restore',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'restore',
                    AbstractRouter::KEY_PREFIX => '',
                ],
            ],
            HttpMethod::DELETE => [
                // Route to delete a permission
                [
                    AbstractRouter::KEY_URI => DIRECTORY_SEPARATOR . '{' . PermissionInterface::ID . '}',
                    AbstractRouter::KEY_ACTION => 'destroy',
                    AbstractRouter::KEY_MIDDLEWARE => [],
                    AbstractRouter::KEY_NAME => 'destroy',
                    AbstractRouter::KEY_PREFIX => '',
                ],
            ],
        ];
    }
}
