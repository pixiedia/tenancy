<?php

declare(strict_types=1);

namespace Modules\Customer\Observer;

use Common\PubSub\Facade\PubSubService;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\Customer\Enum\PubTopics;
use Modules\Customer\Mail\CustomerCreated;
use Modules\Customer\Model\Customer;
use Ramsey\Uuid\Uuid;

/**
 * Class CustomerObserver
 */
class CustomerObserver
{
    /**
     * Handle the Customer "creating" event.
     *
     * @param  Customer  $customer  The newly creating customer.
     */
    public function creating(Customer $customer): void
    {
        // Log the creation of the customer
        Log::info("Customer creating: {$customer->getId()}");

        // Generate a UUID for the customer
        $uuid = Uuid::uuid4()->toString();

        // Set the generated UUID for the customer
        $customer->setUuid($uuid);
    }

    /**
     * Handle the Customer "creating" event.
     *
     * @param  Customer  $customer  The newly creating customer.
     */
    public function created(Customer $customer): void
    {
        // Log the creation of the customer
        Log::info("Customer created: {$customer->getId()}");

        // Dispatch the created event
        Event::dispatch(PubTopics::CREATED, $customer->toDataArray());

        // Pubsub the created event
        PubSubService::publish(PubTopics::CREATED, $customer->toDataArray());

        // Send an email
        Mail::to('pixiedia@gmail.com')->send(new CustomerCreated($customer));
    }

    /**
     * Handle the Customer "updated" event.
     *
     * @param  Customer  $customer  The updated customer.
     */
    public function updated(Customer $customer): void
    {
        // Log the update of the customer
        Log::info("Customer updated: {$customer->getId()}");

        // Set the updated at timestamp
        $customer->setUpdatedAt(now());

        // Dispatch the updated event
        Event::dispatch(PubTopics::UPDATED, $customer->toDataArray());

        // Pubsub the updated event
        PubSubService::publish(PubTopics::UPDATED, $customer->toDataArray());
    }

    /**
     * Handle the Customer "deleted" event.
     *
     * @param  Customer  $customer  The deleted customer.
     */
    public function deleted(Customer $customer): void
    {
        // Log the deletion of the customer
        Log::info("Customer deleted: {$customer->getId()}");

        // Set the deleted at timestamp
        $customer->setDeletedAt(now());

        // Dispatch the deleted event
        Event::dispatch(PubTopics::SOFT_DELETED, $customer->toDataArray());

        // Pubsub the deleted event
        PubSubService::publish(PubTopics::SOFT_DELETED, $customer->toDataArray());
    }

    /**
     * Handle the Customer "forceDeleted" event.
     *
     * @param  Customer  $customer  The force deleted customer.
     */
    public function forceDeleted(Customer $customer): void
    {
        // Log the force deletion of the customer
        Log::info("Customer force deleted: {$customer->getId()}");

        // Set the deleted at timestamp
        $customer->setDeletedAt(now());

        // Dispatch the force deleted event
        Event::dispatch(PubTopics::DELETED, $customer->toDataArray());

        // Pubsub the deleted event
        PubSubService::publish(PubTopics::DELETED, $customer->toDataArray());
    }

    /**
     * Handle the Customer "restored" event.
     *
     * @param  Customer  $customer  The restored customer.
     */
    public function restored(Customer $customer): void
    {
        // Log the restoration of the customer
        Log::info("Customer restored: {$customer->getId()}");

        // Reset the deleted at timestamp
        $customer->setDeletedAt(null);

        // Dispatch the restored event
        Event::dispatch(PubTopics::RECOVERED, $customer->toDataArray());

        // Pubsub the restored event
        PubSubService::publish(PubTopics::RECOVERED, $customer->toDataArray());
    }
}
