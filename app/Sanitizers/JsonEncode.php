<?php

declare(strict_types=1);

namespace App\Sanitizers;

use App\Util\PHPUtil;
use ArondeParon\RequestSanitizer\Contracts\Sanitizer;

/**
 * Class JsonEncode
 */
class JsonEncode implements Sanitizer
{
    /**
     * Encode JSON data into an associative array.
     *
     * @param  mixed  $input  The input data to sanitize (JSON string).
     * @return array|null The encoded array or null if decoding fails.
     */
    public function sanitize($input)
    {
        // Encode JSON data
        return PHPUtil::jsonEncode($input);
    }
}
