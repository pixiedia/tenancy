<?php

declare(strict_types=1);

namespace Modules\Team\Model;

use Illuminate\Database\Eloquent\Model;
use Modules\Team\Interface\Data\TeamTenantInterface;

/**
 * Class TeamTenant
 */
class TeamTenant extends Model implements TeamTenantInterface
{
    /**
     * The name of the "created at" column.
     *
     * @var string|null
     */
    const CREATED_AT = 'created_at';

    /**
     * The name of the "updated at" column.
     *
     * @var string|null
     */
    const UPDATED_AT = 'updated_at';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = TeamTenantInterface::TABLE;
}
