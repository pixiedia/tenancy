<?php

declare(strict_types=1);

namespace Modules\Permission\Provider;

use App\Abstractions\AbstractModuleProvider;
use Modules\Permission\Interface\Data\PermissionInterface;
use Modules\Permission\Interface\PermissionRepositoryInterface;
use Modules\Permission\Interface\PermissionServiceInterface;
use Modules\Permission\Model\Permission;
use Modules\Permission\Repository\PermissionRepository;
use Modules\Permission\Router\V1\PermissionRoutes;
use Modules\Permission\Service\PermissionService;

/**
 * Class PermissionModuleProvider
 */
class PermissionModuleProvider extends AbstractModuleProvider
{
    /**
     * PermissionModuleProvider constructor.
     *
     * Initializes the module provider with route, seeder, and migration files.
     */
    public function __construct()
    {
        // Glob all route files matching the defined path
        $this->routers = [PermissionRoutes::class];

        // Define the path to the seeder files for the module
        $seederPath = base_path('app/Modules/Permission/Seeder/*.php');

        // Glob all seeder files matching the defined path
        $this->seeders = glob($seederPath);

        // Define the path to the migration files for the module
        $migrationPath = base_path('app/Modules/Permission/Migration/*.php');

        // Glob all migration files matching the defined path
        $this->migrations = glob($migrationPath);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function registerBindings()
    {
        // Bind PermissionInterface to Permission
        app()->bind(PermissionInterface::class, Permission::class);

        // Bind PermissionRepositoryInterface to PermissionRepository
        app()->bind(PermissionRepositoryInterface::class, fn ($app) => new PermissionRepository($app->make(PermissionInterface::class)));

        // Bind PermissionServiceInterface to PermissionService
        app()->bind(PermissionServiceInterface::class, fn ($app) => new PermissionService($app->make(PermissionRepositoryInterface::class)));
    }
}
